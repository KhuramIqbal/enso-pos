-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2018 at 09:04 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wisdom_pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `sma_addresses`
--

CREATE TABLE `sma_addresses` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `line1` varchar(50) NOT NULL,
  `line2` varchar(50) DEFAULT NULL,
  `city` varchar(25) NOT NULL,
  `postal_code` varchar(20) DEFAULT NULL,
  `state` varchar(25) NOT NULL,
  `country` varchar(50) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_addresses`
--

INSERT INTO `sma_addresses` (`id`, `company_id`, `line1`, `line2`, `city`, `postal_code`, `state`, `country`, `phone`, `updated_at`) VALUES
(1, 10, '545 Old Road', 'Ea impedit veniam ea id ab eos eiusmod minim odit ', 'Aliqua Voluptatibus obcae', 'Quo libero aute quis', 'Dolorem modi consectetur ', 'Ut et iste delectus laboriosam anim ut facere aper', '+611-57-2668819', '2017-09-28 09:51:35'),
(2, 11, 'Lorem ipsum', 'Loremm', 'lahore', '3800', 'punka', 'pakisa', '0342424852', '2017-11-16 22:29:54'),
(3, 12, 'sdgdfgd', '564eyrty', 'fdg', 'dfg', 'dfg', 'rgdg', '434564', '2018-04-06 10:47:03'),
(4, 12, 'House # 262, johar town F block', 'House # 262, johar town F block', 'Lahore', '54000', 'Punjab', 'Pakistan', '03424578522', '2018-04-06 16:20:03'),
(5, 12, 'sadsadasd', 'asdsad', 'sad', 'sad', 'sadsa', 'sd', '214324324', '2018-04-06 16:53:31');

-- --------------------------------------------------------

--
-- Table structure for table `sma_adjustments`
--

CREATE TABLE `sma_adjustments` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` text,
  `attachment` varchar(55) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `count_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_adjustments`
--

INSERT INTO `sma_adjustments` (`id`, `date`, `reference_no`, `warehouse_id`, `note`, `attachment`, `created_by`, `updated_by`, `updated_at`, `count_id`) VALUES
(1, '2017-04-22 18:58:00', '102 Costume', 2, '', 'd95d908a453fb4159d5c70a5da72154b.csv', 3, NULL, NULL, NULL),
(2, '2017-04-24 04:45:00', '103-104-105', 2, '', 'ed144b7ab1ca43436ef5c1889040a2ba.csv', 3, NULL, NULL, NULL),
(3, '2017-04-24 07:26:00', '108 Ballet', 2, '', '818b79327b5556bbb6017820001a4d9a.csv', 3, NULL, NULL, NULL),
(4, '2017-04-24 08:52:00', 'April22-Adj5', 2, '', 'aba0bb497770cfa598ab5ccdca8235e0.csv', 3, NULL, NULL, NULL),
(5, '2017-06-07 08:23:00', 'Scarfs', 2, '', NULL, 6, NULL, NULL, NULL),
(7, '2017-06-15 12:27:00', 'PR/2017/06/0003', 2, '', NULL, 6, NULL, NULL, 9),
(8, '2017-06-15 12:34:00', 'Fancy Frocks 101', 2, '', NULL, 3, NULL, NULL, NULL),
(9, '2017-06-16 00:01:00', '107Pets/109Tops', 2, '', NULL, 3, NULL, NULL, NULL),
(10, '2017-06-16 00:28:00', '200 Series - Boys', 2, '', NULL, 3, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_adjustment_items`
--

CREATE TABLE `sma_adjustment_items` (
  `id` int(11) NOT NULL,
  `adjustment_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_adjustment_items`
--

INSERT INTO `sma_adjustment_items` (`id`, `adjustment_id`, `product_id`, `option_id`, `quantity`, `warehouse_id`, `serial_no`, `type`) VALUES
(82, 4, 461, NULL, '10.0000', 2, '', 'addition'),
(83, 4, 207, NULL, '4.0000', 2, '', 'addition'),
(84, 4, 206, NULL, '2.0000', 2, '', 'addition'),
(85, 4, 205, NULL, '5.0000', 2, '', 'addition'),
(86, 4, 204, NULL, '1.0000', 2, '', 'addition'),
(87, 4, 203, NULL, '2.0000', 2, '', 'addition'),
(88, 4, 202, NULL, '2.0000', 2, '', 'addition'),
(89, 4, 460, NULL, '6.0000', 2, '', 'addition'),
(90, 4, 460, NULL, '6.0000', 2, '', 'subtraction'),
(91, 4, 461, NULL, '10.0000', 2, '', 'subtraction'),
(4462, 9, 397, NULL, '10.0000', 2, '', 'addition'),
(4463, 9, 398, NULL, '2.0000', 2, '', 'addition'),
(4464, 9, 399, NULL, '5.0000', 2, '', 'addition'),
(4465, 9, 400, NULL, '14.0000', 2, '', 'addition'),
(4466, 9, 401, NULL, '2.0000', 2, '', 'addition'),
(4467, 9, 402, NULL, '1.0000', 2, '', 'addition'),
(4468, 9, 403, NULL, '4.0000', 2, '', 'addition'),
(4469, 9, 404, NULL, '1.0000', 2, '', 'addition'),
(4470, 9, 406, NULL, '1.0000', 2, '', 'addition'),
(4471, 9, 387, NULL, '8.0000', 2, '', 'addition'),
(4472, 9, 388, NULL, '2.0000', 2, '', 'addition'),
(4473, 9, 389, NULL, '1.0000', 2, '', 'addition'),
(4474, 9, 390, NULL, '1.0000', 2, '', 'addition'),
(4475, 9, 391, NULL, '1.0000', 2, '', 'addition'),
(4476, 9, 392, NULL, '1.0000', 2, '', 'addition'),
(4477, 9, 393, NULL, '2.0000', 2, '', 'addition'),
(4478, 9, 394, NULL, '1.0000', 2, '', 'addition'),
(4479, 9, 395, NULL, '3.0000', 2, '', 'addition'),
(5856, 3, 396, NULL, '7.0000', 2, '', 'addition'),
(5857, 3, 225, NULL, '2.0000', 2, '', 'addition'),
(5858, 3, 226, NULL, '1.0000', 2, '', 'addition'),
(5859, 3, 227, NULL, '4.0000', 2, '', 'addition'),
(5860, 3, 228, NULL, '3.0000', 2, '', 'addition'),
(5861, 3, 229, NULL, '7.0000', 2, '', 'addition'),
(5862, 3, 230, NULL, '7.0000', 2, '', 'addition'),
(5863, 3, 231, NULL, '1.0000', 2, '', 'addition'),
(5864, 3, 233, NULL, '1.0000', 2, '', 'addition'),
(5865, 3, 235, NULL, '5.0000', 2, '', 'addition'),
(5866, 3, 236, NULL, '2.0000', 2, '', 'addition'),
(5867, 3, 237, NULL, '1.0000', 2, '', 'addition'),
(5868, 3, 238, NULL, '8.0000', 2, '', 'addition'),
(5869, 3, 239, NULL, '6.0000', 2, '', 'addition'),
(5870, 3, 240, NULL, '11.0000', 2, '', 'addition'),
(5871, 3, 241, NULL, '6.0000', 2, '', 'addition'),
(5872, 3, 242, NULL, '1.0000', 2, '', 'addition'),
(6238, 10, 385, NULL, '1.0000', 2, '', 'addition'),
(6239, 10, 384, NULL, '6.0000', 2, '', 'addition'),
(6240, 10, 383, NULL, '2.0000', 2, '', 'addition'),
(6241, 10, 382, NULL, '3.0000', 2, '', 'addition'),
(6242, 10, 381, NULL, '5.0000', 2, '', 'addition'),
(6243, 10, 380, NULL, '1.0000', 2, '', 'addition'),
(6244, 10, 379, NULL, '4.0000', 2, '', 'addition'),
(6245, 10, 378, NULL, '3.0000', 2, '', 'addition'),
(6246, 10, 376, NULL, '5.0000', 2, '', 'addition'),
(6247, 10, 375, NULL, '13.0000', 2, '', 'addition'),
(6248, 10, 374, NULL, '6.0000', 2, '', 'addition'),
(6249, 10, 459, NULL, '4.0000', 2, '', 'addition'),
(6250, 10, 458, NULL, '9.0000', 2, '', 'addition'),
(6251, 10, 457, NULL, '6.0000', 2, '', 'addition'),
(6252, 10, 456, NULL, '8.0000', 2, '', 'addition'),
(6253, 10, 455, NULL, '3.0000', 2, '', 'addition'),
(6254, 10, 453, NULL, '1.0000', 2, '', 'addition'),
(6255, 10, 454, NULL, '5.0000', 2, '', 'addition'),
(6256, 10, 452, NULL, '1.0000', 2, '', 'addition'),
(6257, 10, 451, NULL, '7.0000', 2, '', 'addition'),
(6258, 10, 450, NULL, '2.0000', 2, '', 'addition'),
(6259, 10, 449, NULL, '2.0000', 2, '', 'addition'),
(6260, 10, 448, NULL, '3.0000', 2, '', 'addition'),
(6261, 10, 446, NULL, '2.0000', 2, '', 'addition'),
(6262, 10, 445, NULL, '5.0000', 2, '', 'addition'),
(6263, 10, 444, NULL, '9.0000', 2, '', 'addition'),
(6264, 10, 443, NULL, '11.0000', 2, '', 'addition'),
(6265, 10, 442, NULL, '3.0000', 2, '', 'addition'),
(6266, 10, 441, NULL, '1.0000', 2, '', 'addition'),
(6267, 10, 440, NULL, '2.0000', 2, '', 'addition'),
(6268, 10, 439, NULL, '3.0000', 2, '', 'addition'),
(6269, 10, 438, NULL, '4.0000', 2, '', 'addition'),
(6270, 10, 437, NULL, '3.0000', 2, '', 'addition'),
(6271, 10, 436, NULL, '1.0000', 2, '', 'addition'),
(6272, 10, 435, NULL, '3.0000', 2, '', 'addition'),
(6273, 10, 434, NULL, '4.0000', 2, '', 'addition'),
(6274, 10, 432, NULL, '3.0000', 2, '', 'addition'),
(6275, 10, 431, NULL, '3.0000', 2, '', 'addition'),
(6276, 10, 430, NULL, '3.0000', 2, '', 'addition'),
(6277, 10, 429, NULL, '1.0000', 2, '', 'addition'),
(6278, 10, 428, NULL, '1.0000', 2, '', 'addition'),
(6279, 10, 427, NULL, '2.0000', 2, '', 'addition'),
(6280, 10, 426, NULL, '4.0000', 2, '', 'addition'),
(6281, 10, 425, NULL, '5.0000', 2, '', 'addition'),
(6282, 10, 453, NULL, '1.0000', 2, '', 'addition'),
(6283, 10, 600, NULL, '3.0000', 2, '', 'addition'),
(6284, 10, 598, NULL, '7.0000', 2, '', 'addition'),
(6285, 10, 599, NULL, '3.0000', 2, '', 'addition'),
(7755, 2, 615, NULL, '5.0000', 2, '', 'addition'),
(7756, 2, 129, NULL, '3.0000', 2, '', 'addition'),
(7757, 2, 207, NULL, '4.0000', 2, '', 'addition'),
(7758, 2, 124, NULL, '2.0000', 2, '', 'addition'),
(7759, 2, 83, NULL, '0.0000', 2, '', 'addition'),
(7760, 2, 208, NULL, '7.0000', 2, '', 'addition'),
(7761, 2, 215, NULL, '9.0000', 2, '', 'addition'),
(7762, 2, 217, NULL, '1.0000', 2, '', 'addition'),
(7763, 2, 218, NULL, '1.0000', 2, '', 'addition'),
(7764, 2, 219, NULL, '2.0000', 2, '', 'addition'),
(7765, 2, 220, NULL, '1.0000', 2, '', 'addition'),
(7766, 2, 222, NULL, '1.0000', 2, '', 'addition'),
(7767, 2, 223, NULL, '3.0000', 2, '', 'addition'),
(7768, 2, 192, NULL, '5.0000', 2, '', 'addition'),
(7769, 2, 193, NULL, '1.0000', 2, '', 'addition'),
(7770, 2, 194, NULL, '6.0000', 2, '', 'addition'),
(7771, 2, 195, NULL, '0.0000', 2, '', 'addition'),
(7772, 2, 197, NULL, '11.0000', 2, '', 'addition'),
(7773, 2, 198, NULL, '12.0000', 2, '', 'addition'),
(7774, 2, 199, NULL, '1.0000', 2, '', 'addition'),
(7775, 2, 200, NULL, '5.0000', 2, '', 'addition'),
(7776, 2, 201, NULL, '4.0000', 2, '', 'addition'),
(7777, 2, 121, NULL, '3.0000', 2, '', 'addition'),
(7778, 2, 115, NULL, '3.0000', 2, '', 'addition'),
(7779, 2, 123, NULL, '2.0000', 2, '', 'addition'),
(7780, 2, 128, NULL, '2.0000', 2, '', 'addition'),
(7781, 2, 113, NULL, '2.0000', 2, '', 'addition'),
(7782, 2, 126, NULL, '0.0000', 2, '', 'addition'),
(7783, 2, 122, NULL, '4.0000', 2, '', 'addition'),
(7784, 2, 112, NULL, '3.0000', 2, '', 'addition'),
(7785, 2, 114, NULL, '3.0000', 2, '', 'addition'),
(7786, 2, 205, NULL, '2.0000', 2, '', 'addition'),
(7787, 2, 606, NULL, '5.0000', 2, '', 'addition'),
(7788, 2, 607, NULL, '5.0000', 2, '', 'addition'),
(7789, 2, 608, NULL, '5.0000', 2, '', 'addition'),
(7790, 2, 609, NULL, '5.0000', 2, '', 'addition'),
(7791, 2, 610, NULL, '5.0000', 2, '', 'addition'),
(7792, 2, 611, NULL, '5.0000', 2, '', 'addition'),
(7793, 2, 612, NULL, '5.0000', 2, '', 'addition'),
(7794, 2, 613, NULL, '5.0000', 2, '', 'addition'),
(7795, 1, 614, NULL, '5.0000', 2, '', 'addition'),
(7796, 1, 92, NULL, '1.0000', 2, '', 'subtraction'),
(7797, 1, 162, 2, '4.0000', 2, '', 'addition'),
(7798, 1, 163, NULL, '1.0000', 2, '', 'addition'),
(7799, 1, 164, NULL, '3.0000', 2, '', 'addition'),
(7800, 1, 167, NULL, '3.0000', 2, '', 'addition'),
(7801, 1, 168, NULL, '1.0000', 2, '', 'addition'),
(7802, 1, 169, NULL, '5.0000', 2, '', 'addition'),
(7803, 1, 170, NULL, '2.0000', 2, '', 'addition'),
(7804, 1, 171, NULL, '3.0000', 2, '', 'addition'),
(7805, 1, 172, NULL, '3.0000', 2, '', 'addition'),
(7806, 1, 173, NULL, '12.0000', 2, '', 'addition'),
(7807, 1, 174, NULL, '6.0000', 2, '', 'addition'),
(7808, 1, 175, NULL, '6.0000', 2, '', 'addition'),
(7809, 1, 176, NULL, '4.0000', 2, '', 'addition'),
(7810, 1, 177, NULL, '6.0000', 2, '', 'addition'),
(7811, 1, 178, NULL, '1.0000', 2, '', 'addition'),
(7812, 1, 179, NULL, '1.0000', 2, '', 'addition'),
(7813, 1, 180, NULL, '6.0000', 2, '', 'addition'),
(7814, 1, 181, NULL, '2.0000', 2, '', 'addition'),
(7815, 1, 183, NULL, '1.0000', 2, '', 'addition'),
(7816, 1, 184, NULL, '5.0000', 2, '', 'addition'),
(7817, 1, 185, NULL, '5.0000', 2, '', 'addition'),
(7818, 1, 186, NULL, '7.0000', 2, '', 'addition'),
(7819, 1, 187, NULL, '10.0000', 2, '', 'addition'),
(7820, 1, 188, NULL, '7.0000', 2, '', 'addition'),
(7821, 1, 189, NULL, '7.0000', 2, '', 'addition'),
(7822, 1, 190, NULL, '5.0000', 2, '', 'addition'),
(7823, 1, 191, NULL, '4.0000', 2, '', 'addition'),
(7824, 1, 616, NULL, '3.0000', 2, '', 'addition'),
(8189, 8, 622, NULL, '3.0000', 2, '', 'addition'),
(8190, 8, 621, NULL, '3.0000', 2, '', 'addition'),
(8191, 8, 620, NULL, '3.0000', 2, '', 'addition'),
(8192, 8, 619, NULL, '1.0000', 2, '', 'addition'),
(8193, 8, 618, NULL, '3.0000', 2, '', 'addition'),
(8194, 8, 617, NULL, '5.0000', 2, '', 'addition'),
(8195, 8, 78, NULL, '3.0000', 2, '', 'addition'),
(8196, 8, 75, NULL, '1.0000', 2, '', 'subtraction'),
(8197, 8, 603, NULL, '1.0000', 2, '', 'addition'),
(8198, 8, 602, NULL, '4.0000', 2, '', 'addition'),
(8199, 8, 161, NULL, '2.0000', 2, '', 'addition'),
(8200, 8, 160, NULL, '2.0000', 2, '', 'addition'),
(8201, 8, 159, NULL, '3.0000', 2, '', 'addition'),
(8202, 8, 158, NULL, '3.0000', 2, '', 'addition'),
(8203, 8, 157, NULL, '6.0000', 2, '', 'addition'),
(8204, 8, 156, NULL, '1.0000', 2, '', 'addition'),
(8205, 8, 155, NULL, '3.0000', 2, '', 'addition'),
(8206, 8, 153, NULL, '1.0000', 2, '', 'addition'),
(8207, 8, 150, NULL, '0.0000', 2, '', 'addition'),
(8208, 8, 149, NULL, '4.0000', 2, '', 'addition'),
(8209, 8, 148, NULL, '1.0000', 2, '', 'addition'),
(8210, 8, 144, NULL, '3.0000', 2, '', 'addition'),
(8211, 8, 143, NULL, '2.0000', 2, '', 'addition'),
(8212, 8, 142, NULL, '2.0000', 2, '', 'addition'),
(8213, 8, 139, NULL, '3.0000', 2, '', 'addition'),
(8214, 8, 140, NULL, '6.0000', 2, '', 'addition'),
(8215, 8, 141, NULL, '5.0000', 2, '', 'addition'),
(8216, 8, 73, NULL, '5.0000', 2, '', 'addition'),
(8217, 8, 81, NULL, '1.0000', 2, '', 'subtraction'),
(8345, 5, 553, NULL, '4.0000', 2, '', 'addition'),
(8346, 5, 552, NULL, '1.0000', 2, '', 'addition'),
(8347, 5, 551, NULL, '3.0000', 2, '', 'addition'),
(8348, 5, 550, NULL, '3.0000', 2, '', 'addition'),
(8349, 5, 549, NULL, '5.0000', 2, '', 'addition'),
(8350, 5, 548, NULL, '5.0000', 2, '', 'addition'),
(8351, 5, 585, NULL, '3.0000', 2, '', 'addition'),
(8352, 5, 584, NULL, '8.0000', 2, '', 'addition'),
(8353, 5, 583, NULL, '16.0000', 2, '', 'addition'),
(8354, 5, 582, NULL, '1.0000', 2, '', 'addition'),
(8355, 5, 581, NULL, '0.0000', 2, '', 'addition'),
(8356, 5, 580, NULL, '1.0000', 2, '', 'addition'),
(8357, 5, 578, NULL, '5.0000', 2, '', 'addition'),
(8358, 5, 579, NULL, '6.0000', 2, '', 'addition'),
(8359, 5, 577, NULL, '5.0000', 2, '', 'addition'),
(8360, 5, 576, NULL, '1.0000', 2, '', 'addition'),
(8361, 5, 575, NULL, '0.0000', 2, '', 'addition'),
(8362, 5, 574, NULL, '0.0000', 2, '', 'addition'),
(8363, 5, 465, NULL, '11.0000', 2, '', 'addition'),
(8364, 5, 506, NULL, '5.0000', 2, '', 'addition'),
(8365, 5, 530, NULL, '1.0000', 2, '', 'addition'),
(8366, 5, 529, NULL, '1.0000', 2, '', 'addition'),
(8367, 5, 528, NULL, '1.0000', 2, '', 'addition'),
(8368, 5, 527, NULL, '0.0000', 2, '', 'addition'),
(8369, 5, 526, NULL, '1.0000', 2, '', 'addition'),
(8370, 5, 525, NULL, '0.0000', 2, '', 'addition'),
(8371, 5, 524, NULL, '1.0000', 2, '', 'addition'),
(8372, 5, 523, NULL, '1.0000', 2, '', 'addition'),
(8373, 5, 522, NULL, '0.0000', 2, '', 'addition'),
(8374, 5, 521, NULL, '1.0000', 2, '', 'addition'),
(8375, 5, 520, NULL, '1.0000', 2, '', 'addition'),
(8376, 5, 519, NULL, '0.0000', 2, '', 'addition'),
(8377, 5, 518, NULL, '1.0000', 2, '', 'addition'),
(8378, 5, 517, NULL, '1.0000', 2, '', 'addition'),
(8379, 5, 516, NULL, '0.0000', 2, '', 'addition'),
(8380, 5, 515, NULL, '0.0000', 2, '', 'addition'),
(8381, 5, 514, NULL, '0.0000', 2, '', 'addition'),
(8382, 5, 513, NULL, '2.0000', 2, '', 'addition'),
(8383, 5, 512, NULL, '0.0000', 2, '', 'addition'),
(8384, 5, 511, NULL, '1.0000', 2, '', 'addition'),
(8385, 5, 510, NULL, '0.0000', 2, '', 'addition'),
(8386, 5, 509, NULL, '1.0000', 2, '', 'addition'),
(8387, 5, 508, NULL, '1.0000', 2, '', 'addition'),
(8388, 5, 505, NULL, '0.0000', 2, '', 'addition'),
(8389, 5, 507, NULL, '0.0000', 2, '', 'addition'),
(8390, 5, 504, NULL, '1.0000', 2, '', 'addition'),
(8391, 5, 503, NULL, '5.0000', 2, '', 'addition'),
(8392, 5, 460, NULL, '3.0000', 2, '', 'addition'),
(8393, 5, 461, NULL, '7.0000', 2, '', 'addition'),
(8394, 5, 462, NULL, '7.0000', 2, '', 'addition'),
(8395, 5, 463, NULL, '3.0000', 2, '', 'addition'),
(8396, 5, 464, NULL, '7.0000', 2, '', 'addition'),
(8397, 5, 466, NULL, '10.0000', 2, '', 'addition'),
(8398, 5, 467, NULL, '1.0000', 2, '', 'addition'),
(8399, 5, 468, NULL, '6.0000', 2, '', 'addition'),
(8400, 5, 469, NULL, '11.0000', 2, '', 'addition'),
(8401, 5, 470, NULL, '7.0000', 2, '', 'addition'),
(8402, 5, 471, NULL, '2.0000', 2, '', 'addition'),
(8403, 5, 472, NULL, '10.0000', 2, '', 'addition'),
(8404, 5, 473, NULL, '1.0000', 2, '', 'addition'),
(8405, 5, 474, NULL, '4.0000', 2, '', 'addition'),
(8406, 5, 475, NULL, '6.0000', 2, '', 'addition'),
(8407, 5, 476, NULL, '4.0000', 2, '', 'addition'),
(8408, 5, 477, NULL, '6.0000', 2, '', 'addition'),
(8409, 5, 478, NULL, '3.0000', 2, '', 'addition'),
(8410, 5, 479, NULL, '1.0000', 2, '', 'addition'),
(8411, 5, 480, NULL, '1.0000', 2, '', 'addition'),
(8412, 5, 481, NULL, '0.0000', 2, '', 'addition'),
(8413, 5, 482, NULL, '14.0000', 2, '', 'addition'),
(8414, 5, 483, NULL, '6.0000', 2, '', 'addition'),
(8415, 5, 488, NULL, '2.0000', 2, '', 'addition'),
(8416, 5, 484, NULL, '4.0000', 2, '', 'addition'),
(8417, 5, 485, NULL, '8.0000', 2, '', 'addition'),
(8418, 5, 486, NULL, '7.0000', 2, '', 'addition'),
(8419, 5, 532, NULL, '2.0000', 2, '', 'addition'),
(8420, 5, 487, NULL, '7.0000', 2, '', 'addition'),
(8421, 5, 489, NULL, '2.0000', 2, '', 'addition'),
(8422, 5, 490, NULL, '10.0000', 2, '', 'addition'),
(8423, 5, 491, NULL, '7.0000', 2, '', 'addition'),
(8424, 5, 492, NULL, '7.0000', 2, '', 'addition'),
(8425, 5, 493, NULL, '8.0000', 2, '', 'addition'),
(8426, 5, 494, NULL, '7.0000', 2, '', 'addition'),
(8427, 5, 495, NULL, '0.0000', 2, '', 'addition'),
(8428, 5, 496, NULL, '8.0000', 2, '', 'addition'),
(8429, 5, 497, NULL, '10.0000', 2, '', 'addition'),
(8430, 5, 498, NULL, '9.0000', 2, '', 'addition'),
(8431, 5, 499, NULL, '5.0000', 2, '', 'addition'),
(8432, 5, 500, NULL, '6.0000', 2, '', 'addition'),
(8433, 5, 501, NULL, '3.0000', 2, '', 'addition'),
(8434, 5, 502, NULL, '7.0000', 2, '', 'addition'),
(8435, 5, 531, NULL, '1.0000', 2, '', 'addition'),
(8436, 5, 533, NULL, '1.0000', 2, '', 'addition'),
(8437, 5, 534, NULL, '1.0000', 2, '', 'addition'),
(8438, 5, 535, NULL, '1.0000', 2, '', 'addition'),
(8439, 5, 554, NULL, '3.0000', 2, '', 'addition'),
(8440, 5, 555, NULL, '1.0000', 2, '', 'addition'),
(8441, 5, 556, NULL, '2.0000', 2, '', 'addition'),
(8442, 5, 557, NULL, '2.0000', 2, '', 'addition'),
(8443, 5, 558, NULL, '6.0000', 2, '', 'addition'),
(8444, 5, 559, NULL, '3.0000', 2, '', 'addition'),
(8445, 5, 560, NULL, '3.0000', 2, '', 'addition'),
(8446, 5, 561, NULL, '4.0000', 2, '', 'addition'),
(8447, 5, 562, NULL, '5.0000', 2, '', 'addition'),
(8448, 5, 563, NULL, '1.0000', 2, '', 'addition'),
(8449, 5, 564, NULL, '3.0000', 2, '', 'addition'),
(8450, 5, 565, NULL, '4.0000', 2, '', 'addition'),
(8451, 5, 566, NULL, '5.0000', 2, '', 'addition'),
(8452, 5, 567, NULL, '4.0000', 2, '', 'addition'),
(8453, 5, 568, NULL, '2.0000', 2, '', 'addition'),
(8454, 5, 569, NULL, '5.0000', 2, '', 'addition'),
(8455, 5, 570, NULL, '0.0000', 2, '', 'addition'),
(8456, 5, 571, NULL, '3.0000', 2, '', 'addition'),
(8457, 5, 572, NULL, '3.0000', 2, '', 'addition'),
(8458, 5, 573, NULL, '2.0000', 2, '', 'addition'),
(8459, 5, 586, NULL, '4.0000', 2, '', 'addition'),
(8460, 5, 586, NULL, '2.0000', 2, '', 'addition'),
(8461, 5, 588, NULL, '5.0000', 2, '', 'addition'),
(8462, 5, 589, NULL, '8.0000', 2, '', 'addition'),
(8463, 5, 590, NULL, '3.0000', 2, '', 'addition'),
(8464, 5, 591, NULL, '6.0000', 2, '', 'addition'),
(8465, 5, 592, NULL, '5.0000', 2, '', 'addition'),
(8466, 5, 593, NULL, '4.0000', 2, '', 'addition'),
(8467, 5, 594, NULL, '6.0000', 2, '', 'addition'),
(8468, 5, 595, NULL, '3.0000', 2, '', 'addition'),
(8469, 5, 596, NULL, '1.0000', 2, '', 'addition'),
(8470, 5, 597, NULL, '1.0000', 2, '', 'addition'),
(8471, 5, 162, 1, '3.0000', 2, '', 'addition'),
(8703, 7, 179, NULL, '1.0000', 2, '', 'subtraction'),
(8704, 7, 107, NULL, '2.0000', 2, '', 'addition'),
(8705, 7, 109, NULL, '0.0000', 2, '', 'addition'),
(8706, 7, 106, NULL, '5.0000', 2, '', 'addition'),
(8707, 7, 108, NULL, '1.0000', 2, '', 'addition'),
(8708, 7, 103, NULL, '1.0000', 2, '', 'addition'),
(8709, 7, 104, NULL, '5.0000', 2, '', 'addition'),
(8710, 7, 105, NULL, '2.0000', 2, '', 'addition'),
(8711, 7, 102, NULL, '7.0000', 2, '', 'addition'),
(8712, 7, 151, NULL, '1.0000', 2, '', 'addition'),
(8713, 7, 191, NULL, '1.0000', 2, '', 'addition'),
(8714, 7, 190, NULL, '2.0000', 2, '', 'addition'),
(8715, 7, 188, NULL, '1.0000', 2, '', 'addition'),
(8716, 7, 187, NULL, '1.0000', 2, '', 'addition'),
(8717, 7, 184, NULL, '3.0000', 2, '', 'addition'),
(8718, 7, 183, NULL, '1.0000', 2, '', 'addition'),
(8719, 7, 181, NULL, '1.0000', 2, '', 'addition'),
(8720, 7, 180, NULL, '0.0000', 2, '', 'addition'),
(8721, 7, 179, NULL, '1.0000', 2, '', 'addition'),
(8722, 7, 175, NULL, '2.0000', 2, '', 'addition'),
(8723, 7, 174, NULL, '3.0000', 2, '', 'addition'),
(8724, 7, 173, NULL, '3.0000', 2, '', 'addition'),
(8725, 7, 171, NULL, '1.0000', 2, '', 'addition'),
(8726, 7, 170, NULL, '1.0000', 2, '', 'addition'),
(8727, 7, 168, NULL, '1.0000', 2, '', 'addition'),
(8728, 7, 95, NULL, '5.0000', 2, '', 'addition'),
(8729, 7, 93, NULL, '5.0000', 2, '', 'addition'),
(8730, 7, 90, NULL, '1.0000', 2, '', 'addition'),
(8731, 7, 89, NULL, '7.0000', 2, '', 'addition'),
(8732, 7, 88, NULL, '2.0000', 2, '', 'addition'),
(8733, 7, 85, NULL, '3.0000', 2, '', 'addition'),
(8734, 7, 82, NULL, '4.0000', 2, '', 'addition'),
(8735, 7, 79, NULL, '1.0000', 2, '', 'addition'),
(8736, 7, 76, NULL, '2.0000', 2, '', 'addition'),
(8737, 7, 74, NULL, '6.0000', 2, '', 'addition'),
(8738, 7, 72, NULL, '4.0000', 2, '', 'addition'),
(8739, 7, 71, NULL, '2.0000', 2, '', 'addition'),
(8740, 7, 69, NULL, '3.0000', 2, '', 'addition'),
(8741, 7, 66, NULL, '5.0000', 2, '', 'addition'),
(8742, 7, 63, NULL, '5.0000', 2, '', 'addition'),
(8743, 7, 62, NULL, '4.0000', 2, '', 'addition'),
(8744, 7, 60, NULL, '3.0000', 2, '', 'addition'),
(8745, 7, 59, NULL, '3.0000', 2, '', 'addition'),
(8746, 7, 57, NULL, '4.0000', 2, '', 'addition'),
(8747, 7, 56, NULL, '4.0000', 2, '', 'addition'),
(8748, 7, 55, NULL, '5.0000', 2, '', 'addition'),
(8749, 7, 54, NULL, '4.0000', 2, '', 'addition'),
(8750, 7, 53, NULL, '5.0000', 2, '', 'addition'),
(8751, 7, 52, NULL, '1.0000', 2, '', 'addition'),
(8752, 7, 51, NULL, '2.0000', 2, '', 'addition'),
(8753, 7, 50, NULL, '1.0000', 2, '', 'addition'),
(8754, 7, 1, NULL, '4.0000', 2, '', 'addition'),
(8755, 7, 242, NULL, '1.0000', 2, '', 'addition'),
(8756, 7, 241, NULL, '3.0000', 2, '', 'addition'),
(8757, 7, 240, NULL, '1.0000', 2, '', 'addition'),
(8758, 7, 239, NULL, '1.0000', 2, '', 'addition'),
(8759, 7, 238, NULL, '3.0000', 2, '', 'addition'),
(8760, 7, 236, NULL, '1.0000', 2, '', 'addition'),
(8761, 7, 235, NULL, '4.0000', 2, '', 'addition'),
(8762, 7, 231, NULL, '1.0000', 2, '', 'addition'),
(8763, 7, 229, NULL, '5.0000', 2, '', 'addition'),
(8764, 7, 225, NULL, '2.0000', 2, '', 'addition'),
(8765, 7, 207, NULL, '3.0000', 2, '', 'addition'),
(8766, 7, 206, NULL, '2.0000', 2, '', 'addition'),
(8767, 7, 205, NULL, '5.0000', 2, '', 'addition'),
(8768, 7, 203, NULL, '1.0000', 2, '', 'addition'),
(8769, 7, 199, NULL, '1.0000', 2, '', 'addition'),
(8770, 7, 197, NULL, '1.0000', 2, '', 'addition'),
(8771, 7, 223, NULL, '2.0000', 2, '', 'addition'),
(8772, 7, 222, NULL, '1.0000', 2, '', 'addition'),
(8773, 7, 220, NULL, '1.0000', 2, '', 'addition'),
(8774, 7, 216, NULL, '15.0000', 2, '', 'addition'),
(8775, 7, 215, NULL, '6.0000', 2, '', 'addition'),
(8776, 7, 213, NULL, '1.0000', 2, '', 'addition'),
(8777, 7, 210, NULL, '0.0000', 2, '', 'addition'),
(8778, 7, 209, NULL, '1.0000', 2, '', 'addition'),
(8779, 7, 208, NULL, '7.0000', 2, '', 'addition'),
(8780, 7, 162, 1, '1.0000', 2, '', 'subtraction'),
(8781, 7, 162, 2, '1.0000', 2, '', 'subtraction'),
(8782, 7, 162, 4, '2.0000', 2, '', 'addition');

-- --------------------------------------------------------

--
-- Table structure for table `sma_api_keys`
--

CREATE TABLE `sma_api_keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reference` varchar(40) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_api_limits`
--

CREATE TABLE `sma_api_limits` (
  `id` int(11) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `count` int(10) NOT NULL,
  `hour_started` int(11) NOT NULL,
  `api_key` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_api_logs`
--

CREATE TABLE `sma_api_logs` (
  `id` int(11) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `method` varchar(6) NOT NULL,
  `params` text,
  `api_key` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` varchar(1) NOT NULL,
  `response_code` smallint(3) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_authors`
--

CREATE TABLE `sma_authors` (
  `id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `translated_name` varchar(255) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `is_updated` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_authors`
--

INSERT INTO `sma_authors` (`id`, `code`, `name`, `translated_name`, `image`, `slug`, `is_updated`) VALUES
(11, '201', 'Bella Forrest', 'بيلا فورست', NULL, 'bella-forrest', 1),
(12, '202', 'James Patterson', 'جيمس باترسون', NULL, 'james-patterson', 1),
(13, '203', 'Catherine Bybee', 'كاثرين بيبي', NULL, 'catherine-bybee', 1),
(14, '204', 'Peter Schweizer', 'بيتر سويس', NULL, 'peter-schweizer', 1),
(15, '205', 'Madeleine L Engle', 'مادلين إل إنجل', NULL, 'madeleine-l-engle', 1),
(16, '206', 'Kristin Hannah', 'كريستين هانا', NULL, 'kristin-hannah', 1),
(17, '207', 'Roger Priddy', 'روجر بريدي', NULL, 'roger-priddy', 1),
(18, '208', 'Nora Roberts', 'نورا روبرتس', NULL, 'nora-roberts', 1),
(19, '209', 'Stephen Kings', 'الملك ستيفن', NULL, 'stephen-king', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sma_brands`
--

CREATE TABLE `sma_brands` (
  `id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `image` varchar(50) DEFAULT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `is_updated` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_brands`
--

INSERT INTO `sma_brands` (`id`, `code`, `name`, `image`, `slug`, `is_updated`) VALUES
(1, '201', 'English', NULL, 'english', 0),
(2, '202', 'Arabic', NULL, 'arabic', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_calendar`
--

CREATE TABLE `sma_calendar` (
  `id` int(11) NOT NULL,
  `title` varchar(55) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  `color` varchar(7) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_calendar`
--

INSERT INTO `sma_calendar` (`id`, `title`, `description`, `start`, `end`, `color`, `user_id`) VALUES
(1, 'Today gift free', 'Hello Lorem ipsum dollar smith', '2017-10-24 00:00:00', '2017-10-25 00:00:00', '#3a87ad', 1),
(2, 'Contact Us', '', '2017-10-24 09:05:00', '2017-10-29 14:55:00', '#000000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sma_captcha`
--

CREATE TABLE `sma_captcha` (
  `captcha_id` bigint(13) UNSIGNED NOT NULL,
  `captcha_time` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(16) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `word` varchar(20) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_cart`
--

CREATE TABLE `sma_cart` (
  `id` varchar(40) NOT NULL,
  `time` varchar(30) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_cart`
--

INSERT INTO `sma_cart` (`id`, `time`, `user_id`, `data`) VALUES
('05869664c7303f91c8d9241dbc6837cc', '1502201805', NULL, '{"cart_total":338,"total_item_tax":0,"total_items":2,"total_unique_items":2,"eeae6d43de233fb1d8e29bd3a0cbbc94":{"id":"009e4b83361e63c947ed8611fb303b17","product_id":"64","qty":1,"name":"Dropping Ribbon Flower- skin","code":"11534531","price":169,"tax":"0.00","image":"da14f0f06a8778b890ce0f51ca6b2fb4.png","option":false,"options":null,"rowid":"eeae6d43de233fb1d8e29bd3a0cbbc94","row_tax":"0.0000","subtotal":"169.0000"},"228566d9d263631731d3b146ab1e452f":{"id":"e1448f2bfd18cecd3f8ccecd58523733","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"228566d9d263631731d3b146ab1e452f","row_tax":"0.0000","subtotal":"169.0000"}}'),
('08f7cc7841bce43da73e8d2b815bc984', '1513083324', 1, '{"cart_total":44.1,"total_item_tax":0,"total_items":1,"total_unique_items":1,"a458bd4927a7973de2a8c739d648554f":{"id":"97363836288b2c09fb226e58a2e8b393","product_id":"580","qty":1,"name":"Army- 30005","code":"301-30005","price":44.1,"tax":"0.00","image":"301-30005.jpg","option":false,"options":null,"rowid":"a458bd4927a7973de2a8c739d648554f","row_tax":"0.0000","subtotal":"44.1000"}}'),
('0bb0e0912dd17bc995307455a16d8b71', '1508134849', NULL, '{"cart_total":417,"total_item_tax":0,"total_items":3,"total_unique_items":3,"377a5ace1cb7268dc7ec8fe8d9b1b807":{"id":"c7b50740853bbd840a3e6e98af7b1e20","product_id":"238","qty":1,"name":"Next Stock Yellow Bellet","code":"108-8014","price":129,"tax":"0.00","image":"108-8014.jpg","option":false,"options":null,"rowid":"377a5ace1cb7268dc7ec8fe8d9b1b807","row_tax":"0.0000","subtotal":"129.0000"},"e87486ab135d2d6791deb88cc424b90d":{"id":"b6ccc1a67e7e397db5598ecf1083e2a0","product_id":"238","qty":1,"name":"Next Stock Yellow Bellet","code":"108-8014","price":129,"tax":"0.00","image":"108-8014.jpg","option":false,"options":null,"rowid":"e87486ab135d2d6791deb88cc424b90d","row_tax":"0.0000","subtotal":"129.0000"},"b61b0ed52f9486194b622b380a27983a":{"id":"f34c748c41419046be16bf80c3da8c85","product_id":"62","qty":1,"name":"Embroidered Net Gowns - Blue","code":"11534529","price":159,"tax":"0.00","image":"67cbdd35e6f48aec087187aace803e53.png","option":false,"options":null,"rowid":"b61b0ed52f9486194b622b380a27983a","row_tax":"0.0000","subtotal":"159.0000"}}'),
('1b7d68de95ef5fb55d61d77598289a81', '1522995287', NULL, '{"cart_total":599,"total_item_tax":0,"total_items":1,"total_unique_items":1,"ba393948144352ebdc346902786fc388":{"id":"e8d526d31ddb98343e0fca47d70b7416","product_id":"719","qty":1,"name":"Ambush (Michael Bennett)","code":"02646516","price":599,"tax":"0.00","image":"e550a7c7fa66f8242d0734b027380b98.jpg","option":false,"options":null,"rowid":"ba393948144352ebdc346902786fc388","row_tax":"0.0000","subtotal":"599.0000"}}'),
('34b8a949ccdfacf5fdbf7d027fb74211', '1510584511', 1, '{"cart_total":45,"total_item_tax":0,"total_items":2,"total_unique_items":2,"538028b75767a25c7fcaa3f6906759f2":{"id":"d44253c950710153464fc3f06dfc399a","product_id":"630","qty":1,"name":"Tiger Shirt","code":"205-2052","price":45,"tax":"0.00","image":"205-2052.jpg","option":false,"options":null,"rowid":"538028b75767a25c7fcaa3f6906759f2","row_tax":"0.0000","subtotal":"45.0000"},"dcbaac00602f35e17c5043749c5e1a28":{"id":"2baa570956fdfc4a295bb52d87bf1313","product_id":"630","qty":1,"name":"Tiger Shirt","code":"205-2052","price":0,"tax":"0.00","image":"205-2052.jpg","option":false,"options":null,"rowid":"dcbaac00602f35e17c5043749c5e1a28","row_tax":"0.0000","subtotal":"0.0000"}}'),
('359da7055b08c49c080a09ecb5ced349', '1522937810', 1, '{"cart_total":2396,"total_item_tax":0,"total_items":4,"total_unique_items":4,"03eb98dbdc76a059b09eda012fe432a5":{"id":"caf570e3a818fee800c7f7bd7fcdd5be","product_id":"719","qty":1,"name":"Ambush (Michael Bennett)","code":"02646516","price":599,"tax":"0.00","image":"e550a7c7fa66f8242d0734b027380b98.jpg","option":false,"options":null,"rowid":"03eb98dbdc76a059b09eda012fe432a5","row_tax":"0.0000","subtotal":"599.0000"},"040cce002409871620a7918b2c2c8101":{"id":"9bcf18f06dfb1510dd2ae9d7e03d08e3","product_id":"752","qty":1,"name":"Ambush Jekson","code":"68325902","price":599,"tax":"0.00","image":"6b9664fcef8ed62b132c1339f0015bb6.jpg","option":false,"options":null,"rowid":"040cce002409871620a7918b2c2c8101","row_tax":"0.0000","subtotal":"599.0000"},"fc0e6254e7c0e5a2660a9ce569013446":{"id":"3c85fc211936f5e6544b6937ace1b107","product_id":"752","qty":1,"name":"Ambush Jekson","code":"68325902","price":599,"tax":"0.00","image":"6b9664fcef8ed62b132c1339f0015bb6.jpg","option":false,"options":null,"rowid":"fc0e6254e7c0e5a2660a9ce569013446","row_tax":"0.0000","subtotal":"599.0000"},"ba0eb494ca31536de8320f564ac8dd44":{"id":"ad13db1ee9ded2337fb2a7f539a950c0","product_id":"752","qty":1,"name":"Ambush Jekson","code":"68325902","price":599,"tax":"0.00","image":"6b9664fcef8ed62b132c1339f0015bb6.jpg","option":false,"options":null,"rowid":"ba0eb494ca31536de8320f564ac8dd44","row_tax":"0.0000","subtotal":"599.0000"}}'),
('452308982103871fe280baf9048b18e8', '1502187308', NULL, '{"cart_total":338,"total_item_tax":0,"total_items":2,"total_unique_items":2,"030609b632076bdf771157b8ebe09a48":{"id":"9a51be149e83c28fcf23319c7077eb4f","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"030609b632076bdf771157b8ebe09a48","row_tax":"0.0000","subtotal":"169.0000"},"7678b481eb3f2a4ec4e9b3e8966c76e1":{"id":"bec9396764f8449672b63eab8b64ffc0","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"7678b481eb3f2a4ec4e9b3e8966c76e1","row_tax":"0.0000","subtotal":"169.0000"}}'),
('55210d228febfd3c78466659a14fdc3c', '1512628287', NULL, '{"cart_total":89,"total_item_tax":0,"total_items":1,"total_unique_items":1,"1401e2791dcb637fa65086250e77695a":{"id":"b54fe9a9a8066dcda20697d05257604f","product_id":"618","qty":1,"name":"RAINBOW SUMMER DRESS","code":"101-1069","price":89,"tax":"0.00","image":"101-1069.jpg","option":false,"options":null,"rowid":"1401e2791dcb637fa65086250e77695a","row_tax":"0.0000","subtotal":"89.0000"}}'),
('5588c0e24635a53b0edafa188bc22b29', '1512238248', 1, '{"cart_total":178,"total_item_tax":0,"total_items":3,"total_unique_items":3,"eccbe87b35cc4392a297566775ec1932":{"id":"3d82a714dbcd319db172b3fc98d12e45","product_id":"618","qty":1,"name":"RAINBOW SUMMER DRESS","code":"101-1069","price":0,"tax":"0.00","image":"101-1069.jpg","option":false,"options":null,"rowid":"eccbe87b35cc4392a297566775ec1932","row_tax":"0.0000","subtotal":"0.0000"},"a3db831f65d41e19e9a5f4bdefd16e24":{"id":"6e7d3d5ff45db3ce96800122443af630","product_id":"618","qty":1,"name":"RAINBOW SUMMER DRESS","code":"101-1069","price":89,"tax":"0.00","image":"101-1069.jpg","option":false,"options":null,"rowid":"a3db831f65d41e19e9a5f4bdefd16e24","row_tax":"0.0000","subtotal":"89.0000"},"2b2868fb0c234027252126d166f8027f":{"id":"8d7b661246114163b5f09efd0742da18","product_id":"618","qty":1,"name":"RAINBOW SUMMER DRESS","code":"101-1069","price":89,"tax":"0.00","image":"101-1069.jpg","option":false,"options":null,"rowid":"2b2868fb0c234027252126d166f8027f","row_tax":"0.0000","subtotal":"89.0000"}}'),
('55bd45121371940dd24ca41557bdb2f2', '1522940552', 3, '{"cart_total":10782,"total_item_tax":0,"total_items":18,"total_unique_items":11,"bdbe44e9a6c94a76e644edfe2643d264":{"id":"629e235e18df0158d8aaeeff986eea9c","product_id":"749","qty":1,"name":"Michael Sheet","code":"60026189","price":599,"tax":"0.00","image":"068b61a296918b729d1f44240e373251.jpg","option":false,"options":null,"rowid":"bdbe44e9a6c94a76e644edfe2643d264","row_tax":"0.0000","subtotal":"599.0000"},"94d4e67e90b727845e1684671ae57e54":{"id":"7bcdb2c7505509c999c3464cb254a3d4","product_id":"749","qty":1,"name":"Michael Sheet","code":"60026189","price":599,"tax":"0.00","image":"068b61a296918b729d1f44240e373251.jpg","option":false,"options":null,"rowid":"94d4e67e90b727845e1684671ae57e54","row_tax":"0.0000","subtotal":"599.0000"},"a116e676dc1e62ecc42aaa38105dc908":{"id":"9534fff32af08adf032a135232a6d429","product_id":"749","qty":1,"name":"Michael Sheet","code":"60026189","price":599,"tax":"0.00","image":"068b61a296918b729d1f44240e373251.jpg","option":false,"options":null,"rowid":"a116e676dc1e62ecc42aaa38105dc908","row_tax":"0.0000","subtotal":"599.0000"},"b894bf9024459104e371394eb18b005d":{"id":"c5730fc89717f9a2136dc3bad467adf4","product_id":"749","qty":1,"name":"Michael Sheet","code":"60026189","price":599,"tax":"0.00","image":"068b61a296918b729d1f44240e373251.jpg","option":false,"options":null,"rowid":"b894bf9024459104e371394eb18b005d","row_tax":"0.0000","subtotal":"599.0000"},"4c89c6670ff5530d97ba4271fdf97dd2":{"id":"95a5c39cd4fe7718b3f59f20aaf61c91","product_id":"749","qty":1,"name":"Michael Sheet","code":"60026189","price":599,"tax":"0.00","image":"068b61a296918b729d1f44240e373251.jpg","option":false,"options":null,"rowid":"4c89c6670ff5530d97ba4271fdf97dd2","row_tax":"0.0000","subtotal":"599.0000"},"82c89c4226dc37ef1dab131b53064038":{"id":"fcd8c09ad52f454332e528aa81106412","product_id":"749","qty":1,"name":"Michael Sheet","code":"60026189","price":599,"tax":"0.00","image":"068b61a296918b729d1f44240e373251.jpg","option":false,"options":null,"rowid":"82c89c4226dc37ef1dab131b53064038","row_tax":"0.0000","subtotal":"599.0000"},"943b1f6a31c5f0998d69bf2c1e447b3a":{"id":"1fadd91bc379bcdb8947834e8163d8f4","product_id":"749","qty":2,"name":"Michael Sheet","code":"60026189","price":599,"tax":"0.00","image":"068b61a296918b729d1f44240e373251.jpg","option":false,"options":null,"rowid":"943b1f6a31c5f0998d69bf2c1e447b3a","row_tax":"0.0000","subtotal":"1198.0000"},"0144914cb5e927e92a39e9730d8ae83a":{"id":"921a059d3602f3191a8ca277e066847c","product_id":"749","qty":1,"name":"Michael Sheet","code":"60026189","price":599,"tax":"0.00","image":"068b61a296918b729d1f44240e373251.jpg","option":false,"options":null,"rowid":"0144914cb5e927e92a39e9730d8ae83a","row_tax":"0.0000","subtotal":"599.0000"},"03993d48fbc39b8122b6a5d62ea11da1":{"id":"3c09533a47b150ec6e074fd86f2f556c","product_id":"749","qty":5,"name":"Michael Sheet","code":"60026189","price":599,"tax":"0.00","image":"068b61a296918b729d1f44240e373251.jpg","option":false,"options":null,"rowid":"03993d48fbc39b8122b6a5d62ea11da1","row_tax":"0.0000","subtotal":"2995.0000"},"ba691cb6dfa80432d43377511df01117":{"id":"1ed7b38cc826cbf8806897f6a11536fd","product_id":"719","qty":3,"name":"Ambush (Michael Bennett)","code":"02646516","price":599,"tax":"0.00","image":"e550a7c7fa66f8242d0734b027380b98.jpg","option":false,"options":null,"rowid":"ba691cb6dfa80432d43377511df01117","row_tax":"0.0000","subtotal":"1797.0000"},"17d0beafc2f8802335a748b047651582":{"id":"a274a9e7722d7f4d9055e1dc287c5758","product_id":"719","qty":1,"name":"Ambush (Michael Bennett)","code":"02646516","price":599,"tax":"0.00","image":"e550a7c7fa66f8242d0734b027380b98.jpg","option":false,"options":null,"rowid":"17d0beafc2f8802335a748b047651582","row_tax":"0.0000","subtotal":"599.0000"}}'),
('5e3b781c95ec010e2947d7b23dae84d8', '1508831061', 1, '{"cart_total":338,"total_item_tax":0,"total_items":2,"total_unique_items":2,"5b4ac21219cb8176de1aca7fba1c9223":{"id":"60fb388d6515028e004cde7ddca7937e","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"5b4ac21219cb8176de1aca7fba1c9223","row_tax":"0.0000","subtotal":"169.0000"},"e2271f7a81b6c46266a787fa594fda2c":{"id":"dffd2322cdb28fec6a49adb9307dddd4","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"e2271f7a81b6c46266a787fa594fda2c","row_tax":"0.0000","subtotal":"169.0000"}}'),
('65030421f10624b56ae865aff7c331ed', '1512896790', NULL, '{"cart_total":209,"total_item_tax":0,"total_items":1,"total_unique_items":1,"f12a719c841c3caaeaf15f6d8e256d8c":{"id":"72a25de4da5a8a833cd8e9d153995a41","product_id":"162","qty":1,"name":"Elsa Tutu Gown","code":"102-2001","price":209,"tax":"0.00","image":"642e24157c95f4b66bf93abd4003f374.jpg","option":"1","options":[{"id":"1","name":"2-3 years","price":"0.0000","total_quantity":"20.0000","quantity":"6.0000"},{"id":"2","name":"4-5 years","price":"0.0000","total_quantity":"22.0000","quantity":"7.0000"},{"id":"3","name":"6-7 years","price":"0.0000","total_quantity":"20.0000","quantity":"5.0000"},{"id":"4","name":"8-9 years","price":"0.0000","total_quantity":"14.0000","quantity":"7.0000"},{"id":"5","name":"10-11 years","price":"0.0000","total_quantity":"10.0000","quantity":"3.0000"}],"rowid":"f12a719c841c3caaeaf15f6d8e256d8c","row_tax":"0.0000","subtotal":"209.0000"}}'),
('665a5aecf93ab6be56d45b54e79babdb', '1511164150', NULL, '{"cart_total":94,"total_item_tax":0,"total_items":1,"total_unique_items":1,"021ebbcc1322a80b5241068da9ac07a3":{"id":"a4ff70e0dccc8c47703998f17ce26f82","product_id":"193","qty":1,"name":"3 Chest Flower gold tutu","code":"105-5002","price":94,"tax":"0.00","image":"105-5002.jpg","option":false,"options":null,"rowid":"021ebbcc1322a80b5241068da9ac07a3","row_tax":"0.0000","subtotal":"94.0000"}}'),
('66d7ae776d7426cfed570848ada3e47f', '1523177236', NULL, '{"cart_total":48543,"total_item_tax":0,"total_items":3,"total_unique_items":2,"092c819dbb16ba619769cf35b44db82c":{"id":"9c06ba8e13a8daceaa30737731656566","product_id":"719","qty":2,"name":"Ambush (Michael Bennett)","code":"02646516","price":23972,"tax":"0.00","image":"e550a7c7fa66f8242d0734b027380b98.jpg","option":"3","options":[{"id":"3","name":"2-3 years","price":"12.0000","total_quantity":"7.0000","quantity":"7.0000"},{"id":"4","name":"6-7 years","price":"45.0000","total_quantity":"7.0000","quantity":"4.0000"}],"rowid":"092c819dbb16ba619769cf35b44db82c","row_tax":"0.0000","subtotal":"47944.0000"},"d2c43706641b3e0a5459913dfeb2c06e":{"id":"2f3598970a8f587c9bf1e07619124be5","product_id":"751","qty":1,"name":"Bennett Dpem","code":"80930914","price":599,"tax":"0.00","image":"6e917f3a22d8e2b2932da7b1ab2a595e.jpg","option":false,"options":null,"rowid":"d2c43706641b3e0a5459913dfeb2c06e","row_tax":"0.0000","subtotal":"599.0000"}}'),
('6b2f2ecb2e03d443734e22b3601443d0', '1503565784', 1, '{"cart_total":487,"total_item_tax":0,"total_items":3,"total_unique_items":3,"52317cbe59e1c226b8d69596b94ffb9e":{"id":"b3e58723810c2fe272d3c08c93847f05","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"52317cbe59e1c226b8d69596b94ffb9e","row_tax":"0.0000","subtotal":"169.0000"},"4606da91476ada7c04d23e4d3e588ffd":{"id":"b3b3f99a64501c93898f1484940a5275","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"4606da91476ada7c04d23e4d3e588ffd","row_tax":"0.0000","subtotal":"169.0000"},"58746af083b2cbb4d007e9ce3c62974a":{"id":"6b6bf20e48d7e8de63fefb0e42e4f0c1","product_id":"443","qty":1,"name":"SmallBox Brown Velvet 3 Pcs Suit","code":"202-2019","price":149,"tax":"0.00","image":"202-2019.jpg","option":false,"options":null,"rowid":"58746af083b2cbb4d007e9ce3c62974a","row_tax":"0.0000","subtotal":"149.0000"}}'),
('70418bd9f312164ac44a9107e4783b2e', '1511776732', 1, '{"cart_total":169,"total_item_tax":0,"total_items":1,"total_unique_items":1,"92753f5259ef9f0c0159f7ff9aa9440a":{"id":"a7726327c2caecca03ac00bcd9fbff61","product_id":"59","qty":1,"name":"Tinker bell Satin Gown","code":"11534526","price":169,"tax":"0.00","image":"1c55138ed635e078e76f32b9e0b37888.png","option":false,"options":null,"rowid":"92753f5259ef9f0c0159f7ff9aa9440a","row_tax":"0.0000","subtotal":"169.0000"}}'),
('78986c7e0bfd23aba0d75961b98afcf9', '1501144745', NULL, '{"cart_total":40,"total_item_tax":0,"total_items":1,"total_unique_items":1,"cfedfc662f70910b8f28a5abf1602ebb":{"id":"29813e937fe44c0d4ed093c1932343d7","product_id":"689","qty":1,"name":"Crocket Shoes - Red","code":"111-1039","price":40,"tax":"0.00","image":"111-1039.jpg","option":false,"options":null,"rowid":"cfedfc662f70910b8f28a5abf1602ebb","row_tax":"0.0000","subtotal":"40.0000"}}'),
('7ba505c7e7d9b16a4044a150ea5420a2', '1503412881', 1, '{"cart_total":5408,"total_item_tax":0,"total_items":32,"total_unique_items":32,"5ce13794608ee5ee8dd3fe550cfe3046":{"id":"2476025104420e0ccef5bdd90c9a237f","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"5ce13794608ee5ee8dd3fe550cfe3046","row_tax":"0.0000","subtotal":"169.0000"},"49545e0cdc05f8e490ea522e032911dc":{"id":"566e1243d47c8bf44c0de35620d118b6","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"49545e0cdc05f8e490ea522e032911dc","row_tax":"0.0000","subtotal":"169.0000"},"b34cedff17862348f60370fdaf45dd09":{"id":"597094f3c9f87a692985e4a71c9977f2","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"b34cedff17862348f60370fdaf45dd09","row_tax":"0.0000","subtotal":"169.0000"},"c666ee08e372f303f0532955a55532dd":{"id":"28a32c9b9a16fb944f5fb1354279c566","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"c666ee08e372f303f0532955a55532dd","row_tax":"0.0000","subtotal":"169.0000"},"21e0d0fb2877ca9b93d8afa9b8c24d99":{"id":"527f795c1d42f15f33a9a25b5ec3eb9e","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"21e0d0fb2877ca9b93d8afa9b8c24d99","row_tax":"0.0000","subtotal":"169.0000"},"a9a677f6615ca8a01fd564f8e3f38416":{"id":"1b01a4bfb5b411f1cd6b27a4f8f7a819","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"a9a677f6615ca8a01fd564f8e3f38416","row_tax":"0.0000","subtotal":"169.0000"},"f359b461e966bf38cee11b13fb6325f4":{"id":"ffb552af7b3ac557d194086aba25b612","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"f359b461e966bf38cee11b13fb6325f4","row_tax":"0.0000","subtotal":"169.0000"},"4b2df31bb6abbbde98b3a50d2e77966d":{"id":"e72dcc9eb2b9673658f7038353347f18","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"4b2df31bb6abbbde98b3a50d2e77966d","row_tax":"0.0000","subtotal":"169.0000"},"eaad802ba433eeb8028c11811774e8e3":{"id":"db7e63ca7c30d1029a2f2db513b845ad","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"eaad802ba433eeb8028c11811774e8e3","row_tax":"0.0000","subtotal":"169.0000"},"9d565881e378c49da2991a80a48a81a0":{"id":"42c25e2094dfde65fd6f290ee72101cf","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"9d565881e378c49da2991a80a48a81a0","row_tax":"0.0000","subtotal":"169.0000"},"54342961ab27e4ef9a979025aac8a462":{"id":"9f9a83af04c8dc68431c414a3a810f7c","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"54342961ab27e4ef9a979025aac8a462","row_tax":"0.0000","subtotal":"169.0000"},"7f1468af186ea3d6bd603166b64512ac":{"id":"3a5746d313cfb75cc48b32a6a6d59728","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"7f1468af186ea3d6bd603166b64512ac","row_tax":"0.0000","subtotal":"169.0000"},"24c9588ed5bba03e04b1663933198319":{"id":"d4a732d7b34394bb54b4eb7f108d53f4","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"24c9588ed5bba03e04b1663933198319","row_tax":"0.0000","subtotal":"169.0000"},"a72a0ece4ffe2f31f5cad63291883785":{"id":"a5dd440b01571ca2f7e10a86478aa9ac","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"a72a0ece4ffe2f31f5cad63291883785","row_tax":"0.0000","subtotal":"169.0000"},"67156368b527e7fef87a7fa3d3465c49":{"id":"5be8230c2a54235db02014a96ef650c6","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"67156368b527e7fef87a7fa3d3465c49","row_tax":"0.0000","subtotal":"169.0000"},"9fdf647061ae288760f94c2b746683af":{"id":"adbddf91a46a1110d08ae67ce5b497bf","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"9fdf647061ae288760f94c2b746683af","row_tax":"0.0000","subtotal":"169.0000"},"aee05daa1714394fac61b1f4d631c3ad":{"id":"a96939b85718fb9d6fcca8686b114c3d","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"aee05daa1714394fac61b1f4d631c3ad","row_tax":"0.0000","subtotal":"169.0000"},"676b050d4d8ca0178af5a4a2741b095c":{"id":"fdecb87df7f18000ba2806472faebf33","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"676b050d4d8ca0178af5a4a2741b095c","row_tax":"0.0000","subtotal":"169.0000"},"88ad2a0c4641a60248990e9d5f2ed4a4":{"id":"c2c6542e6d41f4cbebbb703dd698e00d","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"88ad2a0c4641a60248990e9d5f2ed4a4","row_tax":"0.0000","subtotal":"169.0000"},"89d8f84f0f7b55be41d79a384896c19c":{"id":"ec7cb7e2b923568fefd5bfa48abcf9b5","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"89d8f84f0f7b55be41d79a384896c19c","row_tax":"0.0000","subtotal":"169.0000"},"79ad47453eee6a5d3df29793cfa06967":{"id":"f0c2674ef37c004d779f2d713bd0c563","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"79ad47453eee6a5d3df29793cfa06967","row_tax":"0.0000","subtotal":"169.0000"},"47e0938c88ad5e2f8a88ffb3495b197b":{"id":"b3c237020a7807d8f0704d0b7bc62b02","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"47e0938c88ad5e2f8a88ffb3495b197b","row_tax":"0.0000","subtotal":"169.0000"},"87f956f84fb43183cec33567118a0e22":{"id":"1e56656f981f9ee1ba5b2a0e6e3105d0","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"87f956f84fb43183cec33567118a0e22","row_tax":"0.0000","subtotal":"169.0000"},"a74786c77744def421c15be114546de3":{"id":"69d54050155b0ee9cc5075de7a88b9b4","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"a74786c77744def421c15be114546de3","row_tax":"0.0000","subtotal":"169.0000"},"0fec96965acba3da91d517ea231a6da2":{"id":"65113f4e5542d0587224e8b3652bf2aa","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"0fec96965acba3da91d517ea231a6da2","row_tax":"0.0000","subtotal":"169.0000"},"9906e429b5839c09c52b96071ffac0c2":{"id":"343f2d88a1db84870c0c47faacd1a97c","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"9906e429b5839c09c52b96071ffac0c2","row_tax":"0.0000","subtotal":"169.0000"},"a2b3a4de04f6b9ca214f1020bb136409":{"id":"f8519c66ecf58f041f27f090fabb52a5","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"a2b3a4de04f6b9ca214f1020bb136409","row_tax":"0.0000","subtotal":"169.0000"},"52c824f2f9a92d779e831136063755f5":{"id":"be1cd1360990db3a1f34d408848d5ffb","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"52c824f2f9a92d779e831136063755f5","row_tax":"0.0000","subtotal":"169.0000"},"bf8329370c80e9d6b12d447fb6ad1630":{"id":"58df6812681196e7e4a2b5d32f78b7ee","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"bf8329370c80e9d6b12d447fb6ad1630","row_tax":"0.0000","subtotal":"169.0000"},"97959adc95901dff7006521890eedbb5":{"id":"b1fdaf13db722bb23f7c9f4774979a1d","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"97959adc95901dff7006521890eedbb5","row_tax":"0.0000","subtotal":"169.0000"},"a156f557a1423361c08d50f52aa249da":{"id":"51a3e29e140b771787226aa7406edb9f","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"a156f557a1423361c08d50f52aa249da","row_tax":"0.0000","subtotal":"169.0000"},"5c276a241e5a64dee61ee45e779aa4f7":{"id":"994e059767754590572c64313013e432","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"5c276a241e5a64dee61ee45e779aa4f7","row_tax":"0.0000","subtotal":"169.0000"}}');
INSERT INTO `sma_cart` (`id`, `time`, `user_id`, `data`) VALUES
('8080d32d7f7b25695f461119aa0e106b', '1506513855', NULL, '{"cart_total":10819,"total_item_tax":0,"total_items":71,"total_unique_items":71,"76a3a46010b11f6c4c9d7ec39deabe75":{"id":"bc04b7b52b0ca7dc91f1127fb46167ba","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"76a3a46010b11f6c4c9d7ec39deabe75","row_tax":"0.0000","subtotal":"169.0000"},"087de1d82ae100bdc667393caf70a189":{"id":"39e64e9e2e1626ee0e3c525ab5af7be3","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"087de1d82ae100bdc667393caf70a189","row_tax":"0.0000","subtotal":"169.0000"},"5e5dedb7567843553956ac671bda10ef":{"id":"8d5e1284ec9d1d7131a204414cba985a","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"5e5dedb7567843553956ac671bda10ef","row_tax":"0.0000","subtotal":"94.0000"},"89a0d3c737f5eeb7af4ca4c70dd67f4f":{"id":"395422b7f90fd797fcad452e723753c5","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"89a0d3c737f5eeb7af4ca4c70dd67f4f","row_tax":"0.0000","subtotal":"94.0000"},"c4d0a9e3bf7ca80ac5026632bda031c3":{"id":"64d16479b39db6579849b34d3efdc7fe","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"c4d0a9e3bf7ca80ac5026632bda031c3","row_tax":"0.0000","subtotal":"169.0000"},"097d383d55d9c4bc427f2ff34cf791ac":{"id":"a711cf267485e79f25b3b23ddf052159","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"097d383d55d9c4bc427f2ff34cf791ac","row_tax":"0.0000","subtotal":"169.0000"},"09f786deb7ed808fe767794183f8a916":{"id":"bd1e93f7d93d8d796560cbb667ca20fe","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"09f786deb7ed808fe767794183f8a916","row_tax":"0.0000","subtotal":"169.0000"},"eb99a2a0c67cff30bb030ff6fd7aaf94":{"id":"43f406f385cad9d26d2e50ec14141e70","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"eb99a2a0c67cff30bb030ff6fd7aaf94","row_tax":"0.0000","subtotal":"169.0000"},"a8ed271ac425a9b9b483920dc3d76b7f":{"id":"99fe02131487c8e72e7ce9f121c7a5fe","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"a8ed271ac425a9b9b483920dc3d76b7f","row_tax":"0.0000","subtotal":"169.0000"},"a35c8f30aac11f6c8ca1aa4bae646aa9":{"id":"9502d2102fc460e8833cae97fc506e63","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"a35c8f30aac11f6c8ca1aa4bae646aa9","row_tax":"0.0000","subtotal":"169.0000"},"b28ec501a6286b58d90eb11b7e75f66c":{"id":"b191900793a29506241d08b031f3d686","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"b28ec501a6286b58d90eb11b7e75f66c","row_tax":"0.0000","subtotal":"169.0000"},"84005ec81d039e78489548f46003cb60":{"id":"bd5e9eab6dc8d36afa4b8173168e3a3e","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"84005ec81d039e78489548f46003cb60","row_tax":"0.0000","subtotal":"169.0000"},"dd91dd0c43ed5a89d1ca8accf8696c80":{"id":"36027bc83137a95c69f91484cc961d93","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"dd91dd0c43ed5a89d1ca8accf8696c80","row_tax":"0.0000","subtotal":"169.0000"},"c90740ff0918e553f6fb5ad7a4f984c0":{"id":"700527301c0c665dee24e602fb7fca5c","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"c90740ff0918e553f6fb5ad7a4f984c0","row_tax":"0.0000","subtotal":"169.0000"},"64d3b3b28b1b38d06c5eb71edadafd77":{"id":"46fd6aba0ebed9b3ef71e114ad870a8b","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"64d3b3b28b1b38d06c5eb71edadafd77","row_tax":"0.0000","subtotal":"169.0000"},"0e789993cc0dd53e9c0298ef7955348a":{"id":"96bf36d4065948df9f89eb058952b77c","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"0e789993cc0dd53e9c0298ef7955348a","row_tax":"0.0000","subtotal":"169.0000"},"59c07f5ebc5afe7fa5858d132a32b5bc":{"id":"abba478ca4d564de24805b01345fcea4","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"59c07f5ebc5afe7fa5858d132a32b5bc","row_tax":"0.0000","subtotal":"169.0000"},"75f6a0b29a4375b9542d52c937370f73":{"id":"ba0d5706f39ecb0dd87d27730ff70f55","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"75f6a0b29a4375b9542d52c937370f73","row_tax":"0.0000","subtotal":"169.0000"},"85305815f082c59e2f753f3c39cae58d":{"id":"2ece495a8d0fb8c5b8b86bd79b3c0b6f","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"85305815f082c59e2f753f3c39cae58d","row_tax":"0.0000","subtotal":"169.0000"},"e116203887c64b7958be9267bc400ce2":{"id":"720029bbec70c31714b7c61dcf0d4303","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"e116203887c64b7958be9267bc400ce2","row_tax":"0.0000","subtotal":"169.0000"},"6d19699efb7bbdb0fbd5abeb49e7ea83":{"id":"ecb0ff184b543b910f0622513ab305af","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"6d19699efb7bbdb0fbd5abeb49e7ea83","row_tax":"0.0000","subtotal":"169.0000"},"93b2ede179aff3e14fb1290a845aa21a":{"id":"3535641b118c85178a5c57d050e547d1","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"93b2ede179aff3e14fb1290a845aa21a","row_tax":"0.0000","subtotal":"94.0000"},"931b12e2a23b1bf9a6f3cf96133bff8d":{"id":"a029b40d20c81267e9106976ea4aa4fc","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"931b12e2a23b1bf9a6f3cf96133bff8d","row_tax":"0.0000","subtotal":"94.0000"},"b42417aa45bd2d62f1b4ec961d72830d":{"id":"2c7dfaeee6507c8e2029db39d63c897d","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"b42417aa45bd2d62f1b4ec961d72830d","row_tax":"0.0000","subtotal":"94.0000"},"9b27d2964c8c5e040a31a04c5918ff5f":{"id":"b59c804fb59d6e9493dddfc54726c8e1","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"9b27d2964c8c5e040a31a04c5918ff5f","row_tax":"0.0000","subtotal":"94.0000"},"9fd05e66b64a522ee72e928e180bcc17":{"id":"b2f91f74ac4cdfa8f7a0155e016d5b04","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"9fd05e66b64a522ee72e928e180bcc17","row_tax":"0.0000","subtotal":"94.0000"},"e5e108918503703aaaa41ae840d47ccc":{"id":"88966ffa47cf64da6a959f5768fe8b81","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"e5e108918503703aaaa41ae840d47ccc","row_tax":"0.0000","subtotal":"169.0000"},"f499be9db20cc9b011cdd6df336839e6":{"id":"ac478333d8c56aa7fdea2479ce48e9ef","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"f499be9db20cc9b011cdd6df336839e6","row_tax":"0.0000","subtotal":"169.0000"},"b3be605f1368356bf67d6cab8552f53f":{"id":"f4028f6615090b69b4cc530a6e00607d","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"b3be605f1368356bf67d6cab8552f53f","row_tax":"0.0000","subtotal":"94.0000"},"285c3436793bbe66c1b2e40221f4f830":{"id":"cc36a5796b167576b8e7ea3e7825f7ec","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"285c3436793bbe66c1b2e40221f4f830","row_tax":"0.0000","subtotal":"169.0000"},"1acb2de42171ddccb8ffa66b98272617":{"id":"9309280b5d2a0b7ba775c3d03b9dfdd8","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"1acb2de42171ddccb8ffa66b98272617","row_tax":"0.0000","subtotal":"169.0000"},"3fe4766937d1181d779c539ab46d4a72":{"id":"69e9704e66b4a10f41b67e8b81a75fd4","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"3fe4766937d1181d779c539ab46d4a72","row_tax":"0.0000","subtotal":"169.0000"},"a664643f28d4dedde9de8e38feb0eff3":{"id":"acf740ffb8cb0e19c82b6dd908d9665e","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"a664643f28d4dedde9de8e38feb0eff3","row_tax":"0.0000","subtotal":"169.0000"},"e496c5d8b27f93811b00aee66dae9b6e":{"id":"28acb12c37b3604cbba8a9bb13d7f33f","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"e496c5d8b27f93811b00aee66dae9b6e","row_tax":"0.0000","subtotal":"169.0000"},"cb4847c0b76ee5e98985170aaf517b47":{"id":"fd2ce3ad9cd740cd9cf96058f3d6ea76","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"cb4847c0b76ee5e98985170aaf517b47","row_tax":"0.0000","subtotal":"169.0000"},"ddcbf9af3a681ef00b87b95bc2cf67fa":{"id":"1fc3fb6c1f29d65449ee0dc718012cd1","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"ddcbf9af3a681ef00b87b95bc2cf67fa","row_tax":"0.0000","subtotal":"94.0000"},"932c799e709059a8053d279a1d2b8b39":{"id":"474f5571a514f43caf22b7a22c7d6fe7","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"932c799e709059a8053d279a1d2b8b39","row_tax":"0.0000","subtotal":"169.0000"},"a16df1fd02d40824ea4b7f92a04c54b3":{"id":"548c23c4ca7dd32c2a7e6606fa77a1c9","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"a16df1fd02d40824ea4b7f92a04c54b3","row_tax":"0.0000","subtotal":"169.0000"},"f9a90eb14242511fd0c023c7db35d100":{"id":"ff150e7a21ac2dc43de4ca7da73a2f2e","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"f9a90eb14242511fd0c023c7db35d100","row_tax":"0.0000","subtotal":"169.0000"},"a21450ce06e122597374f2e2f7276874":{"id":"c5c3f2edfc11ab102241ab5b38945944","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"a21450ce06e122597374f2e2f7276874","row_tax":"0.0000","subtotal":"169.0000"},"b67ee7f5739b56a42e8acd9ac0b3a7c9":{"id":"a2667269b6a5e01404b992af987eebaf","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"b67ee7f5739b56a42e8acd9ac0b3a7c9","row_tax":"0.0000","subtotal":"94.0000"},"c6b114680b1e4a7e1be169dd1a3d9c57":{"id":"da102ce8921ae8350715a2a6f91b9400","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"c6b114680b1e4a7e1be169dd1a3d9c57","row_tax":"0.0000","subtotal":"169.0000"},"ece6a159cb1218a8b2ff160b7138f05c":{"id":"d6f05b8f3f9ef82dd9bd2280018775f1","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"ece6a159cb1218a8b2ff160b7138f05c","row_tax":"0.0000","subtotal":"169.0000"},"3d362eebea1ea0071cefa98f2612bc1e":{"id":"1bd0c0f27ee916a7972bbf7b430f41b6","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"3d362eebea1ea0071cefa98f2612bc1e","row_tax":"0.0000","subtotal":"169.0000"},"4bba94c8e3e27caac091c63986c2d19d":{"id":"91a6eaee23da4927cc324e218c3deb27","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"4bba94c8e3e27caac091c63986c2d19d","row_tax":"0.0000","subtotal":"94.0000"},"7883d869bb10f128fdb3f2d5eff765c5":{"id":"e0ef5f64418e47fd57ffc05e3e69bcdf","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"7883d869bb10f128fdb3f2d5eff765c5","row_tax":"0.0000","subtotal":"169.0000"},"b46329d292241b4f60b89f38a99da4b5":{"id":"7f846e12cf19b57726ffe8f14f1286d6","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"b46329d292241b4f60b89f38a99da4b5","row_tax":"0.0000","subtotal":"169.0000"},"d1ae33d1b7f90dad370f1009b4e3239f":{"id":"6a18fb52c14a65c81f8e881758407d5e","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"d1ae33d1b7f90dad370f1009b4e3239f","row_tax":"0.0000","subtotal":"169.0000"},"7ed6db4aa54d6f38d8dbf03323faa7be":{"id":"68fc15856598abafc40a0338135a9f8b","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"7ed6db4aa54d6f38d8dbf03323faa7be","row_tax":"0.0000","subtotal":"169.0000"},"44c10c6fc9f1943ccd51e3c2fa9b2038":{"id":"02c5657c1147e77b294d795b098eb774","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"44c10c6fc9f1943ccd51e3c2fa9b2038","row_tax":"0.0000","subtotal":"169.0000"},"875e7c95139f4a37c8b2087cf2011663":{"id":"4c086139efeeb125012699fd82c82186","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"875e7c95139f4a37c8b2087cf2011663","row_tax":"0.0000","subtotal":"169.0000"},"9979459c38b1faf6221d59d69f1f5bd3":{"id":"09d6216ae8cff81132d0130cb96e5aba","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"9979459c38b1faf6221d59d69f1f5bd3","row_tax":"0.0000","subtotal":"169.0000"},"1a76eb787cb4023ffa45e20399e7b4bc":{"id":"35194cda81a64fb3aeee0d1f1fab70e9","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"1a76eb787cb4023ffa45e20399e7b4bc","row_tax":"0.0000","subtotal":"169.0000"},"71ee1b77a18979ca14f7da77b10066e0":{"id":"1a756087235e2f300ff737301da99ae3","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"71ee1b77a18979ca14f7da77b10066e0","row_tax":"0.0000","subtotal":"169.0000"},"1be000b92c312a53ce23abd08e4ea591":{"id":"3082cf67f5bcb5abb95d34b14d37a632","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"1be000b92c312a53ce23abd08e4ea591","row_tax":"0.0000","subtotal":"169.0000"},"b47e96338922a0de6375f13a72895633":{"id":"2bd841eb3acd74783275f69d57cffd40","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"b47e96338922a0de6375f13a72895633","row_tax":"0.0000","subtotal":"169.0000"},"922e58d75c61f66a51d7ef7500434840":{"id":"31ac87be653fe256b71369fd0d25d2d8","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"922e58d75c61f66a51d7ef7500434840","row_tax":"0.0000","subtotal":"169.0000"},"b14b261f070b167b5fc5786a68abd791":{"id":"730493ef69e57b584456190d352eacfd","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"b14b261f070b167b5fc5786a68abd791","row_tax":"0.0000","subtotal":"169.0000"},"44ff222dad425eb2b29cf7a58d72fe4f":{"id":"3d7a3ae6efc732c493628d06ee29059e","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"44ff222dad425eb2b29cf7a58d72fe4f","row_tax":"0.0000","subtotal":"169.0000"},"94909d464a03c33b5de47c99cbfc5ef2":{"id":"99c1e679afb4fe90a934bf0d453aecfb","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"94909d464a03c33b5de47c99cbfc5ef2","row_tax":"0.0000","subtotal":"169.0000"},"0a83fb28acfd02d127b0b67d5b252600":{"id":"218a3e51472c4ad27e1f50bf1dd9f5db","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"0a83fb28acfd02d127b0b67d5b252600","row_tax":"0.0000","subtotal":"94.0000"},"6d2be1f2d1d64973057d88b41b576dee":{"id":"9b776ee423f6bb29d1a6928fd461dfec","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"6d2be1f2d1d64973057d88b41b576dee","row_tax":"0.0000","subtotal":"94.0000"},"9b2972247163d699f4fd89cd90661142":{"id":"c5b7f38ac8b01e393355e335a6d4928e","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"9b2972247163d699f4fd89cd90661142","row_tax":"0.0000","subtotal":"94.0000"},"058219982eb82c80b169d31324576270":{"id":"85d95b07bbf97730154607dc3a3addd2","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"058219982eb82c80b169d31324576270","row_tax":"0.0000","subtotal":"169.0000"},"e10928bc5807d1e32c5eec03db969a96":{"id":"f0f3124dd2c0eb17645fbeabfd763ceb","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"e10928bc5807d1e32c5eec03db969a96","row_tax":"0.0000","subtotal":"169.0000"},"39f94c602c8c9f13dee2d77be53e9439":{"id":"29f2645178e3f45c60f9cfde829f492b","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"39f94c602c8c9f13dee2d77be53e9439","row_tax":"0.0000","subtotal":"169.0000"},"2756bd34f2684d6584902efa5412aca2":{"id":"d0e1ce5e84a550d1a8e4c693a5f02002","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"2756bd34f2684d6584902efa5412aca2","row_tax":"0.0000","subtotal":"94.0000"},"b88a405d3ba4c5940f06cc70b8a188d3":{"id":"70768d937cb015ffbb2de32b158181dc","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"b88a405d3ba4c5940f06cc70b8a188d3","row_tax":"0.0000","subtotal":"94.0000"},"d11594b0739b5d6530182edb2357024c":{"id":"2f68456d33b04bb6d22d84ecf7581a69","product_id":"56","qty":1,"name":"Vintage Printed Chiffon, Self Block","code":"11534523","price":189,"tax":"0.00","image":"686df5c67585f872d9e500720f9daa99.png","option":false,"options":null,"rowid":"d11594b0739b5d6530182edb2357024c","row_tax":"0.0000","subtotal":"189.0000"},"3d925abfff68f4ff0324f352a62db7f9":{"id":"21c9441c492aa475c6f78ec008a1d8f4","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"3d925abfff68f4ff0324f352a62db7f9","row_tax":"0.0000","subtotal":"169.0000"},"be46261b22401e55a558a8e3f6ce0971":{"id":"f87d14d9569f6b17e7741ff08c95ed76","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"be46261b22401e55a558a8e3f6ce0971","row_tax":"0.0000","subtotal":"169.0000"}}'),
('81c39c17d4e8ae8a0585a2b5328114a7', '1505907997', NULL, '{"cart_total":318,"total_item_tax":0,"total_items":2,"total_unique_items":2,"20be9ca7b4f8232d6ac5576859636dd2":{"id":"6d4eb5ee9032fc8a7cb8db948d883a70","product_id":"72","qty":1,"name":"lace patch Flower,pearls - Red","code":"11534539","price":159,"tax":"0.00","image":"a8ba09b73bc32b2b13db5443cbdd6a02.png","option":false,"options":null,"rowid":"20be9ca7b4f8232d6ac5576859636dd2","row_tax":"0.0000","subtotal":"159.0000"},"94baa7e5b1742a2cd0381f62f7861272":{"id":"00e5c89dc489688bd2941118e0524839","product_id":"72","qty":1,"name":"lace patch Flower,pearls - Red","code":"11534539","price":159,"tax":"0.00","image":"a8ba09b73bc32b2b13db5443cbdd6a02.png","option":false,"options":null,"rowid":"94baa7e5b1742a2cd0381f62f7861272","row_tax":"0.0000","subtotal":"159.0000"}}'),
('9285bf62a6163c2f119e983094462ef5', '1512626321', 1, '{"cart_total":267,"total_item_tax":0,"total_items":3,"total_unique_items":3,"d1bce7075f2ca981fab9aff181712b3a":{"id":"f64168a175356d13cee93c96245b50d5","product_id":"627","qty":1,"name":"YELLOW LACE DRESS","code":"101-1078","price":89,"tax":"0.00","image":"101-1078.jpg","option":false,"options":null,"rowid":"d1bce7075f2ca981fab9aff181712b3a","row_tax":"0.0000","subtotal":"89.0000"},"4830db0a14be36db60b647964c4cbb08":{"id":"8c1a13fa698199db1a10974b4fd14a39","product_id":"627","qty":1,"name":"YELLOW LACE DRESS","code":"101-1078","price":89,"tax":"0.00","image":"101-1078.jpg","option":false,"options":null,"rowid":"4830db0a14be36db60b647964c4cbb08","row_tax":"0.0000","subtotal":"89.0000"},"eb6fdc36b281b7d5eabf33396c2683a2":{"id":618,"name":"RAINBOW SUMMER DRESS","price":89,"qty":1,"rowid":"eb6fdc36b281b7d5eabf33396c2683a2","row_tax":"0.0000","subtotal":"89.0000"}}'),
('97a1efee4d383210db5d5bb025195fcb', '1522496893', NULL, '{"cart_total":1265,"total_item_tax":0,"total_items":5,"total_unique_items":5,"1121ad27493c4b2d554e23b1b0d10a97":{"id":"82b892279ce4fa49fc7783c96dfe9aa9","product_id":"705","qty":1,"name":"Say You''re Sorry (Morgan Dane)","code":"22237809","price":299,"tax":"0.00","image":"e560d3cf82fd7a4116ed46b818bab367.jpg","option":false,"options":null,"rowid":"1121ad27493c4b2d554e23b1b0d10a97","row_tax":"0.0000","subtotal":"299.0000"},"97af02c4239ff1426ef450be9b6f3643":{"id":"3e7f0acd6d2d7e1686e3d1a247166e3a","product_id":"707","qty":1,"name":"Her Last Goodbye (Morgan Dane)","code":"02144840","price":199,"tax":"0.00","image":"9f596e608df2471c9b0faf16c66b57a2.jpg","option":false,"options":null,"rowid":"97af02c4239ff1426ef450be9b6f3643","row_tax":"0.0000","subtotal":"199.0000"},"0f04add52e2b0c71e0eb6469b3e01aa4":{"id":"f8554f0daf408d388f9ab6d9b60a5ef3","product_id":"707","qty":1,"name":"Her Last Goodbye (Morgan Dane)","code":"02144840","price":199,"tax":"0.00","image":"9f596e608df2471c9b0faf16c66b57a2.jpg","option":false,"options":null,"rowid":"0f04add52e2b0c71e0eb6469b3e01aa4","row_tax":"0.0000","subtotal":"199.0000"},"5718aca7b4f9ff1045143506ab500867":{"id":"ea682e2fe0d0d6d7163cd08000e1bb96","product_id":"707","qty":1,"name":"Her Last Goodbye (Morgan Dane)","code":"02144840","price":199,"tax":"0.00","image":"9f596e608df2471c9b0faf16c66b57a2.jpg","option":false,"options":null,"rowid":"5718aca7b4f9ff1045143506ab500867","row_tax":"0.0000","subtotal":"199.0000"},"c12fcf4f0ae0825370aca368067c7e3b":{"id":"a1b5ab2bf517afe4b5e6be4f63b6b7a9","product_id":"706","qty":1,"name":"Bones Don''t Lie (Morgan Dane)","code":"68252297","price":369,"tax":"0.00","image":"d7a8d2f549d14501dbb5d000b0cfbc53.jpg","option":false,"options":null,"rowid":"c12fcf4f0ae0825370aca368067c7e3b","row_tax":"0.0000","subtotal":"369.0000"}}'),
('98a3713b8bed3300fd0050c1a35516cb', '1501504853', 1, '{"cart_total":765,"total_item_tax":0,"total_items":5,"total_unique_items":5,"b7f75481ec7f176e001dac4e742f846c":{"id":"3c56d1530b1ff0298db3a3a8d06caa99","product_id":"185","qty":1,"name":"ANNA TUTU DRESS","code":"KF-Costm-2024","price":119,"tax":"0.00","image":"","option":false,"options":null,"rowid":"b7f75481ec7f176e001dac4e742f846c","row_tax":"0.0000","subtotal":"119.0000"},"a49d85e25c9b32620906e8b4194dfa0a":{"id":"1438fe710a04d5542f93782f1f9e278d","product_id":"59","qty":1,"name":"Tinker bell Satin Gown","code":"11534526","price":169,"tax":"0.00","image":"1c55138ed635e078e76f32b9e0b37888.png","option":false,"options":null,"rowid":"a49d85e25c9b32620906e8b4194dfa0a","row_tax":"0.0000","subtotal":"169.0000"},"0873e1e5f9b6c8ec2c96cf54f46c5b80":{"id":"2399dd89723aedf20920dfb0c9a78635","product_id":"62","qty":1,"name":"Embroidered Net Gowns - Blue","code":"11534529","price":159,"tax":"0.00","image":"67cbdd35e6f48aec087187aace803e53.png","option":false,"options":null,"rowid":"0873e1e5f9b6c8ec2c96cf54f46c5b80","row_tax":"0.0000","subtotal":"159.0000"},"8686318580b041782b6b4f066509c513":{"id":"93f938bbf56caeab13847e44c237eb18","product_id":"72","qty":1,"name":"lace patch Flower,pearls - Red","code":"11534539","price":159,"tax":"0.00","image":"a8ba09b73bc32b2b13db5443cbdd6a02.png","option":false,"options":null,"rowid":"8686318580b041782b6b4f066509c513","row_tax":"0.0000","subtotal":"159.0000"},"03fb631dcd5a78b263f909959c5648ed":{"id":"f8ca4ecba62fe68ace8e7c9973e55b3c","product_id":"61","qty":1,"name":"Embroidered Net Gowns - Purple","code":"11534528","price":159,"tax":"0.00","image":"49d6e4224e261212ca7d2c530b5b10d2.png","option":false,"options":null,"rowid":"03fb631dcd5a78b263f909959c5648ed","row_tax":"0.0000","subtotal":"159.0000"}}'),
('b45c0a12d5a5def1f6ae6c31034fea92', '1509974170', NULL, '{"cart_total":338,"total_item_tax":0,"total_items":2,"total_unique_items":2,"f5cc3d6305bfce13f74493c94bf5e8e1":{"id":"71e2bb54788bad4a741324ed87f004b8","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"f5cc3d6305bfce13f74493c94bf5e8e1","row_tax":"0.0000","subtotal":"169.0000"},"36817e6930bea99fe16723cdfb33eea6":{"id":"576ad4216d2e30d2e0043e9209b9d836","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"36817e6930bea99fe16723cdfb33eea6","row_tax":"0.0000","subtotal":"169.0000"}}'),
('be3306f7ed731ae0083437cd29028dfe', '1514367507', 1, '{"cart_total":189,"total_item_tax":0,"total_items":1,"total_unique_items":1,"4aef90c0e50b9bffd5e306503c06429d":{"id":"456eef71d1a7a568df84b4d47060d6b9","product_id":"51","qty":1,"name":"Chest Dropping Lace patch Gown with Sequins - Marron","code":"11534518","price":189,"tax":"0.00","image":"d34a015b8211db837e79f4a1a54cb884.jpg","option":false,"options":[{"id":"12","name":"2-3 years","price":"50.0000","total_quantity":"2.0000","quantity":"2.0000"}],"rowid":"4aef90c0e50b9bffd5e306503c06429d","row_tax":"0.0000","subtotal":"189.0000"}}'),
('c8c8007af964fa99fb9ea77929b4d4df', '1506513186', NULL, '{"cart_total":5875,"total_item_tax":0,"total_items":42,"total_unique_items":42,"66c5ae012cafda0e6e36e86d68567e3a":{"id":"01be33cf629325b0cbf594ec6dfdf42d","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"66c5ae012cafda0e6e36e86d68567e3a","row_tax":"0.0000","subtotal":"169.0000"},"31817bfbf84091597e90710b4dc89cbe":{"id":"c2b44c6202123792c59c5b909be6e481","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"31817bfbf84091597e90710b4dc89cbe","row_tax":"0.0000","subtotal":"169.0000"},"92d60577729b3a50efe413fb5a7cb4c9":{"id":"a416d845e43206eece0490210c2df97f","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"92d60577729b3a50efe413fb5a7cb4c9","row_tax":"0.0000","subtotal":"94.0000"},"3e617ec4fdc996830fffa4fe6af6ed54":{"id":"ece7f5b1f0b353328af14dfc0b41947e","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"3e617ec4fdc996830fffa4fe6af6ed54","row_tax":"0.0000","subtotal":"169.0000"},"d5983d6fdd9d557caa28cec92213e63e":{"id":"f1b7bf74a0ce00009c2e49b3787f9676","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"d5983d6fdd9d557caa28cec92213e63e","row_tax":"0.0000","subtotal":"169.0000"},"edbab0776fa552d71821f4c974eb6b1d":{"id":"a09086230490def29b8cf45223416fc4","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"edbab0776fa552d71821f4c974eb6b1d","row_tax":"0.0000","subtotal":"169.0000"},"2f32f0394c4a785d121aa6bb5259f316":{"id":"4d04f1627d5c619ac9ec73190c76f1d7","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"2f32f0394c4a785d121aa6bb5259f316","row_tax":"0.0000","subtotal":"169.0000"},"dc64c1f6a81c2b76d80eb484dd27049d":{"id":"ceccf3377ebfdaf9d8fe00b57427ca0d","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"dc64c1f6a81c2b76d80eb484dd27049d","row_tax":"0.0000","subtotal":"94.0000"},"1895d8fae59bdd130371e3725562cf39":{"id":"3d6d0207cfc74d06c690fb6178274a93","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"1895d8fae59bdd130371e3725562cf39","row_tax":"0.0000","subtotal":"169.0000"},"37639049fc16c6a8a635bfb8d1ea7381":{"id":"52b826778873f57930caca4c4b97bb89","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"37639049fc16c6a8a635bfb8d1ea7381","row_tax":"0.0000","subtotal":"169.0000"},"be0a21690aa1fd944c4baef11a372731":{"id":"72c4b00a40bdbd8fab499b3b1db8f284","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"be0a21690aa1fd944c4baef11a372731","row_tax":"0.0000","subtotal":"169.0000"},"f686e20d0662b02cc858dfb96bfdc4d4":{"id":"d2329e4fe172a5340c70414d3b30940d","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"f686e20d0662b02cc858dfb96bfdc4d4","row_tax":"0.0000","subtotal":"169.0000"},"9137420bc1625f8d54a560b9b7466a37":{"id":"b38421279a9896633f23985aa1321dab","product_id":"571","qty":1,"name":"3Shade\\/Yellow Peach Purple-30106","code":"312-30106","price":45,"tax":"0.00","image":"","option":false,"options":null,"rowid":"9137420bc1625f8d54a560b9b7466a37","row_tax":"0.0000","subtotal":"45.0000"},"87bf2a228d245967add63aa5336225b1":{"id":"b8966f95a20572c80f69a06ac9bdf401","product_id":"571","qty":1,"name":"3Shade\\/Yellow Peach Purple-30106","code":"312-30106","price":45,"tax":"0.00","image":"","option":false,"options":null,"rowid":"87bf2a228d245967add63aa5336225b1","row_tax":"0.0000","subtotal":"45.0000"},"7876a31bd74a5523c27eefee34cbded0":{"id":"8eb5ef109318c68172df8ee509cccd4d","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"7876a31bd74a5523c27eefee34cbded0","row_tax":"0.0000","subtotal":"94.0000"},"2f60d2bcf9fe807b99720e651da19d5d":{"id":"b767e2fa03b92c9d942ff9c7860a6b95","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"2f60d2bcf9fe807b99720e651da19d5d","row_tax":"0.0000","subtotal":"169.0000"},"fa23fc5aa8905614257ad7f12c81e101":{"id":"621934356342d43888e1d5a5aed323cc","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"fa23fc5aa8905614257ad7f12c81e101","row_tax":"0.0000","subtotal":"169.0000"},"0d519c4828c4da1716dae9d2e8a778ae":{"id":"acb6b663af2db1c6dea2145a6b1cb04b","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"0d519c4828c4da1716dae9d2e8a778ae","row_tax":"0.0000","subtotal":"169.0000"},"e8fe99b6f190a6b57faea40b5b47b15b":{"id":"ec66ec8040b00d67050153792ebce966","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"e8fe99b6f190a6b57faea40b5b47b15b","row_tax":"0.0000","subtotal":"169.0000"},"febf24fd9d79fd155dc17837e453e267":{"id":"0e2341e99a13951718947b79a109aac3","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"febf24fd9d79fd155dc17837e453e267","row_tax":"0.0000","subtotal":"169.0000"},"0ab2b56d96e3fa8d183c168f05fed93a":{"id":"8456aca30ac01eca53661eb76fb0e255","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"0ab2b56d96e3fa8d183c168f05fed93a","row_tax":"0.0000","subtotal":"169.0000"},"a9f3ac41b8f0a0e9e7caa5eff9940a81":{"id":"18d14a6cda909bd479513f2c6de6330f","product_id":"53","qty":1,"name":"Chest Dropping Floral Gown with Pearl - Peach","code":"11534520","price":169,"tax":"0.00","image":"19fba35fdb43e5e9e40209a0b5162b02.png","option":false,"options":null,"rowid":"a9f3ac41b8f0a0e9e7caa5eff9940a81","row_tax":"0.0000","subtotal":"169.0000"},"ae6c53c6446f151813107d75edddf369":{"id":"59951fd770571c4c501f9ce7c03423b5","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"ae6c53c6446f151813107d75edddf369","row_tax":"0.0000","subtotal":"169.0000"},"48f16efb5ba2fcd18fa37093aca5a3ee":{"id":"a72ce65df8556c0b3aeb3161ccd3e5e4","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"48f16efb5ba2fcd18fa37093aca5a3ee","row_tax":"0.0000","subtotal":"169.0000"},"f8e1afc0424e67207fad834a69a12556":{"id":"f53dd5a157e2e20d7bd110be0c25dc10","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"f8e1afc0424e67207fad834a69a12556","row_tax":"0.0000","subtotal":"169.0000"},"cdb299d5d2b54869690a4de79c13cd81":{"id":"0dfbc467db4c82b8c5ae70ae545835db","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"cdb299d5d2b54869690a4de79c13cd81","row_tax":"0.0000","subtotal":"94.0000"},"8b7ca7beecacbc917e4f371f8cab4a16":{"id":"b3174a1ca7ea886d4a5657589f47aeab","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"8b7ca7beecacbc917e4f371f8cab4a16","row_tax":"0.0000","subtotal":"94.0000"},"80121d4e09cc41e86d1a84cf56b6a7f8":{"id":"6903e6b8b9df259a7e00694dcc317dd7","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"80121d4e09cc41e86d1a84cf56b6a7f8","row_tax":"0.0000","subtotal":"94.0000"},"eadb79fede8163c70ccd9e0cbbb6d113":{"id":"a64a640a4521163c2e703aca89cd4b95","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"eadb79fede8163c70ccd9e0cbbb6d113","row_tax":"0.0000","subtotal":"94.0000"},"148047cf3acf6ea62033a5dd6c52a637":{"id":"5bbf57fe8babe147031a3ce800b99234","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"148047cf3acf6ea62033a5dd6c52a637","row_tax":"0.0000","subtotal":"169.0000"},"3bf828c34e7dd58948eb24fe27f6a800":{"id":"89c3985077798f36826292d7aa27ff27","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"3bf828c34e7dd58948eb24fe27f6a800","row_tax":"0.0000","subtotal":"169.0000"},"6949785f03a8ccac882f66ac01a0dee4":{"id":"8e47748be3c7c0a609d0be9a9cbc773f","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"6949785f03a8ccac882f66ac01a0dee4","row_tax":"0.0000","subtotal":"169.0000"},"1aa785f4f844c5f3b378564291cc827b":{"id":"b2facc8583d818a96c6c91d7fda28e67","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"1aa785f4f844c5f3b378564291cc827b","row_tax":"0.0000","subtotal":"169.0000"},"6229df56a980844e3d42bd991748dd67":{"id":"f1e3676628b443bb635afea2217aa22d","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"6229df56a980844e3d42bd991748dd67","row_tax":"0.0000","subtotal":"94.0000"},"c1acf9679cc79c9948cf195b39ae8a83":{"id":"bf3d69f2eeba8cdb92bca17069e35575","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"c1acf9679cc79c9948cf195b39ae8a83","row_tax":"0.0000","subtotal":"94.0000"},"9e2310397c5306011987f515246bfea4":{"id":"024dbc86ec56cbf76897f31c38bba94a","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"9e2310397c5306011987f515246bfea4","row_tax":"0.0000","subtotal":"94.0000"},"275947770575566d1d90bce0c5d8044d":{"id":"7ba6e04cb0e4eb775550effb620e19d5","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"275947770575566d1d90bce0c5d8044d","row_tax":"0.0000","subtotal":"94.0000"},"c140d3fae9848714c139c65099a959ab":{"id":"45d18b0ab21e642becb101fe8b7d8c96","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"c140d3fae9848714c139c65099a959ab","row_tax":"0.0000","subtotal":"169.0000"},"429abac043447da532c83a885f933d40":{"id":"a253515459268755f605696661e35e9a","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"429abac043447da532c83a885f933d40","row_tax":"0.0000","subtotal":"169.0000"},"a00fb406cb8cd26fc9b636b4cb3f17fe":{"id":"0de32a0f49d9f8a045085afce8a5192f","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"a00fb406cb8cd26fc9b636b4cb3f17fe","row_tax":"0.0000","subtotal":"94.0000"},"f18c282b3a81b296ebcc2bd10fb23e29":{"id":"bea2d263ff61c8fcc9103c2258137788","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"f18c282b3a81b296ebcc2bd10fb23e29","row_tax":"0.0000","subtotal":"169.0000"},"e68cb3765fb1a7798bacc3880ad9f8c1":{"id":"4886fd34a130cf861b953504f0c09f9e","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"e68cb3765fb1a7798bacc3880ad9f8c1","row_tax":"0.0000","subtotal":"94.0000"}}'),
('c8ee7d91c052c37d6e3927f37789adde', '1513594971', 1, '{"cart_total":50,"total_item_tax":0,"total_items":1,"total_unique_items":1,"f7e165d544f7f1283d1144564475081e":{"id":"9a7481c2e59dbb3d6cd6518704667873","product_id":"700","qty":1,"name":"First custom product","code":"25050462","price":50,"tax":"0.00","image":"dd1035bd154a5272bd9fbc2fd2012dc2.png","option":false,"options":null,"rowid":"f7e165d544f7f1283d1144564475081e","row_tax":"0.0000","subtotal":"50.0000"}}'),
('c93ca14645a8ca046708d4d9221520fd', '1501229445', 1, '{"cart_total":850,"total_item_tax":0,"total_items":5,"total_unique_items":3,"cb7983654bc0578175fa05e6aae03e56":{"id":"53f6a7ee515c0dde3eaf880b09feb831","product_id":"51","qty":1,"name":"Chest Dropping Lace patch Gown with Sequins - Marron","code":"11534518","price":189,"tax":"0.00","image":"1cfbdf40fe2437ffef755c50f5e67410.png","option":false,"options":null,"rowid":"cb7983654bc0578175fa05e6aae03e56","row_tax":"0.0000","subtotal":"189.0000"},"871a2631e7fa167b8448910a696f1d0a":{"id":"102ec6c442577f787f05068314228e13","product_id":"51","qty":3,"name":"Chest Dropping Lace patch Gown with Sequins - Marron","code":"11534518","price":189,"tax":"0.00","image":"1cfbdf40fe2437ffef755c50f5e67410.png","option":false,"options":null,"rowid":"871a2631e7fa167b8448910a696f1d0a","row_tax":"0.0000","subtotal":"567.0000"},"7a2b5fc46a0f63d4067be7cdf7543293":{"id":"d1bf79b72de02ab7b2f4d9a80b535d00","product_id":"193","qty":1,"name":"3 Chest Flower gold tutu","code":"105-5002","price":94,"tax":"0.00","image":"105-5002.jpg","option":false,"options":null,"rowid":"7a2b5fc46a0f63d4067be7cdf7543293","row_tax":"0.0000","subtotal":"94.0000"}}'),
('cddbed2fee65e83c42d9c5b0837c3b72', '1502982775', NULL, '{"cart_total":169,"total_item_tax":0,"total_items":1,"total_unique_items":1,"20551dcbf6d771fd4f8b5746a45d6194":{"id":"eee3258446e61792c66e29702a51806d","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"20551dcbf6d771fd4f8b5746a45d6194","row_tax":"0.0000","subtotal":"169.0000"}}'),
('d01e686394b278bbcade8e10f32df03b', '1523037557', 3, '{"cart_total":95888,"total_item_tax":0,"total_items":4,"total_unique_items":4,"071541487508af87a9ef36202960703e":{"id":"b02afbd268ac876e3fbc3c78ca371a1e","product_id":"719","qty":1,"name":"Ambush (Michael Bennett)","code":"02646516","price":23972,"tax":"0.00","image":"e550a7c7fa66f8242d0734b027380b98.jpg","option":"3","options":[{"id":"3","name":"2-3 years","price":"12.0000","total_quantity":"7.0000","quantity":"7.0000"},{"id":"4","name":"6-7 years","price":"45.0000","total_quantity":"7.0000","quantity":"4.0000"}],"rowid":"071541487508af87a9ef36202960703e","row_tax":"0.0000","subtotal":"23972.0000"},"53bfb67b6ccd61244c0d25924e1fa863":{"id":"533c813b38bc553f939da2ef3560baf9","product_id":"719","qty":1,"name":"Ambush (Michael Bennett)","code":"02646516","price":23972,"tax":"0.00","image":"e550a7c7fa66f8242d0734b027380b98.jpg","option":"3","options":[{"id":"3","name":"2-3 years","price":"12.0000","total_quantity":"7.0000","quantity":"7.0000"},{"id":"4","name":"6-7 years","price":"45.0000","total_quantity":"7.0000","quantity":"4.0000"}],"rowid":"53bfb67b6ccd61244c0d25924e1fa863","row_tax":"0.0000","subtotal":"23972.0000"},"89acd4b308832b01b0211a2474e524c4":{"id":"7e01ced7c51ab4de30d5233a3673fb27","product_id":"719","qty":1,"name":"Ambush (Michael Bennett)","code":"02646516","price":23972,"tax":"0.00","image":"e550a7c7fa66f8242d0734b027380b98.jpg","option":"3","options":[{"id":"3","name":"2-3 years","price":"12.0000","total_quantity":"7.0000","quantity":"7.0000"},{"id":"4","name":"6-7 years","price":"45.0000","total_quantity":"7.0000","quantity":"4.0000"}],"rowid":"89acd4b308832b01b0211a2474e524c4","row_tax":"0.0000","subtotal":"23972.0000"},"c8b33a4bcca8226bf246ceef4d6cb9ab":{"id":"e492c4acd661fa0d9336451eaa3e9b79","product_id":"719","qty":1,"name":"Ambush (Michael Bennett)","code":"02646516","price":23972,"tax":"0.00","image":"e550a7c7fa66f8242d0734b027380b98.jpg","option":"3","options":[{"id":"3","name":"2-3 years","price":"12.0000","total_quantity":"7.0000","quantity":"7.0000"},{"id":"4","name":"6-7 years","price":"45.0000","total_quantity":"7.0000","quantity":"4.0000"}],"rowid":"c8b33a4bcca8226bf246ceef4d6cb9ab","row_tax":"0.0000","subtotal":"23972.0000"}}');
INSERT INTO `sma_cart` (`id`, `time`, `user_id`, `data`) VALUES
('edc4e01712cd9fc95fdd5ba9995fb67a', '1509720028', 1, '{"cart_total":309,"total_item_tax":0,"total_items":3,"total_unique_items":3,"5151682e8bff223dc6551c0078e58054":{"id":"82c4aca96f5df7d17581806337d299ab","product_id":"599","qty":1,"name":"Grey White Lines","code":"202-2050","price":149,"tax":"0.00","image":"","option":false,"options":null,"rowid":"5151682e8bff223dc6551c0078e58054","row_tax":"0.0000","subtotal":"149.0000"},"7dc9b87643459d5296ffd54eb680a468":{"id":"8c843722a9f5b9c37d008e47d0a75ed9","product_id":"599","qty":1,"name":"Grey White Lines","code":"202-2050","price":0,"tax":"0.00","image":"","option":false,"options":null,"rowid":"7dc9b87643459d5296ffd54eb680a468","row_tax":"0.0000","subtotal":"0.0000"},"e992b419b26d6e115d459db3e6354a05":{"id":"4fc63043c0043e94d62810fcfb86a2b5","product_id":"447","qty":1,"name":"Blackstrip Grey 3 Pcs Suit","code":"202-2023","price":160,"tax":"0.00","image":"202-2023.jpg","option":false,"options":null,"rowid":"e992b419b26d6e115d459db3e6354a05","row_tax":"0.0000","subtotal":"160.0000"}}'),
('f57387ebd1ba15871770f98ad6188f8f', '1505908735', NULL, '{"cart_total":972,"total_item_tax":0,"total_items":15,"total_unique_items":15,"652ab608639bef877491f48e85ac677a":{"id":"28f4f79aabb5d472fed7b401effa99aa","product_id":"57","qty":1,"name":"Dropping lace patch, Cape - Pink","code":"11534524","price":169,"tax":"0.00","image":"ad9a2b5c29c9e700d46921cc0fc7a57c.png","option":false,"options":null,"rowid":"652ab608639bef877491f48e85ac677a","row_tax":"0.0000","subtotal":"169.0000"},"a54805b79fca3ffc75a5544ab66775d8":{"id":"662bdcd9168f01cecba097f0db4d3eb6","product_id":"57","qty":1,"name":"Dropping lace patch, Cape - Pink","code":"11534524","price":169,"tax":"0.00","image":"ad9a2b5c29c9e700d46921cc0fc7a57c.png","option":false,"options":null,"rowid":"a54805b79fca3ffc75a5544ab66775d8","row_tax":"0.0000","subtotal":"169.0000"},"9d85dbe065c90d67b8d316a515d658b3":{"id":"ce0e37a21434a44fee0c14dbe2c1a2e8","product_id":"628","qty":1,"name":"Summer Dress with Doll Design","code":"101-1079","price":50,"tax":"0.00","image":"101-1079.jpg","option":false,"options":null,"rowid":"9d85dbe065c90d67b8d316a515d658b3","row_tax":"0.0000","subtotal":"50.0000"},"97b78efc6e870e416cb9f13867067377":{"id":"8d087d0b3418b456354aa348466506ab","product_id":"632","qty":1,"name":"Tweeter Shirt","code":"205-2054","price":45,"tax":"0.00","image":"205-2054.jpg","option":false,"options":null,"rowid":"97b78efc6e870e416cb9f13867067377","row_tax":"0.0000","subtotal":"45.0000"},"d6ab511e31bfb015af1bc12fc9d3b070":{"id":"d793da7903ef84665c0c870ed1671949","product_id":"629","qty":1,"name":"White Black\\/Black Border Pink Belt","code":"101-1080","price":89,"tax":"0.00","image":"101-1080.jpg","option":false,"options":null,"rowid":"d6ab511e31bfb015af1bc12fc9d3b070","row_tax":"0.0000","subtotal":"89.0000"},"9bf5943cc057b0880caef035bcdbab7a":{"id":"493d66639544f1d13b4e885eada81e3a","product_id":"637","qty":1,"name":"Black And White Stripe\\/Stripes","code":"202-2059","price":45,"tax":"0.00","image":"202-2059.jpg","option":false,"options":null,"rowid":"9bf5943cc057b0880caef035bcdbab7a","row_tax":"0.0000","subtotal":"45.0000"},"711dced1362cf71b9142d717cc0c5651":{"id":"48dfc8a82330d969e547e92d92a5db78","product_id":"638","qty":1,"name":"Sailor\\/Blue White Stripes","code":"202-2060","price":45,"tax":"0.00","image":"202-2060.jpg","option":false,"options":null,"rowid":"711dced1362cf71b9142d717cc0c5651","row_tax":"0.0000","subtotal":"45.0000"},"9819e1e9077d2ac7a58f7ae05d6fbf4f":{"id":"7e386e71c37addbcd65c90b67cf448d2","product_id":"633","qty":1,"name":"Minon Shirt","code":"205-2055","price":45,"tax":"0.00","image":"205-2055.jpg","option":false,"options":null,"rowid":"9819e1e9077d2ac7a58f7ae05d6fbf4f","row_tax":"0.0000","subtotal":"45.0000"},"5cd63ee8ca155189d61df1ba09055d9b":{"id":"c62dfb1f4119e533f6d9ec25e1bbe2f0","product_id":"632","qty":1,"name":"Tweeter Shirt","code":"205-2054","price":45,"tax":"0.00","image":"205-2054.jpg","option":false,"options":null,"rowid":"5cd63ee8ca155189d61df1ba09055d9b","row_tax":"0.0000","subtotal":"45.0000"},"892b3e05015647c10214161e889b82a8":{"id":"f11efca5c2808bc8755ea3fbff52a64b","product_id":"632","qty":1,"name":"Tweeter Shirt","code":"205-2054","price":45,"tax":"0.00","image":"205-2054.jpg","option":false,"options":null,"rowid":"892b3e05015647c10214161e889b82a8","row_tax":"0.0000","subtotal":"45.0000"},"8198f1f2b83bb7ce7ffe599c59b59c9a":{"id":"da874cfac34fd6e2d65e0c120faeaa7f","product_id":"636","qty":1,"name":"Pirate Shirt","code":"205-2058","price":45,"tax":"0.00","image":"205-2058.jpg","option":false,"options":null,"rowid":"8198f1f2b83bb7ce7ffe599c59b59c9a","row_tax":"0.0000","subtotal":"45.0000"},"bfe2a88e7f94e5fd63d745c14595c8a5":{"id":"ecf77da3c82a89b579eccc9a9995b593","product_id":"637","qty":1,"name":"Black And White Stripe\\/Stripes","code":"202-2059","price":45,"tax":"0.00","image":"202-2059.jpg","option":false,"options":null,"rowid":"bfe2a88e7f94e5fd63d745c14595c8a5","row_tax":"0.0000","subtotal":"45.0000"},"2bc4a1947235792d5f4a95b13e988e68":{"id":"af9489dbe13236e1d93bf2c91b9ed775","product_id":"637","qty":1,"name":"Black And White Stripe\\/Stripes","code":"202-2059","price":45,"tax":"0.00","image":"202-2059.jpg","option":false,"options":null,"rowid":"2bc4a1947235792d5f4a95b13e988e68","row_tax":"0.0000","subtotal":"45.0000"},"5f0725dec705f17181e7f4c36763bf7c":{"id":"a9d2f3c070843a6cd36541141316fab2","product_id":"637","qty":1,"name":"Black And White Stripe\\/Stripes","code":"202-2059","price":45,"tax":"0.00","image":"202-2059.jpg","option":false,"options":null,"rowid":"5f0725dec705f17181e7f4c36763bf7c","row_tax":"0.0000","subtotal":"45.0000"},"28d5037360db00007b7274e78c043cba":{"id":"4823704641c1591f8d7180ea41269874","product_id":"638","qty":1,"name":"Sailor\\/Blue White Stripes","code":"202-2060","price":45,"tax":"0.00","image":"202-2060.jpg","option":false,"options":null,"rowid":"28d5037360db00007b7274e78c043cba","row_tax":"0.0000","subtotal":"45.0000"}}'),
('fc98aace3e1c9874ceaae25399736add', '1510431854', 1, '{"cart_total":129,"total_item_tax":0,"total_items":2,"total_unique_items":2,"c0df9ee2bcb65dfefca55d938d7b395f":{"id":"7447b12746b6f5a7b9783a3b03204b46","product_id":"238","qty":1,"name":"Next Stock Yellow Bellet","code":"108-8014","price":0,"tax":"0.00","image":"108-8014.jpg","option":false,"options":null,"rowid":"c0df9ee2bcb65dfefca55d938d7b395f","row_tax":"0.0000","subtotal":"0.0000"},"a9552cec78458af1119c4d98cae9ef30":{"id":"119429beb25b1d30b015c7a30079a9f9","product_id":"238","qty":1,"name":"Next Stock Yellow Bellet","code":"108-8014","price":129,"tax":"0.00","image":"108-8014.jpg","option":false,"options":null,"rowid":"a9552cec78458af1119c4d98cae9ef30","row_tax":"0.0000","subtotal":"129.0000"}}'),
('ff3fc776ac1826e25f377d37c9bcf47d', '1505311581', 1, '{"cart_total":5282,"total_item_tax":0,"total_items":53,"total_unique_items":53,"ad618c607086f40e2a05cc046b2993f6":{"id":"de1fac9fb1a70e57aac7d0b081ba99af","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"ad618c607086f40e2a05cc046b2993f6","row_tax":"0.0000","subtotal":"169.0000"},"313cfdefdcad0403b19b54577541b4de":{"id":"f969e96012e8feda73f92e10cfa0fed3","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"313cfdefdcad0403b19b54577541b4de","row_tax":"0.0000","subtotal":"94.0000"},"bf650a8e7159abe3af297d5cf4e7737b":{"id":"91331d4e06c877b9822f27821eaf2223","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"bf650a8e7159abe3af297d5cf4e7737b","row_tax":"0.0000","subtotal":"94.0000"},"53d33a1dfbdf379bbfdaa1f7e2864655":{"id":"fab8913867103796a09000aa6a3835f9","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"53d33a1dfbdf379bbfdaa1f7e2864655","row_tax":"0.0000","subtotal":"94.0000"},"95855543c32c4ddb044a07a56695d08d":{"id":"8c6db8820b942b035b5c21f76c7142d3","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"95855543c32c4ddb044a07a56695d08d","row_tax":"0.0000","subtotal":"94.0000"},"ba225db6028f6df5b6e35185b30e49ad":{"id":"4283557a0b557f06b3342b7441800b74","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"ba225db6028f6df5b6e35185b30e49ad","row_tax":"0.0000","subtotal":"94.0000"},"a6966417eddb02243bbb3e3f0fd56f52":{"id":"e55d5bb3050aad56b1d6125fc648b784","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"a6966417eddb02243bbb3e3f0fd56f52","row_tax":"0.0000","subtotal":"94.0000"},"085b9783e065eccc88e1a80654f51baf":{"id":"18192b4b576325821b813fdaca4973e8","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"085b9783e065eccc88e1a80654f51baf","row_tax":"0.0000","subtotal":"94.0000"},"6bd7b2c020359ce7b7f40334f0fb7bae":{"id":"d5cd98ec17c4d788e2349f8f6588d615","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"6bd7b2c020359ce7b7f40334f0fb7bae","row_tax":"0.0000","subtotal":"169.0000"},"3302defabccf6fd0de0a556e157a68f3":{"id":"20e2648f4be195599b1c2ea7c8a6b045","product_id":"198","qty":1,"name":"3 Flower tutu - Red","code":"105-5007","price":169,"tax":"0.00","image":"105-5007.jpg","option":false,"options":null,"rowid":"3302defabccf6fd0de0a556e157a68f3","row_tax":"0.0000","subtotal":"169.0000"},"e5b757625a6b950379800efb5ba0b8cb":{"id":"8aecd15e403ee55d6ec7c9cb1b5311b3","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"e5b757625a6b950379800efb5ba0b8cb","row_tax":"0.0000","subtotal":"94.0000"},"870391b81526ae514d0134d003b46f03":{"id":"346319c155b241534efc1cc0557363bd","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"870391b81526ae514d0134d003b46f03","row_tax":"0.0000","subtotal":"94.0000"},"1b4bee83661cbd4269df965ac4d49819":{"id":"04dbb9257ce83e2f17dcab7730eec1f8","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"1b4bee83661cbd4269df965ac4d49819","row_tax":"0.0000","subtotal":"94.0000"},"a46ad415e8dd1dd53eb8e77bfe6bd952":{"id":"e8ddc02eda29fd5301d89c8291725c9e","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"a46ad415e8dd1dd53eb8e77bfe6bd952","row_tax":"0.0000","subtotal":"94.0000"},"2f846e266f23afcc33a62e172df5a253":{"id":"e07963cc3f356012605a7a6a05187395","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"2f846e266f23afcc33a62e172df5a253","row_tax":"0.0000","subtotal":"94.0000"},"e5392a1cd9cee367e9dbd365f6c37e7b":{"id":"52893e8db6a38641c5593b3f1834ef4e","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"e5392a1cd9cee367e9dbd365f6c37e7b","row_tax":"0.0000","subtotal":"94.0000"},"6d9cbfb07e3206311072e069276cc8b9":{"id":"dc14e2d762be4bbea67c1b931b6bcb4e","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"6d9cbfb07e3206311072e069276cc8b9","row_tax":"0.0000","subtotal":"94.0000"},"7e9008f2eec38560968b66efae37a9db":{"id":"c977ce80918ac8927da0ce6aa564210c","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"7e9008f2eec38560968b66efae37a9db","row_tax":"0.0000","subtotal":"94.0000"},"b5c1dd2189ce23b70663f7b381378228":{"id":"b2bec0aac5d2859cdd1b97a6071896ae","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"b5c1dd2189ce23b70663f7b381378228","row_tax":"0.0000","subtotal":"94.0000"},"68d33a1c511b4cf5ee87a8554302e5a6":{"id":"5c749403c9323325ae687d28a415e7bc","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"68d33a1c511b4cf5ee87a8554302e5a6","row_tax":"0.0000","subtotal":"94.0000"},"b0f9d1a8d5f43b998d01de69a0152f18":{"id":"cc19c291ecc93c1946ac6be66dc8f2ce","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"b0f9d1a8d5f43b998d01de69a0152f18","row_tax":"0.0000","subtotal":"94.0000"},"40bbc1149da9c5f25e87476aaff7ead2":{"id":"f28a32f1e30768dc62159bf73f5178dc","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"40bbc1149da9c5f25e87476aaff7ead2","row_tax":"0.0000","subtotal":"94.0000"},"e2ab5031bd9f2810c073473cf3e595b4":{"id":"3b6ab9dc8c83f298d321ea1602f0daca","product_id":"203","qty":1,"name":"3 flower tutu - Pink","code":"105-5012","price":169,"tax":"0.00","image":"105-5012.jpg","option":false,"options":null,"rowid":"e2ab5031bd9f2810c073473cf3e595b4","row_tax":"0.0000","subtotal":"169.0000"},"5c17b0cb84c8a9c28d715b30467f8574":{"id":"6fce2d842ef300b2a9e45553e38b8df2","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"5c17b0cb84c8a9c28d715b30467f8574","row_tax":"0.0000","subtotal":"94.0000"},"60fcc4e50265ea1bffc85c9329241f86":{"id":"eb3c2eb9d331711d64cc71f55e53b963","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"60fcc4e50265ea1bffc85c9329241f86","row_tax":"0.0000","subtotal":"94.0000"},"6a00bc8e25d7d428f47b1ed69cceeef2":{"id":"3b48c21a4e3aa6ad3c48bcc23240b8e2","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"6a00bc8e25d7d428f47b1ed69cceeef2","row_tax":"0.0000","subtotal":"94.0000"},"b83cb6e23e7a6f5734a1ddb4c50c4b30":{"id":"63762a49eda7e12eb8deaffc4c820565","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"b83cb6e23e7a6f5734a1ddb4c50c4b30","row_tax":"0.0000","subtotal":"94.0000"},"d798143fad56ef8373a6e8f114499778":{"id":"d08bca0bdd83a9948696765ac2736b83","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"d798143fad56ef8373a6e8f114499778","row_tax":"0.0000","subtotal":"94.0000"},"18468294542aa4ea1fd6797cc834edc9":{"id":"54de2ed768dcc9145f010d8bb8198edd","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"18468294542aa4ea1fd6797cc834edc9","row_tax":"0.0000","subtotal":"94.0000"},"4d55467ac602cd0c91f5229b47ea90ba":{"id":"28e4dc95a452f513197cf375387acfcb","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"4d55467ac602cd0c91f5229b47ea90ba","row_tax":"0.0000","subtotal":"94.0000"},"8f86283fa91356170da7317eb87a8b8b":{"id":"3fe654c8086a47ee2a4255d2c34d1935","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"8f86283fa91356170da7317eb87a8b8b","row_tax":"0.0000","subtotal":"94.0000"},"7d232f2b7defec49a5da48540b183120":{"id":"ecdc410b0c40d22f029fc7424e1393d9","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"7d232f2b7defec49a5da48540b183120","row_tax":"0.0000","subtotal":"94.0000"},"62932b396d76b0995c3aa087dee4d10b":{"id":"7a857117093341807e3102552ae3b0a0","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"62932b396d76b0995c3aa087dee4d10b","row_tax":"0.0000","subtotal":"94.0000"},"4e91cb844a3d3cbbd2b30368dbff9bfb":{"id":"8d61751bcddd0880814a355a7a8063ce","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"4e91cb844a3d3cbbd2b30368dbff9bfb","row_tax":"0.0000","subtotal":"94.0000"},"bfa9b37bfc0e44d1f1626d8536b556fb":{"id":"614d352871a2d892d6d868d815733ab6","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"bfa9b37bfc0e44d1f1626d8536b556fb","row_tax":"0.0000","subtotal":"94.0000"},"2558a77b15658b4f7c6974af3344374f":{"id":"387ca7f6baaa02e6888189523cddd327","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"2558a77b15658b4f7c6974af3344374f","row_tax":"0.0000","subtotal":"94.0000"},"266f38f032203b273be0045689cb4dd6":{"id":"d668fd2311153e35ba1ed3c55edd88fc","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"266f38f032203b273be0045689cb4dd6","row_tax":"0.0000","subtotal":"94.0000"},"46ad093d1b8b47b3287e5a263832948a":{"id":"240de84e035d6d1e8dd09f3545bc316f","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"46ad093d1b8b47b3287e5a263832948a","row_tax":"0.0000","subtotal":"94.0000"},"509a759946e9745bea36651bd0a4c5f1":{"id":"0fc87d1437aa7e9cdfd0514fe00d5471","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"509a759946e9745bea36651bd0a4c5f1","row_tax":"0.0000","subtotal":"94.0000"},"7af520fb21430a988c023edbcac5cde9":{"id":"ab008342d39ec4fe30e19165939b5d0f","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"7af520fb21430a988c023edbcac5cde9","row_tax":"0.0000","subtotal":"94.0000"},"4cfa9ca4b170bab547082c1b1ed1aa8b":{"id":"f87dba72a9c63383d5cc77ecd10444a3","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"4cfa9ca4b170bab547082c1b1ed1aa8b","row_tax":"0.0000","subtotal":"94.0000"},"91699f8045af2fef4ae159a9c4fa1ef6":{"id":"6043916107fd379c6422f9e7db3fa2c1","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"91699f8045af2fef4ae159a9c4fa1ef6","row_tax":"0.0000","subtotal":"94.0000"},"ee958253bb5015198aafd9bfb27effeb":{"id":"42ba15c91007413d336d956df7cf1d77","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"ee958253bb5015198aafd9bfb27effeb","row_tax":"0.0000","subtotal":"94.0000"},"5fecc7651b2d19c37bd568f05b766f22":{"id":"42be84754a3c4f94486d48d2b62e8382","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"5fecc7651b2d19c37bd568f05b766f22","row_tax":"0.0000","subtotal":"94.0000"},"de03a6911c20259ca761cae2fcdf0b37":{"id":"e041e7971d09277647dc22ec0516edd0","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"de03a6911c20259ca761cae2fcdf0b37","row_tax":"0.0000","subtotal":"94.0000"},"191d03ae4f4c89e9d72246031a43e4f3":{"id":"e9ec737308baf4189ad383afa5bc536c","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"191d03ae4f4c89e9d72246031a43e4f3","row_tax":"0.0000","subtotal":"94.0000"},"03d4f5d9990ccd36de80e8d06d37d9a4":{"id":"c4e0eae31b42dccdb7850cce82a4a127","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"03d4f5d9990ccd36de80e8d06d37d9a4","row_tax":"0.0000","subtotal":"94.0000"},"51f489220399c4818606e9437f2117f3":{"id":"57737fce7981d83f733b2bb8c59cabae","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"51f489220399c4818606e9437f2117f3","row_tax":"0.0000","subtotal":"94.0000"},"6a208701dec38987d60bec9ae2009e8e":{"id":"6a21b82b72d81ead62d7c1ec09677bb6","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"6a208701dec38987d60bec9ae2009e8e","row_tax":"0.0000","subtotal":"94.0000"},"a4feb7d7eba0de102b045f38dd9cfb96":{"id":"eb3740d5e37bb02913b9dda8f0ae4743","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"a4feb7d7eba0de102b045f38dd9cfb96","row_tax":"0.0000","subtotal":"94.0000"},"4c21dc13e0b0fcd21d336134bd94f9bf":{"id":"af0ec06f9c7a7ce58813fa541198ccbb","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"4c21dc13e0b0fcd21d336134bd94f9bf","row_tax":"0.0000","subtotal":"94.0000"},"7c22cde6440b7ef4557923b2931d6579":{"id":"c6300bafb9858aa41a6fbffb5cd6d6fa","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"7c22cde6440b7ef4557923b2931d6579","row_tax":"0.0000","subtotal":"94.0000"},"4baaaad3d3794ea7bd13a90827951b85":{"id":"43c5df60efc4163fb3d3d8ba2b108df7","product_id":"202","qty":1,"name":"Black classic tutu","code":"105-5011","price":94,"tax":"0.00","image":"105-5011.jpg","option":false,"options":null,"rowid":"4baaaad3d3794ea7bd13a90827951b85","row_tax":"0.0000","subtotal":"94.0000"}}');

-- --------------------------------------------------------

--
-- Table structure for table `sma_categories`
--

CREATE TABLE `sma_categories` (
  `id` int(11) NOT NULL,
  `code` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  `translated_category_name` varchar(255) DEFAULT NULL,
  `image` varchar(55) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `category_carousel` int(11) NOT NULL DEFAULT '0',
  `is_updated` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_categories`
--

INSERT INTO `sma_categories` (`id`, `code`, `name`, `translated_category_name`, `image`, `parent_id`, `slug`, `category_carousel`, `is_updated`) VALUES
(39, '101', 'Dane Series', 'داين سلسلة', NULL, 0, 'dane-series', 0, 1),
(40, '102', 'Drama', 'دراما', NULL, 0, 'drama', 0, 1),
(41, '103', 'Satire', 'هجاء', NULL, 0, 'satire', 0, 1),
(42, '104', 'Science fiction', 'الخيال العلمي', NULL, 0, 'science-fiction', 0, 1),
(43, '105', 'Action and Adventure', 'الحركة والمغامرة', NULL, 0, 'action-and-adventure', 0, 1),
(44, '106', 'Mystery', 'الغموض', NULL, 0, 'mystery', 0, 1),
(45, '107', 'Horror', 'رعب', NULL, 0, 'horror', 0, 1),
(46, '108', 'Self help', 'مساعدة ذاتية', NULL, 0, 'self-help', 0, 1),
(47, '109', 'Fake path', 'طريق مزيف', NULL, 0, 'fake-path', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sma_cod`
--

CREATE TABLE `sma_cod` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT 'COD',
  `account_email` varchar(255) NOT NULL DEFAULT 'wisdom@moneybookers.com',
  `secret_word` varchar(20) NOT NULL DEFAULT 'mbtest',
  `cod_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_cod`
--

INSERT INTO `sma_cod` (`id`, `active`, `name`, `account_email`, `secret_word`, `cod_currency`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(3, 1, 'COD', 'wisdom@moneybookers.com', 'mbtest', 'AED', '50.0000', '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_combo_items`
--

CREATE TABLE `sma_combo_items` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `item_code` varchar(20) NOT NULL,
  `quantity` decimal(12,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_companies`
--

CREATE TABLE `sma_companies` (
  `id` int(11) NOT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `group_name` varchar(20) NOT NULL,
  `customer_group_id` int(11) DEFAULT NULL,
  `customer_group_name` varchar(100) DEFAULT NULL,
  `name` varchar(55) NOT NULL,
  `company` varchar(255) NOT NULL,
  `vat_no` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(55) DEFAULT NULL,
  `state` varchar(55) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `cf1` varchar(100) DEFAULT NULL,
  `cf2` varchar(100) DEFAULT NULL,
  `cf3` varchar(100) DEFAULT NULL,
  `cf4` varchar(100) DEFAULT NULL,
  `cf5` varchar(100) DEFAULT NULL,
  `cf6` varchar(100) DEFAULT NULL,
  `invoice_footer` text,
  `payment_term` int(11) DEFAULT '0',
  `logo` varchar(255) DEFAULT 'logo.png',
  `award_points` int(11) DEFAULT '0',
  `deposit_amount` decimal(25,4) DEFAULT NULL,
  `price_group_id` int(11) DEFAULT NULL,
  `price_group_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_companies`
--

INSERT INTO `sma_companies` (`id`, `group_id`, `group_name`, `customer_group_id`, `customer_group_name`, `name`, `company`, `vat_no`, `address`, `city`, `state`, `postal_code`, `country`, `phone`, `email`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `invoice_footer`, `payment_term`, `logo`, `award_points`, `deposit_amount`, `price_group_id`, `price_group_name`) VALUES
(1, 3, 'customer', 1, 'General', 'Walk-in Customer', 'Walk-in Customer', '', 'Customer Address', 'Petaling Jaya', 'Selangor', '46000', 'Malaysia', '0123456789', 'customer@tecdiary.com', '', '', '', '', '', '', NULL, 0, 'logo.png', 4303, NULL, NULL, NULL),
(2, 4, 'supplier', NULL, NULL, 'Yiwu Polyster Scarfs and Capes', 'Yiwu Polyster Scarfs and Capes', '', 'Supplier Address', 'Petaling Jaya', 'Selangor', '46050', 'Malaysia', '0123456789', 'supplier@tecdiary.com', '-', '-', '-', '-', '-', '-', NULL, 0, 'logo.png', 0, NULL, NULL, NULL),
(3, NULL, 'biller', NULL, NULL, 'Mian Saleem', 'Test Biller', '5555', 'Biller adddress', 'City', '', '', 'Country', '012345678', 'saleem@tecdiary.com', '', '', '', '', '', '', ' Thank you for shopping with us. Please come again', 0, 'logo1.png', 0, NULL, NULL, NULL),
(4, 4, 'supplier', NULL, NULL, 'Sophia Agent', 'Sophia Agent Market Order', '', 'Market Place, ', 'Market', '', '', '', '23434343232323', 'sdfdfds@dfdf.com', '', '', '', '', '', '', NULL, 0, 'logo.png', 0, NULL, NULL, NULL),
(5, 3, 'customer', 4, 'New Customer (+10)', 'Walk-in Outlet Mall ', 'Walk-in Outlet Mall ', '', 'Dubai Outlet Mall', 'Dubai', '', '', '', '2323232323', 'sdsdssdds@gmail.com', '', '', '', '', '', '', NULL, 0, 'logo.png', -201, NULL, 1, 'Default'),
(6, 3, 'customer', 1, 'General', 'Walk-in DFC', 'Walk-in DFC', '', 'Dubai Festival City ', 'Dubai', 'Dubayy', '', 'United Arab Emirates', '3232445232', 'weeeeeferf@dfdf.com', '', '', '', '', '', '', NULL, 0, 'logo.png', -64, NULL, 1, 'Default'),
(7, 4, 'supplier', NULL, NULL, 'Cherry Pettiskirts ', 'Yiwu Rand - Kapu Petti Skirts ', '', 'Dubai', 'Dubai', 'Dubayy', '', 'United Arab Emirates', '32424532', 'qwewweqeqwewf@wsdsd.com', '', '', '', '', '', '', NULL, 0, 'logo.png', 0, NULL, NULL, NULL),
(8, 4, 'supplier', NULL, NULL, 'Iris Elsa ', 'Iris Elsa Tutu Factory', '', 'Dubai', 'Dubai', 'Dubayy', '', 'United Arab Emirates', '501560472', 'ssdsd@dww.com', '', '', '', '+971501560472', '', '', NULL, 0, 'logo.png', 0, NULL, NULL, NULL),
(9, 3, 'customer', 1, 'General', 'Ken Edgar', 'fit panter', NULL, NULL, NULL, NULL, NULL, NULL, '00526785105', 'ummarakram@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 1, 'Default'),
(10, 3, 'customer', 1, 'General', 'umar Akram', 'Keln', NULL, NULL, NULL, NULL, NULL, NULL, '3424248166', 'ummarakram@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 1, 'Default'),
(11, 3, 'customer', 1, 'General', 'Waseem Akram', 'anc', NULL, NULL, NULL, NULL, NULL, NULL, '03424248255', 'waheed1553@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', -230, NULL, 1, 'Default'),
(12, 3, 'customer', 1, 'General', 'Ali Murazta', 'wisdom', NULL, NULL, NULL, NULL, NULL, NULL, '0342424812666', 'ali@murtaza.net', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 120, NULL, 1, 'Default'),
(13, 3, 'customer', 1, 'General', 'akash ahmed', 'wisdom', NULL, NULL, NULL, NULL, NULL, NULL, '2132324234', 'akashahmed@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 1, 'Default');

-- --------------------------------------------------------

--
-- Table structure for table `sma_costing`
--

CREATE TABLE `sma_costing` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sale_item_id` int(11) NOT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `purchase_item_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `purchase_net_unit_cost` decimal(25,4) DEFAULT NULL,
  `purchase_unit_cost` decimal(25,4) DEFAULT NULL,
  `sale_net_unit_price` decimal(25,4) NOT NULL,
  `sale_unit_price` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT NULL,
  `inventory` tinyint(1) DEFAULT '0',
  `overselling` tinyint(1) DEFAULT '0',
  `option_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_costing`
--

INSERT INTO `sma_costing` (`id`, `date`, `product_id`, `sale_item_id`, `sale_id`, `purchase_item_id`, `quantity`, `purchase_net_unit_cost`, `purchase_unit_cost`, `sale_net_unit_price`, `sale_unit_price`, `quantity_balance`, `inventory`, `overselling`, `option_id`) VALUES
(1, '2018-04-05', 749, 1, 1, 961, '1.0000', '350.0000', '350.0000', '604.0000', '604.0000', '3.0000', 1, 0, 1),
(2, '2018-04-06', 719, 4, 3, NULL, '1.0000', '350.0000', '350.0000', '23960.0000', '23960.0000', NULL, 1, 1, NULL),
(3, '2018-04-11', 751, 15, 11, NULL, '1.0000', '350.0000', '350.0000', '599.0000', '599.0000', NULL, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_currencies`
--

CREATE TABLE `sma_currencies` (
  `id` int(11) NOT NULL,
  `code` varchar(5) NOT NULL,
  `name` varchar(55) NOT NULL,
  `rate` decimal(12,4) NOT NULL,
  `auto_update` tinyint(1) NOT NULL DEFAULT '0',
  `symbol` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_currencies`
--

INSERT INTO `sma_currencies` (`id`, `code`, `name`, `rate`, `auto_update`, `symbol`) VALUES
(4, 'AED', 'UAE Dhs', '1.0000', 0, NULL),
(5, 'درهم', 'dhs', '1.0000', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_customer_groups`
--

CREATE TABLE `sma_customer_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `percent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_customer_groups`
--

INSERT INTO `sma_customer_groups` (`id`, `name`, `percent`) VALUES
(1, 'General', 0),
(2, 'Reseller', -5),
(3, 'Distributor', -15),
(4, 'New Customer (+10)', 10);

-- --------------------------------------------------------

--
-- Table structure for table `sma_date_format`
--

CREATE TABLE `sma_date_format` (
  `id` int(11) NOT NULL,
  `js` varchar(20) NOT NULL,
  `php` varchar(20) NOT NULL,
  `sql` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_date_format`
--

INSERT INTO `sma_date_format` (`id`, `js`, `php`, `sql`) VALUES
(1, 'mm-dd-yyyy', 'm-d-Y', '%m-%d-%Y'),
(2, 'mm/dd/yyyy', 'm/d/Y', '%m/%d/%Y'),
(3, 'mm.dd.yyyy', 'm.d.Y', '%m.%d.%Y'),
(4, 'dd-mm-yyyy', 'd-m-Y', '%d-%m-%Y'),
(5, 'dd/mm/yyyy', 'd/m/Y', '%d/%m/%Y'),
(6, 'dd.mm.yyyy', 'd.m.Y', '%d.%m.%Y');

-- --------------------------------------------------------

--
-- Table structure for table `sma_deliveries`
--

CREATE TABLE `sma_deliveries` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_id` int(11) NOT NULL,
  `do_reference_no` varchar(50) NOT NULL,
  `sale_reference_no` varchar(50) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `attachment` varchar(50) DEFAULT NULL,
  `delivered_by` varchar(50) DEFAULT NULL,
  `received_by` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_deliveries`
--

INSERT INTO `sma_deliveries` (`id`, `date`, `sale_id`, `do_reference_no`, `sale_reference_no`, `customer`, `address`, `note`, `status`, `attachment`, `delivered_by`, `received_by`, `created_by`, `updated_by`, `updated_at`) VALUES
(1, '2017-11-16 22:42:00', 23, 'DO/2017/11/0001', 'SALE/2017/11/0001', 'Waseem Akram', '<p>Lorem ipsum<br>Loremm<br>lahore lahore<br>3800 pakisa<br>Tel: 0342424852</p>', '', 'delivered', NULL, 'Umar ', 'Wasim', 1, NULL, NULL),
(2, '2017-12-19 07:56:00', 31, 'DO2017/12/0001', 'SALE2017/12/0002', 'Waseem Akram', '<p>Lorem ipsum<br>Loremm<br>lahore lahore<br>3800 pakisa<br>Tel: 0342424852</p>', '&lt;p&gt;Its done&period;&lt;&sol;p&gt;', 'delivered', NULL, '', 'Nasir ali', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_deposits`
--

CREATE TABLE `sma_deposits` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `amount` decimal(25,4) NOT NULL,
  `paid_by` varchar(50) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_expenses`
--

CREATE TABLE `sma_expenses` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference` varchar(50) NOT NULL,
  `amount` decimal(25,4) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_by` varchar(55) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_expense_categories`
--

CREATE TABLE `sma_expense_categories` (
  `id` int(11) NOT NULL,
  `code` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_gift_cards`
--

CREATE TABLE `sma_gift_cards` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `card_no` varchar(20) NOT NULL,
  `value` decimal(25,4) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `balance` decimal(25,4) NOT NULL,
  `expiry` date DEFAULT NULL,
  `created_by` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_gift_cards`
--

INSERT INTO `sma_gift_cards` (`id`, `date`, `card_no`, `value`, `customer_id`, `customer`, `balance`, `expiry`, `created_by`) VALUES
(1, '2018-04-05 08:21:11', '5878422801151142', '52.0000', 12, 'wisdom', '0.0000', '2020-04-09', '1'),
(2, '2018-04-05 11:19:35', '12345678', '25.0000', 12, 'wisdom', '0.0000', '2020-04-07', '1'),
(3, '2018-04-06 11:28:11', '142', '31.0000', 1, 'Walk-in Customer', '0.0000', '2020-04-08', '1'),
(4, '2018-04-06 11:28:38', '143', '20.0000', 12, 'wisdom', '0.0000', '2020-04-10', '1'),
(5, '2018-04-06 12:23:20', '2222', '40.0000', 12, 'wisdom', '0.0000', '2020-04-07', '1'),
(6, '2018-04-06 12:46:44', '7777', '24000.0000', 12, 'wisdom', '0.0000', '2020-04-09', '1'),
(7, '2018-04-06 15:39:46', '8888', '24000.0000', 12, 'wisdom', '40.0000', '2020-04-16', '1');

-- --------------------------------------------------------

--
-- Table structure for table `sma_gift_card_topups`
--

CREATE TABLE `sma_gift_card_topups` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `card_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_groups`
--

CREATE TABLE `sma_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_groups`
--

INSERT INTO `sma_groups` (`id`, `name`, `description`) VALUES
(1, 'owner', 'Owner'),
(2, 'admin', 'Administrator'),
(3, 'customer', 'Customer'),
(4, 'supplier', 'Supplier'),
(8, 'manager', 'Its manager team. To manage the website.');

-- --------------------------------------------------------

--
-- Table structure for table `sma_landing_page_block`
--

CREATE TABLE `sma_landing_page_block` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `number_of_products` int(11) NOT NULL,
  `order_number` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sma_landing_page_block`
--

INSERT INTO `sma_landing_page_block` (`id`, `title`, `number_of_products`, `order_number`, `status`, `updated_at`) VALUES
(27, 'Top Selling Books', 5, 1, 1, '2018-03-21 08:07:46'),
(28, 'Most Visited Books', 5, 2, 1, '2018-03-21 08:32:37');

-- --------------------------------------------------------

--
-- Table structure for table `sma_landing_page_products`
--

CREATE TABLE `sma_landing_page_products` (
  `id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sma_landing_page_products`
--

INSERT INTO `sma_landing_page_products` (`id`, `block_id`, `product_id`) VALUES
(8, 21, 71),
(14, 21, 1),
(15, 21, 443),
(16, 21, 109),
(17, 21, 51),
(18, 21, 52),
(19, 21, 53),
(20, 21, 96),
(26, 23, 1),
(27, 23, 94),
(35, 24, 139),
(41, 22, 50),
(42, 22, 56),
(43, 22, 70),
(44, 22, 89),
(45, 22, 90),
(46, 25, 1),
(47, 25, 52),
(48, 25, 89),
(49, 25, 95),
(50, 25, 612),
(51, 18, 50),
(52, 18, 53),
(53, 18, 55),
(54, 18, 57),
(55, 18, 61),
(56, 18, 89),
(57, 18, 95),
(58, 18, 96),
(59, 18, 618),
(60, 26, 460),
(61, 26, 462),
(62, 26, 491),
(63, 26, 498),
(64, 26, 580),
(65, 27, 710),
(66, 27, 711),
(67, 27, 712),
(68, 27, 713),
(69, 27, 714),
(70, 28, 715),
(71, 28, 716),
(72, 28, 717),
(73, 28, 718),
(74, 28, 719);

-- --------------------------------------------------------

--
-- Table structure for table `sma_login_attempts`
--

CREATE TABLE `sma_login_attempts` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_login_attempts`
--

INSERT INTO `sma_login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(1, 0x3a3a31, 'khuram@wiztech.bz', 1523865132),
(2, 0x3a3a31, 'khuram@wistech.iz', 1523865593);

-- --------------------------------------------------------

--
-- Table structure for table `sma_migrations`
--

CREATE TABLE `sma_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_migrations`
--

INSERT INTO `sma_migrations` (`version`) VALUES
(316);

-- --------------------------------------------------------

--
-- Table structure for table `sma_notifications`
--

CREATE TABLE `sma_notifications` (
  `id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `from_date` datetime DEFAULT NULL,
  `till_date` datetime DEFAULT NULL,
  `scope` tinyint(1) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_notifications`
--

INSERT INTO `sma_notifications` (`id`, `comment`, `date`, `from_date`, `till_date`, `scope`) VALUES
(2, '<p>Hello Omar how are you?</p>', '2017-10-26 11:02:14', '2017-10-25 16:00:00', '2017-10-31 16:00:00', 3);

-- --------------------------------------------------------

--
-- Table structure for table `sma_order_ref`
--

CREATE TABLE `sma_order_ref` (
  `ref_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `so` int(11) NOT NULL DEFAULT '1',
  `qu` int(11) NOT NULL DEFAULT '1',
  `po` int(11) NOT NULL DEFAULT '1',
  `to` int(11) NOT NULL DEFAULT '1',
  `pos` int(11) NOT NULL DEFAULT '1',
  `do` int(11) NOT NULL DEFAULT '1',
  `pay` int(11) NOT NULL DEFAULT '1',
  `re` int(11) NOT NULL DEFAULT '1',
  `rep` int(11) NOT NULL DEFAULT '1',
  `ex` int(11) NOT NULL DEFAULT '1',
  `ppay` int(11) NOT NULL DEFAULT '1',
  `qa` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_order_ref`
--

INSERT INTO `sma_order_ref` (`ref_id`, `date`, `so`, `qu`, `po`, `to`, `pos`, `do`, `pay`, `re`, `rep`, `ex`, `ppay`, `qa`) VALUES
(1, '2017-12-01', 16, 1, 1, 1, 7, 2, 16, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sma_pages`
--

CREATE TABLE `sma_pages` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `title` varchar(60) NOT NULL,
  `description` varchar(180) NOT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `body` text NOT NULL,
  `active` tinyint(1) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_no` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_pages`
--

INSERT INTO `sma_pages` (`id`, `name`, `title`, `description`, `slug`, `body`, `active`, `updated_at`, `order_no`) VALUES
(1, 'My Style Preference', 'My Style Preference', 'Lorem ipsum dollar smtih.', 'my-style-preference', '<p><p>Lorem ipsum dollar Smith.</p></p>', 1, '2017-08-01 09:02:35', 1),
(3, 'Support', 'Support', 'Lorem ipsum dollar smtih.', 'support', '<p>Lorem ipsum dollar smtih.Lorem ipsum dollar smtih.</p>', 1, '2017-08-01 08:01:59', 2),
(4, 'Download App', 'Download App', 'Lorem ipsum dollar smtih.', 'download-app', '<p>Lorem ipsum dollar smtih.</p>', 1, '2017-08-01 08:03:39', 3),
(5, 'Landing Page', 'Landing Page Text', 'Landing Page Text', 'landing-page-text', '<p><p><h2>Shop for Stunning Sarees Online</h2><p>You can never go wrong with <a href="https://www.voonik.com/women-clothing/women-sarees">Sarees</a>. These timeless classics will give your fashion a whole new dimension. Be it a glamorous evening spent with friends or some quality time shared with family, these gorgeous drapes will make you the centre of all attention. There is always a wide variety to choose from, be it sheer chiffon sarees or classic cotton weaves, any style can take your everyday look a notch higher. And the biggest good news is you can get your hands on these vibrant beauties right from your couch, all thanks to online shopping! So without further ado, add these lovelies to your wardrobe!</p><p><section></section></p><div><section><h4>Beautiful Dresses at Best Prices</h4><p>Ever wondered how to bring out that girlie side of you? Well, fret not as <a href="https://www.voonik.com/women-clothing/dresses">dresses</a> can be your ideal partner as you gear up for the pretty parade. Take your pick from fit and flares, body cons, floral maxis, playful skater dresses and more to put together a chic look. From college fresher parties to those Saturday night-outs, these versatile oh-so-girly pieces will give you a glam makeover in no time. And with online shopping searching for pretty dresses is now a piece of cake. It’s never too late to play dress up, so get started now!</p><p><strong>Kurtis</strong></p><p>This has always been a wardrobe favourite. Elegant styles, minimal designs and ultimate comfort makes this tailor-made piece an everyday staple. This traditional piece of clothing will spoil you with it’s varieties, from it’s straight-cut styles to anarkalis to glamorous suits suited for all occasions. You can turn to floral kurtas for a touch of romance or Aztec printed basics for some old-world charm. Take a cue from evergreen solid styles and look simple but sophisticated in no time. With designs that will never fail to flatter you, kurtas can up your fashion game at a moment’s notice. At Voonik, we bring you the best of the styles at jaw dropping prices. So sit back, relax and let your perfect <a href="https://www.voonik.com/women-clothing/kurtas-kurtis">Kurtis buy online</a> find you, through us!</p><p><strong>Lehengas</strong></p><p>Weddings are all about fun, frolic and of course, FASHION! Wedding galas are incomplete without shimmery <a href="https://www.voonik.com/women-clothing/lehengas">lehengas</a>, ravishing sarees and sparkling jewellery. When all the women are busy looking their best, luxurious lehengas will make it easier for you to make a fashion statement. With exquisite embroidery, fabulous fabrics and vibrant shades, putting together a royal look was never this easy. At Voonik, we bring you collections that will blow your mind. If you are looking forward to looking like a diva, lehengas are the secret you need to know. So get prepared for gushing compliments and envious looks as you take the world by storm on the D-day!</p><p><strong>Jeans</strong></p><p>It’s time you accepted that jeans are your best friend and your ultimate saviour on any bad hair day. Be it your favourite top or the kurti you haven''t worn in ages, a pair of <a href="https://www.voonik.com/women-clothing/women-jeans">jeans</a> with the perfect fit can enliven any of your looks. With fashion taking bigger leaps every season, Jeans have diversified into cult varieties. Be it the boot-cut basics, flared boyfriend jeans or the ultra-slim ripped ones, a pair of denims is something you can never go wrong with. The denim fever will always be in fashion and you can join the herd with classic styles that top in trend and comfort. So why wait? Get your perfect pair right away!</p><p><strong>Tops & tunics</strong></p><p>Now here’s something to make you feel on “top” of the world. The quickest way to slip in and out of looks is to have some smart top wear that go with all your favourite bottoms. An exhaustive array of styles, colours and designs will keep you busy till you go berserk trying to select one! But don''t stop there, because these basic beauties can give you an edge anytime and let you look bright all day. To pamper you a little more, Voonik makes it easier for you with drool-worthy styles right at your fingertips. On top of that, there are killer deals and unlimited options! If you still don''t believe us, just log in and <a href="https://www.voonik.com/women-clothing/women-tops-tunics">buy tops and tunics online</a>!</p><p><strong>Shirts</strong></p><p>Who said shirts only belong to formal parties or a boring day in office? Preppy prints and crisp styles has taken this piece of western-wear to the top of the must-have list. Pair them with jeans or go for cropped styles with palazzos, your options are really endless. Moreover, never seen before colours and tailor-made cuts will ensure you look your best 24/7. You can never have enough of this “it” piece as it takes you from girly to grown-up in no time. Add to that awesome comfort and sober designs and you have a winner! If you are curious about where to find the shirt that suits you, well, it’s right in front of you! Online shopping will throw up a plethora of options for you and make you look all prim and proper in a jiffy. Happy shirt-ing!</p><p>Before you think that’s all that we have in store for you, we would like to share our collection of belts,scarves,watches,gadgets,<wbr>sunglasses, socks and hair accessories that can refurbish your wardrobe! So go on, log in to Voonik for some refreshing retail therapy. We promise you wont be disappointed. Happy shopping!</p></section></div></p></p>', 0, '2017-08-02 08:37:37', 6),
(6, 'Contact Us', 'Contact Us', 'Contact', 'contact-us', '<h3>Our Location</h3>\r\n<div class="panel panel-default">\r\n        <div class="panel-body">\r\n          <div class="row">\r\n                        <div class="col-sm-3"><strong>Enso</strong><br>\r\n              <address>\r\n              Dubai<br>\r\n<br>\r\nUnited Arab Emirates              </address>\r\n                            <a href="https://maps.google.com/maps?q=51.5032131,-0.1278824&hl=en-gb&t=m&z=15" target="_blank" class="btn btn-info"><i class="fa fa-map-marker"></i> View Google Map</a>\r\n                          </div>\r\n            <div class="col-sm-3"><strong>Telephone</strong><br>\r\n              +971 4 881 1911<br>\r\n              <br>\r\n                            <strong>Fax</strong><br>\r\n              +971 4 881 1912                          </div>\r\n            <div class="col-sm-3">\r\n                                        </div>\r\n          </div>\r\n        </div>\r\n      </div>', 0, '2018-04-01 11:06:50', 6),
(7, 'Shipping Policy', 'Shipping Policy', 'Shipping Policy', 'shipping-policy', '<p>Shipping PolicyShipping PolicyShipping PolicyShipping PolicyShipping PolicyShipping PolicyShipping Policy</p>', 0, '2017-11-25 12:09:34', 20),
(8, 'our history', 'Our History', 'Ensō, a term from Japanese aesthetics, symbolizes strength, clarity, elegance, and enlightenment.', 'our-history', '<div><h1>Our Story</h1></div><p>Ensō, a term from Japanese aesthetics, symbolizes strength, clarity, elegance, and enlightenment. These qualities shape our approach towards the production process, supply-chain, and customer service. By working closely with customers, selecting strong partners, and tapping into the knowledge base of our textile network, we ensure a commitment to transparency, reliability, and fine products. Ensō, a term from Japanese aesthetics, symbolizes strength, clarity, elegance, and enlightenment. These qualities shape our approach towards the production process, supply-chain, and customer service. By working closely with customers, selecting strong partners, and tapping into the knowledge base of our textile network, we ensure a commitment to transparency, reliability, and fine products.</p>', 0, '2018-03-31 14:02:34', 9),
(9, 'House coll', 'House Collection', 'Lorem ipsum dolor sit amet', 'house-collection', '<p class="col-lg-6 col-xs-12">\r\n <img src="http://localhost/wisdom_pos/assets/uploads/home-img-1.jpg" class="img-responsive">\r\n</p>\r\n<p class="col-lg-6 col-xs-12">\r\n</p>\r\n<h3>House Collection</h3>\r\n<p>\r\n                                             Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.\r\n</p>', 0, '2018-03-31 15:45:22', 10),
(10, 'About Us', 'About Us', '​Bacon ipsum dolor amet salami spare ribs meatball ball tip venison buffalo pork doner', 'about-us', '<p>Bacon ipsum dolor amet salami spare ribs meatball ball tip venison buffalo pork doner sirloin kielbasa ribeye turducken meatloaf. Cow drumstick chuck tongue picanha. Kevin frankfurter jowl spare ribs pastrami filet mignon kielbasa drumstick capicola. Ham hock chuck tenderloin tail ham sirloin. Pastrami corned beef drumstick, doner landjaeger venison hamburger spare ribs.</p><p>Pancetta andouille pastrami ball tip. Corned beef shankle cow porchetta buffalo. Tail bacon burgdoggen ham, pork belly bresaola turducken. Swine meatball ham hock pig pork loin pancetta beef cow porchetta turkey corned beef picanha.</p><p>Tri-tip kevin pork, picanha sausage jerky burgdoggen sirloin chuck alcatra frankfurter flank pork loin. Doner filet mignon strip steak, shankle pig pancetta ham landjaeger turducken andouille flank. Brisket meatloaf bresaola jerky fatback sirloin boudin. Drumstick cupim ribeye, meatball turkey jerky ham pancetta ball tip andouille picanha short loin sirloin kevin. Fatback pork tenderloin, cow pork chop kielbasa chicken capicola ball tip venison doner landjaeger burgdoggen picanha brisket. Turkey cow pancetta cupim prosciutto spare ribs. Beef ribs cupim short loin, fatback pork loin t-bone tongue salami tail shoulder chuck tenderloin picanha.</p><p>Beef ribs doner porchetta, ribeye boudin tongue ham biltong t-bone. Jowl burgdoggen shoulder filet mignon frankfurter salami. Boudin sirloin cupim alcatra beef. Fatback bresaola venison, bacon pancetta tail sirloin leberkas cupim flank pig. Turkey hamburger venison pork loin biltong boudin. Boudin chicken short loin strip steak ham pancetta buffalo swine alcatra leberkas shankle turkey. Brisket doner meatloaf, shank turducken drumstick pork belly kielbasa short loin leberkas meatball beef ribs pork chop landjaeger t-bone.</p><p>Porchetta pastrami capicola landjaeger kevin prosciutto burgdoggen pancetta alcatra shoulder venison sirloin. Brisket picanha flank, kevin shankle boudin rump burgdoggen. Pork belly bacon turkey pancetta capicola. Doner fatback sirloin rump jerky swine.</p><p>Does your lorem ipsum text long for something a little meatier? Give our generator a try… it’s tasty!</p>', 0, '2018-04-01 07:20:09', 12),
(11, 'Contact success', 'Contact Us', 'Your enquiry has been successfully sent to the store owner!', 'contact-success', '<p>Your enquiry has been successfully sent to the store owner!</p>', 0, '2018-04-01 13:47:51', 20);

-- --------------------------------------------------------

--
-- Table structure for table `sma_payments`
--

CREATE TABLE `sma_payments` (
  `id` int(11) NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_id` int(11) DEFAULT NULL,
  `return_id` int(11) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `reference_no` varchar(50) NOT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `paid_by` varchar(20) NOT NULL,
  `cheque_no` varchar(20) DEFAULT NULL,
  `cc_no` varchar(20) DEFAULT NULL,
  `cc_holder` varchar(25) DEFAULT NULL,
  `cc_month` varchar(2) DEFAULT NULL,
  `cc_year` varchar(4) DEFAULT NULL,
  `cc_type` varchar(20) DEFAULT NULL,
  `amount` decimal(25,4) NOT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `pos_paid` decimal(25,4) DEFAULT '0.0000',
  `pos_balance` decimal(25,4) DEFAULT '0.0000',
  `approval_code` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_payments`
--

INSERT INTO `sma_payments` (`id`, `date`, `sale_id`, `return_id`, `purchase_id`, `reference_no`, `transaction_id`, `paid_by`, `cheque_no`, `cc_no`, `cc_holder`, `cc_month`, `cc_year`, `cc_type`, `amount`, `currency`, `created_by`, `attachment`, `type`, `note`, `pos_paid`, `pos_balance`, `approval_code`) VALUES
(1, '2018-04-05 08:25:20', 1, NULL, NULL, 'IPAY2018/04/0011', NULL, 'gift_card', '', '5878422801151142', '', '', '', '', '52.0000', NULL, 1, NULL, 'received', '', '52.0000', '-552.0000', NULL),
(2, '2018-04-05 08:25:20', 1, NULL, NULL, 'IPAY2018/04/0012', NULL, 'cash', '', '', '', '', '', '', '552.0000', NULL, 1, NULL, 'received', '', '610.0000', '58.0000', NULL),
(3, '2018-04-06 11:31:44', 3, NULL, NULL, 'IPAY2018/04/0013', NULL, 'gift_card', '', '142', '', '', '', '', '31.0000', NULL, 1, NULL, 'received', '', '31.0000', '-23929.0000', NULL),
(4, '2018-04-06 11:31:44', 3, NULL, NULL, 'IPAY2018/04/0014', NULL, 'cash', '', '', '', '', '', '', '23929.0000', NULL, 1, NULL, 'received', '', '23929.0000', '0.0000', NULL),
(5, '2018-04-11 12:59:46', 11, NULL, NULL, 'IPAY2018/04/0015', NULL, 'cash', '', '', '', '', '', '', '599.0000', NULL, 1, NULL, 'received', '', '599.0000', '0.0000', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_paypal`
--

CREATE TABLE `sma_paypal` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `name` varchar(255) NOT NULL,
  `account_email` varchar(255) NOT NULL,
  `paypal_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT '2.0000',
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT '3.9000',
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT '4.4000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_paypal`
--

INSERT INTO `sma_paypal` (`id`, `active`, `name`, `account_email`, `paypal_currency`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(1, 0, 'PayPal', 'mianumarakram@gmail.com', 'USD', '0.0000', '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_permissions`
--

CREATE TABLE `sma_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `products-index` tinyint(1) DEFAULT '0',
  `products-add` tinyint(1) DEFAULT '0',
  `products-edit` tinyint(1) DEFAULT '0',
  `products-delete` tinyint(1) DEFAULT '0',
  `products-cost` tinyint(1) DEFAULT '0',
  `products-price` tinyint(1) DEFAULT '0',
  `quotes-index` tinyint(1) DEFAULT '0',
  `quotes-add` tinyint(1) DEFAULT '0',
  `quotes-edit` tinyint(1) DEFAULT '0',
  `quotes-pdf` tinyint(1) DEFAULT '0',
  `quotes-email` tinyint(1) DEFAULT '0',
  `quotes-delete` tinyint(1) DEFAULT '0',
  `sales-index` tinyint(1) DEFAULT '0',
  `sales-add` tinyint(1) DEFAULT '0',
  `sales-edit` tinyint(1) DEFAULT '0',
  `sales-pdf` tinyint(1) DEFAULT '0',
  `sales-email` tinyint(1) DEFAULT '0',
  `sales-delete` tinyint(1) DEFAULT '0',
  `purchases-index` tinyint(1) DEFAULT '0',
  `purchases-add` tinyint(1) DEFAULT '0',
  `purchases-edit` tinyint(1) DEFAULT '0',
  `purchases-pdf` tinyint(1) DEFAULT '0',
  `purchases-email` tinyint(1) DEFAULT '0',
  `purchases-delete` tinyint(1) DEFAULT '0',
  `transfers-index` tinyint(1) DEFAULT '0',
  `transfers-add` tinyint(1) DEFAULT '0',
  `transfers-edit` tinyint(1) DEFAULT '0',
  `transfers-pdf` tinyint(1) DEFAULT '0',
  `transfers-email` tinyint(1) DEFAULT '0',
  `transfers-delete` tinyint(1) DEFAULT '0',
  `customers-index` tinyint(1) DEFAULT '0',
  `customers-add` tinyint(1) DEFAULT '0',
  `customers-edit` tinyint(1) DEFAULT '0',
  `customers-delete` tinyint(1) DEFAULT '0',
  `suppliers-index` tinyint(1) DEFAULT '0',
  `suppliers-add` tinyint(1) DEFAULT '0',
  `suppliers-edit` tinyint(1) DEFAULT '0',
  `suppliers-delete` tinyint(1) DEFAULT '0',
  `sales-deliveries` tinyint(1) DEFAULT '0',
  `sales-add_delivery` tinyint(1) DEFAULT '0',
  `sales-edit_delivery` tinyint(1) DEFAULT '0',
  `sales-delete_delivery` tinyint(1) DEFAULT '0',
  `sales-email_delivery` tinyint(1) DEFAULT '0',
  `sales-pdf_delivery` tinyint(1) DEFAULT '0',
  `sales-gift_cards` tinyint(1) DEFAULT '0',
  `sales-add_gift_card` tinyint(1) DEFAULT '0',
  `sales-edit_gift_card` tinyint(1) DEFAULT '0',
  `sales-delete_gift_card` tinyint(1) DEFAULT '0',
  `pos-index` tinyint(1) DEFAULT '0',
  `sales-return_sales` tinyint(1) DEFAULT '0',
  `reports-index` tinyint(1) DEFAULT '0',
  `reports-warehouse_stock` tinyint(1) DEFAULT '0',
  `reports-quantity_alerts` tinyint(1) DEFAULT '0',
  `reports-expiry_alerts` tinyint(1) DEFAULT '0',
  `reports-products` tinyint(1) DEFAULT '0',
  `reports-daily_sales` tinyint(1) DEFAULT '0',
  `reports-monthly_sales` tinyint(1) DEFAULT '0',
  `reports-sales` tinyint(1) DEFAULT '0',
  `reports-payments` tinyint(1) DEFAULT '0',
  `reports-purchases` tinyint(1) DEFAULT '0',
  `reports-profit_loss` tinyint(1) DEFAULT '0',
  `reports-customers` tinyint(1) DEFAULT '0',
  `reports-suppliers` tinyint(1) DEFAULT '0',
  `reports-staff` tinyint(1) DEFAULT '0',
  `reports-register` tinyint(1) DEFAULT '0',
  `sales-payments` tinyint(1) DEFAULT '0',
  `purchases-payments` tinyint(1) DEFAULT '0',
  `purchases-expenses` tinyint(1) DEFAULT '0',
  `products-adjustments` tinyint(1) NOT NULL DEFAULT '0',
  `bulk_actions` tinyint(1) NOT NULL DEFAULT '0',
  `customers-deposits` tinyint(1) NOT NULL DEFAULT '0',
  `customers-delete_deposit` tinyint(1) NOT NULL DEFAULT '0',
  `products-barcode` tinyint(1) NOT NULL DEFAULT '0',
  `purchases-return_purchases` tinyint(1) NOT NULL DEFAULT '0',
  `reports-expenses` tinyint(1) NOT NULL DEFAULT '0',
  `reports-daily_purchases` tinyint(1) DEFAULT '0',
  `reports-monthly_purchases` tinyint(1) DEFAULT '0',
  `products-stock_count` tinyint(1) DEFAULT '0',
  `edit_price` tinyint(1) DEFAULT '0',
  `returns-index` tinyint(1) DEFAULT '0',
  `returns-add` tinyint(1) DEFAULT '0',
  `returns-edit` tinyint(1) DEFAULT '0',
  `returns-delete` tinyint(1) DEFAULT '0',
  `returns-email` tinyint(1) DEFAULT '0',
  `returns-pdf` tinyint(1) DEFAULT '0',
  `reports-tax` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_permissions`
--

INSERT INTO `sma_permissions` (`id`, `group_id`, `products-index`, `products-add`, `products-edit`, `products-delete`, `products-cost`, `products-price`, `quotes-index`, `quotes-add`, `quotes-edit`, `quotes-pdf`, `quotes-email`, `quotes-delete`, `sales-index`, `sales-add`, `sales-edit`, `sales-pdf`, `sales-email`, `sales-delete`, `purchases-index`, `purchases-add`, `purchases-edit`, `purchases-pdf`, `purchases-email`, `purchases-delete`, `transfers-index`, `transfers-add`, `transfers-edit`, `transfers-pdf`, `transfers-email`, `transfers-delete`, `customers-index`, `customers-add`, `customers-edit`, `customers-delete`, `suppliers-index`, `suppliers-add`, `suppliers-edit`, `suppliers-delete`, `sales-deliveries`, `sales-add_delivery`, `sales-edit_delivery`, `sales-delete_delivery`, `sales-email_delivery`, `sales-pdf_delivery`, `sales-gift_cards`, `sales-add_gift_card`, `sales-edit_gift_card`, `sales-delete_gift_card`, `pos-index`, `sales-return_sales`, `reports-index`, `reports-warehouse_stock`, `reports-quantity_alerts`, `reports-expiry_alerts`, `reports-products`, `reports-daily_sales`, `reports-monthly_sales`, `reports-sales`, `reports-payments`, `reports-purchases`, `reports-profit_loss`, `reports-customers`, `reports-suppliers`, `reports-staff`, `reports-register`, `sales-payments`, `purchases-payments`, `purchases-expenses`, `products-adjustments`, `bulk_actions`, `customers-deposits`, `customers-delete_deposit`, `products-barcode`, `purchases-return_purchases`, `reports-expenses`, `reports-daily_purchases`, `reports-monthly_purchases`, `products-stock_count`, `edit_price`, `returns-index`, `returns-add`, `returns-edit`, `returns-delete`, `returns-email`, `returns-pdf`, `reports-tax`) VALUES
(6, 8, 1, 1, 1, 1, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_pos_register`
--

CREATE TABLE `sma_pos_register` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `cash_in_hand` decimal(25,4) NOT NULL,
  `status` varchar(10) NOT NULL,
  `total_cash` decimal(25,4) DEFAULT NULL,
  `total_cheques` int(11) DEFAULT NULL,
  `total_cc_slips` int(11) DEFAULT NULL,
  `total_cash_submitted` decimal(25,4) DEFAULT NULL,
  `total_cheques_submitted` int(11) DEFAULT NULL,
  `total_cc_slips_submitted` int(11) DEFAULT NULL,
  `note` text,
  `closed_at` timestamp NULL DEFAULT NULL,
  `transfer_opened_bills` varchar(50) DEFAULT NULL,
  `closed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_pos_register`
--

INSERT INTO `sma_pos_register` (`id`, `date`, `user_id`, `cash_in_hand`, `status`, `total_cash`, `total_cheques`, `total_cc_slips`, `total_cash_submitted`, `total_cheques_submitted`, `total_cc_slips_submitted`, `note`, `closed_at`, `transfer_opened_bills`, `closed_by`) VALUES
(1, '2017-04-15 15:54:11', 2, '2000.0000', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '2017-04-24 08:59:04', 4, '881.7500', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '2017-07-01 06:41:42', 5, '504.0000', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '2017-07-02 11:43:18', 6, '504.0000', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '2017-09-26 16:55:45', 1, '500.0000', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_pos_settings`
--

CREATE TABLE `sma_pos_settings` (
  `pos_id` int(1) NOT NULL,
  `cat_limit` int(11) NOT NULL,
  `pro_limit` int(11) NOT NULL,
  `default_category` int(11) NOT NULL,
  `default_customer` int(11) NOT NULL,
  `default_biller` int(11) NOT NULL,
  `display_time` varchar(3) NOT NULL DEFAULT 'yes',
  `cf_title1` varchar(255) DEFAULT NULL,
  `cf_title2` varchar(255) DEFAULT NULL,
  `cf_value1` varchar(255) DEFAULT NULL,
  `cf_value2` varchar(255) DEFAULT NULL,
  `receipt_printer` varchar(55) DEFAULT NULL,
  `cash_drawer_codes` varchar(55) DEFAULT NULL,
  `focus_add_item` varchar(55) DEFAULT NULL,
  `add_manual_product` varchar(55) DEFAULT NULL,
  `customer_selection` varchar(55) DEFAULT NULL,
  `add_customer` varchar(55) DEFAULT NULL,
  `toggle_category_slider` varchar(55) DEFAULT NULL,
  `toggle_subcategory_slider` varchar(55) DEFAULT NULL,
  `cancel_sale` varchar(55) DEFAULT NULL,
  `suspend_sale` varchar(55) DEFAULT NULL,
  `print_items_list` varchar(55) DEFAULT NULL,
  `finalize_sale` varchar(55) DEFAULT NULL,
  `today_sale` varchar(55) DEFAULT NULL,
  `open_hold_bills` varchar(55) DEFAULT NULL,
  `close_register` varchar(55) DEFAULT NULL,
  `keyboard` tinyint(1) NOT NULL,
  `pos_printers` varchar(255) DEFAULT NULL,
  `java_applet` tinyint(1) NOT NULL,
  `product_button_color` varchar(20) NOT NULL DEFAULT 'default',
  `tooltips` tinyint(1) DEFAULT '1',
  `paypal_pro` tinyint(1) DEFAULT '0',
  `stripe` tinyint(1) DEFAULT '0',
  `rounding` tinyint(1) DEFAULT '0',
  `char_per_line` tinyint(4) DEFAULT '42',
  `pin_code` varchar(20) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT 'purchase_code',
  `envato_username` varchar(50) DEFAULT 'envato_username',
  `version` varchar(10) DEFAULT '3.2.2',
  `after_sale_page` tinyint(1) DEFAULT '0',
  `item_order` tinyint(1) DEFAULT '0',
  `authorize` tinyint(1) DEFAULT '0',
  `toggle_brands_slider` varchar(55) DEFAULT NULL,
  `remote_printing` tinyint(1) DEFAULT '1',
  `printer` int(11) DEFAULT NULL,
  `order_printers` varchar(55) DEFAULT NULL,
  `auto_print` tinyint(1) DEFAULT '0',
  `customer_details` tinyint(1) DEFAULT NULL,
  `local_printers` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_pos_settings`
--

INSERT INTO `sma_pos_settings` (`pos_id`, `cat_limit`, `pro_limit`, `default_category`, `default_customer`, `default_biller`, `display_time`, `cf_title1`, `cf_title2`, `cf_value1`, `cf_value2`, `receipt_printer`, `cash_drawer_codes`, `focus_add_item`, `add_manual_product`, `customer_selection`, `add_customer`, `toggle_category_slider`, `toggle_subcategory_slider`, `cancel_sale`, `suspend_sale`, `print_items_list`, `finalize_sale`, `today_sale`, `open_hold_bills`, `close_register`, `keyboard`, `pos_printers`, `java_applet`, `product_button_color`, `tooltips`, `paypal_pro`, `stripe`, `rounding`, `char_per_line`, `pin_code`, `purchase_code`, `envato_username`, `version`, `after_sale_page`, `item_order`, `authorize`, `toggle_brands_slider`, `remote_printing`, `printer`, `order_printers`, `auto_print`, `customer_details`, `local_printers`) VALUES
(1, 22, 20, 1, 1, 3, '1', 'GST Reg', 'VAT Reg', '123456789', '987654321', NULL, 'x1C', 'Ctrl+F3', 'Ctrl+Shift+M', 'Ctrl+Shift+C', 'Ctrl+Shift+A', 'Ctrl+F11', 'Ctrl+F12', 'F4', 'F7', 'F9', 'F8', 'Ctrl+F1', 'Ctrl+F2', 'Ctrl+F10', 1, NULL, 0, 'default', 1, 0, 0, 0, 42, NULL, 'purchase_code', 'envato_username', '3.2.12', 0, 0, 0, '', 1, NULL, 'null', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sma_price_groups`
--

CREATE TABLE `sma_price_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_price_groups`
--

INSERT INTO `sma_price_groups` (`id`, `name`) VALUES
(1, 'Default');

-- --------------------------------------------------------

--
-- Table structure for table `sma_printers`
--

CREATE TABLE `sma_printers` (
  `id` int(11) NOT NULL,
  `title` varchar(55) NOT NULL,
  `type` varchar(25) NOT NULL,
  `profile` varchar(25) NOT NULL,
  `char_per_line` tinyint(3) UNSIGNED DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `ip_address` varbinary(45) DEFAULT NULL,
  `port` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_products`
--

CREATE TABLE `sma_products` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` char(255) NOT NULL,
  `translated_name` varchar(255) DEFAULT NULL,
  `unit` int(11) DEFAULT '2',
  `cost` decimal(25,4) NOT NULL,
  `price` decimal(25,4) NOT NULL,
  `alert_quantity` decimal(15,4) DEFAULT '20.0000',
  `image` varchar(255) DEFAULT 'no_image.png',
  `back_image` varchar(255) NOT NULL DEFAULT 'no_image.png',
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `cf1` varchar(255) DEFAULT NULL,
  `cf2` varchar(255) DEFAULT NULL,
  `cf3` varchar(255) DEFAULT NULL,
  `cf4` varchar(255) DEFAULT NULL,
  `cf5` varchar(255) DEFAULT NULL,
  `cf6` varchar(255) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `tax_rate` int(11) DEFAULT NULL,
  `track_quantity` tinyint(1) DEFAULT '1',
  `details` varchar(1000) DEFAULT NULL,
  `warehouse` int(11) DEFAULT NULL,
  `barcode_symbology` varchar(55) NOT NULL DEFAULT 'code128',
  `file` varchar(100) DEFAULT NULL,
  `product_details` text,
  `tax_method` tinyint(1) DEFAULT '0',
  `type` varchar(55) NOT NULL DEFAULT 'standard',
  `supplier1` int(11) DEFAULT NULL,
  `supplier1price` decimal(25,4) DEFAULT NULL,
  `supplier2` int(11) DEFAULT NULL,
  `supplier2price` decimal(25,4) DEFAULT NULL,
  `supplier3` int(11) DEFAULT NULL,
  `supplier3price` decimal(25,4) DEFAULT NULL,
  `supplier4` int(11) DEFAULT NULL,
  `supplier4price` decimal(25,4) DEFAULT NULL,
  `supplier5` int(11) DEFAULT NULL,
  `supplier5price` decimal(25,4) DEFAULT NULL,
  `promotion` tinyint(1) DEFAULT '0',
  `promo_price` decimal(25,4) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `supplier1_part_no` varchar(50) DEFAULT NULL,
  `supplier2_part_no` varchar(50) DEFAULT NULL,
  `supplier3_part_no` varchar(50) DEFAULT NULL,
  `supplier4_part_no` varchar(50) DEFAULT NULL,
  `supplier5_part_no` varchar(50) DEFAULT NULL,
  `sale_unit` int(11) DEFAULT '2',
  `purchase_unit` int(11) DEFAULT '2',
  `brand` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `publisher` int(11) NOT NULL,
  `no_of_pages` int(11) NOT NULL DEFAULT '0',
  `year_of_publication` date DEFAULT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `approved_by_nmc` tinyint(4) DEFAULT '0',
  `weight` decimal(10,4) DEFAULT NULL,
  `width` decimal(10,4) DEFAULT NULL,
  `height` decimal(10,4) DEFAULT NULL,
  `extra_size` int(11) DEFAULT NULL,
  `free_shipment` tinyint(3) NOT NULL DEFAULT '0',
  `is_updated` tinyint(1) DEFAULT '0',
  `hsn_code` int(11) DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `hide` tinyint(1) NOT NULL DEFAULT '0',
  `second_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_products`
--

INSERT INTO `sma_products` (`id`, `code`, `name`, `translated_name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `back_image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `promotion`, `promo_price`, `start_date`, `end_date`, `supplier1_part_no`, `supplier2_part_no`, `supplier3_part_no`, `supplier4_part_no`, `supplier5_part_no`, `sale_unit`, `purchase_unit`, `brand`, `author`, `publisher`, `no_of_pages`, `year_of_publication`, `slug`, `featured`, `approved_by_nmc`, `weight`, `width`, `height`, `extra_size`, `free_shipment`, `is_updated`, `hsn_code`, `views`, `hide`, `second_name`) VALUES
(719, '02646516', 'Ambush (Michael Bennett)', '', 2, '350.0000', '23960.0000', '2.0000', 'e550a7c7fa66f8242d0734b027380b98.jpg', 'no_image.png', 45, NULL, '', '', '', '', '', '', '5.0000', 1, 1, '', NULL, 'code128', '', '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores ..</p>', 1, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 2, 2, 2, 18, 12, 45, '2010-06-08', 'ambush-michael-bennett', 1, 0, '410.0000', '0.0000', '0.0000', NULL, 0, 0, NULL, 35, 0, ''),
(748, '62527578', 'Michael Bennett', '', 2, '350.0000', '599.0000', '2.0000', '54f5e9b4fac0a59a29b356e0f0128cf5.jpg', 'no_image.png', 45, NULL, '', '', '', '', '', '', NULL, 1, 1, '', NULL, 'code128', '', '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores ..</p>', 1, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 0, 0, 2, NULL, 10, 852, '2018-04-10', 'michael-bennett', 1, 0, '520.0000', '52.0000', '565.0000', NULL, 0, 0, NULL, 3, 0, ''),
(749, '60026189', 'Michael Sheet', '', 2, '350.0000', '599.0000', '2.0000', '068b61a296918b729d1f44240e373251.jpg', 'no_image.png', 45, NULL, '', '', '', '', '', '', '8.0000', 1, 1, '', NULL, 'code128', '', '<div>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</div>', 1, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 0, 0, 2, NULL, 13, 9, '2018-04-11', 'michael-sheet', 1, 0, '50.0000', '458.0000', '655.0000', NULL, 0, 0, NULL, 180, 0, ''),
(750, '87723642', 'Bennett Michael', '', 2, '350.0000', '599.0000', '2.0000', '3060a6129af34757ee3cf15ccc98fa73.jpg', 'no_image.png', 47, NULL, '', '', '', '', '', '', NULL, 1, 1, '', NULL, 'code128', '', '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores ..</p>', 1, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 0, 0, 2, NULL, 13, 20, '2018-03-25', 'bennett-michael', 1, 1, '300.0000', '12.0000', '52.0000', NULL, 0, 0, NULL, 3, 0, ''),
(751, '80930914', 'Bennett Dpem', '', 2, '350.0000', '599.0000', '2.0000', '6e917f3a22d8e2b2932da7b1ab2a595e.jpg', 'no_image.png', 39, NULL, '', '', '', '', '', '', '-1.0000', 1, 1, '', NULL, 'code128', '', '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores ..</p>', 1, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 0, 0, 1, NULL, 12, 24, '2018-04-23', 'bennett-dpem', 1, 0, '0.0000', '12.0000', '85.0000', NULL, 0, 0, NULL, 6, 0, ''),
(752, '68325902', 'Ambush Jekson', '', 2, '350.0000', '599.0000', '2.0000', '6b9664fcef8ed62b132c1339f0015bb6.jpg', 'no_image.png', 45, NULL, '', '', '', '', '', '', NULL, 1, 1, '', NULL, 'code128', '', '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores ..</p>', 1, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 0, 0, 2, NULL, 12, 21, '2018-04-16', 'ambush-jekson', 1, 0, '300.0000', '52.0000', '85.0000', NULL, 0, 0, NULL, 5, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_authors`
--

CREATE TABLE `sma_product_authors` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `is_updated` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sma_product_authors`
--

INSERT INTO `sma_product_authors` (`id`, `product_id`, `author_id`, `status`, `is_updated`) VALUES
(9, 739, 19, 1, 0),
(8, 739, 18, 1, 0),
(7, 739, 17, 1, 0),
(55, 746, 18, 1, 0),
(54, 746, 17, 1, 0),
(53, 746, 16, 1, 0),
(52, 746, 15, 1, 0),
(51, 746, 14, 1, 0),
(50, 746, 13, 1, 0),
(49, 745, 17, 1, 0),
(48, 745, 16, 1, 0),
(47, 745, 15, 1, 0),
(46, 744, 19, 1, 0),
(45, 744, 18, 1, 0),
(44, 744, 17, 1, 0),
(43, 744, 16, 1, 0),
(42, 744, 15, 1, 0),
(41, 744, 14, 1, 0),
(40, 744, 13, 1, 0),
(39, 743, 17, 1, 0),
(38, 743, 16, 1, 0),
(37, 743, 15, 1, 0),
(56, 746, 19, 1, 0),
(70, 747, 19, 1, 0),
(69, 747, 17, 1, 0),
(68, 747, 16, 1, 0),
(67, 747, 15, 1, 0),
(66, 747, 14, 1, 0),
(65, 747, 13, 1, 0),
(64, 747, 12, 1, 0),
(94, 719, 16, 1, 0),
(93, 719, 15, 1, 0),
(83, 748, 18, 1, 0),
(82, 748, 14, 1, 0),
(90, 749, 13, 1, 0),
(88, 750, 17, 1, 0),
(87, 751, 15, 1, 0),
(86, 752, 18, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_photos`
--

CREATE TABLE `sma_product_photos` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `photo` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `is_updated` tinyint(1) DEFAULT '0',
  `gallery_unique_code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_product_photos`
--

INSERT INTO `sma_product_photos` (`id`, `product_id`, `photo`, `status`, `is_updated`, `gallery_unique_code`) VALUES
(42, 749, '6e26a7e95847a08b8c2ea1d4e4192149.jpg', 1, 0, NULL),
(43, 749, 'b187f8b6ea9ca47ccfd92d0269e6c7c1.jpg', 1, 0, NULL),
(44, 749, '8aa575fb49b73060300e726285dfdf4a.jpg', 1, 0, NULL),
(45, 749, '29727a7660b82a60af4c0f230591a103.jpg', 1, 0, NULL),
(46, 749, 'd7621f0f1e18c87dd8a3827a6d66d47e.jpg', 1, 0, NULL),
(47, 749, '98b99eba9e23e92d2096e8cf54c8fad6.jpg', 1, 0, NULL),
(48, 749, '4f2de5f002a26349a701554eeda8f0a6.jpg', 1, 0, NULL),
(49, 749, '9da63d605c40684a904b36eb348fb9a5.jpg', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_prices`
--

CREATE TABLE `sma_product_prices` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price_group_id` int(11) NOT NULL,
  `price` decimal(25,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_variants`
--

CREATE TABLE `sma_product_variants` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT NULL,
  `is_updated` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_product_variants`
--

INSERT INTO `sma_product_variants` (`id`, `product_id`, `name`, `cost`, `price`, `quantity`, `is_updated`) VALUES
(1, 749, '2-3 years', NULL, '5.0000', '8.0000', 0),
(2, 749, '6-7 years', NULL, '6.0000', '10.0000', 0),
(3, 719, '2-3 years', NULL, '12.0000', '7.0000', 0),
(4, 719, '6-7 years', NULL, '45.0000', '7.0000', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_promotions`
--

CREATE TABLE `sma_promotions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `type` tinytext NOT NULL,
  `isMoreThanOne` tinyint(1) DEFAULT '0',
  `disqualifiedRemoval` int(11) NOT NULL,
  `isRedemptionCheck` int(11) NOT NULL,
  `redemptionLimit` int(11) NOT NULL,
  `isCartTotal` int(11) NOT NULL,
  `minAmount` int(11) DEFAULT NULL,
  `maxAmount` int(11) DEFAULT NULL,
  `isDateRange` tinyint(5) DEFAULT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `isTimeRange` tinyint(3) NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `triggerOnItem` tinyint(3) NOT NULL,
  `itemQuantity` tinyint(3) NOT NULL,
  `status` tinytext NOT NULL,
  `inDaysOnly` tinyint(3) DEFAULT NULL,
  `days` varchar(255) NOT NULL,
  `how_many_buy` tinyint(1) NOT NULL DEFAULT '0',
  `how_many_free` tinyint(1) NOT NULL DEFAULT '0',
  `how_much_percentage` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sma_promotions`
--

INSERT INTO `sma_promotions` (`id`, `name`, `title`, `description`, `type`, `isMoreThanOne`, `disqualifiedRemoval`, `isRedemptionCheck`, `redemptionLimit`, `isCartTotal`, `minAmount`, `maxAmount`, `isDateRange`, `startDate`, `endDate`, `isTimeRange`, `startTime`, `endTime`, `triggerOnItem`, `itemQuantity`, `status`, `inDaysOnly`, `days`, `how_many_buy`, `how_many_free`, `how_much_percentage`) VALUES
(54, 'Loremipsim ', 'hello its tile', '<p>Hello its descri</p>', 'buyOneGetCategoryFree', NULL, 0, 0, 0, 0, 0, 0, 1, '2017-12-10', '2017-12-11', 0, '00:00:00', '00:00:00', 0, 0, 'active', NULL, '', 2, 1, NULL),
(55, 'loremup ipsum for different products', 'loremup ipsum for different products', '<p>lorem ipsum dollar msith,</p>', 'buyOneGetOtherProductFree', NULL, 0, 0, 0, 0, 0, 0, 1, '2017-11-13', '2017-11-18', 0, '00:00:00', '00:00:00', 0, 0, 'active', NULL, '', 1, 1, NULL),
(56, 'asdasdasd', 'asdasdasdasdas', '<p>asdasdasd</p>', 'buyOneGetOne', NULL, 0, 0, 0, 0, 0, 0, 1, '2017-11-13', '2017-11-19', 0, '00:00:00', '00:00:00', 0, 0, 'active', NULL, '', 1, 1, NULL),
(57, 'asdas', 'asdasd', '<p>asdasd asdasd</p>', 'buyOneGetCategoryFree', NULL, 0, 0, 0, 0, 0, 0, 1, '2017-12-02', '2017-12-04', 0, '00:00:00', '00:00:00', 0, 0, 'active', NULL, '', 1, 1, NULL),
(58, 'Lorem ipsum dollar smith', '2% Flate Off', '<p>Lorem ipsum dollar smith</p>', 'buyCategoryWithPercentage', NULL, 0, 0, 0, 0, 0, 0, 1, '2017-12-12', '2017-12-17', 0, '00:00:00', '00:00:00', 0, 0, 'inactive', NULL, '', 0, 0, 2),
(60, 'Lorem ipsum dollar smith', 'Dollar smtihm ', '<p>Lorem ipsum dollar smith&nbsp;Lorem ipsum dollar smith</p>', 'buyCategoryWithPercentage', NULL, 0, 0, 0, 0, 0, 0, 1, '2017-12-12', '2017-12-18', 0, '00:00:00', '00:00:00', 0, 0, 'inactive', NULL, '', 0, 0, 2),
(61, 'Lorem ipsum dollar smith', '2% Flate Off', '<p>Lorem ipsum dollar smithLorem ipsum dollar smith</p>', 'buyCategoryWithPercentage', NULL, 0, 0, 0, 0, 0, 0, 1, '2017-12-12', '2017-12-17', 0, '00:00:00', '00:00:00', 0, 0, 'inactive', NULL, '', 0, 0, 5),
(64, 'Lorem ipsum dollar smith', 'Sale 4% off', '<p>Dollar smtihm&nbsp;Dollar smtihm&nbsp;Dollar smtihm</p>', 'buyProductsWithPercentage', NULL, 0, 0, 0, 0, 0, 0, 1, '2017-12-13', '2017-12-18', 0, '00:00:00', '00:00:00', 0, 0, 'inactive', NULL, '', 0, 0, 4),
(73, 'Loot sale offer for limited offer ', 'Flat 20% Off', '<p>Lorem ipsum umar dollar smith.&nbsp;</p>', 'buyCategoryWithPercentage', NULL, 0, 0, 0, 0, 0, 0, 1, '2017-12-12', '2017-12-17', 0, '00:00:00', '00:00:00', 1, 0, 'inactive', NULL, '', 0, 0, 30),
(81, 'Lorem ipsum dollar smith', 'Sale 10% off', '', 'buyProductsWithPercentage', NULL, 0, 0, 0, 0, 0, 0, 1, '2017-12-13', '2017-12-18', 0, '00:00:00', '00:00:00', 1, 0, 'inactive', NULL, '', 0, 0, 10),
(92, 'Lorem ipsum dollar smith.', '10 % flate sale', '', 'buyProductsWithPercentage', NULL, 0, 0, 0, 0, 0, 0, 1, '2017-12-13', '2017-12-18', 0, '00:00:00', '00:00:00', 1, 0, 'active', NULL, '', 0, 0, 10),
(93, 'Loot sale offer for limited offer', 'Flat 30% Off', '<p>Lorem ipsum umar dollar smith.</p>', 'buyCategoryWithPercentage', NULL, 0, 0, 0, 0, 0, 0, 1, '2017-12-12', '2017-12-17', 0, '00:00:00', '00:00:00', 1, 0, 'active', NULL, '', 0, 0, 30);

-- --------------------------------------------------------

--
-- Table structure for table `sma_promotion_for_items`
--

CREATE TABLE `sma_promotion_for_items` (
  `id` int(11) NOT NULL,
  `upselling_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sma_promotion_for_items`
--

INSERT INTO `sma_promotion_for_items` (`id`, `upselling_id`, `item_id`) VALUES
(17, 81, 50),
(18, 81, 650),
(32, 92, 1),
(33, 92, 70),
(34, 92, 102),
(35, 93, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_promotion_items`
--

CREATE TABLE `sma_promotion_items` (
  `id` int(11) NOT NULL,
  `upselling_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `warehouse_id` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sma_promotion_items`
--

INSERT INTO `sma_promotion_items` (`id`, `upselling_id`, `item_id`, `category_id`, `warehouse_id`) VALUES
(173, 54, 0, 9, 1),
(174, 54, 0, 10, 1),
(175, 54, 0, 9, 2),
(176, 54, 0, 10, 2),
(177, 55, 70, NULL, 1),
(178, 55, 643, NULL, 1),
(179, 55, 70, NULL, 2),
(180, 55, 643, NULL, 2),
(181, 56, 630, NULL, 1),
(182, 56, 630, NULL, 2),
(183, 56, 630, NULL, 3),
(184, 57, 0, 28, 1),
(185, 57, 0, 28, 2),
(186, 57, 0, 28, 3),
(187, 58, 0, 27, 1),
(188, 58, 0, 27, 2),
(189, 58, 0, 27, 3),
(193, 60, 0, 27, 1),
(194, 60, 0, 27, 2),
(195, 60, 0, 27, 3),
(196, 61, 0, 27, 1),
(197, 61, 0, 27, 2),
(198, 61, 0, 27, 3),
(202, 63, 0, 29, 2),
(203, 64, 59, NULL, 1),
(204, 64, 66, NULL, 1),
(205, 64, 70, NULL, 1),
(206, 64, 90, NULL, 1),
(207, 64, 59, NULL, 2),
(208, 64, 66, NULL, 2),
(209, 64, 70, NULL, 2),
(210, 64, 90, NULL, 2),
(211, 64, 59, NULL, 3),
(212, 64, 66, NULL, 3),
(213, 64, 70, NULL, 3),
(214, 64, 90, NULL, 3),
(326, 92, 1, NULL, 1),
(327, 92, 70, NULL, 1),
(328, 92, 102, NULL, 1),
(329, 92, 1, NULL, 2),
(330, 92, 70, NULL, 2),
(331, 92, 102, NULL, 2),
(332, 92, 1, NULL, 1),
(333, 92, 70, NULL, 1),
(334, 92, 102, NULL, 1),
(335, 92, 1, NULL, 2),
(336, 92, 70, NULL, 2),
(337, 92, 102, NULL, 2),
(338, 93, 0, 27, 1),
(339, 93, 0, 27, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sma_publishers`
--

CREATE TABLE `sma_publishers` (
  `id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `translated_name` varchar(255) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `is_updated` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_publishers`
--

INSERT INTO `sma_publishers` (`id`, `code`, `name`, `translated_name`, `image`, `slug`, `is_updated`) VALUES
(8, '102', 'Pearson', 'بيرسون', NULL, 'pearson', 1),
(9, '103', 'Reed Elsevier', 'ريد السيفير', NULL, 'reed-elsevier', 1),
(10, '104', 'ThomsonReuters', 'طومسون رويترز', NULL, 'thomsonreuters', 1),
(11, '105', 'Wolters Kluwer', 'ولترز كلوير', NULL, 'wolters-kluwer', 1),
(12, '106', 'Hachette Livre', 'كتاب هاشيت', NULL, 'hachette-livre', 1),
(13, '107', 'Holtzbrinck', 'هولتزبرينك', NULL, 'holtzbrinck', 1),
(14, '108', 'Ants Among', 'النمل بين', NULL, 'ants-among', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchases`
--

CREATE TABLE `sma_purchases` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(11) NOT NULL,
  `supplier` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `product_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_discount` decimal(25,4) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT NULL,
  `product_tax` decimal(25,4) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `paid` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `status` varchar(55) DEFAULT '',
  `payment_status` varchar(20) DEFAULT 'pending',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `payment_term` tinyint(4) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `return_id` int(11) DEFAULT NULL,
  `surcharge` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `return_purchase_ref` varchar(55) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `return_purchase_total` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchase_items`
--

CREATE TABLE `sma_purchase_items` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(20) DEFAULT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT '0.0000',
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL,
  `quantity_received` decimal(15,4) DEFAULT NULL,
  `supplier_part_no` varchar(50) DEFAULT NULL,
  `purchase_item_id` int(11) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_purchase_items`
--

INSERT INTO `sma_purchase_items` (`id`, `purchase_id`, `transfer_id`, `product_id`, `product_code`, `product_name`, `option_id`, `net_unit_cost`, `quantity`, `warehouse_id`, `item_tax`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `expiry`, `subtotal`, `quantity_balance`, `date`, `status`, `unit_cost`, `real_unit_cost`, `quantity_received`, `supplier_part_no`, `purchase_item_id`, `product_unit_id`, `product_unit_code`, `unit_quantity`, `gst`, `cgst`, `sgst`, `igst`) VALUES
(1, NULL, NULL, 99, '17135466', 'Chest Floral Gown with Naked Shoulders - Purple  ', NULL, '41.0000', '0.0000', 1, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-02', 'received', '41.0000', '41.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(2, NULL, NULL, 99, '17135466', 'Chest Floral Gown with Naked Shoulders - Purple  ', NULL, '41.0000', '0.0000', 2, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-02', 'received', '41.0000', '41.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(3, NULL, NULL, 99, '17135466', 'Chest Floral Gown with Naked Shoulders - Purple  ', NULL, '41.0000', '0.0000', 3, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-02', 'received', '41.0000', '41.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(4, NULL, NULL, 100, '17135467', 'Net Gown with Lace Flower Patch & pearls - Beige', NULL, '33.0000', '0.0000', 1, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-02', 'received', '33.0000', '33.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(5, NULL, NULL, 100, '17135467', 'Net Gown with Lace Flower Patch & pearls - Beige', NULL, '33.0000', '0.0000', 2, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-02', 'received', '33.0000', '33.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(6, NULL, NULL, 100, '17135467', 'Net Gown with Lace Flower Patch & pearls - Beige', NULL, '33.0000', '0.0000', 3, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-02', 'received', '33.0000', '33.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(7, NULL, NULL, 101, '17135468', 'Thread Embroidered Gown with Pearl Brooj - Blue ', NULL, '35.0000', '0.0000', 1, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-02', 'received', '35.0000', '35.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(8, NULL, NULL, 101, '17135468', 'Thread Embroidered Gown with Pearl Brooj - Blue ', NULL, '35.0000', '0.0000', 2, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-02', 'received', '35.0000', '35.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(9, NULL, NULL, 101, '17135468', 'Thread Embroidered Gown with Pearl Brooj - Blue ', NULL, '35.0000', '0.0000', 3, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-02', 'received', '35.0000', '35.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(90, NULL, NULL, 1, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(91, NULL, NULL, 50, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(92, NULL, NULL, 51, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(93, NULL, NULL, 52, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(94, NULL, NULL, 53, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(95, NULL, NULL, 54, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(96, NULL, NULL, 55, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(97, NULL, NULL, 56, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(98, NULL, NULL, 57, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(99, NULL, NULL, 58, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(100, NULL, NULL, 59, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(101, NULL, NULL, 60, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(102, NULL, NULL, 61, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '6.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(103, NULL, NULL, 62, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(104, NULL, NULL, 63, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(137, NULL, NULL, 82, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(138, NULL, NULL, 88, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(142, NULL, NULL, 81, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(166, NULL, NULL, 162, 'KF-Costm-2001', 'Elsa tutu', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(167, NULL, NULL, 163, 'KF-Costm-2002', 'Arora WITH WHITE COLAR', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(168, NULL, NULL, 164, 'KF-Costm-2003', 'Repunzel TUTU DRESS', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(169, NULL, NULL, 167, 'KF-Costm-2006', 'CINDERELLA- Gown Pic', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(170, NULL, NULL, 168, 'KF-Costm-2007', 'Snow White Gown', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(171, NULL, NULL, 169, 'KF-Costm-2008', 'MERMAID BABY DRESS', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(172, NULL, NULL, 170, 'KF-Costm-2009', 'MERMAID DRESS', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(173, NULL, NULL, 171, 'KF-Costm-2010', 'MERMAID (GREEN&PURPLE COLOR)', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(174, NULL, NULL, 172, 'KF-Costm-2011', 'Mermaid Tutu dress', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(175, NULL, NULL, 173, 'KF-Costm-2012', 'SOFIAGown WITH GLITTERS', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(176, NULL, NULL, 174, 'KF-Costm-2013', 'SOFIA SATIN DRESS', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(177, NULL, NULL, 175, 'KF-Costm-2014', 'Sofia Tutu Dress', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(178, NULL, NULL, 176, 'KF-Costm-2015', 'Mickey Mouse tutu', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(179, NULL, NULL, 177, 'KF-Costm-2016', 'Minnie Mouse Tutu', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(180, NULL, NULL, 178, 'KF-Costm-2017', 'Pumpkin Tutu', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(181, NULL, NULL, 179, 'KF-Costm-2018', 'TinkerBell Tutu with Wings', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(182, NULL, NULL, 180, 'KF-Costm-2019', 'Pink Fairy Tutu with wings', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(183, NULL, NULL, 181, 'KF-Costm-2020', 'Cape Elsa', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(184, NULL, NULL, 183, 'KF-Costm-2022', 'American Costume tutu', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(185, NULL, NULL, 184, 'KF-Costm-2023', 'Elsa Real Gown', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(186, NULL, NULL, 185, 'KF-Costm-2024', 'ANNA TUTU DRESS', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(187, NULL, NULL, 186, 'KF-Costm-2025', 'ELSA BABY DRESS', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(188, NULL, NULL, 187, 'KF-Costm-2026', 'OLAF TUTU DRESS', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(189, NULL, NULL, 188, 'KF-Costm-2027', 'SUPER GIRL TUTU DRESS', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(190, NULL, NULL, 189, 'KF-Costm-2028', 'BATGIRL TUTU DRESS', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(191, NULL, NULL, 190, 'KF-Costm-2029', 'Hello Kitty Tutu Dress', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(192, NULL, NULL, 191, 'KF-Costm-2030', 'Little Pony Tutu Dress', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-22', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(193, NULL, NULL, 208, '103-3001', 'Baby Rosetto Onesie - Pink', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(194, NULL, NULL, 215, '103-3008', 'Baby IceCream Onesie - White', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(195, NULL, NULL, 217, '104-4001', 'White Lace Skin Flower', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(196, NULL, NULL, 218, '104-4002', 'White Satin Back cape', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(197, NULL, NULL, 219, '104-4003', 'Satin Peach A-Line', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(198, NULL, NULL, 220, '104-4004', 'Raw Satin Purple Gown', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(199, NULL, NULL, 222, '104-4006', 'Peach L.Sleevs Stripe Shldr', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(200, NULL, NULL, 223, '104-4007', 'Red Lace white Satin', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(201, NULL, NULL, 192, '105-5001', 'White Ribbon Flower Tutu', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(202, NULL, NULL, 193, '105-5002', '3 Chest Flower gold tutu', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(203, NULL, NULL, 194, '105-5003', 'White purple hawayin theme', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(204, NULL, NULL, 195, '105-5004', 'YELLOW TUTU DRESS', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(205, NULL, NULL, 197, '105-5006', 'AQUA BLUE PINK FLOWER', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(206, NULL, NULL, 198, '105-5007', '3 Flower tutu - Red', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(207, NULL, NULL, 199, '105-5008', 'Peacock Tutu Dress', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(208, NULL, NULL, 200, '105-5009', 'Orange Blue  tutu old', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(209, NULL, NULL, 201, '105-5010', 'Pink Glitter tutu', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(210, NULL, NULL, 225, '108-8001', 'Cape Shoulder Bellet', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(211, NULL, NULL, 226, '108-8002', 'Blue  Glitters Neck Ballet', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(212, NULL, NULL, 227, '108-8003', 'white Bellet Gold neck cape', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(213, NULL, NULL, 228, '108-8004', 'white Bellet Silver neck cape', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(214, NULL, NULL, 229, '108-8005', 'Pink Bellet Gold neck cape', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(215, NULL, NULL, 230, '108-8006', 'Elsa Bellet Glitter', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(216, NULL, NULL, 231, '108-8007', 'Jazz Ballet Red Neck Stone', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(217, NULL, NULL, 233, '108-8009', 'Jazz Ballet Dance Ribbon', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(218, NULL, NULL, 235, '108-8011', 'Pink Ballet Gold Bow', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(219, NULL, NULL, 236, '108-8012', 'Pink Black Crown Ballet', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(220, NULL, NULL, 237, '108-8013', 'Princess Ballet pink Puple', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(221, NULL, NULL, 238, '108-8014', 'Next Stock Yellow Bellet', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(222, NULL, NULL, 239, '108-8015', 'Half Show Cap Slv Ribbon', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(223, NULL, NULL, 240, '108-8016', 'Stripe Embroidered Pink Ballet', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(224, NULL, NULL, 241, '108-8017', 'Neck Flowers White Ballet', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(225, NULL, NULL, 242, '108-8018', 'Neck Stone Frill Slvs Ballet', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(226, NULL, NULL, 202, '105-5011', 'Black classic tutu', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(227, NULL, NULL, 203, '105-5012', '3 flower tutu - Pink', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(228, NULL, NULL, 204, '105-5013', 'Purple tutu pink flower', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(229, NULL, NULL, 205, '105-5014', 'Green Peacock Tutu', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(230, NULL, NULL, 206, '105-5015', 'Purple Rainbow Tutu Dress', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(231, NULL, NULL, 207, '105-5016', 'Pink Rainbow Tutu Dress', 0, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-04-24', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(238, NULL, NULL, 109, '', '', NULL, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(239, NULL, NULL, 108, '', '', NULL, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(240, NULL, NULL, 102, '', '', NULL, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(257, NULL, NULL, 92, '', '', NULL, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(258, NULL, NULL, 91, '', '', NULL, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '13.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(259, NULL, NULL, 71, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(260, NULL, NULL, 97, '', '', NULL, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(261, NULL, NULL, 85, '', '', NULL, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(262, NULL, NULL, 86, '', '', NULL, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(263, NULL, NULL, 76, '', '', NULL, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(264, NULL, NULL, 98, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(265, NULL, NULL, 84, '', '', NULL, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(266, NULL, NULL, 90, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(281, NULL, NULL, 243, '11534922', 'Chest Floral Gown with naken soulders  - Peach', NULL, '41.0000', '0.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '2017-05-03', 'received', '41.0000', '41.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(282, NULL, NULL, 243, '11534922', 'Chest Floral Gown with naken soulders  - Peach', NULL, '41.0000', '6.0000', 2, '0.0000', NULL, NULL, NULL, NULL, NULL, '246.0000', '6.0000', '2017-05-03', 'received', '41.0000', '41.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(283, NULL, NULL, 243, '11534922', 'Chest Floral Gown with naken soulders  - Peach', NULL, '41.0000', '6.0000', 3, '0.0000', NULL, NULL, NULL, NULL, NULL, '246.0000', '6.0000', '2017-05-03', 'received', '41.0000', '41.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(284, NULL, NULL, 244, 'Kf-Gown-232323', 'Chest Floral Gown, naked shoulders- Peach', NULL, '41.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '246.0000', '0.0000', '2017-05-03', 'received', '41.0000', '41.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(285, NULL, NULL, 95, '', '', NULL, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(286, NULL, NULL, 79, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(287, NULL, NULL, 69, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(288, NULL, NULL, 72, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(319, NULL, NULL, 93, '', '', NULL, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(335, NULL, NULL, 245, '102-2004', 'Sofia Satin Gown - Round cuts ', NULL, '40.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '200.0000', '0.0000', '2017-05-03', 'received', '40.0000', '40.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(385, NULL, NULL, 246, 'KF-Costm-2050', 'Elsa Tutu Gown Long Sleeves ', NULL, '52.0000', '0.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '2017-05-14', 'received', '52.0000', '52.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(386, NULL, NULL, 246, 'KF-Costm-2050', 'Elsa Tutu Gown Long Sleeves ', NULL, '52.0000', '0.0000', 3, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '2017-05-14', 'received', '52.0000', '52.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(408, NULL, NULL, 162, '', '', 2, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '5.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(409, NULL, NULL, 162, '', '', 1, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '5.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(410, NULL, NULL, 162, '', '', 3, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '5.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(411, NULL, NULL, 162, '', '', 4, '0.0000', '3.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '3.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(412, NULL, NULL, 162, '', '', 5, '0.0000', '3.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '3.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(413, NULL, NULL, 246, '', '', 6, '0.0000', '3.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '3.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(414, NULL, NULL, 246, '', '', 7, '0.0000', '3.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '3.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(549, NULL, NULL, 207, '105-5016', 'Pink Rainbow Tutu Dress', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '11.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(550, NULL, NULL, 206, '105-5015', 'Purple Rainbow Tutu Dress', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(551, NULL, NULL, 205, '105-5014', 'Green Peacock Tutu', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '12.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(552, NULL, NULL, 204, '105-5013', 'Purple tutu pink flower', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(553, NULL, NULL, 203, '105-5012', '3 flower tutu - Pink', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(554, NULL, NULL, 202, '105-5011', 'Black classic tutu', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(555, NULL, NULL, 460, '301-30002', 'Ajrak - 30002', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(556, NULL, NULL, 461, '301-30003', 'Mustard Base White Square - 30003', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(557, NULL, NULL, 462, '301-30004', 'Black & White Stripesp - 30004', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(558, NULL, NULL, 463, '301-30006', 'Sea Blue Base Red Flowers - 30006', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(559, NULL, NULL, 464, '301-30007', 'Grey Base/Yellow White-30007', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(560, NULL, NULL, 466, '301-30009', 'Solid Brown/White Lining- 30009', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '10.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(561, NULL, NULL, 467, '301-30010', 'White Base Flowers- 30010', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(562, NULL, NULL, 468, '301-30011', 'peach/Grey Square- 30011', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(563, NULL, NULL, 469, '301-30012', 'White Base/Pink Peach- 30012', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '11.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(564, NULL, NULL, 470, '301-30013', 'White Base/Blue Grey-30013', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(565, NULL, NULL, 471, '301-30015', 'Pink Border/Flowers- 30015', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(566, NULL, NULL, 472, '301-30016', 'Purple Border Pink Flowers- 30016', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '10.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(567, NULL, NULL, 473, '301-30017', 'Black Border/Peach and Yellow-30017', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(568, NULL, NULL, 474, '301-30018', 'Navy Blue Border- 30018', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(569, NULL, NULL, 475, '301-30019', 'Blue Circle Stars- 30019', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(570, NULL, NULL, 476, '301-30021', 'Peach Base Red Small Flowers- 30021', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(571, NULL, NULL, 477, '301-30023', 'Peach Blue Stripes-30023', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(572, NULL, NULL, 478, '301-30024', 'Blue Stripes/ White Flowers-30024', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(573, NULL, NULL, 479, '301-30025', 'Peacock- 30025', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(574, NULL, NULL, 480, '301-30027', 'Stripes Border- 30027', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(575, NULL, NULL, 481, '301-30028', 'Yellow Flowers/Circle-30028', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(576, NULL, NULL, 482, '301-30030', 'Blood Red/ Black&White Stripe-30030', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '14.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(577, NULL, NULL, 483, '301-30031', 'Golden Flowers Border- 30031', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(578, NULL, NULL, 488, '301-30036', 'Blue Borders White Flowers-30036', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(579, NULL, NULL, 484, '301-30032', 'Blue Border/White Flowers-30032', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(580, NULL, NULL, 485, '301-30033', 'Green Base Red Flowers-30033', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '8.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(581, NULL, NULL, 486, '301-30034', 'Solid Peach/White Lining-30034', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(582, NULL, NULL, 532, '305-30058', 'Light Peach-300358', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(583, NULL, NULL, 487, '301-30035', 'White Base/ Geen,Grey-30035', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(584, NULL, NULL, 489, '301-30037', 'Multi Lining Bordes/Blue-30037', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(585, NULL, NULL, 490, '301-30039', 'Pink & Blue Flowers-30039', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '10.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(586, NULL, NULL, 491, '301-30041', 'Black & White Shapes-30041', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(587, NULL, NULL, 492, '301-30043', 'Orange Sliced Flowers-30043', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(588, NULL, NULL, 493, '301-30044', 'Blue Circle/White Triangels-30044', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '8.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(589, NULL, NULL, 494, '301-30045', 'Flowers Red White Blue-30045', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(590, NULL, NULL, 495, '301-30046', 'Small flowers/ Red Lines- 30046', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(591, NULL, NULL, 496, '301-30047', 'Orange Base Blue Flowers-30047', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '8.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(592, NULL, NULL, 497, '301-30048', 'Grey Waves- 30048', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '10.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(593, NULL, NULL, 498, '301-30049', 'Black & White Stripes-30049', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '9.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(594, NULL, NULL, 499, '301-30051', 'Blue Square Flowers-30051', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(595, NULL, NULL, 500, '301-30053', 'white Base/ Purple,Grey-30053', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(596, NULL, NULL, 501, '301-30055', 'Blue Flowers Pattern-30055', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(597, NULL, NULL, 502, '301-30056', 'Double Prnt Red/Blue-30056', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-07', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(598, NULL, NULL, 503, '302-30065', 'Peach Base/Dotted Peach-30065', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(599, NULL, NULL, 504, '302-30067', 'Lime Base/ Peach Dotted-30067', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(600, NULL, NULL, 507, '302-30074', 'Purple Base/Peach Dotted-30074', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(601, NULL, NULL, 505, '302-30070', 'Light Golden Square/ Silver Pearl-30070', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(602, NULL, NULL, 508, '302-30075', 'Peach Base/3 Hearts-30075', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(603, NULL, NULL, 509, '302-30076', 'Pink Base/Peach Dotted-30076', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(604, NULL, NULL, 510, '302-30077', 'Lime Base/3 Hearts-30077', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(605, NULL, NULL, 511, '302-30078', 'Light Grey Square/ Pearls-30078', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(606, NULL, NULL, 512, '302-30079', 'Green Base/ 3 Hearts-30079', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(607, NULL, NULL, 513, '302-30080', 'Light Pink Dotted.wave-30080', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(608, NULL, NULL, 514, '302-30081', 'Peach Dotted Wave-30081', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(609, NULL, NULL, 515, '302-30084', 'Greyish Green Circle/ Net Border-300084', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(610, NULL, NULL, 531, '305-30057', 'Brown-30057', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(611, NULL, NULL, 533, '305-30059', 'Dark Blue-30059', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(612, NULL, NULL, 534, '305-30060', 'Camel-30060', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(613, NULL, NULL, 535, '305-30062', 'Sky Blue-30062', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(614, NULL, NULL, 516, '304-30115', 'Red/Multi Color Circle', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(615, NULL, NULL, 517, '304-30116', 'Pink/Multi Color Cirlce', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(616, NULL, NULL, 518, '304-30117', 'Blue/Pink Blue Flowers-30117', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(617, NULL, NULL, 519, '304-30118', 'Dark Maroon/Net Blue Border-30118', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(618, NULL, NULL, 520, '304-30119', 'Yellow/Net Blue Borde-30119', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(619, NULL, NULL, 521, '304-30120', 'Green/ Multi Color Circles-30120', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(620, NULL, NULL, 522, '304-30121', 'Solid Pink/Blue Flowers-30121', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(621, NULL, NULL, 523, '304-30122', 'Grey/Net Green Flower-30122', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(622, NULL, NULL, 524, '304-30123', 'Pink/Net Green Flower-30123', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(623, NULL, NULL, 525, '304-30124', 'Peach/Net Green Flower-30124', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL);
INSERT INTO `sma_purchase_items` (`id`, `purchase_id`, `transfer_id`, `product_id`, `product_code`, `product_name`, `option_id`, `net_unit_cost`, `quantity`, `warehouse_id`, `item_tax`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `expiry`, `subtotal`, `quantity_balance`, `date`, `status`, `unit_cost`, `real_unit_cost`, `quantity_received`, `supplier_part_no`, `purchase_item_id`, `product_unit_id`, `product_unit_code`, `unit_quantity`, `gst`, `cgst`, `sgst`, `igst`) VALUES
(624, NULL, NULL, 526, '304-30125', 'Aqua Dark/Net Aqua Flowers-30125', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(625, NULL, NULL, 527, '304-30126', 'Light Purple/Net Aqua Flowers-30126', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(626, NULL, NULL, 528, '304-30127', 'Pink/ Black Flowers-30127', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(627, NULL, NULL, 529, '304-30128', 'Purple/White Flower-30128', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(628, NULL, NULL, 530, '304-30129', 'Baby Pink/White Flower-30129', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-08', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(629, NULL, NULL, 506, '302-30071', 'Light Pink Square/Silver Pearl-30071', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-10', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(630, NULL, NULL, 465, '301-30008', 'Black Dotted -30008', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '11.0000', '2017-06-10', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(631, NULL, NULL, 574, '302-30131', 'Black/Golden Heart-30131', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-10', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(632, NULL, NULL, 575, '302-30132', 'Grey/Golden Stone Border-30132', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-10', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(633, NULL, NULL, 576, '302-30134', 'Green/Golden Net Border-30134', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-10', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(634, NULL, NULL, 577, '302-30135', 'Cream/Golden Border-30135', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-10', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(635, NULL, NULL, 579, '302-30136', 'Grey Dotted Wave-30136', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-10', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(636, NULL, NULL, 578, '302-30066', 'Brownish Pink - 30066', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-10', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(637, NULL, NULL, 580, '301-30005', 'Army- 30005', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-10', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(638, NULL, NULL, 581, '301-30137', 'Blue Border/Ajrak- 30137', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-10', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(639, NULL, NULL, 582, '301-30138', 'Black Gold Stripes Border-30138', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-10', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(640, NULL, NULL, 583, '301-30139', 'Black Red White Stripes-30139', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '16.0000', '2017-06-10', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(641, NULL, NULL, 584, '301-30140', 'White Base Ajrak-30140', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '8.0000', '2017-06-10', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(642, NULL, NULL, 585, '301-30141', 'Multi Color Stripes', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-10', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(643, NULL, NULL, 554, '312-30089', 'Grey/Pink Butterflies-30089', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(644, NULL, NULL, 555, '312-30090', 'White/Pink Butterflies Flowers-30090', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(645, NULL, NULL, 556, '312-30091', 'Purple Birds Branches-30091', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(646, NULL, NULL, 557, '312-30092', 'Aqua Printed-30092', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(647, NULL, NULL, 558, '312-30093', 'White Music Notes-30093', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(648, NULL, NULL, 559, '312-30094', 'Light Dark Green/Purple Flowers-30094', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(649, NULL, NULL, 560, '312-30095', 'Green City-30095', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(650, NULL, NULL, 561, '312-30096', 'Light Dark Pink/Orange Flowers-30096', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(651, NULL, NULL, 562, '312-30097', 'Pink/Lime Butterflies-30097', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(652, NULL, NULL, 563, '312-30098', 'Peach/ Lime Butteflies-30098', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(653, NULL, NULL, 564, '312-30099', 'Peach/Black Print-30099', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(654, NULL, NULL, 565, '312-30100', 'AquaBlue/Golden Leave-30100', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(655, NULL, NULL, 566, '312-30101', 'Aqua Green/Blue Flowers-30101', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(656, NULL, NULL, 567, '312-30102', 'Blue City-30102', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(657, NULL, NULL, 568, '312-30103', 'Aqua White/Blue Pink Flowers-30103', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(658, NULL, NULL, 569, '312-30104', 'Blue Printed-30104', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(659, NULL, NULL, 570, '312-30105', 'Yellow Red RosesBlue Border-30105', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(660, NULL, NULL, 571, '312-30106', '3Shade/Yellow Peach Purple-30106', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(661, NULL, NULL, 572, '312-30107', 'Pink City-30107', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(662, NULL, NULL, 573, '312-30108', 'Peach Music Notes-30108', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(663, NULL, NULL, 548, '311-30109', 'Maroon/Yellow Blue Branch-30109', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(664, NULL, NULL, 549, '311-30110', 'Skin/Green Purple Branch-30110', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(665, NULL, NULL, 550, '311-30111', 'Blue/Yellow Red Branch-30111', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(666, NULL, NULL, 551, '311-30112', 'Pink/White Cheeta Print-30112', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(667, NULL, NULL, 552, '311-30113', 'Black/Yellow Pink Branch-30113', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(668, NULL, NULL, 553, '311-30114', 'Red/Yellow Green Branch-30114', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(669, NULL, NULL, 586, '306-30142', 'Solid Dark Grey', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(670, NULL, NULL, 588, '306-30144', 'Solid Blue', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(671, NULL, NULL, 589, '306-30145', 'Solid Cream', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '8.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(672, NULL, NULL, 590, '306-30146', 'Solid Dark Brown', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(673, NULL, NULL, 591, '306-30147', 'Solid Grey', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(674, NULL, NULL, 592, '306-30148', 'Solid Camel', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(675, NULL, NULL, 593, '306-30149', 'Solid Maroon', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(676, NULL, NULL, 594, '306-30150', 'Solid Navy Blue', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(677, NULL, NULL, 595, '306-30151', 'Solid Baby Pink', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(678, NULL, NULL, 596, '306-30152', 'Solid Royal Blue', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(679, NULL, NULL, 597, '306-30153', 'Solid White', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-11', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(683, NULL, NULL, 65, '', '', NULL, '0.0000', '0.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(693, NULL, NULL, 66, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(694, NULL, NULL, 70, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(695, NULL, NULL, 73, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(696, NULL, NULL, 74, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(697, NULL, NULL, 75, '', '', NULL, '0.0000', '6.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(698, NULL, NULL, 77, '', '', NULL, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(699, NULL, NULL, 78, '', '', NULL, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(700, NULL, NULL, 83, '', '', NULL, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(701, NULL, NULL, 94, '', '', NULL, '0.0000', '5.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '0.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(749, NULL, NULL, 208, '103-3001', 'Baby Rosetto Onesie - Pink', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '14.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(750, NULL, NULL, 209, '103-3002', 'Baby Rosetto Onesie - Purple', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(751, NULL, NULL, 210, '103-3003', 'Baby Rosetto Onesie - Red', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(752, NULL, NULL, 213, '103-3006', 'Baby Rosetto Onesie - Blue', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(753, NULL, NULL, 215, '103-3008', 'Baby IceCream Onesie - White', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '15.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(754, NULL, NULL, 216, '103-3009', 'Baby Cupcake Onesie - Pink', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '15.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(755, NULL, NULL, 220, '104-4004', 'Raw Satin Purple Gown', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(756, NULL, NULL, 222, '104-4006', 'Peach L.Sleevs Stripe Shldr', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(757, NULL, NULL, 223, '104-4007', 'Red Lace white Satin', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(758, NULL, NULL, 197, '105-5006', 'AQUA BLUE PINK FLOWER', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '12.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(759, NULL, NULL, 199, '105-5008', 'Peacock Tutu Dress', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(760, NULL, NULL, 225, '108-8001', 'Cape Shoulder Bellet', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(761, NULL, NULL, 229, '108-8005', 'Pink Bellet Gold neck cape', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '12.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(762, NULL, NULL, 231, '108-8007', 'Jazz Ballet Red Neck Stone', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(763, NULL, NULL, 235, '108-8011', 'Pink Ballet Gold Bow', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '9.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(764, NULL, NULL, 236, '108-8012', 'Pink Black Crown Ballet', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(765, NULL, NULL, 238, '108-8014', 'Next Stock Yellow Bellet', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '11.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(766, NULL, NULL, 239, '108-8015', 'Half Show Cap Slv Ribbon', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(767, NULL, NULL, 240, '108-8016', 'Stripe Embroidered Pink Ballet', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '12.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(768, NULL, NULL, 241, '108-8017', 'Neck Flowers White Ballet', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '9.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(769, NULL, NULL, 242, '108-8018', 'Neck Stone Frill Slvs Ballet', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(770, NULL, NULL, 168, 'KF-Costm-2007', 'Snow White Gown', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(771, NULL, NULL, 170, 'KF-Costm-2009', 'MERMAID DRESS', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(772, NULL, NULL, 171, 'KF-Costm-2010', 'MERMAID (GREEN&PURPLE COLOR)', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(773, NULL, NULL, 173, 'KF-Costm-2012', 'SOFIAGown WITH GLITTERS', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '15.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(774, NULL, NULL, 174, 'KF-Costm-2013', 'SOFIA SATIN DRESS', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '9.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(775, NULL, NULL, 175, 'KF-Costm-2014', 'Sofia Tutu Dress', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '8.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(776, NULL, NULL, 179, 'KF-Costm-2018', 'TinkerBell Tutu with Wings', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(777, NULL, NULL, 180, 'KF-Costm-2019', 'Pink Fairy Tutu with wings', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(778, NULL, NULL, 181, 'KF-Costm-2020', 'Cape Elsa', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(779, NULL, NULL, 183, 'KF-Costm-2022', 'American Costume tutu', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(780, NULL, NULL, 184, 'KF-Costm-2023', 'Elsa Real Gown', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '9.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(781, NULL, NULL, 187, 'KF-Costm-2026', 'OLAF TUTU DRESS', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '11.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(782, NULL, NULL, 188, 'KF-Costm-2027', 'SUPER GIRL TUTU DRESS', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '8.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(783, NULL, NULL, 190, 'KF-Costm-2029', 'Hello Kitty Tutu Dress', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(784, NULL, NULL, 191, 'KF-Costm-2030', 'Little Pony Tutu Dress', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(785, NULL, NULL, 151, 'KF-FRK-1054', 'Lace Flower Stone Wline - Red', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(786, NULL, NULL, 139, 'KF-FRK-1040', 'RED PARTY DRESS W/ BEADINGS AT CHEST AND RIBBON', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(787, NULL, NULL, 140, 'KF-FRK-1041', 'GLITTER Chest WITH BOW - Pink', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(788, NULL, NULL, 141, 'KF-FRK-1042', 'GLITTER Chest WITH BOW - Blue', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-15', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(789, NULL, NULL, 142, 'KF-FRK-1043', 'GLITTER Chest WITH BOW - Beige', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(790, NULL, NULL, 143, 'KF-FRK-1044', 'RED PARTY DRESS WITH SEQUENCE AND FLOWER', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(791, NULL, NULL, 144, 'KF-FRK-1045', 'Baby Gown with Small beading at chest - Hotpink', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(792, NULL, NULL, 148, 'KF-FRK-1049', 'Toddler Embelished Organza gown - Green', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(793, NULL, NULL, 149, 'KF-FRK-1050', 'Toddler Lace Gown - Purple', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(794, NULL, NULL, 150, 'KF-FRK-1053', 'Lace Flower, Stone Wline - Hpink', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(795, NULL, NULL, 153, 'KF-FRK-1056', 'White Frock with Pink big Flowers at chest', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(796, NULL, NULL, 155, 'KF-FRK-1058', 'Jacuard Printed Princess Gown with pearl waist', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(797, NULL, NULL, 156, 'KF-FRK-1059', 'Printed Bird Gown', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(798, NULL, NULL, 157, 'KF-FRK-1060', 'Naked Chest Chiffon - Pink', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(799, NULL, NULL, 158, 'KF-FRK-1061', 'Layered Chifoon Gown with chest patch - white', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(800, NULL, NULL, 159, 'KF-FRK-1062', 'Naked Chest Chiffon - Skin', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(801, NULL, NULL, 160, 'KF-FRK-1063', 'Half show Lace Chest with beading, seqins - White', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(802, NULL, NULL, 161, 'KF-FRK-1064', 'Half show Lace Chest with beading, seqins - Pink', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(803, NULL, NULL, 201, '105-5010', 'Pink Glitter tutu', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(804, NULL, NULL, 200, '105-5009', 'Orange Blue  tutu old', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(805, NULL, NULL, 198, '105-5007', '3 Flower tutu - Red', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '12.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(806, NULL, NULL, 195, '105-5004', 'YELLOW TUTU DRESS', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(807, NULL, NULL, 194, '105-5003', 'White purple hawayin theme', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(808, NULL, NULL, 193, '105-5002', '3 Chest Flower gold tutu', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(809, NULL, NULL, 192, '105-5001', 'White Ribbon Flower Tutu', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(810, NULL, NULL, 219, '104-4003', 'Satin Peach A-Line', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(811, NULL, NULL, 218, '104-4002', 'White Satin Back cape', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(812, NULL, NULL, 217, '104-4001', 'White Lace Skin Flower', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(813, NULL, NULL, 189, 'KF-Costm-2028', 'BATGIRL TUTU DRESS', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(814, NULL, NULL, 186, 'KF-Costm-2025', 'ELSA BABY DRESS', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(815, NULL, NULL, 185, 'KF-Costm-2024', 'ANNA TUTU DRESS', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(816, NULL, NULL, 178, 'KF-Costm-2017', 'Pumpkin Tutu', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(817, NULL, NULL, 177, 'KF-Costm-2016', 'Minnie Mouse Tutu', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(818, NULL, NULL, 176, 'KF-Costm-2015', 'Mickey Mouse tutu', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(819, NULL, NULL, 172, 'KF-Costm-2011', 'Mermaid Tutu dress', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(820, NULL, NULL, 169, 'KF-Costm-2008', 'MERMAID BABY DRESS', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(821, NULL, NULL, 167, 'KF-Costm-2006', 'CINDERELLA- Gown Pic', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(822, NULL, NULL, 164, 'KF-Costm-2003', 'Repunzel TUTU DRESS', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(823, NULL, NULL, 163, 'KF-Costm-2002', 'Arora WITH WHITE COLAR', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(824, NULL, NULL, 237, '108-8013', 'Princess Ballet pink Puple', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(825, NULL, NULL, 233, '108-8009', 'Jazz Ballet Dance Ribbon', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(826, NULL, NULL, 230, '108-8006', 'Elsa Bellet Glitter', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(827, NULL, NULL, 228, '108-8004', 'white Bellet Silver neck cape', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(828, NULL, NULL, 227, '108-8003', 'white Bellet Gold neck cape', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(829, NULL, NULL, 226, '108-8002', 'Blue  Glitters Neck Ballet', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(830, NULL, NULL, 396, '108-8019', 'Leotards', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(831, NULL, NULL, 397, '109-9001', 'RUFFLE TOP', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(832, NULL, NULL, 398, '109-9002', 'NET FLOWER DESIGN LONGSLEEVE TOP', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(833, NULL, NULL, 399, '109-9003', 'Cotton Tops', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(834, NULL, NULL, 400, '109-9005', 'Pink Cup Cake', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(835, NULL, NULL, 401, '109-9006', 'Grey Dear', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(836, NULL, NULL, 402, '109-9007', 'Sky Blue Good Night', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(837, NULL, NULL, 403, '109-9008', 'White Love Gold', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(838, NULL, NULL, 404, '109-9009', 'Solid Pink Red Heart', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(839, NULL, NULL, 406, '109-9011', 'Pink Frozen', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(840, NULL, NULL, 387, '107-7045', 'Petti Skirt -  Lime Green', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(841, NULL, NULL, 388, '107-7041', 'Petti Skirt- Blue & Green Ruffle', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(842, NULL, NULL, 389, '107-7092', 'Petti Skirt- Lime Green & Pink Ruffle', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(843, NULL, NULL, 390, '107-7089', 'Petti Skirt- Golden Yellow', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(844, NULL, NULL, 391, '107-7095', 'Petti Skirt- Orange & Blue Ruffle', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(845, NULL, NULL, 392, '107-7063', 'Petti Skirt- Pink with Heart', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(846, NULL, NULL, 393, '107-7140', 'Petti Skirt- Blue with Red Chevron', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(847, NULL, NULL, 394, '107-7301', 'Petti Skirt- UAE Color', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(848, NULL, NULL, 395, '107-7302', 'Petti Skirt- X-mass Color', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(849, NULL, NULL, 395, '107-7302', 'Petti Skirt- X-mass Color', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(850, NULL, NULL, 394, '107-7301', 'Petti Skirt- UAE Color', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(851, NULL, NULL, 393, '107-7140', 'Petti Skirt- Blue with Red Chevron', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(852, NULL, NULL, 392, '107-7063', 'Petti Skirt- Pink with Heart', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(853, NULL, NULL, 391, '107-7095', 'Petti Skirt- Orange & Blue Ruffle', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(854, NULL, NULL, 390, '107-7089', 'Petti Skirt- Golden Yellow', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(855, NULL, NULL, 389, '107-7092', 'Petti Skirt- Lime Green & Pink Ruffle', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(856, NULL, NULL, 388, '107-7041', 'Petti Skirt- Blue & Green Ruffle', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(857, NULL, NULL, 387, '107-7045', 'Petti Skirt -  Lime Green', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '8.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(858, NULL, NULL, 406, '109-9011', 'Pink Frozen', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(859, NULL, NULL, 404, '109-9009', 'Solid Pink Red Heart', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(860, NULL, NULL, 403, '109-9008', 'White Love Gold', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(861, NULL, NULL, 402, '109-9007', 'Sky Blue Good Night', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(862, NULL, NULL, 401, '109-9006', 'Grey Dear', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(863, NULL, NULL, 400, '109-9005', 'Pink Cup Cake', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '14.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(864, NULL, NULL, 399, '109-9003', 'Cotton Tops', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(865, NULL, NULL, 398, '109-9002', 'NET FLOWER DESIGN LONGSLEEVE TOP', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(866, NULL, NULL, 397, '109-9001', 'RUFFLE TOP', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '10.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(867, NULL, NULL, 453, '204-2042', 'spiderman black', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(868, NULL, NULL, 425, '201-2001', 'Shoe Tshirt Set', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(869, NULL, NULL, 426, '201-2002', 'Lining Tshirt Set', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(870, NULL, NULL, 427, '201-2003', 'Zebra Shirt Set', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(871, NULL, NULL, 428, '201-2004', 'Dolphin Shirt Set', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(872, NULL, NULL, 429, '201-2005', 'Green Shirt Set', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(873, NULL, NULL, 430, '201-2006', 'Digital Lining Shirt Set', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(874, NULL, NULL, 431, '201-2007', 'BlueRed long pants', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(875, NULL, NULL, 432, '201-2008', 'US Flag Shirt Set', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(876, NULL, NULL, 434, '201-2010', 'SpiderMan Tshirt', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(877, NULL, NULL, 435, '201-2011', 'RedStripes Long Pants', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(878, NULL, NULL, 436, '201-2012', 'Black Shoe Tshirt Set', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(879, NULL, NULL, 437, '201-2013', 'Floral Printed Long Pants', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL);
INSERT INTO `sma_purchase_items` (`id`, `purchase_id`, `transfer_id`, `product_id`, `product_code`, `product_name`, `option_id`, `net_unit_cost`, `quantity`, `warehouse_id`, `item_tax`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `expiry`, `subtotal`, `quantity_balance`, `date`, `status`, `unit_cost`, `real_unit_cost`, `quantity_received`, `supplier_part_no`, `purchase_item_id`, `product_unit_id`, `product_unit_code`, `unit_quantity`, `gst`, `cgst`, `sgst`, `igst`) VALUES
(880, NULL, NULL, 438, '201-2014', 'Chek Cat Tshrt Set', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(881, NULL, NULL, 439, '201-2015', 'Wang Red Tshrt Set', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(882, NULL, NULL, 440, '201-2016', 'Grey Face Blck Tshrt Set', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(883, NULL, NULL, 441, '202-2017', 'Solid Camel 3 Pcs Suit', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(884, NULL, NULL, 442, '202-2018', 'Solid Black 3 Pcs Suit', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(885, NULL, NULL, 443, '202-2019', 'SmallBox Brown Velvet 3 Pcs Suit', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '11.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(886, NULL, NULL, 444, '202-2020', 'BigBox Brown Velvet 3 Pcs Suit', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '9.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(887, NULL, NULL, 445, '202-2021', 'Pin Dotted Grey 3 Pcs Suit', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(888, NULL, NULL, 446, '202-2022', 'Solid Grey 3 Pcs Suit', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(889, NULL, NULL, 448, '202-2024', 'SmallBox Camel Velvet 3 Pcs Suit', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(890, NULL, NULL, 449, '204-2037', 'Blue Blazer (FORMAL)', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(891, NULL, NULL, 450, '204-2038', 'Yellow Balzer (FORMAL)', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(892, NULL, NULL, 451, '204-2040', 'superman blue', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(893, NULL, NULL, 452, '204-2041', 'spiderman red', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(894, NULL, NULL, 454, '204-2043', 'olaf hoddie blue', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(895, NULL, NULL, 455, '204-2044', 'olaf zipper', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(896, NULL, NULL, 456, '204-2045', 'olaf bike', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '8.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(897, NULL, NULL, 457, '204-2046', 'macquinne zipper', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(898, NULL, NULL, 458, '204-2047', 'raindeer zipper', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '9.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(899, NULL, NULL, 459, '204-2048', 'olaf hoddie yellow', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(900, NULL, NULL, 374, '203-2025', 'BATMAN BLACK', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(901, NULL, NULL, 375, '203-2026', 'BATMAN GRAY', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '13.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(902, NULL, NULL, 376, '203-2027', 'SUPERMAN', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(903, NULL, NULL, 378, '203-2029', 'ROBIN HOOD', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(904, NULL, NULL, 379, '203-2030', 'TRANSFORMER', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(905, NULL, NULL, 380, '203-2031', 'IRON MAN', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(906, NULL, NULL, 381, '203-2032', 'SPIDERMAN BLACK', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(907, NULL, NULL, 382, '203-2033', 'HULK', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(908, NULL, NULL, 383, '203-2034', 'VOLT', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(909, NULL, NULL, 384, '203-2035', 'CAPTAIN AMERICA', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '6.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(910, NULL, NULL, 385, '203-2036', 'INCREDIBLE', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-16', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(911, NULL, NULL, 602, '101-1066', 'Pink Flower Chest', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-19', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(912, NULL, NULL, 603, '101-1067', 'Red Flower Pearl Chest', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-06-19', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(913, NULL, NULL, 600, '202-2051', 'Light White Box', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-19', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(914, NULL, NULL, 598, '202-2049', 'Solid Cream', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '7.0000', '2017-06-19', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(915, NULL, NULL, 599, '202-2050', 'Grey White Lines', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-19', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(916, NULL, NULL, 121, 'KP-GSPD017', 'Vintage Printed Summer - Hot Pink', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-28', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(917, NULL, NULL, 115, 'KP-POPS14', 'PomPom PettiSet - Gold Dots', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-28', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(918, NULL, NULL, 123, 'KP-GPDB08', 'Gold Dot Bloomer Set - Pink', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-28', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(919, NULL, NULL, 128, 'KP-POMP05', 'PomPom Jumpsuit - Black', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-28', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(920, NULL, NULL, 113, 'KP-POPS06', 'PomPom PettiSet', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-28', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(921, NULL, NULL, 126, 'KP-POMP02', 'PomPom Jumpsuit - L.Blue', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '0.0000', '2017-06-28', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(922, NULL, NULL, 122, 'KP-GPDB06', 'Gold Dot Bloomer Set - Black', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-06-28', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(923, NULL, NULL, 112, 'KP-POPS05', 'PomPom PettiSet', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-28', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(924, NULL, NULL, 114, 'KP-POPS13', 'PomPom PettiSet', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-28', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(925, NULL, NULL, 124, 'KP-GPDB10', 'Gold Dot Bloomer Set - Purple', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '2.0000', '2017-06-28', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(926, NULL, NULL, 129, 'KP-POMP06', 'PomPom Jumpsuit - Pink', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-06-29', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(927, NULL, NULL, 606, '104-4008', 'purple/Purple Perals Chest', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-07-01', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(928, NULL, NULL, 607, '104-4009', 'Baby Pink/Pink Pearls Chest Sleeves', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-07-01', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(929, NULL, NULL, 608, '104-4010', 'Purple Flowers Printed/Sleveless', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-07-01', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(930, NULL, NULL, 609, '104-4011', 'PeachPearls/Bow Belt Sleevless', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-07-01', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(931, NULL, NULL, 610, '104-4012', 'White Printed Flower chest/White Pearls Border', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-07-01', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(932, NULL, NULL, 611, '104-4013', 'White Neckless/White Pearls Chest', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-07-01', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(933, NULL, NULL, 612, '104-4014', 'Peach Neckless/Peach Pearls Shoulders', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-07-01', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(934, NULL, NULL, 613, '104-4015', 'White/White Bow Pearls Belt', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-07-01', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(935, NULL, NULL, 614, '102-2032', 'Princess Belle', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-07-02', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(936, NULL, NULL, 615, '105-5017', 'Purple/Purple Flowers Chest', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '5.0000', '2017-07-02', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(937, NULL, NULL, 616, '102-2033', 'Sofia with Sequin Skirt', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-07-02', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(938, NULL, NULL, 617, '101-1068', 'ARAB DRESS', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '4.0000', '2017-07-02', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(939, NULL, NULL, 618, '101-1069', 'RAINBOW SUMMER DRESS', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-07-02', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(940, NULL, NULL, 619, '101-1070', 'PEACH & WHITE PRINTED FLOWER DRESS', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '1.0000', '2017-07-02', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(941, NULL, NULL, 620, '101-1071', 'RED COLOR DRESS W/ GOLD SEQUENCE COLAR', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-07-02', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(942, NULL, NULL, 621, '101-1072', 'MICKEY MOUSE COTTON DRESS', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-07-02', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(943, NULL, NULL, 622, '101-1073', 'Cotton Bud Onesie Dress', NULL, '0.0000', '0.0000', 2, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '3.0000', '2017-07-02', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(944, NULL, NULL, 51, '11534518', 'Chest Dropping Lace patch Gown with Sequins - Marron', 11, '41.0000', '7.0000', 1, '0.0000', 1, '0.0000', NULL, NULL, NULL, '287.0000', '5.0000', '2017-10-15', 'received', '41.0000', NULL, '7.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(945, NULL, NULL, 51, '11534518', 'Chest Dropping Lace patch Gown with Sequins - Marron', 12, '41.0000', '3.0000', 2, '0.0000', 1, '0.0000', NULL, NULL, NULL, '123.0000', '2.0000', '2017-10-16', 'received', '41.0000', NULL, '3.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(946, NULL, NULL, 748, '62527578', 'Michael Bennett', NULL, '350.0000', '0.0000', 1, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2018-04-02', 'received', '350.0000', '350.0000', '0.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(947, NULL, NULL, 748, '62527578', 'Michael Bennett', NULL, '350.0000', '0.0000', 2, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2018-04-02', 'received', '350.0000', '350.0000', '0.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(948, NULL, NULL, 748, '62527578', 'Michael Bennett', NULL, '350.0000', '0.0000', 3, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2018-04-02', 'received', '350.0000', '350.0000', '0.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(949, NULL, NULL, 749, '60026189', 'Michael Sheet', NULL, '350.0000', '0.0000', 1, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2018-04-02', 'received', '350.0000', '350.0000', '0.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(950, NULL, NULL, 749, '60026189', 'Michael Sheet', NULL, '350.0000', '0.0000', 2, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2018-04-02', 'received', '350.0000', '350.0000', '0.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(951, NULL, NULL, 749, '60026189', 'Michael Sheet', NULL, '350.0000', '0.0000', 3, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2018-04-02', 'received', '350.0000', '350.0000', '0.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(952, NULL, NULL, 750, '87723642', 'Bennett Michael ', NULL, '350.0000', '0.0000', 1, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2018-04-02', 'received', '350.0000', '350.0000', '0.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(953, NULL, NULL, 750, '87723642', 'Bennett Michael ', NULL, '350.0000', '0.0000', 2, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2018-04-02', 'received', '350.0000', '350.0000', '0.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(954, NULL, NULL, 750, '87723642', 'Bennett Michael ', NULL, '350.0000', '0.0000', 3, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2018-04-02', 'received', '350.0000', '350.0000', '0.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(955, NULL, NULL, 751, '80930914', 'Bennett Dpem ', NULL, '350.0000', '0.0000', 1, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '-1.0000', '2018-04-02', 'received', '350.0000', '350.0000', '0.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(956, NULL, NULL, 751, '80930914', 'Bennett Dpem ', NULL, '350.0000', '0.0000', 2, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2018-04-02', 'received', '350.0000', '350.0000', '0.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(957, NULL, NULL, 751, '80930914', 'Bennett Dpem ', NULL, '350.0000', '0.0000', 3, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2018-04-02', 'received', '350.0000', '350.0000', '0.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(958, NULL, NULL, 752, '68325902', 'Ambush Jekson', NULL, '350.0000', '0.0000', 1, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2018-04-02', 'received', '350.0000', '350.0000', '0.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(959, NULL, NULL, 752, '68325902', 'Ambush Jekson', NULL, '350.0000', '0.0000', 2, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2018-04-02', 'received', '350.0000', '350.0000', '0.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(960, NULL, NULL, 752, '68325902', 'Ambush Jekson', NULL, '350.0000', '0.0000', 3, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', '2018-04-02', 'received', '350.0000', '350.0000', '0.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(961, NULL, NULL, 749, '60026189', 'Michael Sheet', 1, '350.0000', '4.0000', 1, '0.0000', 1, '0.0000', NULL, NULL, NULL, '1400.0000', '3.0000', '2018-04-03', 'received', '350.0000', NULL, '4.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(962, NULL, NULL, 749, '60026189', 'Michael Sheet', 2, '350.0000', '5.0000', 3, '0.0000', 1, '0.0000', NULL, NULL, NULL, '1750.0000', '5.0000', '2018-04-03', 'received', '350.0000', NULL, '5.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(963, NULL, NULL, 719, '', '', NULL, '0.0000', '0.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '-1.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(964, NULL, NULL, 719, '02646516', 'Ambush (Michael Bennett)', 3, '350.0000', '2.0000', 1, '0.0000', 1, '0.0000', NULL, NULL, NULL, '700.0000', '2.0000', '2018-04-06', 'received', '350.0000', NULL, '2.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL),
(965, NULL, NULL, 719, '02646516', 'Ambush (Michael Bennett)', 4, '350.0000', '4.0000', 2, '0.0000', 1, '0.0000', NULL, NULL, NULL, '1400.0000', '4.0000', '2018-04-06', 'received', '350.0000', NULL, '4.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_quotes`
--

CREATE TABLE `sma_quotes` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `internal_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT NULL,
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `supplier` varchar(55) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_quote_items`
--

CREATE TABLE `sma_quote_items` (
  `id` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_returns`
--

CREATE TABLE `sma_returns` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `staff_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT '0.0000',
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_items` smallint(6) DEFAULT NULL,
  `paid` decimal(25,4) DEFAULT '0.0000',
  `surcharge` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_return_items`
--

CREATE TABLE `sma_return_items` (
  `id` int(11) NOT NULL,
  `return_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_sales`
--

CREATE TABLE `sma_sales` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `staff_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT '0.0000',
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `sale_status` varchar(20) DEFAULT NULL,
  `payment_status` varchar(20) DEFAULT NULL,
  `payment_term` tinyint(4) DEFAULT NULL,
  `payment_by` tinytext,
  `redeem_points` tinyint(4) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_items` smallint(6) DEFAULT NULL,
  `pos` tinyint(1) NOT NULL DEFAULT '0',
  `paid` decimal(25,4) DEFAULT '0.0000',
  `return_id` int(11) DEFAULT NULL,
  `surcharge` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL,
  `return_sale_ref` varchar(55) DEFAULT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `return_sale_total` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `rounding` decimal(10,4) DEFAULT NULL,
  `suspend_note` varchar(255) DEFAULT NULL,
  `api` tinyint(1) DEFAULT '0',
  `shop` tinyint(1) DEFAULT '0',
  `address_id` int(11) DEFAULT NULL,
  `reserve_id` int(11) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `is_sync` tinyint(1) NOT NULL DEFAULT '0',
  `local_id` int(11) NOT NULL DEFAULT '0',
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  `card_amonut` varchar(255) DEFAULT NULL,
  `card_number` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_sales`
--

INSERT INTO `sma_sales` (`id`, `date`, `reference_no`, `customer_id`, `customer`, `biller_id`, `biller`, `warehouse_id`, `note`, `staff_note`, `total`, `product_discount`, `order_discount_id`, `total_discount`, `order_discount`, `product_tax`, `order_tax_id`, `order_tax`, `total_tax`, `shipping`, `grand_total`, `sale_status`, `payment_status`, `payment_term`, `payment_by`, `redeem_points`, `due_date`, `created_by`, `updated_by`, `updated_at`, `total_items`, `pos`, `paid`, `return_id`, `surcharge`, `attachment`, `return_sale_ref`, `sale_id`, `return_sale_total`, `rounding`, `suspend_note`, `api`, `shop`, `address_id`, `reserve_id`, `hash`, `is_sync`, `local_id`, `cgst`, `sgst`, `igst`, `card_amonut`, `card_number`) VALUES
(1, '2018-04-05 08:25:20', 'SALE/POS2018/04/0004', 12, 'wisdom', 3, 'Test Biller', 1, '', '5878422801151142', '604.0000', '0.0000', '', '0.0000', '0.0000', '0.0000', 0, '0.0000', '0.0000', '0.0000', '604.0000', 'completed', 'paid', 0, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, '604.0000', NULL, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', NULL, 0, 0, NULL, NULL, '4defeac431d5a05fe1ac8ca5b21ae40ad9924e88c6ac6f75824b30f24a15eb9a', 0, 0, NULL, NULL, NULL, NULL, NULL),
(2, '2018-04-06 11:24:41', 'SALE2018/04/0008', 12, 'wisdom', 3, 'Test Biller', 2, '', NULL, '24559.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 0, '0.0000', '0.0000', '0.0000', '24534.0000', 'pending', 'pending', NULL, 'cod', 1, NULL, 9, NULL, NULL, 2, 0, '0.0000', NULL, '0.0000', NULL, NULL, NULL, '0.0000', NULL, NULL, 0, 1, 3, NULL, '905b0096e40a36b6a842ea3e532a94be76c65a5753ab9b35a5879e0a89ec5338', 0, 0, NULL, NULL, NULL, NULL, NULL),
(3, '2018-04-06 11:31:44', 'SALE/POS2018/04/0005', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '', '', '23960.0000', '0.0000', '', '0.0000', '0.0000', '0.0000', 0, '0.0000', '0.0000', '0.0000', '23960.0000', 'completed', 'paid', 0, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, '23960.0000', NULL, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', NULL, 0, 0, NULL, NULL, '321e60f096daaa7419b1959884dca0698c68e5dd187eae13100035a8f181832d', 0, 0, NULL, NULL, NULL, NULL, NULL),
(4, '2018-04-06 12:13:59', 'SALE2018/04/0009', 12, 'wisdom', 3, 'Test Biller', 2, '', NULL, '23960.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 0, '0.0000', '0.0000', '0.0000', '23940.0000', 'pending', 'pending', NULL, 'cod', NULL, NULL, 9, NULL, NULL, 1, 0, '0.0000', NULL, '0.0000', NULL, NULL, NULL, '0.0000', NULL, NULL, 0, 1, 3, NULL, 'ecdd9e942091a1b6b0748402c9702d11025530e15adb49a03b08294cc9aac145', 0, 0, NULL, NULL, NULL, NULL, NULL),
(5, '2018-04-06 12:23:43', 'SALE2018/04/0010', 12, 'wisdom', 3, 'Test Biller', 2, '', NULL, '47920.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 0, '0.0000', '0.0000', '0.0000', '47880.0000', 'pending', 'pending', NULL, 'cod', NULL, NULL, 9, NULL, NULL, 2, 0, '0.0000', NULL, '0.0000', NULL, NULL, NULL, '0.0000', NULL, NULL, 0, 1, 3, NULL, '2836ca9189f2517a98e9192d9cee3cc9e462890d0e5cc665ef1d0402e37303e5', 0, 0, NULL, NULL, NULL, '40.0000', '2222'),
(6, '2018-04-06 12:45:43', 'SALE2018/04/0011', 12, 'wisdom', 3, 'Test Biller', 2, '', NULL, '47920.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 0, '0.0000', '0.0000', '0.0000', '47920.0000', 'pending', 'pending', NULL, 'cod', NULL, NULL, 9, NULL, NULL, 2, 0, '0.0000', NULL, '0.0000', NULL, NULL, NULL, '0.0000', NULL, NULL, 0, 1, 3, NULL, 'd7429b744341b5cf7e498e04215c2c2e685cb6fd51d0b6052e305c883f511db7', 0, 0, NULL, NULL, NULL, NULL, NULL),
(7, '2018-04-06 15:28:26', 'SALE2018/04/0012', 12, 'wisdom', 3, 'Test Biller', 2, '', NULL, '23960.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 0, '0.0000', '0.0000', '0.0000', '-40.0000', 'pending', 'pending', NULL, 'cod', NULL, NULL, 9, NULL, NULL, 1, 0, '0.0000', NULL, '0.0000', NULL, NULL, NULL, '0.0000', NULL, NULL, 0, 1, 3, NULL, '3e1ff2754225f5c09a9ebfe005f7374e3fe348353a9148abfde55a433d941c8b', 0, 0, NULL, NULL, NULL, '24000.0000', '7777'),
(8, '2018-04-06 15:42:47', 'SALE2018/04/0013', 12, 'wisdom', 3, 'Test Biller', 2, '', NULL, '23960.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 0, '0.0000', '0.0000', '0.0000', '0.0000', 'pending', 'pending', NULL, 'cod', NULL, NULL, 9, NULL, NULL, 1, 0, '0.0000', NULL, '0.0000', NULL, NULL, NULL, '0.0000', NULL, NULL, 0, 1, 3, NULL, '484b53e790a5532b4980980452237d164e243ee3d0778627cb06ce7885e4b1db', 0, 0, NULL, NULL, NULL, '23960.0000', '8888'),
(9, '2018-04-06 16:54:11', 'SALE2018/04/0014', 12, 'wisdom', 3, 'Test Biller', 2, '', NULL, '23972.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 0, '0.0000', '0.0000', '0.0000', '23972.0000', 'pending', 'pending', NULL, 'cod', NULL, NULL, 9, NULL, NULL, 1, 0, '0.0000', NULL, '0.0000', NULL, NULL, NULL, '0.0000', NULL, NULL, 0, 1, 3, NULL, 'c578f8650544dc366891ecbdcfaeb53e383eb170bba7021f8fe6e087564b0a2d', 0, 0, NULL, NULL, NULL, NULL, NULL),
(10, '2018-04-06 17:06:58', 'SALE2018/04/0015', 10, 'Keln', 3, 'Test Biller', 2, '', NULL, '47944.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 0, '0.0000', '0.0000', '0.0000', '47944.0000', 'pending', 'pending', NULL, 'cod', NULL, NULL, 3, NULL, NULL, 2, 0, '0.0000', NULL, '0.0000', NULL, NULL, NULL, '0.0000', NULL, NULL, 0, 1, 1, NULL, '03cb8b8bc440115982dcfa4909a5d72db2a7c69f278edf92f57126bf677446a6', 0, 0, NULL, NULL, NULL, NULL, NULL),
(11, '2018-04-11 12:59:46', 'SALE/POS2018/04/0006', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '', '', '599.0000', '0.0000', '', '0.0000', '0.0000', '0.0000', 0, '0.0000', '0.0000', '0.0000', '599.0000', 'completed', 'paid', 0, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, '599.0000', NULL, '0.0000', NULL, NULL, NULL, '0.0000', '0.0000', NULL, 0, 0, NULL, NULL, 'c3cb2850c8306c380f4b28a4f649058e36dc96d1bd7c64525feef0701033e964', 0, 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_sale_items`
--

CREATE TABLE `sma_sale_items` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `sale_item_id` int(11) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `is_sync` tinyint(1) NOT NULL DEFAULT '0',
  `local_id` int(11) NOT NULL DEFAULT '0',
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_sale_items`
--

INSERT INTO `sma_sale_items` (`id`, `sale_id`, `product_id`, `product_code`, `product_name`, `product_type`, `option_id`, `net_unit_price`, `unit_price`, `quantity`, `warehouse_id`, `item_tax`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `subtotal`, `serial_no`, `real_unit_price`, `sale_item_id`, `product_unit_id`, `product_unit_code`, `unit_quantity`, `comment`, `is_sync`, `local_id`, `gst`, `cgst`, `sgst`, `igst`) VALUES
(1, 1, 749, '60026189', 'Michael Sheet', 'standard', 1, '604.0000', '604.0000', '1.0000', 1, '0.0000', 1, '0', '0', '0.0000', '604.0000', '', '599.0000', NULL, 2, 'Pc1', '1.0000', '', 0, 0, NULL, NULL, NULL, NULL),
(2, 2, 719, '02646516', 'Ambush (Michael Bennett)', 'standard', NULL, '23960.0000', '23960.0000', '1.0000', 2, '0.0000', 1, '0', NULL, '0.0000', '23960.0000', NULL, '23960.0000', NULL, 2, 'Pc1', '1.0000', NULL, 0, 0, NULL, NULL, NULL, NULL),
(3, 2, 752, '68325902', 'Ambush Jekson', 'standard', NULL, '599.0000', '599.0000', '1.0000', 2, '0.0000', 1, '0', NULL, '0.0000', '599.0000', NULL, '599.0000', NULL, 2, 'Pc1', '1.0000', NULL, 0, 0, NULL, NULL, NULL, NULL),
(4, 3, 719, '02646516', 'Ambush (Michael Bennett)', 'standard', NULL, '23960.0000', '23960.0000', '1.0000', 1, '0.0000', 1, '0', '0', '0.0000', '23960.0000', '', '23960.0000', NULL, 2, 'Pc1', '1.0000', '', 0, 0, NULL, NULL, NULL, NULL),
(5, 4, 719, '02646516', 'Ambush (Michael Bennett)', 'standard', NULL, '23960.0000', '23960.0000', '1.0000', 2, '0.0000', 1, '0', NULL, '0.0000', '23960.0000', NULL, '23960.0000', NULL, 2, 'Pc1', '1.0000', NULL, 0, 0, NULL, NULL, NULL, NULL),
(6, 5, 719, '02646516', 'Ambush (Michael Bennett)', 'standard', NULL, '23960.0000', '23960.0000', '1.0000', 2, '0.0000', 1, '0', NULL, '0.0000', '23960.0000', NULL, '23960.0000', NULL, 2, 'Pc1', '1.0000', NULL, 0, 0, NULL, NULL, NULL, NULL),
(7, 5, 719, '02646516', 'Ambush (Michael Bennett)', 'standard', NULL, '23960.0000', '23960.0000', '1.0000', 2, '0.0000', 1, '0', NULL, '0.0000', '23960.0000', NULL, '23960.0000', NULL, 2, 'Pc1', '1.0000', NULL, 0, 0, NULL, NULL, NULL, NULL),
(8, 6, 719, '02646516', 'Ambush (Michael Bennett)', 'standard', NULL, '23960.0000', '23960.0000', '1.0000', 2, '0.0000', 1, '0', NULL, '0.0000', '23960.0000', NULL, '23960.0000', NULL, 2, 'Pc1', '1.0000', NULL, 0, 0, NULL, NULL, NULL, NULL),
(9, 6, 719, '02646516', 'Ambush (Michael Bennett)', 'standard', NULL, '23960.0000', '23960.0000', '1.0000', 2, '0.0000', 1, '0', NULL, '0.0000', '23960.0000', NULL, '23960.0000', NULL, 2, 'Pc1', '1.0000', NULL, 0, 0, NULL, NULL, NULL, NULL),
(10, 7, 719, '02646516', 'Ambush (Michael Bennett)', 'standard', NULL, '23960.0000', '23960.0000', '1.0000', 2, '0.0000', 1, '0', NULL, '0.0000', '23960.0000', NULL, '23960.0000', NULL, 2, 'Pc1', '1.0000', NULL, 0, 0, NULL, NULL, NULL, NULL),
(11, 8, 719, '02646516', 'Ambush (Michael Bennett)', 'standard', NULL, '23960.0000', '23960.0000', '1.0000', 2, '0.0000', 1, '0', NULL, '0.0000', '23960.0000', NULL, '23960.0000', NULL, 2, 'Pc1', '1.0000', NULL, 0, 0, NULL, NULL, NULL, NULL),
(12, 9, 719, '02646516', 'Ambush (Michael Bennett)', 'standard', 3, '23972.0000', '23972.0000', '1.0000', 2, '0.0000', 1, '0', NULL, '0.0000', '23972.0000', NULL, '23972.0000', NULL, 2, 'Pc1', '1.0000', NULL, 0, 0, NULL, NULL, NULL, NULL),
(13, 10, 719, '02646516', 'Ambush (Michael Bennett)', 'standard', 3, '23972.0000', '23972.0000', '1.0000', 2, '0.0000', 1, '0', NULL, '0.0000', '23972.0000', NULL, '23972.0000', NULL, 2, 'Pc1', '1.0000', NULL, 0, 0, NULL, NULL, NULL, NULL),
(14, 10, 719, '02646516', 'Ambush (Michael Bennett)', 'standard', 3, '23972.0000', '23972.0000', '1.0000', 2, '0.0000', 1, '0', NULL, '0.0000', '23972.0000', NULL, '23972.0000', NULL, 2, 'Pc1', '1.0000', NULL, 0, 0, NULL, NULL, NULL, NULL),
(15, 11, 751, '80930914', 'Bennett Dpem', 'standard', NULL, '599.0000', '599.0000', '1.0000', 1, '0.0000', 1, '0', '0', '0.0000', '599.0000', '', '599.0000', NULL, 2, 'Pc1', '1.0000', '', 0, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_sessions`
--

CREATE TABLE `sma_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_sessions`
--

INSERT INTO `sma_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('0lefts9rcov8vvdq5u1jj0v014s8kpe0', '::1', 1523871031, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837303736383b6964656e746974797c733a31383a226b687572616d40776973746563682e62697a223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226b687572616d40776973746563682e62697a223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353233383730323733223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6572726f727c733a3136373a223c68343e343034204e6f7420466f756e64213c2f68343e3c703e546865207061676520796f7520617265206c6f6f6b696e6720666f722063616e206e6f7420626520666f756e642e3c2f703e687474703a2f2f6c6f63616c686f73742f776973646f6d5f706f732f7468656d65732f64656661756c742f73686f702f6173736574732f6373735f656e736f2f666f6e74732f6e6f652d746578742d626f6f6b2d322e776f666632223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226f6c64223b7d),
('0rur6jdovjo047goe994dm3lrhpt4ipq', '::1', 1523871718, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837313731363b6964656e746974797c733a31383a226b687572616d40776973746563682e62697a223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226b687572616d40776973746563682e62697a223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353233383730373935223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313532333837313136323b72656769737465725f69647c733a313a2235223b636173685f696e5f68616e647c733a383a223530302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031372d30392d32362032303a35353a3435223b6572726f727c733a3136373a223c68343e343034204e6f7420466f756e64213c2f68343e3c703e546865207061676520796f7520617265206c6f6f6b696e6720666f722063616e206e6f7420626520666f756e642e3c2f703e687474703a2f2f6c6f63616c686f73742f776973646f6d5f706f732f7468656d65732f64656661756c742f73686f702f6173736574732f6373735f656e736f2f666f6e74732f6e6f652d746578742d626f6f6b2d322e776f666632223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226f6c64223b7d),
('0tpfst7dvqcjug8ir7vbeumm55dimjuh', '::1', 1523866531, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363533313b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('1gh7timjg33da4dl553so1fehb96qm7e', '::1', 1523865616, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353536323b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b6964656e746974797c733a31383a226b687572616d40776973746563682e62697a223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226b687572616d40776973746563682e62697a223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353233383635323334223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313532333836353631363b72656769737465725f69647c733a313a2235223b636173685f696e5f68616e647c733a383a223530302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031372d30392d32362032303a35353a3435223b6572726f727c733a3132343a223c68343e343034204e6f7420466f756e64213c2f68343e3c703e546865207061676520796f7520617265206c6f6f6b696e6720666f722063616e206e6f7420626520666f756e642e3c2f703e687474703a2f2f6c6f63616c686f73742f776973646f6d5f706f732f61646d696e2f736372697074732f6170702e6a73223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226f6c64223b7d),
('24b6f05lma04ludmk216ev63c4jnn4eh', '::1', 1523866316, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363331363b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('2mrd352lg8tj05uug14d6g3lavqga7dm', '::1', 1523871399, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837313333303b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b6964656e746974797c733a31383a226b687572616d40776973746563682e62697a223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226b687572616d40776973746563682e62697a223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353233383731313630223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313532333837313339393b72656769737465725f69647c733a313a2235223b636173685f696e5f68616e647c733a383a223530302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031372d30392d32362032303a35353a3435223b6572726f727c4e3b),
('2s2b30nblvjf1mut8r8c4clo1ranrlki', '::1', 1523870291, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837303239313b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('33grffikvhun5kv63hvuq1au1msbefcr', '::1', 1523866432, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363433323b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('3otl8l5hci0mekp0r9gq7u2d3rp4deh7', '::1', 1523865899, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353839393b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('3vjbcvgpfkureiahgflc3ag95frpik1r', '::1', 1523877943, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837373934333b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('41fu8esqov6gire4clq1qtbbegg9dgej', '::1', 1523866432, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363433323b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('431fetalgppp193vkq5gavikgum6bcad', '::1', 1523870469, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837303436393b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('45jo0hkntuv5hcbt376ptecqesgpnoae', '::1', 1523866369, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363336393b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('4vttff4t60fpmaln45t9tv0mr3c1hjbi', '::1', 1523865963, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353936333b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('56hem5okpnfsr4dg2lr7acupb8svm8um', '::1', 1523865398, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353339383b),
('59g6putbllk5i4b1lp7nlorst77flalg', '::1', 1523866394, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363339343b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('5b0thh8hov6bf0it64l24mftjp6975dg', '::1', 1523871089, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837313038303b6964656e746974797c733a31383a226b687572616d40776973746563682e62697a223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226b687572616d40776973746563682e62697a223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353233383730323733223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6572726f727c733a3136373a223c68343e343034204e6f7420466f756e64213c2f68343e3c703e546865207061676520796f7520617265206c6f6f6b696e6720666f722063616e206e6f7420626520666f756e642e3c2f703e687474703a2f2f6c6f63616c686f73742f776973646f6d5f706f732f7468656d65732f64656661756c742f73686f702f6173736574732f6373735f656e736f2f666f6e74732f6e6f652d746578742d626f6f6b2d322e776f666632223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226f6c64223b7d),
('5dc737tpjcaqht2f4eqlei62ckc2aegs', '::1', 1523865114, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353131343b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('5r7cu57qrp9mbivkb8e3o6t8od5dfn4q', '::1', 1523870291, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837303239313b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('64ajvtinmu4gn1lsfh0dbk4o6nq9kp96', '::1', 1523866533, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363331373b6964656e746974797c733a31383a226b687572616d40776973746563682e62697a223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226b687572616d40776973746563682e62697a223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353233383636313737223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6572726f727c733a3136373a223c68343e343034204e6f7420466f756e64213c2f68343e3c703e546865207061676520796f7520617265206c6f6f6b696e6720666f722063616e206e6f7420626520666f756e642e3c2f703e687474703a2f2f6c6f63616c686f73742f776973646f6d5f706f732f7468656d65732f64656661756c742f73686f702f6173736574732f6373735f656e736f2f666f6e74732f6e6f652d746578742d626f6f6b2d322e776f666632223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226f6c64223b7d),
('68kbv6a9cl2mrr7kn5vdd8drhtk88ksu', '::1', 1523877375, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837373337353b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('6mf22u3v7q7odpd66c11939upb3o7d9i', '::1', 1523865616, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353631363b),
('70cm1t1mb913b8lc430j0mvep4o626na', '::1', 1523877933, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837373933333b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('74pjlsmb1ttpdun50dhuku3jdt6g2seq', '::1', 1523865179, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353137393b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('74ssab80cg80706ml2p6ic80v1kr5ood', '::1', 1523870463, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837303436333b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('8979ah0m6d80igjbq0c76fkv6ktugihc', '::1', 1523865337, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353333373b6572726f727c733a3132333a223c68343e343034204e6f7420466f756e64213c2f68343e3c703e546865207061676520796f7520617265206c6f6f6b696e6720666f722063616e206e6f7420626520666f756e642e3c2f703e687474703a2f2f6c6f63616c686f73742f776973646f6d5f706f732f61646d696e2f6d616e69666573742e6a736f6e223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226e6577223b7d),
('8b35m571lnoqm4tjhg2212pqn146aorc', '::1', 1523866410, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363431303b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('8cnvg4jl13udpm1trjmt9veu1v617855', '::1', 1523872211, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837323033333b6964656e746974797c733a31383a226b687572616d40776973746563682e62697a223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226b687572616d40776973746563682e62697a223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353233383730373935223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313532333837313136323b72656769737465725f69647c733a313a2235223b636173685f696e5f68616e647c733a383a223530302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031372d30392d32362032303a35353a3435223b6572726f727c733a3136373a223c68343e343034204e6f7420466f756e64213c2f68343e3c703e546865207061676520796f7520617265206c6f6f6b696e6720666f722063616e206e6f7420626520666f756e642e3c2f703e687474703a2f2f6c6f63616c686f73742f776973646f6d5f706f732f7468656d65732f64656661756c742f73686f702f6173736574732f6373735f656e736f2f666f6e74732f6e6f652d746578742d626f6f6b2d322e776f666632223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226f6c64223b7d),
('barou77uojil7tiv11cckvi2ovdnbgeo', '::1', 1523865113, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353131333b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('csi58vl57nt7nnurk68mnp5ufq8n47eo', '::1', 1523866411, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363431313b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('ct86fq7o3p716o592e1hgu441ddpbqci', '::1', 1523865982, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353938313b6572726f727c733a3136373a223c68343e343034204e6f7420466f756e64213c2f68343e3c703e546865207061676520796f7520617265206c6f6f6b696e6720666f722063616e206e6f7420626520666f756e642e3c2f703e687474703a2f2f6c6f63616c686f73742f776973646f6d5f706f732f7468656d65732f64656661756c742f73686f702f6173736574732f6373735f656e736f2f666f6e74732f6e6f652d746578742d626f6f6b2d322e776f666632223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226f6c64223b7d),
('dnmfal5p406vbouhg9bvhenp4vn6a82g', '::1', 1523877937, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837373933373b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('e6ff5qshranmp90gu0b3jrhfa3om894m', '::1', 1523870478, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837303437383b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('ee9hskkli54ceobf4p67pcqdfgmvi75g', '::1', 1523866303, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363330333b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('efhmt6fh04cjq9cegu0dupqciemh5ibu', '::1', 1523865980, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353938303b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('ek7qb8q6482dbknt6v1luhdko0ipu92h', '::1', 1523866411, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363431313b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('eutidi1g4e2iif5v6iu3pnce6ddnao54', '::1', 1523870325, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837303332353b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('f175putv8f0a7r2u91o6njge7cf78l6a', '::1', 1523877365, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837373336353b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('fksl68c66m615sjdi2kfgt2v25ja89q2', '::1', 1523865021, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353032313b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('frig44qq3l6ogq5qmr7me2g23udd2ouf', '::1', 1523865196, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353031383b6964656e746974797c733a31383a226b687572616d40776973746563682e62697a223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226b687572616d40776973746563682e62697a223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353233373334383935223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6572726f727c733a3136373a223c68343e343034204e6f7420466f756e64213c2f68343e3c703e546865207061676520796f7520617265206c6f6f6b696e6720666f722063616e206e6f7420626520666f756e642e3c2f703e687474703a2f2f6c6f63616c686f73742f776973646f6d5f706f732f7468656d65732f64656661756c742f73686f702f6173736574732f6373735f656e736f2f666f6e74732f6e6f652d746578742d626f6f6b2d322e776f666632223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226f6c64223b7d),
('gfcbit1ofhpclledivporl2h4h311k5q', '::1', 1523870209, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837303230393b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('i3p3918ov9haf6on2votbh61l5vdd4qm', '::1', 1523866266, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363236363b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('if4u94u557315lsuj2ruf0tgopff10vf', '::1', 1523870325, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837303332353b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('iqq05gdlkqsqkgasf2fndma28440gqtf', '::1', 1523865194, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353139343b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('ira211eel6h088blessa1voj8f7n46au', '::1', 1523865151, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353135313b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('isbrg4rsuv9h73hm4qrqkp9gfn9nnclh', '::1', 1523870210, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837303230393b6964656e746974797c733a31383a226b687572616d40776973746563682e62697a223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226b687572616d40776973746563682e62697a223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353233383636313737223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6572726f727c733a3136373a223c68343e343034204e6f7420466f756e64213c2f68343e3c703e546865207061676520796f7520617265206c6f6f6b696e6720666f722063616e206e6f7420626520666f756e642e3c2f703e687474703a2f2f6c6f63616c686f73742f776973646f6d5f706f732f7468656d65732f64656661756c742f73686f702f6173736574732f6373735f656e736f2f666f6e74732f6e6f652d746578742d626f6f6b2d322e776f666632223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226f6c64223b7d),
('j6g4d25nfs3tpnucjvciq1mfqg6tu8ne', '::1', 1523872040, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837323034303b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('j8p6891p6qjo4qpi6k8nk5iblcd740k4', '::1', 1523865062, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353036323b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('jau5lojja4ierlq2c80bsm5r6p3fv8b0', '::1', 1523865106, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353130363b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('jflp9lgdlqg6i5gf0fm892rcblsb29ij', '::1', 1523865083, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353038333b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('jh1uio3urieib6eampd211969esavuca', '::1', 1523866379, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363337393b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('jptiuggfrl0jhnhrqdfedfbg9bkgb3ab', '::1', 1523866379, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363337393b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('kdj7r2rqk2ne9f54cehki4i65mtuavg6', '::1', 1523872042, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837313934383b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b6964656e746974797c733a31383a226b687572616d40776973746563682e62697a223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226b687572616d40776973746563682e62697a223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353233383731333630223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313532333837313938393b72656769737465725f69647c733a313a2235223b636173685f696e5f68616e647c733a383a223530302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031372d30392d32362032303a35353a3435223b6572726f727c733a3136373a223c68343e343034204e6f7420466f756e64213c2f68343e3c703e546865207061676520796f7520617265206c6f6f6b696e6720666f722063616e206e6f7420626520666f756e642e3c2f703e687474703a2f2f6c6f63616c686f73742f776973646f6d5f706f732f7468656d65732f64656661756c742f73686f702f6173736574732f6373735f656e736f2f666f6e74732f6e6f652d746578742d626f6f6b2d322e776f666632223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226f6c64223b7d),
('kefjhigvbr8hu2ri6r2quhvfs3krkrl9', '::1', 1523871163, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837313133393b6964656e746974797c733a31383a226b687572616d40776973746563682e62697a223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226b687572616d40776973746563682e62697a223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353233383730373935223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313532333837313136323b72656769737465725f69647c733a313a2235223b636173685f696e5f68616e647c733a383a223530302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031372d30392d32362032303a35353a3435223b),
('kvclg41n07m15lic9b64cdi0hgrcca1j', '::1', 1523878034, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837373933343b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b6964656e746974797c733a31383a226b687572616d40776973746563682e62697a223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226b687572616d40776973746563682e62697a223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353233383731333630223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313532333837313938393b72656769737465725f69647c733a313a2235223b636173685f696e5f68616e647c733a383a223530302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031372d30392d32362032303a35353a3435223b),
('m5int4ii73dp8fcnf83e265csk5nctjj', '::1', 1523872040, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837323034303b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('ms4v8soocesullpt3v0ftr5dcfnip0v0', '::1', 1523865616, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353631363b6572726f727c733a3132333a223c68343e343034204e6f7420466f756e64213c2f68343e3c703e546865207061676520796f7520617265206c6f6f6b696e6720666f722063616e206e6f7420626520666f756e642e3c2f703e687474703a2f2f6c6f63616c686f73742f776973646f6d5f706f732f61646d696e2f6d616e69666573742e6a736f6e223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226e6577223b7d),
('oengq1341nadmc27c43icuhnjp32a1vq', '::1', 1523866187, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363138373b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('p5d97k1urd76rgso43b0phvieqm4nqf3', '::1', 1523870469, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837303436393b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('pjardpl6f7rk3gkidd4is2eg87pbbm40', '::1', 1523865964, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353936343b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('q36o5nqhk94b1ri7s60psppgavleeubd', '::1', 1523870478, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837303232363b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b6964656e746974797c733a31383a226b687572616d40776973746563682e62697a223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226b687572616d40776973746563682e62697a223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353233383636333934223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313532333837303238343b72656769737465725f69647c733a313a2235223b636173685f696e5f68616e647c733a383a223530302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031372d30392d32362032303a35353a3435223b),
('qtbvn54eq4tralr575lmr0tqbp79au98', '::1', 1523870284, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837303238343b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('r8nvbvi76lu852hbg7qv5f37u0ddpk2v', '::1', 1523871163, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837313136333b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('rbetdke9q7vlocvte0qqhr2d04a0rtpp', '::1', 1523866266, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363236363b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('rp72haibvi4dg5d6o1v12td18sthjalg', '::1', 1523865398, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353230373b6964656e746974797c733a31383a226b687572616d40776973746563682e62697a223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226b687572616d40776973746563682e62697a223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353233383635313531223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313532333836353339373b72656769737465725f69647c733a313a2235223b636173685f696e5f68616e647c733a383a223530302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031372d30392d32362032303a35353a3435223b6572726f727c733a3132343a223c68343e343034204e6f7420466f756e64213c2f68343e3c703e546865207061676520796f7520617265206c6f6f6b696e6720666f722063616e206e6f7420626520666f756e642e3c2f703e687474703a2f2f6c6f63616c686f73742f776973646f6d5f706f732f61646d696e2f736372697074732f6170702e6a73223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226f6c64223b7d),
('rrjrenbueoi99gubap12hm05ifpiqaou', '::1', 1523865195, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353139353b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('sos9rksqj90c643ivj1i4qi4um262v3c', '::1', 1523866525, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363532353b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('ssgog4ig23eubsdj3lfgi8l7dmg3s0va', '::1', 1523870690, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837303639303b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('u4mjiju8ovhto1a2tm77pl5lgs8hakr4', '::1', 1523865132, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353133323b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('ugth20rl26h90s8oiq754hr6ttue4s7r', '::1', 1523865398, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353339383b6572726f727c733a3132333a223c68343e343034204e6f7420466f756e64213c2f68343e3c703e546865207061676520796f7520617265206c6f6f6b696e6720666f722063616e206e6f7420626520666f756e642e3c2f703e687474703a2f2f6c6f63616c686f73742f776973646f6d5f706f732f61646d696e2f6d616e69666573742e6a736f6e223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226e6577223b7d),
('uma0hvbkjo8389m20ckq6t492tso28mi', '::1', 1523877376, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837373336363b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b6964656e746974797c733a31383a226b687572616d40776973746563682e62697a223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226b687572616d40776973746563682e62697a223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353233383731333630223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313532333837313938393b72656769737465725f69647c733a313a2235223b636173685f696e5f68616e647c733a383a223530302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031372d30392d32362032303a35353a3435223b),
('upg6845r59ahejl7pl3ckcin30480aqq', '::1', 1523871930, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333837313839313b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b6964656e746974797c733a31383a226b687572616d40776973746563682e62697a223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226b687572616d40776973746563682e62697a223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353233383731313630223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313532333837313933303b72656769737465725f69647c733a313a2235223b636173685f696e5f68616e647c733a383a223530302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031372d30392d32362032303a35353a3435223b6572726f727c4e3b),
('urnmkdr12tl5ctbgoftdc60ds9o4at9c', '::1', 1523865103, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353130333b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b),
('v4urvtlrjh703a70ud5htda7iiqpvtpb', '::1', 1523865337, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836353333373b),
('v83760kl9dvaog27ar9ek0aoaiju1lce', '::1', 1523866531, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532333836363533313b7265717565737465645f706167657c733a393a2261646d696e2f706f73223b);

-- --------------------------------------------------------

--
-- Table structure for table `sma_settings`
--

CREATE TABLE `sma_settings` (
  `setting_id` int(1) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `logo2` varchar(255) NOT NULL,
  `site_name` varchar(55) NOT NULL,
  `language` varchar(20) NOT NULL,
  `default_warehouse` int(2) NOT NULL,
  `accounting_method` tinyint(4) NOT NULL DEFAULT '0',
  `default_currency` varchar(3) NOT NULL,
  `default_tax_rate` int(2) NOT NULL,
  `rows_per_page` int(2) NOT NULL,
  `version` varchar(10) NOT NULL DEFAULT '1.0',
  `default_tax_rate2` int(11) NOT NULL DEFAULT '0',
  `dateformat` int(11) NOT NULL,
  `sales_prefix` varchar(20) DEFAULT NULL,
  `quote_prefix` varchar(20) DEFAULT NULL,
  `purchase_prefix` varchar(20) DEFAULT NULL,
  `transfer_prefix` varchar(20) DEFAULT NULL,
  `delivery_prefix` varchar(20) DEFAULT NULL,
  `payment_prefix` varchar(20) DEFAULT NULL,
  `return_prefix` varchar(20) DEFAULT NULL,
  `returnp_prefix` varchar(20) DEFAULT NULL,
  `expense_prefix` varchar(20) DEFAULT NULL,
  `item_addition` tinyint(1) NOT NULL DEFAULT '0',
  `theme` varchar(20) NOT NULL,
  `product_serial` tinyint(4) NOT NULL,
  `default_discount` int(11) NOT NULL,
  `product_discount` tinyint(1) NOT NULL DEFAULT '0',
  `discount_method` tinyint(4) NOT NULL,
  `tax1` tinyint(4) NOT NULL,
  `tax2` tinyint(4) NOT NULL,
  `overselling` tinyint(1) NOT NULL DEFAULT '0',
  `restrict_user` tinyint(4) NOT NULL DEFAULT '0',
  `restrict_calendar` tinyint(4) NOT NULL DEFAULT '0',
  `timezone` varchar(100) DEFAULT NULL,
  `iwidth` int(11) NOT NULL DEFAULT '0',
  `iheight` int(11) NOT NULL,
  `twidth` int(11) NOT NULL,
  `theight` int(11) NOT NULL,
  `watermark` tinyint(1) DEFAULT NULL,
  `reg_ver` tinyint(1) DEFAULT NULL,
  `allow_reg` tinyint(1) DEFAULT NULL,
  `reg_notification` tinyint(1) DEFAULT NULL,
  `auto_reg` tinyint(1) DEFAULT NULL,
  `protocol` varchar(20) NOT NULL DEFAULT 'mail',
  `mailpath` varchar(55) DEFAULT '/usr/sbin/sendmail',
  `smtp_host` varchar(100) DEFAULT NULL,
  `smtp_user` varchar(100) DEFAULT NULL,
  `smtp_pass` varchar(255) DEFAULT NULL,
  `smtp_port` varchar(10) DEFAULT '25',
  `smtp_crypto` varchar(10) DEFAULT NULL,
  `corn` datetime DEFAULT NULL,
  `customer_group` int(11) NOT NULL,
  `default_email` varchar(100) NOT NULL,
  `mmode` tinyint(1) NOT NULL,
  `bc_fix` tinyint(4) NOT NULL DEFAULT '0',
  `auto_detect_barcode` tinyint(1) NOT NULL DEFAULT '0',
  `captcha` tinyint(1) NOT NULL DEFAULT '1',
  `reference_format` tinyint(1) NOT NULL DEFAULT '1',
  `racks` tinyint(1) DEFAULT '0',
  `attributes` tinyint(1) NOT NULL DEFAULT '0',
  `product_expiry` tinyint(1) NOT NULL DEFAULT '0',
  `decimals` tinyint(2) NOT NULL DEFAULT '2',
  `qty_decimals` tinyint(2) NOT NULL DEFAULT '2',
  `decimals_sep` varchar(2) NOT NULL DEFAULT '.',
  `thousands_sep` varchar(2) NOT NULL DEFAULT ',',
  `invoice_view` tinyint(1) DEFAULT '0',
  `default_biller` int(11) DEFAULT NULL,
  `envato_username` varchar(50) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT NULL,
  `rtl` tinyint(1) DEFAULT '0',
  `each_spent` decimal(15,4) DEFAULT NULL,
  `ca_point` tinyint(4) DEFAULT NULL,
  `each_sale` decimal(15,4) DEFAULT NULL,
  `sa_point` tinyint(4) DEFAULT NULL,
  `update` tinyint(1) DEFAULT '0',
  `sac` tinyint(1) DEFAULT '0',
  `display_all_products` tinyint(1) DEFAULT '0',
  `display_symbol` tinyint(1) DEFAULT NULL,
  `symbol` varchar(50) DEFAULT NULL,
  `remove_expired` tinyint(1) DEFAULT '0',
  `barcode_separator` varchar(2) NOT NULL DEFAULT '_',
  `set_focus` tinyint(1) NOT NULL DEFAULT '0',
  `price_group` int(11) DEFAULT NULL,
  `barcode_img` tinyint(1) NOT NULL DEFAULT '1',
  `ppayment_prefix` varchar(20) DEFAULT 'POP',
  `disable_editing` smallint(6) DEFAULT '90',
  `qa_prefix` varchar(55) DEFAULT NULL,
  `update_cost` tinyint(1) DEFAULT NULL,
  `apis` tinyint(1) NOT NULL DEFAULT '0',
  `state` varchar(100) DEFAULT NULL,
  `pdf_lib` varchar(20) DEFAULT 'mpdf'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_settings`
--

INSERT INTO `sma_settings` (`setting_id`, `logo`, `logo2`, `site_name`, `language`, `default_warehouse`, `accounting_method`, `default_currency`, `default_tax_rate`, `rows_per_page`, `version`, `default_tax_rate2`, `dateformat`, `sales_prefix`, `quote_prefix`, `purchase_prefix`, `transfer_prefix`, `delivery_prefix`, `payment_prefix`, `return_prefix`, `returnp_prefix`, `expense_prefix`, `item_addition`, `theme`, `product_serial`, `default_discount`, `product_discount`, `discount_method`, `tax1`, `tax2`, `overselling`, `restrict_user`, `restrict_calendar`, `timezone`, `iwidth`, `iheight`, `twidth`, `theight`, `watermark`, `reg_ver`, `allow_reg`, `reg_notification`, `auto_reg`, `protocol`, `mailpath`, `smtp_host`, `smtp_user`, `smtp_pass`, `smtp_port`, `smtp_crypto`, `corn`, `customer_group`, `default_email`, `mmode`, `bc_fix`, `auto_detect_barcode`, `captcha`, `reference_format`, `racks`, `attributes`, `product_expiry`, `decimals`, `qty_decimals`, `decimals_sep`, `thousands_sep`, `invoice_view`, `default_biller`, `envato_username`, `purchase_code`, `rtl`, `each_spent`, `ca_point`, `each_sale`, `sa_point`, `update`, `sac`, `display_all_products`, `display_symbol`, `symbol`, `remove_expired`, `barcode_separator`, `set_focus`, `price_group`, `barcode_img`, `ppayment_prefix`, `disable_editing`, `qa_prefix`, `update_cost`, `apis`, `state`, `pdf_lib`) VALUES
(1, 'ce1f549c-b781-4812-8d.png', 'ce1f549c-b781-4812-8d1.png', 'University Book House', 'english', 1, 2, 'AED', 1, 10, '3.2.12', 0, 5, 'SALE', 'QUOTE', 'PO', 'TR', 'DO', 'IPAY', 'SR', 'PR', '', 0, 'default', 1, 1, 1, 1, 1, 0, 1, 1, 0, 'Asia/Dubai', 1000, 1000, 61, 80, 0, 0, 0, 0, NULL, 'mail', '/usr/sbin/sendmail', 'pop.gmail.com', 'contact@sma.tecdiary.org', 'jEFTM4T63AiQ9dsidxhPKt9CIg4HQjCN58n/RW9vmdC/UDXCzRLR469ziZ0jjpFlbOg43LyoSmpJLBkcAHh0Yw==', '25', NULL, NULL, 1, 'info@ubh.com', 0, 4, 1, 0, 2, 1, 1, 0, 2, 2, '.', ',', 0, 3, 'wistech', '24171add-5348-46e3-84da-5e15b144df76', 0, '10.0000', 2, NULL, NULL, 1, 0, 0, 0, '', 0, '_', 0, 1, 1, 'POP', 90, '', 1, 0, 'AN', 'mpdf');

-- --------------------------------------------------------

--
-- Table structure for table `sma_shop_settings`
--

CREATE TABLE `sma_shop_settings` (
  `shop_id` int(11) NOT NULL,
  `shop_name` varchar(55) NOT NULL,
  `description` varchar(160) NOT NULL,
  `warehouse` int(11) NOT NULL,
  `biller` int(11) NOT NULL,
  `about_link` varchar(55) NOT NULL,
  `terms_link` varchar(55) NOT NULL,
  `privacy_link` varchar(55) NOT NULL,
  `contact_link` varchar(55) NOT NULL,
  `payment_text` varchar(100) NOT NULL,
  `follow_text` varchar(100) NOT NULL,
  `facebook` varchar(55) NOT NULL,
  `twitter` varchar(55) DEFAULT NULL,
  `google_plus` varchar(55) DEFAULT NULL,
  `instagram` varchar(55) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `cookie_message` varchar(180) DEFAULT NULL,
  `cookie_link` varchar(55) DEFAULT NULL,
  `slider` text,
  `shipping` int(11) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT 'purchase_code',
  `envato_username` varchar(50) DEFAULT 'envato_username',
  `version` varchar(10) DEFAULT '3.2.2',
  `logo` varchar(55) DEFAULT NULL,
  `bank_details` varchar(255) DEFAULT NULL,
  `products_page` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_shop_settings`
--

INSERT INTO `sma_shop_settings` (`shop_id`, `shop_name`, `description`, `warehouse`, `biller`, `about_link`, `terms_link`, `privacy_link`, `contact_link`, `payment_text`, `follow_text`, `facebook`, `twitter`, `google_plus`, `instagram`, `phone`, `email`, `cookie_message`, `cookie_link`, `slider`, `shipping`, `purchase_code`, `envato_username`, `version`, `logo`, `bank_details`, `products_page`) VALUES
(1, 'ENSO Linen Architects', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since...', 2, 3, 'contact-us', 'shipping-policy', 'support', 'contact-us', 'We accept PayPal or you can pay with your credit/debit cards.', 'Please click the link below to follow us on social media.', 'http://facebook.com/ubh', 'http://twitter.com/ubh', 'https://plus.google.com/+google', 'https://www.instagram.com/ubh/', '+971 5 32481', 'info@ubhouse.com', 'We use cookies to improve your experience on our website. By browsing this website, you agree to our use of cookies.', '', '[{"image":"83eecd7d683291f60f361f4a11483365.jpg","link":"","caption":"<h1>Bed & Bath Linen<\\/h1><p>In japanese aesthetics, the ens\\u014d symbolises strength, clarity and elegance.<\\/p><p>  These qualities underpin our approach towards textile production,<\\/p><p>  Product selection, and client servicing.<\\/p>"},{"link":"","caption":"<h1>Bed & Bath Linen<\\/h1><p>In japanese aesthetics, the ens\\u014d symbolises strength, clarity and elegance.  These qualities underpin our approach towards textile production,  Product selection, and client servicing.<\\/p>"},{"link":"","caption":""},{"link":"","caption":""},{"link":"","caption":""}]', 0, '49a359b9-8ce9-4a5b-b7ac-8c9641f9d067', 'wistech', '3.2.13', 'sma-shop.png', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_sizing_info`
--

CREATE TABLE `sma_sizing_info` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `tab1_label` varchar(255) NOT NULL,
  `tab1_description` longtext NOT NULL,
  `tab2_label` varchar(255) DEFAULT NULL,
  `tab2_description` longtext,
  `tab3_label` varchar(255) DEFAULT NULL,
  `tab3_description` longtext,
  `tab4_label` varchar(255) DEFAULT NULL,
  `tab4_description` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sma_sizing_info`
--

INSERT INTO `sma_sizing_info` (`id`, `title`, `tab1_label`, `tab1_description`, `tab2_label`, `tab2_description`, `tab3_label`, `tab3_description`, `tab4_label`, `tab4_description`) VALUES
(1, 'Silk full shirt dress', 'Size Chart', '<p>Once you know your body measurements,consult the Size Chart on the product pages for actual item measurements to determine which size you should purchase.</p>\r\n\r\n<div class="table-responsive">\r\n<table class="table-bordered table-condensed table-hover table-striped">\r\n	<thead>\r\n		<tr>\r\n			<td style="text-align:center">Size</td>\r\n			<td style="text-align:center">Cross Shoulder(cm)</td>\r\n			<td style="text-align:center">Chest Width(cm)</td>\r\n			<td style="text-align:center">Waist Width(cm)</td>\r\n			<td style="text-align:center">Sleeve Length(cm)</td>\r\n			<td style="text-align:center">Skirt length(cm)</td>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style="text-align:center">S</td>\r\n			<td style="text-align:center">37</td>\r\n			<td style="text-align:center">88</td>\r\n			<td style="text-align:center">82</td>\r\n			<td style="text-align:center">54</td>\r\n			<td style="text-align:center">104</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="text-align:center">M</td>\r\n			<td style="text-align:center">38</td>\r\n			<td style="text-align:center">92</td>\r\n			<td style="text-align:center">86</td>\r\n			<td style="text-align:center">55</td>\r\n			<td style="text-align:center">106</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="text-align:center">L</td>\r\n			<td style="text-align:center">39</td>\r\n			<td style="text-align:center">96</td>\r\n			<td style="text-align:center">90</td>\r\n			<td style="text-align:center">56</td>\r\n			<td style="text-align:center">108</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="text-align:center">XL</td>\r\n			<td style="text-align:center">41</td>\r\n			<td style="text-align:center">102</td>\r\n			<td style="text-align:center">96</td>\r\n			<td style="text-align:center">57</td>\r\n			<td style="text-align:center">110</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>\r\n\r\n<p>*These charts are for reference only. Fit may vary depending on the construction, materials and manufacturer.</p>\r\n', 'How to Measure', '<p><strong>How to Measure</strong></p>\r\n\r\n<p>To choose the correct size for you measure your body as follows:</p>\r\n\r\n<p><img alt="shirt measure" src="/ckfinder/userfiles/files/HTB1fT5jQVXXXXb1XpXX760XFXXXQ.png" style="height:353px; width:658px" /></p>\r\n', '', '', '', ''),
(4, 'Pent Shirt standar size', 'how to measuse', '<p>Lorem ipsum dollar mtih&nbsp;</p>\r\n\r\n<p>Hope I can explain this properly</p>\r\n\r\n<p>I am using form validation with Codeigniter. If my form returns a string id back to the controller, how can I use this to perform a database query and use the result array to perform an update...</p>\r\n\r\n<p>Does that make sense?</p>\r\n\r\n<p>I am using form validation with Codeigniter. If my form returns a string id back to the controller, how can I use this to perform a database query and use the result array to perform an update...</p>\r\n\r\n<p>Does that make sense?</p>\r\n', 'Congrats!', '<p>Department of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproduction</p>\r\n', 'Urban Dictionary', '<p>Department of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproduction</p>\r\n', 'Rural Dictionary', '<p>Department of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproductionDepartment of Physiology, Development and Neuroscience &middot; Research &middot; Cellular and systems physiology &middot; Developmental biology and reproduction</p>\r\n'),
(5, 'Size shirts with extra iuox', 'extra ioux', '<p><img alt="" src="/ckfinder/userfiles/files/default.png" style="height:180px; width:250px" /></p>\r\n', 'lowes ioxu', '<p><img alt="" src="/ckfinder/userfiles/files/15b75a2391585e635cfaafb829ec8b15.png" style="height:256px; width:256px" /></p>\r\n', 'fwew oic', '<p><img alt="" src="/ckfinder/userfiles/files/71aabd1634ffd3515a33f4777e501283.png" style="height:256px; width:256px" /></p>\r\n', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sma_skrill`
--

CREATE TABLE `sma_skrill` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT 'Skrill',
  `account_email` varchar(255) NOT NULL DEFAULT 'testaccount2@moneybookers.com',
  `secret_word` varchar(20) NOT NULL DEFAULT 'mbtest',
  `skrill_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_skrill`
--

INSERT INTO `sma_skrill` (`id`, `active`, `name`, `account_email`, `secret_word`, `skrill_currency`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(2, 0, 'Skrill', 'testaccount2@moneybookers.com', 'mbtest', 'USD', '0.0000', '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_stock_counts`
--

CREATE TABLE `sma_stock_counts` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `initial_file` varchar(50) NOT NULL,
  `final_file` varchar(50) DEFAULT NULL,
  `brands` varchar(50) DEFAULT NULL,
  `brand_names` varchar(100) DEFAULT NULL,
  `categories` varchar(50) DEFAULT NULL,
  `category_names` varchar(100) DEFAULT NULL,
  `note` text,
  `products` int(11) DEFAULT NULL,
  `rows` int(11) DEFAULT NULL,
  `differences` int(11) DEFAULT NULL,
  `matches` int(11) DEFAULT NULL,
  `missing` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `finalized` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_stock_counts`
--

INSERT INTO `sma_stock_counts` (`id`, `date`, `reference_no`, `warehouse_id`, `type`, `initial_file`, `final_file`, `brands`, `brand_names`, `categories`, `category_names`, `note`, `products`, `rows`, `differences`, `matches`, `missing`, `created_by`, `updated_by`, `updated_at`, `finalized`) VALUES
(9, '2017-06-15 12:27:00', '', 2, 'full', '29dab70e8889b55f5f41f3eed452315a.csv', 'a5f0e79d4ed3832a67e0be7efdcfb818.csv', '', '', '', '', '', 268, 273, 79, 45, 149, 6, 6, '2017-06-15 16:00:27', 1),
(10, '2017-06-24 07:08:00', '', 2, 'partial', '634a17199829dc17e805cf013eae39f7.csv', NULL, '1', 'KiddiFits', '1', 'Kids Fancy Frocks ', NULL, 58, 58, NULL, NULL, NULL, 6, NULL, NULL, NULL),
(11, '2017-06-24 07:15:00', '', 2, 'partial', '2a0381c3a465a143f77c4180fdc364d7.csv', NULL, '1', 'KiddiFits', '1', 'Kids Fancy Frocks ', NULL, 58, 58, NULL, NULL, NULL, 6, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_stock_count_items`
--

CREATE TABLE `sma_stock_count_items` (
  `id` int(11) NOT NULL,
  `stock_count_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_variant` varchar(55) DEFAULT NULL,
  `product_variant_id` int(11) DEFAULT NULL,
  `expected` decimal(15,4) NOT NULL,
  `counted` decimal(15,4) NOT NULL,
  `cost` decimal(25,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_stock_count_items`
--

INSERT INTO `sma_stock_count_items` (`id`, `stock_count_id`, `product_id`, `product_code`, `product_name`, `product_variant`, `product_variant_id`, `expected`, `counted`, `cost`) VALUES
(1, 3, 162, '102-2001', 'Elsa Tutu Gown', '2-3 years', 1, '5.0000', '4.0000', '52.0000'),
(2, 3, 162, '102-2001', 'Elsa Tutu Gown', '4-5 years', 2, '5.0000', '4.0000', '52.0000'),
(3, 3, 162, '102-2001', 'Elsa Tutu Gown', '8-9 years', 4, '3.0000', '5.0000', '52.0000'),
(4, 3, 208, '103-3001', 'Baby Rosetto Onesie - Pink', '', NULL, '7.0000', '0.0000', '30.0000'),
(5, 3, 209, '103-3002', 'Baby Rosetto Onesie - Purple', '', NULL, '0.0000', '2.0000', '30.0000'),
(6, 3, 210, '103-3003', 'Baby Rosetto Onesie - Red', '', NULL, '0.0000', '1.0000', '30.0000'),
(7, 3, 213, '103-3006', 'Baby Rosetto Onesie - Blue', '', NULL, '0.0000', '1.0000', '30.0000'),
(8, 3, 215, '103-3008', 'Baby IceCream Onesie - White', '', NULL, '5.0000', '11.0000', '30.0000'),
(9, 3, 216, '103-3009', 'Baby Cupcake Onesie - Pink', '', NULL, '0.0000', '9.0000', '30.0000'),
(10, 3, 220, '104-4004', 'Raw Satin Purple Gown', '', NULL, '1.0000', '0.0000', '90.0000'),
(11, 3, 222, '104-4006', 'Peach L.Sleevs Stripe Shldr', '', NULL, '1.0000', '0.0000', '90.0000'),
(12, 3, 223, '104-4007', 'Red Lace white Satin', '', NULL, '3.0000', '1.0000', '90.0000'),
(13, 3, 197, '105-5006', 'AQUA BLUE PINK FLOWER', '', NULL, '11.0000', '10.0000', '55.0000'),
(14, 3, 199, '105-5008', 'Peacock Tutu Dress', '', NULL, '1.0000', '0.0000', '65.0000'),
(15, 3, 203, '105-5012', '3 flower tutu - Pink', '', NULL, '2.0000', '1.0000', '50.0000'),
(16, 3, 205, '105-5014', 'Green Peacock Tutu', '', NULL, '5.0000', '0.0000', '65.0000'),
(17, 3, 206, '105-5015', 'Purple Rainbow Tutu Dress', '', NULL, '2.0000', '0.0000', '50.0000'),
(18, 3, 207, '105-5016', 'Pink Rainbow Tutu Dress', '', NULL, '4.0000', '1.0000', '50.0000'),
(19, 3, 225, '108-8001', 'Cape Shoulder Bellet', '', NULL, '2.0000', '0.0000', '40.0000'),
(20, 3, 229, '108-8005', 'Pink Bellet Gold neck cape', '', NULL, '7.0000', '1.0000', '40.0000'),
(21, 3, 231, '108-8007', 'Jazz Ballet Red Neck Stone', '', NULL, '1.0000', '0.0000', '40.0000'),
(22, 3, 235, '108-8011', 'Pink Ballet Gold Bow', '', NULL, '5.0000', '1.0000', '40.0000'),
(23, 3, 236, '108-8012', 'Pink Black Crown Ballet', '', NULL, '2.0000', '1.0000', '40.0000'),
(24, 3, 238, '108-8014', 'Next Stock Yellow Bellet', '', NULL, '8.0000', '5.0000', '40.0000'),
(25, 3, 239, '108-8015', 'Half Show Cap Slv Ribbon', '', NULL, '6.0000', '5.0000', '40.0000'),
(26, 3, 240, '108-8016', 'Stripe Embroidered Pink Ballet', '', NULL, '11.0000', '10.0000', '40.0000'),
(27, 3, 241, '108-8017', 'Neck Flowers White Ballet', '', NULL, '6.0000', '3.0000', '40.0000'),
(28, 3, 242, '108-8018', 'Neck Stone Frill Slvs Ballet', '', NULL, '1.0000', '0.0000', '40.0000'),
(29, 3, 1, '11534516', 'Chest Dropping Floral Net Gown - Skin', '', NULL, '6.0000', '1.0000', '43.0000'),
(30, 3, 50, '11534517', 'Chest Dropping Floral Net Gown - Purple', '', NULL, '6.0000', '5.0000', '43.0000'),
(334, 9, 162, '102-2001', 'Elsa Tutu Gown', '2-3 years', 1, '5.0000', '4.0000', '52.0000'),
(335, 9, 162, '102-2001', 'Elsa Tutu Gown', '4-5 years', 2, '5.0000', '4.0000', '52.0000'),
(336, 9, 162, '102-2001', 'Elsa Tutu Gown', '8-9 years', 4, '3.0000', '5.0000', '52.0000'),
(337, 9, 208, '103-3001', 'Baby Rosetto Onesie - Pink', '', NULL, '7.0000', '0.0000', '30.0000'),
(338, 9, 209, '103-3002', 'Baby Rosetto Onesie - Purple', '', NULL, '0.0000', '2.0000', '30.0000'),
(339, 9, 210, '103-3003', 'Baby Rosetto Onesie - Red', '', NULL, '0.0000', '1.0000', '30.0000'),
(340, 9, 213, '103-3006', 'Baby Rosetto Onesie - Blue', '', NULL, '0.0000', '1.0000', '30.0000'),
(341, 9, 215, '103-3008', 'Baby IceCream Onesie - White', '', NULL, '5.0000', '11.0000', '30.0000'),
(342, 9, 216, '103-3009', 'Baby Cupcake Onesie - Pink', '', NULL, '0.0000', '9.0000', '30.0000'),
(343, 9, 220, '104-4004', 'Raw Satin Purple Gown', '', NULL, '1.0000', '0.0000', '90.0000'),
(344, 9, 222, '104-4006', 'Peach L.Sleevs Stripe Shldr', '', NULL, '1.0000', '0.0000', '90.0000'),
(345, 9, 223, '104-4007', 'Red Lace white Satin', '', NULL, '3.0000', '1.0000', '90.0000'),
(346, 9, 197, '105-5006', 'AQUA BLUE PINK FLOWER', '', NULL, '11.0000', '10.0000', '55.0000'),
(347, 9, 199, '105-5008', 'Peacock Tutu Dress', '', NULL, '1.0000', '0.0000', '65.0000'),
(348, 9, 203, '105-5012', '3 flower tutu - Pink', '', NULL, '2.0000', '1.0000', '50.0000'),
(349, 9, 205, '105-5014', 'Green Peacock Tutu', '', NULL, '5.0000', '0.0000', '65.0000'),
(350, 9, 206, '105-5015', 'Purple Rainbow Tutu Dress', '', NULL, '2.0000', '0.0000', '50.0000'),
(351, 9, 207, '105-5016', 'Pink Rainbow Tutu Dress', '', NULL, '4.0000', '1.0000', '50.0000'),
(352, 9, 225, '108-8001', 'Cape Shoulder Bellet', '', NULL, '2.0000', '0.0000', '40.0000'),
(353, 9, 229, '108-8005', 'Pink Bellet Gold neck cape', '', NULL, '7.0000', '1.0000', '40.0000'),
(354, 9, 231, '108-8007', 'Jazz Ballet Red Neck Stone', '', NULL, '1.0000', '0.0000', '40.0000'),
(355, 9, 235, '108-8011', 'Pink Ballet Gold Bow', '', NULL, '5.0000', '1.0000', '40.0000'),
(356, 9, 236, '108-8012', 'Pink Black Crown Ballet', '', NULL, '2.0000', '1.0000', '40.0000'),
(357, 9, 238, '108-8014', 'Next Stock Yellow Bellet', '', NULL, '8.0000', '5.0000', '40.0000'),
(358, 9, 239, '108-8015', 'Half Show Cap Slv Ribbon', '', NULL, '6.0000', '5.0000', '40.0000'),
(359, 9, 240, '108-8016', 'Stripe Embroidered Pink Ballet', '', NULL, '11.0000', '10.0000', '40.0000'),
(360, 9, 241, '108-8017', 'Neck Flowers White Ballet', '', NULL, '6.0000', '3.0000', '40.0000'),
(361, 9, 242, '108-8018', 'Neck Stone Frill Slvs Ballet', '', NULL, '1.0000', '0.0000', '40.0000'),
(362, 9, 1, '11534516', 'Chest Dropping Floral Net Gown - Skin', '', NULL, '6.0000', '1.0000', '43.0000'),
(363, 9, 50, '11534517', 'Chest Dropping Floral Net Gown - Purple', '', NULL, '6.0000', '5.0000', '43.0000'),
(364, 9, 51, '11534518', 'Chest Dropping Lace patch Gown with Sequins - Marron', '', NULL, '6.0000', '4.0000', '41.0000'),
(365, 9, 52, '11534519', 'Chest Dropping Lace patch Gown with Sequins - Purple', '', NULL, '6.0000', '5.0000', '41.0000'),
(366, 9, 53, '11534520', 'Chest Dropping Floral Gown with Pearl - Peach', '', NULL, '6.0000', '1.0000', '41.0000'),
(367, 9, 54, '11534521', 'Chest Floral Gown with naken soulders - Purple', '', NULL, '4.0000', '0.0000', '41.0000'),
(368, 9, 55, '11534522', 'Chest Floral Gown with naken soulders  - Blue', '', NULL, '6.0000', '1.0000', '41.0000'),
(369, 9, 56, '11534523', 'Vintage Printed Chiffon, Self Block', '', NULL, '4.0000', '0.0000', '48.0000'),
(370, 9, 57, '11534524', 'Dropping lace patch, Cape - Pink', '', NULL, '6.0000', '2.0000', '42.0000'),
(371, 9, 59, '11534526', 'Tinker bell Satin Gown', '', NULL, '6.0000', '3.0000', '42.0000'),
(372, 9, 60, '11534527', 'Embelished Chest Gown with pearls', '', NULL, '6.0000', '3.0000', '41.0000'),
(373, 9, 62, '11534529', 'Embroidered Net Gowns - Blue', '', NULL, '4.0000', '0.0000', '40.0000'),
(374, 9, 63, '11534530', 'Dropping Ribbon Flower Net Gowns - Blue', '', NULL, '6.0000', '1.0000', '40.0000'),
(375, 9, 66, '11534533', 'Dropping Ribbon Flower Net Gowns - Red', '', NULL, '0.0000', '6.0000', '40.0000'),
(376, 9, 69, '11534536', 'Embellished top, stone belt - skin', '', NULL, '6.0000', '3.0000', '34.0000'),
(377, 9, 71, '11534538', 'Half layered Sequin gown', '', NULL, '6.0000', '4.0000', '38.0000'),
(378, 9, 72, '11534539', 'lace patch Flower,pearls - Red', '', NULL, '6.0000', '1.0000', '33.0000'),
(379, 9, 74, '11534541', 'lace patch Flower ,pearls - pink', '', NULL, '0.0000', '6.0000', '33.0000'),
(380, 9, 76, '11534543', 'Lace Toddler, L.back, stone -Blue', '', NULL, '5.0000', '3.0000', '33.0000'),
(381, 9, 79, '11534546', 'Toddler L.back, Rbn, White', '', NULL, '6.0000', '4.0000', '36.0000'),
(382, 9, 82, '11534549', 'Cut Glitter Chest with Front Bow - RED', '', NULL, '6.0000', '2.0000', '35.0000'),
(383, 9, 85, '11534552', 'Toddler, lace, RBN Knot, Cream', '', NULL, '5.0000', '4.0000', '33.0000'),
(384, 9, 88, '11534555', 'beads chest,3 ribbon edging, Red', '', NULL, '6.0000', '4.0000', '36.0000'),
(385, 9, 89, '11534556', 'Block plate, Sequin patch- purple', '', NULL, '12.0000', '5.0000', '45.0000'),
(386, 9, 90, '11534557', 'Block plate, Sequin patch - Blue', '', NULL, '6.0000', '5.0000', '45.0000'),
(387, 9, 93, '11534560', 'Anna gown with Cape', '', NULL, '5.0000', '0.0000', '24.0000'),
(388, 9, 95, '11534562', 'Auroura Sleeping Beauty Gown', '', NULL, '5.0000', '0.0000', '29.0000'),
(389, 9, 168, 'KF-Costm-2007', 'Snow White Gown', '', NULL, '1.0000', '0.0000', '40.0000'),
(390, 9, 170, 'KF-Costm-2009', 'MERMAID DRESS', '', NULL, '2.0000', '1.0000', '40.0000'),
(391, 9, 171, 'KF-Costm-2010', 'MERMAID (GREEN&PURPLE COLOR)', '', NULL, '3.0000', '2.0000', '40.0000'),
(392, 9, 173, 'KF-Costm-2012', 'SOFIAGown WITH GLITTERS', '', NULL, '12.0000', '9.0000', '35.0000'),
(393, 9, 174, 'KF-Costm-2013', 'SOFIA SATIN DRESS', '', NULL, '7.0000', '4.0000', '45.0000'),
(394, 9, 175, 'KF-Costm-2014', 'Sofia Tutu Dress', '', NULL, '8.0000', '6.0000', '60.0000'),
(395, 9, 179, 'KF-Costm-2018', 'TinkerBell Tutu with Wings', '', NULL, '7.0000', '8.0000', '65.0000'),
(396, 9, 180, 'KF-Costm-2019', 'Pink Fairy Tutu with wings', '', NULL, '6.0000', '7.0000', '65.0000'),
(397, 9, 181, 'KF-Costm-2020', 'Cape Elsa', '', NULL, '2.0000', '1.0000', '20.0000'),
(398, 9, 183, 'KF-Costm-2022', 'American Costume tutu', '', NULL, '1.0000', '0.0000', '30.0000'),
(399, 9, 184, 'KF-Costm-2023', 'Elsa Real Gown', '', NULL, '5.0000', '4.0000', '30.0000'),
(400, 9, 187, 'KF-Costm-2026', 'OLAF TUTU DRESS', '', NULL, '10.0000', '9.0000', '40.0000'),
(401, 9, 188, 'KF-Costm-2027', 'SUPER GIRL TUTU DRESS', '', NULL, '7.0000', '6.0000', '40.0000'),
(402, 9, 190, 'KF-Costm-2029', 'Hello Kitty Tutu Dress', '', NULL, '5.0000', '3.0000', '50.0000'),
(403, 9, 191, 'KF-Costm-2030', 'Little Pony Tutu Dress', '', NULL, '4.0000', '3.0000', '50.0000'),
(404, 9, 151, 'KF-FRK-1054', 'Lace Flower Stone Wline - Red', '', NULL, '0.0000', '1.0000', '50.0000'),
(405, 9, 102, 'KP-PETS009', 'Baby Pink Petti Skirt - Solid', '', NULL, '10.0000', '3.0000', '30.0000'),
(406, 9, 105, 'KP-PETS010', 'B.Pink Skirt, Rainbow Ruffle', '', NULL, '3.0000', '1.0000', '30.0000'),
(407, 9, 104, 'KP-PETS068', 'Red Petti Skirt - Solid', '', NULL, '5.0000', '0.0000', '30.0000'),
(408, 9, 103, 'KP-PETS073', 'light Purple Pettiskirt - Solid', '', NULL, '3.0000', '2.0000', '30.0000'),
(409, 9, 108, 'KP-PETS148', 'Ash Grey Petti Skirt - Solid', '', NULL, '5.0000', '4.0000', '33.0000'),
(410, 9, 106, 'KP-PETS169', 'Peach Petti Skirt - Solid', '', NULL, '5.0000', '0.0000', '33.0000'),
(411, 9, 109, 'KP-PETS177', 'Skin Petti Tutu Skirt - Solid', '', NULL, '5.0000', '4.0000', '33.0000'),
(412, 9, 107, 'KP-PETS180', 'Pastel Pink Petti Skirt - Solid', '', NULL, '3.0000', '1.0000', '30.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_suspended_bills`
--

CREATE TABLE `sma_suspended_bills` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) DEFAULT NULL,
  `count` int(11) NOT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `suspend_note` varchar(255) DEFAULT NULL,
  `shipping` decimal(15,4) DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_suspended_items`
--

CREATE TABLE `sma_suspended_items` (
  `id` int(11) NOT NULL,
  `suspend_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_tax_rates`
--

CREATE TABLE `sma_tax_rates` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `rate` decimal(12,4) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_tax_rates`
--

INSERT INTO `sma_tax_rates` (`id`, `name`, `code`, `rate`, `type`) VALUES
(1, 'No Tax', 'NT', '0.0000', '2'),
(2, 'VAT @10%', 'VAT10', '10.0000', '1'),
(3, 'GST @6%', 'GST', '6.0000', '1'),
(4, 'VAT @20%', 'VT20', '20.0000', '1');

-- --------------------------------------------------------

--
-- Table structure for table `sma_transfers`
--

CREATE TABLE `sma_transfers` (
  `id` int(11) NOT NULL,
  `transfer_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `from_warehouse_id` int(11) NOT NULL,
  `from_warehouse_code` varchar(55) NOT NULL,
  `from_warehouse_name` varchar(55) NOT NULL,
  `to_warehouse_id` int(11) NOT NULL,
  `to_warehouse_code` varchar(55) NOT NULL,
  `to_warehouse_name` varchar(55) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT NULL,
  `grand_total` decimal(25,4) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `status` varchar(55) NOT NULL DEFAULT 'pending',
  `shipping` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_transfer_items`
--

CREATE TABLE `sma_transfer_items` (
  `id` int(11) NOT NULL,
  `transfer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) DEFAULT NULL,
  `quantity_balance` decimal(15,4) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_translated_category_name`
--

CREATE TABLE `sma_translated_category_name` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `category_arabic_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_translated_product_name`
--

CREATE TABLE `sma_translated_product_name` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_arabic_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_tst`
--

CREATE TABLE `sma_tst` (
  `id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(50) CHARACTER SET utf8 NOT NULL,
  `name` char(255) CHARACTER SET utf8 NOT NULL,
  `translated_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) NOT NULL,
  `alert_quantity` decimal(15,4) DEFAULT '20.0000',
  `image` varchar(255) CHARACTER SET utf8 DEFAULT 'no_image.png',
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `cf1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `cf2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `cf3` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `cf4` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `cf5` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `cf6` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `tax_rate` int(11) DEFAULT NULL,
  `track_quantity` tinyint(1) DEFAULT '1',
  `details` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `warehouse` int(11) DEFAULT NULL,
  `barcode_symbology` varchar(55) CHARACTER SET utf8 NOT NULL DEFAULT 'code128',
  `file` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `product_details` text CHARACTER SET utf8,
  `tax_method` tinyint(1) DEFAULT '0',
  `type` varchar(55) CHARACTER SET utf8 NOT NULL DEFAULT 'standard',
  `supplier1` int(11) DEFAULT NULL,
  `supplier1price` decimal(25,4) DEFAULT NULL,
  `supplier2` int(11) DEFAULT NULL,
  `supplier2price` decimal(25,4) DEFAULT NULL,
  `supplier3` int(11) DEFAULT NULL,
  `supplier3price` decimal(25,4) DEFAULT NULL,
  `supplier4` int(11) DEFAULT NULL,
  `supplier4price` decimal(25,4) DEFAULT NULL,
  `supplier5` int(11) DEFAULT NULL,
  `supplier5price` decimal(25,4) DEFAULT NULL,
  `promotion` tinyint(1) DEFAULT '0',
  `promo_price` decimal(25,4) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `supplier1_part_no` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `supplier2_part_no` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `supplier3_part_no` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `supplier4_part_no` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `supplier5_part_no` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `sale_unit` int(11) DEFAULT NULL,
  `purchase_unit` int(11) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL,
  `slug` varchar(55) CHARACTER SET utf8 DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `weight` decimal(10,4) DEFAULT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '1',
  `free_shipment` tinyint(3) NOT NULL DEFAULT '0',
  `is_updated` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sma_tst`
--

INSERT INTO `sma_tst` (`id`, `code`, `name`, `translated_name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `promotion`, `promo_price`, `start_date`, `end_date`, `supplier1_part_no`, `supplier2_part_no`, `supplier3_part_no`, `supplier4_part_no`, `supplier5_part_no`, `sale_unit`, `purchase_unit`, `brand`, `slug`, `featured`, `weight`, `status`, `free_shipment`, `is_updated`) VALUES
(1, '11534516', 'Chest Dropping Floral Net Gown - Skin', 'الصدر اسقاط الزهور صافي ثوب - الجلد', 1, '43.0000', '169.0000', '0.0000', '6c5c5a1a1191de64098022277389f8f9.png', 1, NULL, '', '', '', '', '', '', '16.0000', NULL, 1, '', NULL, 'code128', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'chest-dropping-floral-net-gown-skin', NULL, NULL, 1, 0, 0),
(50, '11534517', 'Chest Dropping Floral Net Gown - Purple', 'الصدر إسقاط الزهور صافي ثوب - الأرجواني', 1, '43.0000', '169.0000', '2.0000', '7eeeb9f8c8126e2d4f281e1e54c57f67.png', 1, NULL, '', '', '', '', '', '', '13.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'chest-dropping-floral-net-gown-purple', NULL, NULL, 1, 0, 0),
(51, '11534518', 'Chest Dropping Lace patch Gown with Sequins - Marron', 'الصدر إسقاط الدانتيل التصحيح ثوب مع الترتر - مارون', 2, '41.0000', '189.0000', '2.0000', 'd34a015b8211db837e79f4a1a54cb884.jpg', 1, NULL, '', '', '', '', '', '', '21.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '30.0000', '2017-08-10', '2017-08-12', '', NULL, NULL, NULL, NULL, 2, 2, 1, 'chest-dropping-lace-patch-gown-with-sequins-marron', 1, '0.0000', 1, 1, 1),
(52, '11534519', 'Chest Dropping Lace patch Gown with Sequins - Purple', 'الصدر إسقاط الدانتيل التصحيح ثوب مع الترتر - الأرجواني', 1, '41.0000', '189.0000', '2.0000', '9d4eb3c939d86303234ecb79a1381549.png', 1, NULL, '', '', '', '', '', '', '13.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'chest-dropping-lace-patch-gown-with-sequins-purple', 1, NULL, 1, 0, 0),
(53, '11534520', 'Chest Dropping Floral Gown with Pearl - Peach', 'الصدر اسقاط ثوب الزهور مع اللؤلؤ - الخوخ', 1, '41.0000', '169.0000', '2.0000', '19fba35fdb43e5e9e40209a0b5162b02.png', 1, NULL, '', '', '', '', '', '', '11.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'chest-dropping-floral-gown-with-pearl-peach', NULL, NULL, 1, 0, 0),
(54, '11534521', 'Chest Floral Gown with naken soulders - Purple', 'الصدر ثوب الأزهار مع سرودرز صاخبة - الأرجواني', 1, '41.0000', '159.0000', '2.0000', 'f4c38126ddaa2aefc79de00aa79ddb0a.png', 1, NULL, '', '', '', '', '', '', '8.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'chest-floral-gown-with-naken-soulders-purple', 1, NULL, 1, 0, 0),
(55, '11534522', 'Chest Floral Gown with naken soulders  - Blue', 'الصدر ثوب الأزهار مع الصراصير أزعج - الأزرق', 1, '41.0000', '159.0000', '2.0000', '7c3bc87df0d77ff98267ab88605d9935.png', 1, NULL, '', '', '', '', '', '', '17.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'chest-floral-gown-with-naken-soulders-blue', 1, NULL, 1, 0, 0),
(56, '11534523', 'Vintage Printed Chiffon, Self Block', 'خمر مطبوعة الشيفون ثوب مع كتلة النسيج الذاتي', 1, '48.0000', '189.0000', '2.0000', '686df5c67585f872d9e500720f9daa99.png', 1, NULL, '', '', '', '', '', '', '14.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'vintage-printed-chiffon-self-block', 1, NULL, 1, 0, 0),
(57, '11534524', 'Dropping lace patch, Cape - Pink', 'الصدر إسقاط الدانتيل التصحيح ثوب مع الرأس - الوردي', 1, '42.0000', '169.0000', '2.0000', 'ad9a2b5c29c9e700d46921cc0fc7a57c.png', 1, NULL, '', '', '', '', '', '', '16.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'dropping-lace-patch-cape-pink', 1, NULL, 1, 0, 0),
(58, '11534525', 'top Dropping lace patch, Cape- White', 'الصدر إسقاط الدانتيل التصحيح ثوب مع كيب - أبيض', 1, '42.0000', '169.0000', '2.0000', '38f2ae5fa3a401f26156704a4b6b3dfe.png', 1, NULL, '', '', '', '', '', '', '12.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'top-dropping-lace-patch-cape-white', 1, NULL, 1, 0, 0),
(59, '11534526', 'Tinker bell Satin Gown', 'تينكر جرس ثوب الساتان', 1, '42.0000', '169.0000', '2.0000', '1c55138ed635e078e76f32b9e0b37888.png', 1, NULL, '', '', '', '', '', '', '15.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'tinker-bell-satin-gown', 1, NULL, 1, 0, 0),
(60, '11534527', 'Embelished Chest Gown with pearls', 'مزين ثوب الصدر مع اللؤلؤ', 1, '41.0000', '189.0000', '2.0000', '58c6348c06234ae8e856d5dfd01e2212.png', 1, NULL, '', '', '', '', '', '', '15.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'embelished-chest-gown-with-pearls', 1, NULL, 1, 0, 0),
(61, '11534528', 'Embroidered Net Gowns - Purple', 'مطرز صافي العباءات - الأرجواني', 1, '40.0000', '159.0000', '2.0000', '49d6e4224e261212ca7d2c530b5b10d2.png', 1, NULL, '', '', '', '', '', '', '6.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'embroidered-net-gowns-purple', 1, NULL, 1, 0, 0),
(62, '11534529', 'Embroidered Net Gowns - Blue', 'مطرز صافي العباءات - الأزرق', 1, '40.0000', '159.0000', '2.0000', '67cbdd35e6f48aec087187aace803e53.png', 1, NULL, '', '', '', '', '', '', '8.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'embroidered-net-gowns-blue', 1, NULL, 1, 0, 0),
(63, '11534530', 'Dropping Ribbon Flower Net Gowns - Blue', 'إسقاط الشريط زهرة صافي أثواب - الأزرق', 1, '40.0000', '169.0000', '2.0000', 'c2c2ff9db292f4f303047103c37656a1.png', 1, NULL, '', '', '', '', '', '', '16.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'dropping-ribbon-flower-net-gowns-blue', 1, NULL, 1, 0, 0),
(64, '11534531', 'Dropping Ribbon Flower- skin', 'إسقاط الشريط زهرة صافي العباءات - شامبين', 1, '40.0000', '169.0000', '2.0000', 'da14f0f06a8778b890ce0f51ca6b2fb4.png', 1, NULL, '', '', '', '', '', '', '11.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'dropping-ribbon-flower-skin', 1, NULL, 1, 0, 0),
(65, '11534532', 'Dropping Ribbon Flower - Pink', 'إسقاط الشريط زهرة صافي أثواب - الوردي', 1, '40.0000', '169.0000', '2.0000', '409ea59edf61cf3b13c92fefa1352196.png', 1, NULL, '', '', '', '', '', '', '12.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'dropping-ribbon-flower-pink', 1, NULL, 1, 0, 0),
(66, '11534533', 'Dropping Ribbon Flower - Red', 'إسقاط الشريط زهرة صافي أثواب - الأحمر', 1, '40.0000', '169.0000', '2.0000', 'd4f8d8c42ea79015a1fa04b9d5083d7c.png', 1, NULL, '', '', '', '', '', '', '17.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'dropping-ribbon-flower-red', 1, NULL, 1, 0, 0),
(68, '11534535', 'Embellished, stone belt- Pink', 'فستان مزين بالخرز مع حزام حرير - زهري', 1, '34.0000', '159.0000', '2.0000', 'e40c568e5afc3e12f8ce65eec49f34c8.png', 1, NULL, '', '', '', '', '', '', '12.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'embellished-stone-belt-pink', 1, NULL, 1, 0, 0),
(69, '11534536', 'Embellished top, stone belt - skin', 'فستان منمق مع حزام من الحجر - تشامبانج', 1, '34.0000', '159.0000', '2.0000', 'a02bfd23035abda28a1ec71d88faa7b9.png', 1, NULL, '', '', '', '', '', '', '15.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'embellished-top-stone-belt-skin', 1, NULL, 1, 0, 0),
(70, '11534537', 'AutumnTree with Chiffon Flowers', 'المطبوعة كتلة الشيفون ثوب', 1, '38.0000', '169.0000', '2.0000', '8e7f698ddbbfaa15ee97e809a0147f6b.jpeg', 1, NULL, '', '', '', '', '', '', '2.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'autumntree-with-chiffon-flowers', 1, NULL, 1, 0, 0),
(71, '11534538', 'Half layered Sequin gown', 'نصف الطبقات الترتر ثوب', 1, '38.0000', '169.0000', '2.0000', '28f1186320f97ba57a279f244f7572f3.png', 1, NULL, '', '', '', '', '', '', '14.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'half-layered-sequin-gown', NULL, NULL, 1, 0, 0),
(72, '11534539', 'lace patch Flower,pearls - Red', 'صافي ثوب مع الدانتيل زهرة التصحيح واللؤلؤ - الأحمر', 1, '33.0000', '159.0000', '2.0000', 'a8ba09b73bc32b2b13db5443cbdd6a02.png', 1, NULL, '', '', '', '', '', '', '16.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'lace-patch-flowerpearls-red', 1, NULL, 1, 0, 0),
(73, '11534540', 'lace patch Flower ,pearls - blue', 'صافي ثوب مع الدانتيل زهرة التصحيح واللؤلؤ - الأزرق', 1, '33.0000', '159.0000', '2.0000', '56fd31003dde8c38f1de94b631160ca1.png', 1, NULL, '', '', '', '', '', '', '11.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'lace-patch-flower-pearls-blue', NULL, NULL, 1, 0, 0),
(74, '11534541', 'lace patch Flower ,pearls - pink', 'صافي ثوب مع الدانتيل زهرة التصحيح واللؤلؤ - الوردي', 1, '33.0000', '159.0000', '2.0000', '1952fb58efc10fff49b681a293ecf63c.png', 1, NULL, '', '', '', '', '', '', '18.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'lace-patch-flower-pearls-pink', NULL, NULL, 1, 0, 0),
(75, '11534542', 'Glitter Chest, L.Back - Blue', 'بريق الصدر الصدر مع عودة طويلة - الأزرق', 1, '36.0000', '169.0000', '2.0000', '6d728fa84f8c8ced69f173de1bc6b253.png', 1, NULL, '', '', '', '', '', '', '11.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'glitter-chest-lback-blue', NULL, NULL, 1, 0, 0),
(76, '11534543', 'Lace Toddler, L.back, stone -Blue', 'الدانتيل الصدر طفل ثوب طويل مع الظهر والحجر الخصر خط - الأزرق', 1, '33.0000', '149.0000', '2.0000', '0f3997aaa1397a0e855e7153ac67963c.png', 3, NULL, '', '', '', '', '', '', '7.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'lace-toddler-lback-stone-blue', NULL, NULL, 1, 0, 0),
(77, '11534544', 'Lace Toddler, L.back, stone  - RED', 'الدانتيل الصدر طفل ثوب طويل مع الظهر والحجر الخصر خط - الأحمر', 1, '33.0000', '139.0000', '2.0000', 'a8cc312c096b8297d8f0ac6719a6dff3.png', 3, NULL, '', '', '', '', '', '', '10.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'lace-toddler-lback-stone-red', NULL, NULL, 1, 0, 0),
(78, '11534545', 'Toddler, Lace, L.Back Ribbon- H.Pink', 'الدانتيل ثوب الصدر مع طويلة الشريط باكم متفوقا - الوردي الساخن', 1, '36.0000', '149.0000', '2.0000', 'b0d4cd8b2ba18a45a5743460fb400eec.png', 1, NULL, '', '', '', '', '', '', '13.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'toddler-lace-lback-ribbon-hpink', NULL, NULL, 1, 0, 0),
(79, '11534546', 'Toddler L.back, Rbn, White', 'الدانتيل ثوب الصدر مع طويلة الشريط باكم متفوقا - أبيض', 1, '36.0000', '149.0000', '2.0000', 'fa37575b2e8f311dd531b76c138f9555.png', 1, NULL, '', '', '', '', '', '', '13.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'toddler-lback-rbn-white', NULL, NULL, 1, 0, 0),
(80, '11534547', 'Thread Embroidered, pearl brooj - Blue', 'موضوع إمبروديرد ثوب مع اللؤلؤ بروج - الأزرق', 1, '35.0000', '159.0000', '2.0000', 'f5de21353a8fae63a9427eb5aba2de0c.png', 1, NULL, '', '', '', '', '', '', NULL, NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'thread-embroidered-pearl-brooj-blue', NULL, NULL, 1, 0, 0),
(81, '11534548', 'Cut Glitter Chest with Front Bow - Pink', 'قطع بريق الصدر مع الجبهة القوس - الوردي', 1, '35.0000', '159.0000', '2.0000', '324c42860c1f8cb5f5606d0ec2ee2921.png', 1, NULL, '', '', '', '', '', '', '10.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'cut-glitter-chest-with-front-bow-pink', NULL, NULL, 1, 0, 0),
(82, '11534549', 'Cut Glitter Chest with Front Bow - RED', 'قطع بريق الصدر مع الجبهة القوس - الأحمر', 1, '35.0000', '159.0000', '2.0000', 'e28497b58b2c618fe5e07189aa1ec945.png', 1, NULL, '', '', '', '', '', '', '16.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'cut-glitter-chest-with-front-bow-red', NULL, NULL, 1, 0, 0),
(83, '11534550', 'Lace line Ribbon, toddler- Blue', 'الدانتيل إمبليشمنت والشريط فليرد ثوب - الأزرق', 1, '33.0000', '139.0000', '2.0000', 'ff4301e78ca8c3128a3f4f9c3e29ca35.png', 3, NULL, '', '', '', '', '', '', '10.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'lace-line-ribbon-toddler-blue', NULL, NULL, 1, 0, 0),
(84, '11534551', 'Lace line Ribbon, toddler- SGreen', 'الدانتيل إمبليشمنت والشريط فليرد ثوب - سيغرين', 1, '33.0000', '139.0000', '2.0000', '3375f176057febcc10644de876720eed.png', 3, NULL, '', '', '', '', '', '', '10.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'lace-line-ribbon-toddler-sgreen', NULL, NULL, 1, 0, 0),
(85, '11534552', 'Toddler, lace, RBN Knot, Cream', 'الدانتيل ثوب الصدر مع عقدة الشريط والتفوق - كريم', 1, '33.0000', '139.0000', '2.0000', 'aeea457ad3d7919bfa7fba5aff1eed3d.png', 3, NULL, '', '', '', '', '', '', '13.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'toddler-lace-rbn-knot-cream', NULL, NULL, 1, 0, 0),
(86, '11534553', 'Toddler, lace, RBN Knot, Maroon', 'الدانتيل الصدر ثوب مع عقدة الشريط والتفوق - مارون', 1, '33.0000', '139.0000', '2.0000', '57000b45ed5d1b93490507e20e063650.png', 3, NULL, '', '', '', '', '', '', '10.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'toddler-lace-rbn-knot-maroon', NULL, NULL, 1, 0, 0),
(88, '11534555', 'beads chest,3 ribbon edging, Red', 'حجر الصدر بثوب مع 3 الشريط متفوقا - الأحمر', 1, '36.0000', '169.0000', '2.0000', 'c8624a7d78c4476de5fa0675d7f1296d.png', 1, NULL, '', '', '', '', '', '', '13.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'beads-chest3-ribbon-edging-red', NULL, NULL, 1, 0, 0),
(89, '11534556', 'Block plate, Sequin patch- purple', 'بلوك لوحة ثوب مع الترتر الدانتيل في الصدر - الأرجواني', 1, '45.0000', '189.0000', '2.0000', '61ec10ffb2239bda0db5d51c1daf266b.png', 1, NULL, '', '', '', '', '', '', '18.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'block-plate-sequin-patch-purple', NULL, NULL, 1, 0, 0),
(90, '11534557', 'Block plate, Sequin patch - Blue', 'بلوك لوحة ثوب مع الترتر الدانتيل في الصدر - الأزرق', 1, '45.0000', '189.0000', '2.0000', 'de36b557526e8e555e4b4ed578d8b267.png', 1, NULL, '', '', '', '', '', '', '12.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'block-plate-sequin-patch-blue', NULL, NULL, 1, 0, 0),
(91, '11534558', 'Elsa Gown with Small cape', 'إلسا، ثوب نسوي، ب، الجزء الضيق من الظهر، الرأس', 1, '22.0000', '119.0000', '2.0000', 'f20aa11d4a85eff244069b306c3a18e4.png', 2, NULL, '', '', '', '', '', '', '33.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'elsa-gown-with-small-cape', NULL, NULL, 1, 0, 0),
(92, '11534559', 'Repunzel Satin Gown', 'ريبوزيل ثوب الساتان', 1, '22.0000', '149.0000', '2.0000', '009a549b904d502ef975f18ae57d2a4a.png', 2, NULL, '', '', '', '', '', '', '9.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'repunzel-satin-gown', NULL, NULL, 1, 0, 0),
(93, '11534560', 'Anna gown with Cape', 'آنا، ب، الرأس', 1, '24.0000', '139.0000', '2.0000', '4fe37b369898ed3ae6a691fcd50d8199.png', 2, NULL, '', '', '', '', '', '', '10.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'anna-gown-with-cape', NULL, NULL, 1, 0, 0),
(94, '11534561', 'Elsa Gown with long cape Glitter Sleeves', 'إلسا ثوب مع طويلة الأكمام بريق الرأس', 1, '22.0000', '139.0000', '2.0000', '154f3f9adcc8e32c2896542674ab9be3.png', 2, NULL, '', '', '', '', '', '', '10.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'elsa-gown-with-long-cape-glitter-sleeves', NULL, NULL, 1, 0, 0),
(95, '11534562', 'Auroura Sleeping Beauty Gown', 'أورورا ثوب النوم النائم', 1, '29.0000', '169.0000', '2.0000', '1d4047799f569c72887ee3f1493ea8c0.png', 2, NULL, '', '', '', '', '', '', '15.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'auroura-sleeping-beauty-gown', NULL, NULL, 1, 0, 0),
(96, '11534563', 'Little Poney dress with Ribbon lines', 'ليتل بوني اللباس مع خطوط الشريط', 1, '18.0000', '94.0000', '2.0000', '2728716aa3415e5d31d1ffe7e619b3ad.png', 2, NULL, '', '', '', '', '', '', '5.0000', 1, 1, '', NULL, 'code25', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'little-poney-dress-with-ribbon-lines', NULL, NULL, 1, 0, 0),
(97, '11534564', 'Little Ponney, Chiffon layers', 'ليتل بوني اللباس مع طبقات الشيفون', 1, '18.0000', '94.0000', '2.0000', 'bbcc19beafdf4576bf11c0e0c7aaf681.png', 2, NULL, '', '', '', '', '', '', '10.0000', NULL, 1, '', NULL, 'code25', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'little-ponney-chiffon-layers', NULL, NULL, 1, 0, 0),
(98, '17135465', 'Polka Printed Satin - Blue', 'بولكا دوت فستان من الحرير مع شرائط حمراء - أزرق', 1, '32.0000', '139.0000', '2.0000', '623f777b035aed5130c6362d83172953.png', 1, NULL, '', '', '', '', '', '', '12.0000', NULL, 1, '', NULL, 'code25', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'polka-printed-satin-blue', NULL, NULL, 1, 0, 0),
(100, '17135467', 'Lace Flower Patch & pearls - Beige', 'صافي ثوب مع الدانتيل زهرة التصحيح واللؤلؤ - البيج', 1, '33.0000', '159.0000', '2.0000', '013481e567126ae008aa30f966f46af0.png', 1, NULL, '', '', '', '', '', '', '6.0000', NULL, 1, '', NULL, 'code25', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 0, 0, 1, 'lace-flower-patch-pearls-beige', NULL, NULL, 1, 0, 0),
(101, '17135468', 'Thread Embroidered, Pearl Brooj - Blue', 'موضوع مطرز ثوب مع اللؤلؤ بروج - الأزرق', 1, '33.0000', '159.0000', '2.0000', '5f0e6bd42cc94c47f4f1fa4148cf0119.png', 1, NULL, '', '', '', '', '', '', '6.0000', NULL, 1, '', NULL, 'code25', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 0, 0, 1, 'thread-embroidered-pearl-brooj-blue1', NULL, NULL, 1, 0, 0),
(102, 'KP-PETS009', 'Baby Pink Petti Skirt - Solid', 'طفل بيتي الوردي تنورة - الصلبة', 1, '30.0000', '99.0000', '0.0000', 'no_image.png', 7, NULL, '', '', '', '', '', '', '27.0000', NULL, 1, '', NULL, 'code128', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 0, 0, 1, 'baby-pink-petti-skirt-solid', NULL, NULL, 1, 0, 0),
(103, 'KP-PETS073', 'light Purple Pettiskirt - Solid', 'الضوء الأرجواني بيتيسكيرت - الصلبة', 1, '30.0000', '99.0000', '2.0000', 'no_image.png', 7, NULL, '', '', '', '', '', '', '7.0000', NULL, 1, '', NULL, 'code128', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'light-purple-pettiskirt-solid', NULL, NULL, 1, 0, 0),
(104, 'KP-PETS068', 'Red Petti Skirt - Solid', 'تنورة حمراء بيتي - سوليد', 1, '30.0000', '99.0000', '2.0000', 'no_image.png', 7, NULL, '', '', '', '', '', '', '15.0000', NULL, 1, '', NULL, 'code128', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'red-petti-skirt-solid', NULL, NULL, 1, 0, 0),
(105, 'KP-PETS010', 'B.Pink Skirt, Rainbow Ruffle', 'طفل بيتي الوردي تنورة مع قوس قزح الكشكشة', 1, '30.0000', '99.0000', '0.0000', 'no_image.png', 7, NULL, '', '', '', '', '', '', '7.0000', NULL, 1, '', NULL, 'code128', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'bpink-skirt-rainbow-ruffle', NULL, NULL, 1, 0, 0),
(106, 'KP-PETS169', 'Peach Petti Skirt - Solid', 'الخوخ بيتي تنورة - الصلبة', 1, '33.0000', '99.0000', '0.0000', 'no_image.png', 7, NULL, '', '', '', '', '', '', '15.0000', NULL, 1, '', NULL, 'code128', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'peach-petti-skirt-solid', NULL, NULL, 1, 0, 0),
(107, 'KP-PETS180', 'Pastel Pink Petti Skirt - Solid', 'تنورة من الباستيل الوردي - سوليد', 1, '30.0000', '99.0000', '0.0000', 'no_image.png', 7, NULL, '', '', '', '', '', '', '8.0000', NULL, 1, '', NULL, 'code128', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'pastel-pink-petti-skirt-solid', NULL, NULL, 1, 0, 0),
(108, 'KP-PETS148', 'Ash Grey Petti Skirt - Solid', 'بنطلون بيتي رمادي - سادة', 1, '33.0000', '99.0000', '0.0000', 'no_image.png', 7, NULL, '', '', '', '', '', '', '11.0000', NULL, 1, '', NULL, 'code128', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'ash-grey-petti-skirt-solid', NULL, NULL, 1, 0, 0),
(109, 'KP-PETS177', 'Skin Petti Tutu Skirt - Solid', 'الجلد بيتي توتو تنورة - الصلبة', 1, '33.0000', '99.0000', '0.0000', 'no_image.png', 7, NULL, '', '', '', '', '', '', '10.0000', NULL, 1, '', NULL, 'code128', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'skin-petti-tutu-skirt-solid', NULL, NULL, 1, 0, 0),
(110, 'KP-POPS01', 'PomPom PettiSet', 'بومبوم بيتيسيت', 1, '27.1200', '79.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pompom-pettiset', NULL, NULL, 1, 0, 0),
(111, 'KP-POPS04', 'PomPom PettiSet', 'بومبوم بيتيسيت', 1, '27.1200', '79.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pompom-pettiset1', NULL, NULL, 1, 0, 0),
(112, 'KP-POPS05', 'PomPom PettiSet', 'بومبوم بيتيسيت', 1, '27.1200', '79.0000', '2.0000', 'KP-POPS05.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pompom-pettiset1', NULL, NULL, 1, 0, 0),
(113, 'KP-POPS06', 'PomPom PettiSet', 'بومبوم بيتيسيت', 1, '27.1200', '79.0000', '2.0000', 'KP-POPS06.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pompom-pettiset1', NULL, NULL, 1, 0, 0),
(114, 'KP-POPS13', 'PomPom PettiSet', 'بومبوم بيتيسيت', 1, '30.0000', '79.0000', '2.0000', 'KP-POPS13.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pompom-pettiset1', NULL, NULL, 1, 0, 0),
(115, 'KP-POPS14', 'PomPom PettiSet - Gold Dots', 'بومبوم بيتيسيت - نقاط الذهب', 1, '30.0000', '79.0000', '2.0000', 'KP-POPS14.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pompom-pettiset-gold-dots', NULL, NULL, 1, 0, 0),
(116, 'KP-POPS17', 'PomPom PettiSet - Gold Dots', 'بومبوم بيتيسيت - نقاط الذهب', 1, '30.0000', '79.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pompom-pettiset-gold-dots1', NULL, NULL, 1, 0, 0),
(117, 'KP-POPS18', 'PomPom PettiSet - Gold Dots', 'بومبوم بيتيسيت - نقاط الذهب', 1, '30.0000', '79.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pompom-pettiset-gold-dots1', NULL, NULL, 1, 0, 0),
(118, 'KP-POPS20', 'PomPom PettiSet - Gold Dots', 'بومبوم بيتيسيت - نقاط الذهب', 1, '30.0000', '79.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pompom-pettiset-gold-dots1', NULL, NULL, 1, 0, 0),
(119, 'KP-GSPD015', 'Vintage Printed Summer - Peach', 'خمر مطبوعة الصيف - الخوخ', 1, '25.5300', '94.0000', '2.0000', '', 1, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', NULL, NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'vintage-printed-summer-peach', NULL, NULL, 1, 0, 0),
(120, 'KP-GSPD016', 'Vintage Printed Summer - Pink', 'خمر مطبوعة الصيف - الوردي', 1, '25.5300', '94.0000', '2.0000', '', 1, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', NULL, NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'vintage-printed-summer-pink', NULL, NULL, 1, 0, 0),
(121, 'KP-GSPD017', 'Vintage Printed Summer - Hot Pink', 'خمر مطبوعة الصيف - الوردي الساخن', 1, '25.5300', '94.0000', '2.0000', 'KP-GSPD017.jpg', 1, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '3.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'vintage-printed-summer-hot-pink', NULL, NULL, 1, 0, 0),
(122, 'KP-GPDB06', 'Gold Dot Bloomer Set - Black', 'غولد دوت بلومر سيت - بلاك', 1, '25.0000', '74.0000', '2.0000', 'KP-GPDB06.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'gold-dot-bloomer-set-black', NULL, NULL, 1, 0, 0),
(123, 'KP-GPDB08', 'Gold Dot Bloomer Set - Pink', 'طقم ذهب بلومر - وردي', 1, '25.0000', '74.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'gold-dot-bloomer-set-pink', NULL, NULL, 1, 0, 0),
(124, 'KP-GPDB10', 'Gold Dot Bloomer Set - Purple', 'طقم ذهب بلومر - بنفسجي', 1, '25.0000', '74.0000', '2.0000', 'KP-GPDB10.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'gold-dot-bloomer-set-purple', NULL, NULL, 1, 0, 0),
(125, 'KP-POMP01', 'PomPom Jumpsuit - Hot pink', 'بومبوم بذلة - الوردي الساخن', 1, '21.0000', '69.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pompom-jumpsuit-hot-pink', NULL, NULL, 1, 0, 0),
(126, 'KP-POMP02', 'PomPom Jumpsuit - L.Blue', 'بذلة بومبوم - L.Blue', 1, '21.0000', '69.0000', '2.0000', 'KP-POMP02.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', NULL, 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pompom-jumpsuit-lblue', NULL, NULL, 1, 0, 0),
(127, 'KP-POMP03', 'PomPom Jumpsuit - Peach', 'بذلة بومبوم - الخوخ', 1, '21.0000', '69.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pompom-jumpsuit-peach', NULL, NULL, 1, 0, 0),
(128, 'KP-POMP05', 'PomPom Jumpsuit - Black', 'بومبوم بذلة - أسود', 1, '21.0000', '69.0000', '2.0000', 'KP-POMP05.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pompom-jumpsuit-black', NULL, NULL, 1, 0, 0),
(129, 'KP-POMP06', 'PomPom Jumpsuit - Pink', 'بومبوم بذلة - الوردي', 1, '21.0000', '69.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pompom-jumpsuit-pink', NULL, NULL, 1, 0, 0),
(130, 'KP-BBWH23', 'Sequin Bow - Head band', 'الترتر القوس - رئيس الفرقة', 1, '5.0000', '25.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'sequin-bow-head-band', NULL, NULL, 1, 0, 0),
(131, 'KP-BBWH24', 'Sequin Bow - Head band', 'الترتر القوس - رئيس الفرقة', 1, '5.0000', '25.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'sequin-bow-head-band1', NULL, NULL, 1, 0, 0),
(132, 'KP-BBWH25', 'Sequin Bow - Head band', 'الترتر القوس - رئيس الفرقة', 1, '5.0000', '25.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'sequin-bow-head-band1', NULL, NULL, 1, 0, 0),
(133, 'KP-BBWH26', 'Sequin Bow - Head band', 'الترتر القوس - رئيس الفرقة', 1, '5.0000', '25.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'sequin-bow-head-band1', NULL, NULL, 1, 0, 0),
(134, 'KP-REH029', 'Sequin Knot Band - Gold', 'الترتر عقدة الفرقة - الذهب', 1, '4.0000', '25.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'sequin-knot-band-gold', NULL, NULL, 1, 0, 0),
(135, 'KP-REH033', 'Sequin Knot Band - Green', 'الترتر عقدة الفرقة - الأخضر', 1, '4.0000', '25.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'sequin-knot-band-green', NULL, NULL, 1, 0, 0),
(136, 'KP-REH035', 'Sequin Knot Band - Blue', 'الترتر عقدة الفرقة - الأزرق', 1, '4.0000', '25.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'sequin-knot-band-blue', NULL, NULL, 1, 0, 0),
(137, 'KP-REH036', 'Sequin Knot Band - Pink', 'الترتر عقدة الفرقة - الوردي', 1, '4.0000', '25.0000', '2.0000', '', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'sequin-knot-band-pink', NULL, NULL, 1, 0, 0),
(138, '98976656', 'Gosping hello meida', 'جوسبينغ مرحبا ميدا', 1, '75.0000', '85.0000', '0.0000', 'no_image.png', 9, NULL, '', '', '', '', '', '', '0.0000', NULL, 1, '', NULL, 'code128', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'gosping-hello-meida', NULL, NULL, 1, 0, 0),
(139, 'KF-FRK-1040', 'RED PARTY DRESS W/ BEADINGS AT CHEST AND RIBBON', 'ريد بارتي دريس W / بيدينغس أت تشيست أند ريبون', 1, '40.0000', '94.0000', '2.0000', '', 1, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'red-party-dress-w-beadings-at-chest-and-ribbon', NULL, NULL, 1, 0, 0),
(140, 'KF-FRK-1041', 'GLITTER Chest WITH BOW - Pink', 'بريق الصدر مع القوس - الوردي', 1, '40.0000', '94.0000', '2.0000', '', 1, NULL, '', '', '', '', '', '', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'glitter-chest-with-bow-pink', NULL, NULL, 1, 0, 0),
(141, 'KF-FRK-1042', 'GLITTER Chest WITH BOW - Blue', 'بريق الصدر مع القوس - الأزرق', 1, '40.0000', '94.0000', '2.0000', '', 1, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'glitter-chest-with-bow-blue', NULL, NULL, 1, 0, 0),
(142, 'KF-FRK-1043', 'GLITTER Chest WITH BOW - Beige', 'غليتر الصدر مع القوس - بيج', 1, '40.0000', '94.0000', '2.0000', '', 1, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'glitter-chest-with-bow-beige', NULL, NULL, 1, 0, 0),
(143, 'KF-FRK-1044', 'RED PARTY DRESS WITH SEQUENCE AND FLOWER', 'ريد، أقام سهرة، أعد، ب، تسلس، أيضا، فلاور', 1, '45.0000', '139.0000', '2.0000', '', 1, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'red-party-dress-with-sequence-and-flower', NULL, NULL, 1, 0, 0),
(144, 'KF-FRK-1045', 'Baby Gown with Small beading at chest - Hotpink', 'ثوب طفل مع الديكور الصغيرة في الصدر - هوتبينك', 1, '35.0000', '89.0000', '2.0000', '', 3, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'baby-gown-with-small-beading-at-chest-hotpink', NULL, NULL, 1, 0, 0),
(145, 'KF-FRK-1046', 'Baby Gown with Small beading at chest - pink', 'ثوب طفل مع الديكور الصغيرة في الصدر - الوردي', 1, '35.0000', '89.0000', '2.0000', '', 3, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'baby-gown-with-small-beading-at-chest-pink', NULL, NULL, 1, 0, 0),
(146, 'KF-FRK-1047', 'Baby Gown with Small beading at chest - Peach', 'ثوب طفل مع الديكور الصغيرة في الصدر - الخوخ', 1, '35.0000', '89.0000', '2.0000', '', 3, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'baby-gown-with-small-beading-at-chest-peach', NULL, NULL, 1, 0, 0),
(147, 'KF-FRK-1048', 'Toddler Lace Gown - Green', 'طفل الدانتيل ثوب - الأخضر', 1, '35.0000', '129.0000', '2.0000', '', 3, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'toddler-lace-gown-green', NULL, NULL, 1, 0, 0),
(148, 'KF-FRK-1049', 'Toddler Embelished Organza gown - Green', 'طفل مبطن الأورجانزا ثوب - الأخضر', 1, '35.0000', '129.0000', '2.0000', '', 3, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'toddler-embelished-organza-gown-green', NULL, NULL, 1, 0, 0),
(149, 'KF-FRK-1050', 'Toddler Lace Gown - Purple', 'طفل الدانتيل ثوب - الأرجواني', 1, '35.0000', '129.0000', '2.0000', '', 3, NULL, '', '', '', '', '', '', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'toddler-lace-gown-purple', NULL, NULL, 1, 0, 0),
(150, 'KF-FRK-1053', 'Lace Flower, Stone Wline - Hpink', 'الدانتيل زهرة ثوب مع حجر الخصر خط - الوردي الساخن', 1, '50.0000', '149.0000', '2.0000', '', 1, NULL, '', '', '', '', '', '', NULL, NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'lace-flower-stone-wline-hpink', NULL, NULL, 1, 0, 0),
(151, 'KF-FRK-1054', 'Lace Flower Stone Wline - Red', 'الدانتيل زهرة ثوب مع حجر الخصر خط - الأحمر', 1, '50.0000', '149.0000', '2.0000', '', 1, NULL, '', '', '', '', '', '', '1.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'lace-flower-stone-wline-red', NULL, NULL, 1, 0, 0),
(153, 'KF-FRK-1056', 'White Frock with Pink big Flowers at chest', 'أبيض، أعد، ب، ثوب قرنفلي اللون، الزهرات الكبيرة، إلى، تشيست', 1, '45.0000', '100.0000', '2.0000', '', 1, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'white-frock-with-pink-big-flowers-at-chest', NULL, NULL, 1, 0, 0),
(154, 'KF-FRK-1057', 'Gliter Chest Long back Gown - Red', 'غليتر الصدر طويل الظهر ثوب - الأحمر', 1, '50.0000', '169.0000', '2.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'gliter-chest-long-back-gown-red', NULL, NULL, 1, 0, 0),
(155, 'KF-FRK-1058', 'Jacuard Printed Princess Gown with pearl waist', 'جاكوارد ثوب الأميرة المطبوعة مع الخصر اللؤلؤ', 1, '40.0000', '139.0000', '2.0000', '', 1, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'jacuard-printed-princess-gown-with-pearl-waist', NULL, NULL, 1, 0, 0),
(156, 'KF-FRK-1059', 'Printed Bird Gown', 'مطبوعة ثوب الطيور', 1, '40.0000', '139.0000', '2.0000', '', 1, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'printed-bird-gown', NULL, NULL, 1, 0, 0),
(157, 'KF-FRK-1060', 'Naked Chest Chiffon - Pink', 'عارية الصدر الشيفون - الوردي', 1, '40.0000', '139.0000', '2.0000', '', 1, NULL, '', '', '', '', '', '', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'naked-chest-chiffon-pink', NULL, NULL, 1, 0, 0),
(158, 'KF-FRK-1061', 'Layered Chifoon Gown with chest patch - white', 'الطبقات تشيفون ثوب مع الصدر التصحيح - أبيض', 1, '40.0000', '159.0000', '2.0000', '', 1, NULL, '', '', '', '', '', '', '3.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'layered-chifoon-gown-with-chest-patch-white', NULL, NULL, 1, 0, 0),
(159, 'KF-FRK-1062', 'Naked Chest Chiffon - Skin', 'عارية الصدر الشيفون - الجلد', 1, '40.0000', '139.0000', '2.0000', '', 1, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'naked-chest-chiffon-skin', NULL, NULL, 1, 0, 0),
(160, 'KF-FRK-1063', 'Half show Lace Chest with beading, seqins - White', 'نصف عرض الدانتيل الصدر مع الديكور، سيكينز - البيج', 1, '40.0000', '100.0000', '2.0000', '', 1, NULL, '', '', '', '', '', '', '2.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'half-show-lace-chest-with-beading-seqins-white', NULL, NULL, 1, 0, 0),
(161, 'KF-FRK-1064', 'Half show Lace Chest with beading, seqins - Pink', 'نصف عرض الرباط الصدر مع الديكور، سيكينز - الوردي', 1, '40.0000', '100.0000', '2.0000', '', 1, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'half-show-lace-chest-with-beading-seqins-pink', NULL, NULL, 1, 0, 0),
(162, '102-2001', 'Elsa Tutu Gown', 'إلسا توتو', 2, '52.0000', '209.0000', '2.0000', '642e24157c95f4b66bf93abd4003f374.jpg', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '86.0000', 0, 1, '', NULL, 'code128', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 2, 2, 1, 'elsa-tutu-gown', NULL, '0.0000', 1, 0, 0),
(163, 'KF-Costm-2002', 'Arora WITH WHITE COLAR', 'أرورا مع اللون الأبيض', 1, '40.0000', '169.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'arora-with-white-colar', NULL, NULL, 1, 0, 0),
(164, 'KF-Costm-2003', 'Repunzel TUTU DRESS', 'ريبونزل توتو اللباس', 1, '40.0000', '119.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'repunzel-tutu-dress', NULL, NULL, 1, 0, 0),
(165, 'KF-Costm-2004', 'Repunzel Gown', 'ثوب ريبونزيل', 1, '40.0000', '149.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'repunzel-gown', NULL, NULL, 1, 0, 0),
(166, 'KF-Costm-2005', 'Cindrella Gown - Chest Pic', 'سيندريلا ثوب - الصدر بيك', 1, '40.0000', '149.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'cindrella-gown-chest-pic', NULL, NULL, 1, 0, 0),
(167, 'KF-Costm-2006', 'CINDERELLA- Gown Pic', 'CINDERELLA- ثوب بيك', 1, '40.0000', '149.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'cinderella-gown-pic', NULL, NULL, 1, 0, 0),
(168, 'KF-Costm-2007', 'Snow White Gown', 'ثوب الثلج الأبيض', 1, '40.0000', '149.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'snow-white-gown', NULL, NULL, 1, 0, 0);
INSERT INTO `sma_tst` (`id`, `code`, `name`, `translated_name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `promotion`, `promo_price`, `start_date`, `end_date`, `supplier1_part_no`, `supplier2_part_no`, `supplier3_part_no`, `supplier4_part_no`, `supplier5_part_no`, `sale_unit`, `purchase_unit`, `brand`, `slug`, `featured`, `weight`, `status`, `free_shipment`, `is_updated`) VALUES
(169, 'KF-Costm-2008', 'MERMAID BABY DRESS', 'مرميد بيبي دريس', 1, '40.0000', '94.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'mermaid-baby-dress', NULL, NULL, 1, 0, 0),
(170, 'KF-Costm-2009', 'MERMAID DRESS', 'فستان مرميد', 1, '40.0000', '149.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'mermaid-dress', NULL, NULL, 1, 0, 0),
(171, 'KF-Costm-2010', 'MERMAID (GREEN&PURPLE COLOR)', 'مرميد (الأخضر و الأرجواني اللون)', 1, '40.0000', '149.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'mermaid-greenpurple-color', NULL, NULL, 1, 0, 0),
(172, 'KF-Costm-2011', 'Mermaid Tutu dress', 'حورية البحر توتو اللباس', 1, '40.0000', '119.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'mermaid-tutu-dress', NULL, NULL, 1, 0, 0),
(173, 'KF-Costm-2012', 'SOFIAGown WITH GLITTERS', 'صوفياغون مع غليترز', 1, '35.0000', '139.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '15.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'sofiagown-with-glitters', NULL, NULL, 1, 0, 0),
(174, 'KF-Costm-2013', 'SOFIA SATIN DRESS', 'صوفيا فستان الساتان', 1, '45.0000', '149.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '9.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'sofia-satin-dress', NULL, NULL, 1, 0, 0),
(175, 'KF-Costm-2014', 'Sofia Tutu Dress', 'صوفيا توتو اللباس', 1, '60.0000', '189.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '8.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'sofia-tutu-dress', NULL, NULL, 1, 0, 0),
(176, 'KF-Costm-2015', 'Mickey Mouse tutu', 'ميكي ماوس توتو', 1, '65.0000', '189.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'mickey-mouse-tutu', NULL, NULL, 1, 0, 0),
(177, 'KF-Costm-2016', 'Minnie Mouse Tutu', 'ميني ماوس توتو', 1, '65.0000', '189.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'minnie-mouse-tutu', NULL, NULL, 1, 0, 0),
(178, 'KF-Costm-2017', 'Pumpkin Tutu', 'القرع توتو', 1, '50.0000', '169.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pumpkin-tutu', NULL, NULL, 1, 0, 0),
(179, 'KF-Costm-2018', 'TinkerBell Tutu with Wings', 'تينكربيل توتو مع أجنحة', 1, '65.0000', '189.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'tinkerbell-tutu-with-wings', NULL, NULL, 1, 0, 0),
(180, 'KF-Costm-2019', 'Pink Fairy Tutu with wings', 'الوردي الجنية توتو مع أجنحة', 1, '65.0000', '189.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-fairy-tutu-with-wings', NULL, NULL, 1, 0, 0),
(181, 'KF-Costm-2020', 'Cape Elsa', 'الرأس، إلسا', 1, '20.0000', '50.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'cape-elsa', NULL, NULL, 1, 0, 0),
(182, 'KF-Costm-2021', 'Mermaid Green Tutu dress', 'حورية البحر الأخضر توتو اللباس', 1, '50.0000', '149.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'mermaid-green-tutu-dress', NULL, NULL, 1, 0, 0),
(183, 'KF-Costm-2022', 'American Costume tutu', 'زي أمريكي، توتو', 1, '30.0000', '94.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'american-costume-tutu', NULL, NULL, 1, 0, 0),
(184, 'KF-Costm-2023', 'Elsa Real Gown', 'إلسا ريال ثوب', 1, '30.0000', '139.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '8.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'elsa-real-gown', NULL, NULL, 1, 0, 0),
(185, 'KF-Costm-2024', 'ANNA TUTU DRESS', 'أنا توتو اللباس', 1, '35.0000', '119.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'anna-tutu-dress', NULL, NULL, 1, 0, 0),
(186, 'KF-Costm-2025', 'ELSA BABY DRESS', 'إلسا بيبي دريس', 1, '30.0000', '79.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '7.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'elsa-baby-dress', NULL, NULL, 1, 0, 0),
(187, 'KF-Costm-2026', 'OLAF TUTU DRESS', 'أولاف توتو اللباس', 1, '40.0000', '149.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '11.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'olaf-tutu-dress', NULL, NULL, 1, 0, 0),
(188, 'KF-Costm-2027', 'SUPER GIRL TUTU DRESS', 'سوبر فتاة توتو اللباس', 1, '40.0000', '149.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '8.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'super-girl-tutu-dress', NULL, NULL, 1, 0, 0),
(189, 'KF-Costm-2028', 'BATGIRL TUTU DRESS', 'باتجيرل توتو اللباس', 1, '40.0000', '149.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '7.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'batgirl-tutu-dress', NULL, NULL, 1, 0, 0),
(190, 'KF-Costm-2029', 'Hello Kitty Tutu Dress', 'مرحبا كيتي توتو اللباس', 1, '50.0000', '169.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '7.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'hello-kitty-tutu-dress', NULL, NULL, 1, 0, 0),
(191, 'KF-Costm-2030', 'Little Pony Tutu Dress', 'ليتل المهر توتو اللباس', 1, '50.0000', '169.0000', '2.0000', '', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 1, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'little-pony-tutu-dress', NULL, NULL, 1, 0, 0),
(192, '105-5001', 'White Ribbon Flower Tutu', 'الأبيض الشريط زهرة توتو', 1, '50.0000', '94.0000', '2.0000', '105-5001.jpg', 5, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '5.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'white-ribbon-flower-tutu', NULL, NULL, 1, 0, 0),
(193, '105-5002', '3 Chest Flower gold tutu', '3 الصدر زهرة الذهب توتو', 2, '50.0000', '94.0000', '2.0000', '105-5002.jpg', 5, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '1.0000', 0, 1, '', NULL, 'code128', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 2, 2, 1, '3-chest-flower-gold-tutu', NULL, '0.0000', 1, 0, 0),
(194, '105-5003', 'White purple hawayin theme', 'الأبيض هوايين موضوع الأرجواني', 1, '50.0000', '189.0000', '2.0000', '105-5003.jpg', 5, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '6.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'white-purple-hawayin-theme', NULL, NULL, 1, 0, 0),
(195, '105-5004', 'YELLOW TUTU DRESS', 'الأصفر توتو اللباس', 1, '50.0000', '149.0000', '2.0000', '105-5004.jpg', 5, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', NULL, NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'yellow-tutu-dress', NULL, NULL, 1, 0, 0),
(196, '105-5005', 'PONY TUTU BIG FLOWERS', 'بوني، توتو، الزهرات الكبيرة', 1, '50.0000', '169.0000', '2.0000', '105-5005.jpg', 5, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', NULL, NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'pony-tutu-big-flowers', NULL, NULL, 1, 0, 0),
(197, '105-5006', 'AQUA BLUE PINK FLOWER', 'أكوا الأزرق الوردي زهرة', 1, '55.0000', '169.0000', '2.0000', '105-5006.jpg', 5, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '12.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'aqua-blue-pink-flower', NULL, NULL, 1, 0, 0),
(198, '105-5007', '3 Flower tutu - Red', '3 زهرة توتو - الأحمر', 1, '50.0000', '169.0000', '2.0000', '105-5007.jpg', 5, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '12.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, '3-flower-tutu-red', NULL, NULL, 1, 0, 0),
(199, '105-5008', 'Peacock Tutu Dress', 'الطاووس توتو اللباس', 1, '65.0000', '189.0000', '2.0000', '105-5008.jpg', 5, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '2.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'peacock-tutu-dress', NULL, NULL, 1, 0, 0),
(200, '105-5009', 'Orange Blue  tutu old', 'البرتقال الأزرق توتو القديم', 1, '40.0000', '94.0000', '2.0000', '105-5009.jpg', 5, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '5.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'orange-blue-tutu-old', NULL, NULL, 1, 0, 0),
(201, '105-5010', 'Pink Glitter tutu', 'الوردي بريق توتو', 1, '40.0000', '94.0000', '2.0000', '105-5010.jpg', 5, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '4.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-glitter-tutu', NULL, NULL, 1, 0, 0),
(202, '105-5011', 'Black classic tutu', 'أسود توتو الكلاسيكية', 1, '40.0000', '94.0000', '2.0000', '105-5011.jpg', 5, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '2.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'black-classic-tutu', NULL, NULL, 1, 0, 0),
(203, '105-5012', '3 flower tutu - Pink', '3 زهرة توتو - الوردي', 2, '50.0000', '169.0000', '2.0000', '105-5012.jpg', 5, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '3.0000', 0, 1, '', NULL, 'code128', NULL, '<p>Go sporty this summer with this vintage navy and white striped v-neck t-shirt from the Nike. Perfect for pairing with denim and white kicks for a stylish sporty vibe.</p>', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 2, 2, 1, '3-flower-tutu-pink', NULL, '0.0000', 1, 1, 0),
(204, '105-5013', 'Purple tutu pink flower', 'الأرجواني توتو زهرة وردي', 1, '50.0000', '169.0000', '2.0000', '105-5013.jpg', 5, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '1.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'purple-tutu-pink-flower', NULL, NULL, 1, 0, 0),
(205, '105-5014', 'Green Peacock Tutu', 'الطاووس الأخضر توتو', 1, '65.0000', '189.0000', '2.0000', '105-5014.jpg', 5, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '12.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'green-peacock-tutu', NULL, NULL, 1, 0, 0),
(206, '105-5015', 'Purple Rainbow Tutu Dress', 'الأرجواني قوس قزح توتو اللباس', 1, '50.0000', '169.0000', '2.0000', '105-5015.jpg', 5, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '4.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'purple-rainbow-tutu-dress', NULL, NULL, 1, 0, 0),
(207, '105-5016', 'Pink Rainbow Tutu Dress', 'الوردي قوس قزح توتو اللباس', 1, '50.0000', '169.0000', '2.0000', '105-5016.jpg', 5, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '11.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-rainbow-tutu-dress', NULL, NULL, 1, 0, 0),
(208, '103-3001', 'Baby Rosetto Onesie - Pink', 'طفل روزيتو نيسيي - الوردي', 1, '30.0000', '94.0000', '2.0000', '103-3001.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '14.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'baby-rosetto-onesie-pink', NULL, NULL, 1, 0, 0),
(209, '103-3002', 'Baby Rosetto Onesie - Purple', 'طفل روزيتو نيسي - الأرجواني', 1, '30.0000', '94.0000', '2.0000', '103-3002.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '1.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'baby-rosetto-onesie-purple', NULL, NULL, 1, 0, 0),
(210, '103-3003', 'Baby Rosetto Onesie - Red', 'الطفل روزيتو نيسي - الأحمر', 1, '30.0000', '94.0000', '2.0000', '103-3003.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', NULL, NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'baby-rosetto-onesie-red', NULL, NULL, 1, 0, 0),
(211, '103-3004', 'Baby Rosetto Onesie - Hot Pink', 'طفل روتيتو نيسيي - الوردي الساخن', 1, '30.0000', '94.0000', '2.0000', '103-3004.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', NULL, NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'baby-rosetto-onesie-hot-pink', NULL, NULL, 1, 0, 0),
(212, '103-3005', 'Baby Rosetto Onesie - White', 'الطفل روزيتو نيسي - أبيض', 1, '30.0000', '94.0000', '2.0000', '103-3005.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', NULL, NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'baby-rosetto-onesie-white', NULL, NULL, 1, 0, 0),
(213, '103-3006', 'Baby Rosetto Onesie - Blue', 'طفل روزيتو نيسي - الأزرق', 1, '30.0000', '94.0000', '2.0000', '103-3006.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '1.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'baby-rosetto-onesie-blue', NULL, NULL, 1, 0, 0),
(214, '103-3007', 'Baby Rosetto Onesie - Yellow', 'طفل روزيتو نيسيي - الأصفر', 1, '30.0000', '94.0000', '2.0000', '103-3007.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', NULL, NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'baby-rosetto-onesie-yellow', NULL, NULL, 1, 0, 0),
(215, '103-3008', 'Baby IceCream Onesie - White', 'الطفل إيسكرم نيسيي - أبيض', 1, '30.0000', '79.0000', '2.0000', '103-3008.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '15.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'baby-icecream-onesie-white', NULL, NULL, 1, 0, 0),
(216, '103-3009', 'Baby Cupcake Onesie - Pink', 'بيبي كب كيك نيسيي - الوردي', 1, '30.0000', '79.0000', '2.0000', '103-3009.jpg', 3, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '15.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'baby-cupcake-onesie-pink', NULL, NULL, 1, 0, 0),
(217, '104-4001', 'White Lace Skin Flower', 'الدانتيل الأبيض زهرة الجلد', 1, '90.0000', '259.0000', '2.0000', '104-4001.jpg', 4, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '1.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'white-lace-skin-flower', NULL, NULL, 1, 0, 0),
(218, '104-4002', 'White Satin Back cape', 'أبيض، الحرير، ظهر، الرأس', 1, '90.0000', '259.0000', '2.0000', '104-4002.jpg', 4, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '1.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'white-satin-back-cape', NULL, NULL, 1, 0, 0),
(219, '104-4003', 'Satin Peach A-Line', 'الساتان الخوخ ألف خط', 1, '90.0000', '289.0000', '2.0000', '104-4003.jpg', 4, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '2.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'satin-peach-a-line', NULL, NULL, 1, 0, 0),
(220, '104-4004', 'Raw Satin Purple Gown', 'الخام الساتان ثوب الأرجواني', 1, '90.0000', '279.0000', '2.0000', '104-4004.jpg', 4, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '2.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'raw-satin-purple-gown', NULL, NULL, 1, 0, 0),
(221, '104-4005', 'BEIGE SATIN Long Back', 'بيج ساتين طويل الظهر', 1, '90.0000', '289.0000', '2.0000', '104-4005.jpg', 4, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', NULL, NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'beige-satin-long-back', NULL, NULL, 1, 0, 0),
(222, '104-4006', 'Peach L.Sleevs Stripe Shldr', 'الخوخ L.Sleevs شريط شلدر', 1, '90.0000', '289.0000', '2.0000', '104-4006.jpg', 4, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '2.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'peach-lsleevs-stripe-shldr', NULL, NULL, 1, 0, 0),
(223, '104-4007', 'Red Lace white Satin', 'الأحمر الدانتيل الأبيض الساتان', 1, '90.0000', '289.0000', '2.0000', '104-4007.jpg', 4, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '5.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'red-lace-white-satin', NULL, NULL, 1, 0, 0),
(224, '103-3010', 'Baby Super Girl Onesie', 'الطفل سوبر فتاة نيسيي', 1, '30.0000', '79.0000', '0.0000', '103-3010.jpg', 3, NULL, '', '', '', '', '', '', NULL, NULL, 1, '', NULL, 'code128', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'baby-super-girl-onesie', NULL, NULL, 1, 0, 0),
(225, '108-8001', 'Cape Shoulder Bellet', 'الرأس، تقع على عاتقه، بيليت', 1, '40.0000', '129.0000', '2.0000', '108-8001.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '4.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'cape-shoulder-bellet', NULL, NULL, 1, 0, 0),
(226, '108-8002', 'Blue  Glitters Neck Ballet', 'الأزرق يلمع الرقبة الباليه', 1, '40.0000', '89.0000', '2.0000', '108-8002.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '1.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'blue-glitters-neck-ballet', NULL, NULL, 1, 0, 0),
(227, '108-8003', 'white Bellet Gold neck cape', 'أبيض، بيليت، الذهب، العنق، كيب', 1, '40.0000', '129.0000', '2.0000', '108-8003.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '4.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'white-bellet-gold-neck-cape', NULL, NULL, 1, 0, 0),
(228, '108-8004', 'white Bellet Silver neck cape', 'أبيض، بيليت، طبق فضي للمائدة، العنق، كيب', 1, '40.0000', '129.0000', '2.0000', '108-8004.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '3.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'white-bellet-silver-neck-cape', NULL, NULL, 1, 0, 0),
(229, '108-8005', 'Pink Bellet Gold neck cape', 'الوردي بيليت الذهب الرقبة الرأس', 1, '40.0000', '129.0000', '2.0000', '108-8005.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '12.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-bellet-gold-neck-cape', NULL, NULL, 1, 0, 0),
(230, '108-8006', 'Elsa Bellet Glitter', 'إلسا، بليت، بريق', 1, '40.0000', '129.0000', '2.0000', '108-8006.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '7.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'elsa-bellet-glitter', NULL, NULL, 1, 0, 0),
(231, '108-8007', 'Jazz Ballet Red Neck Stone', 'الجاز الباليه الأحمر الرقبة الحجر', 1, '40.0000', '129.0000', '2.0000', '108-8007.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '2.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'jazz-ballet-red-neck-stone', NULL, NULL, 1, 0, 0),
(232, '108-8008', 'Black Silver Ballet', 'الأسود، طبق فضي للمائدة، باليت', 1, '40.0000', '129.0000', '2.0000', '108-8008.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', NULL, NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'black-silver-ballet', NULL, NULL, 1, 0, 0),
(233, '108-8009', 'Jazz Ballet Dance Ribbon', 'الجاز الباليه الرقص الشريط', 1, '40.0000', '129.0000', '2.0000', '108-8009.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '1.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'jazz-ballet-dance-ribbon', NULL, NULL, 1, 0, 0),
(234, '108-8010', 'Pink Ballet Dancing Girl', 'ثوب قرنفلي اللون، رقص الباليه، إمرأة متزوجة', 1, '40.0000', '129.0000', '2.0000', '108-8010.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', NULL, NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-ballet-dancing-girl', NULL, NULL, 1, 0, 0),
(235, '108-8011', 'Pink Ballet Gold Bow', 'الوردي الباليه القوس الذهب', 1, '40.0000', '129.0000', '2.0000', '108-8011.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '9.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-ballet-gold-bow', NULL, NULL, 1, 0, 0),
(236, '108-8012', 'Pink Black Crown Ballet', 'الوردي التاج الأسود الباليه', 1, '40.0000', '129.0000', '2.0000', '108-8012.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '3.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-black-crown-ballet', NULL, NULL, 1, 0, 0),
(237, '108-8013', 'Princess Ballet pink Puple', 'أميرة الباليه، ثوب قرنفلي اللون، بوبل', 1, '40.0000', '129.0000', '2.0000', '108-8013.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '1.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'princess-ballet-pink-puple', NULL, NULL, 1, 0, 0),
(238, '108-8014', 'Next Stock Yellow Bellet', 'التالي الأسهم الأصفر بيليه', 1, '40.0000', '129.0000', '2.0000', '108-8014.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '11.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'next-stock-yellow-bellet', NULL, NULL, 1, 0, 0),
(239, '108-8015', 'Half Show Cap Slv Ribbon', 'نصف عرض كاب سلف الشريط', 1, '40.0000', '129.0000', '2.0000', '108-8015.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '7.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'half-show-cap-slv-ribbon', NULL, NULL, 1, 0, 0),
(240, '108-8016', 'Stripe Embroidered Pink Ballet', 'شريط مطرزة الوردي الباليه', 1, '40.0000', '129.0000', '2.0000', '108-8016.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '12.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'stripe-embroidered-pink-ballet', NULL, NULL, 1, 0, 0),
(241, '108-8017', 'Neck Flowers White Ballet', 'الزهور الرقبة الأبيض الباليه', 1, '40.0000', '129.0000', '2.0000', '108-8017.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '9.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'neck-flowers-white-ballet', NULL, NULL, 1, 0, 0),
(242, '108-8018', 'Neck Stone Frill Slvs Ballet', 'العنق حجر سلف سلف الباليه', 1, '40.0000', '129.0000', '2.0000', '108-8018.jpg', 8, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '2.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'neck-stone-frill-slvs-ballet', NULL, NULL, 1, 0, 0),
(244, '101-1114', 'top Floral, Naked Shoulder - Peach', ' الوردي', 1, '41.0000', '159.0000', '2.0000', 'no_image.png', 1, NULL, '', '', '', '', '', '', '6.0000', NULL, 1, '', NULL, 'code128', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'top-floral-naked-shoulder-peach', NULL, NULL, 1, 0, 0),
(245, '102-2004', 'Sofia Satin Gown - Round cuts', '', 1, '40.0000', '159.0000', '0.0000', 'no_image.png', 2, NULL, '', '', '', '', '', '', '5.0000', NULL, 1, '', NULL, 'code128', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 1, 'sofia-satin-gown-round-cuts', NULL, NULL, 1, 0, 0),
(246, '102-2031', 'Elsa Tutu Gown L.S.', 'tutu', 1, '52.0000', '209.0000', '2.0000', 'no_image.png', 2, NULL, 'CF1', 'CF2', 'CF3', 'CF4', 'CF5', 'CF6', '19.0000', NULL, 1, '', NULL, 'code128', '', '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 0, 0, 1, 'elsa-tutu-gown-ls', NULL, NULL, 1, 0, 0),
(374, '203-2025', 'BATMAN BLACK', '', 1, '40.0000', '135.0000', '2.0000', '203-2025.jpg', 11, NULL, '', '', '', '', '', '', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'batman-black', NULL, NULL, 1, 0, 0),
(375, '203-2026', 'BATMAN GRAY', '', 1, '40.0000', '135.0000', '2.0000', '203-2026.jpg', 11, NULL, '', '', '', '', '', '', '13.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'batman-gray', NULL, NULL, 1, 0, 0),
(376, '203-2027', 'SUPERMAN', '', 1, '40.0000', '135.0000', '2.0000', '203-2027.jpg', 11, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'superman', NULL, NULL, 1, 0, 0),
(377, '203-2028', 'SPIDERMAN RED', '', 1, '40.0000', '135.0000', '2.0000', '203-2028.jpg', 11, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'spiderman-red', NULL, NULL, 1, 0, 0),
(378, '203-2029', 'ROBIN HOOD', '', 1, '40.0000', '135.0000', '2.0000', '203-2029.jpg', 11, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'robin-hood', NULL, NULL, 1, 0, 0),
(379, '203-2030', 'TRANSFORMER', '', 1, '40.0000', '135.0000', '2.0000', '203-2030.jpg', 11, NULL, '', '', '', '', '', '', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'transformer', NULL, NULL, 1, 0, 0),
(380, '203-2031', 'IRON MAN', '', 1, '40.0000', '135.0000', '2.0000', '203-2031.jpg', 11, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'iron-man', NULL, NULL, 1, 0, 0),
(381, '203-2032', 'SPIDERMAN BLACK', '', 1, '40.0000', '135.0000', '2.0000', '203-2032.jpg', 11, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'spiderman-black', NULL, NULL, 1, 0, 0),
(382, '203-2033', 'HULK', '', 1, '40.0000', '135.0000', '2.0000', '203-2033.jpg', 11, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'hulk', NULL, NULL, 1, 0, 0),
(383, '203-2034', 'VOLT', '', 1, '40.0000', '135.0000', '2.0000', '203-2034.jpg', 11, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'volt', NULL, NULL, 1, 0, 0),
(384, '203-2035', 'CAPTAIN AMERICA', '', 1, '40.0000', '135.0000', '2.0000', '203-2035.jpg', 11, NULL, '', '', '', '', '', '', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'captain-america', NULL, NULL, 1, 0, 0),
(385, '203-2036', 'INCREDIBLE', '', 1, '40.0000', '135.0000', '2.0000', '203-2036.jpg', 11, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'incredible', NULL, NULL, 1, 0, 0),
(386, '107-7008', 'Petti Skirt - White', '', 1, '30.0000', '99.0000', '2.0000', '107-7008.jpg', 7, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'petti-skirt-white', NULL, NULL, 1, 0, 0),
(387, '107-7045', 'Petti Skirt -  Lime Green', '', 1, '30.0000', '99.0000', '2.0000', '107-7045.jpg', 7, NULL, '', '', '', '', '', '', '8.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'petti-skirt-lime-green', NULL, NULL, 1, 0, 0),
(388, '107-7041', 'Petti Skirt- Blue & Green Ruffle', '', 1, '30.0000', '99.0000', '2.0000', '107-7041.jpg', 7, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'petti-skirt-blue-green-ruffle', NULL, NULL, 1, 0, 0),
(389, '107-7092', 'Petti Skirt- Lime Green & Pink Ruffle', '', 1, '30.0000', '99.0000', '2.0000', '107-7092.jpg', 7, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'petti-skirt-lime-green-pink-ruffle', NULL, NULL, 1, 0, 0),
(390, '107-7089', 'Petti Skirt- Golden Yellow', '', 1, '30.0000', '99.0000', '2.0000', '107-7089.jpg', 7, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'petti-skirt-golden-yellow', NULL, NULL, 1, 0, 0),
(391, '107-7095', 'Petti Skirt- Orange & Blue Ruffle', '', 1, '30.0000', '99.0000', '2.0000', '107-7095.jpg', 7, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'petti-skirt-orange-blue-ruffle', NULL, NULL, 1, 0, 0),
(392, '107-7063', 'Petti Skirt- Pink with Heart', '', 1, '30.0000', '99.0000', '2.0000', '107-7063.jpg', 7, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'petti-skirt-pink-with-heart', NULL, NULL, 1, 0, 0),
(393, '107-7140', 'Petti Skirt- Blue with Red Chevron', '', 1, '30.0000', '99.0000', '2.0000', '107-7140.jpg', 7, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'petti-skirt-blue-with-red-chevron', NULL, NULL, 1, 0, 0),
(394, '107-7301', 'Petti Skirt- UAE Color', '', 1, '30.0000', '99.0000', '2.0000', '107-7301.jpg', 7, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'petti-skirt-uae-color', NULL, NULL, 1, 0, 0),
(395, '107-7302', 'Petti Skirt- X-mass Color', '', 1, '30.0000', '99.0000', '2.0000', '107-7302.jpg', 7, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'petti-skirt-x-mass-color', NULL, NULL, 1, 0, 0),
(396, '108-8019', 'Leotards', '', 1, '25.0000', '79.0000', '2.0000', '', 8, NULL, '', '', '', '', '', '', '7.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'leotards', NULL, NULL, 1, 0, 0),
(397, '109-9001', 'RUFFLE TOP', '', 1, '20.0000', '45.0000', '2.0000', '109-9001.jpg', 19, NULL, '', '', '', '', '', '', '10.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'ruffle-top', NULL, NULL, 1, 0, 0),
(398, '109-9002', 'NET FLOWER DESIGN LONGSLEEVE TOP', '', 1, '12.0000', '35.0000', '2.0000', '109-9002.jpg', 19, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'net-flower-design-longsleeve-top', NULL, NULL, 1, 0, 0),
(399, '109-9003', 'Cotton Tops', '', 1, '15.0000', '45.0000', '2.0000', '109-9003.jpg', 19, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'cotton-tops', NULL, NULL, 1, 0, 0),
(400, '109-9005', 'Pink Cup Cake', '', 1, '15.0000', '45.0000', '2.0000', '109-9005.jpg', 19, NULL, '', '', '', '', '', '', '14.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-cup-cake', NULL, NULL, 1, 0, 0),
(401, '109-9006', 'Grey Dear', '', 1, '15.0000', '45.0000', '2.0000', '109-9006.jpg', 19, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'grey-dear', NULL, NULL, 1, 0, 0),
(402, '109-9007', 'Sky Blue Good Night', '', 1, '15.0000', '45.0000', '2.0000', '109-9007.jpg', 19, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'sky-blue-good-night', NULL, NULL, 1, 0, 0),
(403, '109-9008', 'White Love Gold', '', 1, '15.0000', '45.0000', '2.0000', '109-9008.jpg', 19, NULL, '', '', '', '', '', '', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'white-love-gold', NULL, NULL, 1, 0, 0),
(404, '109-9009', 'Solid Pink Red Heart', '', 1, '15.0000', '45.0000', '2.0000', '109-9009.jpg', 19, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'solid-pink-red-heart', NULL, NULL, 1, 0, 0),
(405, '109-9010', 'Wite and black Dotted', '', 1, '15.0000', '45.0000', '2.0000', '109-9010.jpg', 19, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'wite-and-black-dotted', NULL, NULL, 1, 0, 0),
(406, '109-9011', 'Pink Frozen', '', 1, '15.0000', '45.0000', '2.0000', '109-9011.jpg', 19, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-frozen', NULL, NULL, 1, 0, 0),
(407, '110-1101', 'GRAY LACE FLOWER', '', 1, '30.0000', '79.0000', '2.0000', '110-1101.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'gray-lace-flower', NULL, NULL, 1, 0, 0),
(408, '110-1102', 'NAVY BLUE LACE FLOWER', '', 1, '30.0000', '79.0000', '2.0000', '110-1102.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'navy-blue-lace-flower', NULL, NULL, 1, 0, 0),
(409, '110-1103', 'PINK LACE FLOWER', '', 1, '30.0000', '79.0000', '2.0000', '110-1103.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-lace-flower', NULL, NULL, 1, 0, 0),
(410, '110-1104', 'ROYAL BLUE W/ FLOWER STONE AND PEARL', '', 1, '30.0000', '79.0000', '2.0000', '110-1104.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'royal-blue-w-flower-stone-and-pearl', NULL, NULL, 1, 0, 0),
(411, '110-1105', 'BURBERRY LIKE WINTER DRESS', '', 1, '30.0000', '79.0000', '2.0000', '110-1105.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'burberry-like-winter-dress', NULL, NULL, 1, 0, 0),
(412, '110-1106', 'YELLOW W/ BLACK TOP 2', '', 1, '30.0000', '79.0000', '2.0000', '110-1106.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'yellow-w-black-top-2', NULL, NULL, 1, 0, 0),
(413, '110-1107', 'YELLOW W/ BLACK & RED', '', 1, '30.0000', '79.0000', '2.0000', '110-1107.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'yellow-w-black-red', NULL, NULL, 1, 0, 0),
(414, '110-1108', 'HOT PINK W/ BUTTERFLY', '', 1, '30.0000', '79.0000', '2.0000', '110-1108.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'hot-pink-w-butterfly', NULL, NULL, 1, 0, 0),
(415, '110-1109', 'RED WINTER DRESS 2 SET', '', 1, '30.0000', '79.0000', '2.0000', '110-1109.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'red-winter-dress-2-set', NULL, NULL, 1, 0, 0),
(416, '110-1110', 'PINK W/ BUTTERFLY DESIGN 2 SET', '', 1, '30.0000', '79.0000', '2.0000', '110-1110.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-w-butterfly-design-2-set', NULL, NULL, 1, 0, 0),
(417, '110-1111', 'PINK & PURPLE 2 SET WINTER DRESS', '', 1, '30.0000', '79.0000', '2.0000', '110-1111.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-purple-2-set-winter-dress', NULL, NULL, 1, 0, 0),
(418, '110-1112', 'PINK W/ TULIP FLOWER DESIGN', '', 1, '30.0000', '79.0000', '2.0000', '110-1112.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-w-tulip-flower-design', NULL, NULL, 1, 0, 0),
(419, '110-1113', 'ORANGE & YELLOW', '', 1, '30.0000', '79.0000', '2.0000', '110-1113.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'orange-yellow', NULL, NULL, 1, 0, 0),
(420, '110-1114', 'Baby Pink W/Pink Yellow Flower', '', 1, '30.0000', '79.0000', '2.0000', '110-1114.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'baby-pink-wpink-yellow-flower', NULL, NULL, 1, 0, 0),
(421, '110-1115', 'Baby Pink W/Pink Blue Flower', '', 1, '30.0000', '79.0000', '2.0000', '110-1115.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'baby-pink-wpink-blue-flower', NULL, NULL, 1, 0, 0),
(422, '110-1116', 'Blue/Yellow Flower', '', 1, '30.0000', '79.0000', '2.0000', '110-1116.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'blueyellow-flower', NULL, NULL, 1, 0, 0),
(423, '110-1117', 'Purple/ Aqua Yellow Flower', '', 1, '30.0000', '79.0000', '2.0000', '110-1117.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'purple-aqua-yellow-flower', NULL, NULL, 1, 0, 0);
INSERT INTO `sma_tst` (`id`, `code`, `name`, `translated_name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `promotion`, `promo_price`, `start_date`, `end_date`, `supplier1_part_no`, `supplier2_part_no`, `supplier3_part_no`, `supplier4_part_no`, `supplier5_part_no`, `sale_unit`, `purchase_unit`, `brand`, `slug`, `featured`, `weight`, `status`, `free_shipment`, `is_updated`) VALUES
(424, '110-1118', 'PINK COAT', '', 1, '30.0000', '79.0000', '2.0000', '110-1118.jpg', 20, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-coat', NULL, NULL, 1, 0, 0),
(425, '201-2001', 'Shoe Tshirt Set', '', 1, '30.0000', '89.0000', '2.0000', '201-2001.jpg', 9, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'shoe-tshirt-set', NULL, NULL, 1, 0, 0),
(426, '201-2002', 'Lining Tshirt Set', '', 1, '30.0000', '89.0000', '2.0000', '201-2002.jpg', 9, NULL, '', '', '', '', '', '', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'lining-tshirt-set', NULL, NULL, 1, 0, 0),
(427, '201-2003', 'Zebra Shirt Set', '', 1, '30.0000', '89.0000', '2.0000', '201-2003.jpg', 9, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'zebra-shirt-set', NULL, NULL, 1, 0, 0),
(428, '201-2004', 'Dolphin Shirt Set', '', 1, '30.0000', '89.0000', '2.0000', '201-2004.jpg', 9, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'dolphin-shirt-set', NULL, NULL, 1, 0, 0),
(429, '201-2005', 'Green Shirt Set', '', 1, '30.0000', '89.0000', '2.0000', '201-2005.jpg', 9, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'green-shirt-set', NULL, NULL, 1, 0, 0),
(430, '201-2006', 'Digital Lining Shirt Set', '', 1, '30.0000', '89.0000', '2.0000', '201-2006.jpg', 9, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'digital-lining-shirt-set', NULL, NULL, 1, 0, 0),
(431, '201-2007', 'BlueRed long pants', '', 1, '35.0000', '94.0000', '2.0000', '201-2007.jpg', 9, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'bluered-long-pants', NULL, NULL, 1, 0, 0),
(432, '201-2008', 'US Flag Shirt Set', '', 1, '30.0000', '89.0000', '2.0000', '201-2008.jpg', 9, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'us-flag-shirt-set', NULL, NULL, 1, 0, 0),
(433, '201-2009', 'Polo Red Shorts', '', 1, '30.0000', '89.0000', '2.0000', '201-2009.jpg', 9, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'polo-red-shorts', NULL, NULL, 1, 0, 0),
(434, '201-2010', 'SpiderMan Tshirt', '', 1, '30.0000', '89.0000', '2.0000', '201-2010.jpg', 9, NULL, '', '', '', '', '', '', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'spiderman-tshirt', NULL, NULL, 1, 0, 0),
(435, '201-2011', 'RedStripes Long Pants', '', 1, '35.0000', '94.0000', '2.0000', '201-2011.jpg', 9, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'redstripes-long-pants', NULL, NULL, 1, 0, 0),
(436, '201-2012', 'Black Shoe Tshirt Set', '', 1, '30.0000', '89.0000', '2.0000', '201-2012.jpg', 9, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'black-shoe-tshirt-set', NULL, NULL, 1, 0, 0),
(437, '201-2013', 'Floral Printed Long Pants', '', 1, '35.0000', '94.0000', '2.0000', '201-2013.jpg', 9, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'floral-printed-long-pants', NULL, NULL, 1, 0, 0),
(438, '201-2014', 'Chek Cat Tshrt Set', '', 1, '30.0000', '89.0000', '2.0000', '201-2014.jpg', 9, NULL, '', '', '', '', '', '', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'chek-cat-tshrt-set', NULL, NULL, 1, 0, 0),
(439, '201-2015', 'Wang Red Tshrt Set', '', 1, '30.0000', '89.0000', '2.0000', '201-2015.jpg', 9, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'wang-red-tshrt-set', NULL, NULL, 1, 0, 0),
(440, '201-2016', 'Grey Face Blck Tshrt Set', '', 1, '30.0000', '89.0000', '2.0000', '201-2016.jpg', 9, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'grey-face-blck-tshrt-set', NULL, NULL, 1, 0, 0),
(441, '202-2017', 'Solid Camel 3 Pcs Suit', '', 1, '50.0000', '149.0000', '2.0000', '202-2017.jpg', 10, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'solid-camel-3-pcs-suit', NULL, NULL, 1, 0, 0),
(442, '202-2018', 'Solid Black 3 Pcs Suit', '', 1, '50.0000', '149.0000', '2.0000', '202-2018.jpg', 10, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'solid-black-3-pcs-suit', NULL, NULL, 1, 0, 0),
(443, '202-2019', 'SmallBox Brown Velvet 3 Pcs Suit', '', 1, '50.0000', '149.0000', '2.0000', '202-2019.jpg', 10, NULL, '', '', '', '', '', '', '11.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'smallbox-brown-velvet-3-pcs-suit', NULL, NULL, 1, 0, 0),
(444, '202-2020', 'BigBox Brown Velvet 3 Pcs Suit', '', 1, '50.0000', '130.0000', '2.0000', '202-2020.jpg', 10, NULL, '', '', '', '', '', '', '9.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'bigbox-brown-velvet-3-pcs-suit', NULL, NULL, 1, 0, 0),
(445, '202-2021', 'Pin Dotted Grey 3 Pcs Suit', '', 1, '50.0000', '149.0000', '2.0000', '202-2021.jpg', 10, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pin-dotted-grey-3-pcs-suit', NULL, NULL, 1, 0, 0),
(446, '202-2022', 'Solid Grey 3 Pcs Suit', '', 1, '50.0000', '149.0000', '2.0000', '202-2022.jpg', 10, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'solid-grey-3-pcs-suit', NULL, NULL, 1, 0, 0),
(447, '202-2023', 'Blackstrip Grey 3 Pcs Suit', '', 1, '50.0000', '160.0000', '2.0000', '202-2023.jpg', 10, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'blackstrip-grey-3-pcs-suit', NULL, NULL, 1, 0, 0),
(448, '202-2024', 'SmallBox Camel Velvet 3 Pcs Suit', '', 1, '50.0000', '149.0000', '2.0000', '202-2024.jpg', 10, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'smallbox-camel-velvet-3-pcs-suit', NULL, NULL, 1, 0, 0),
(449, '204-2037', 'Blue Blazer (FORMAL)', '', 1, '20.0000', '49.0000', '2.0000', '', 12, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'blue-blazer-formal', NULL, NULL, 1, 0, 0),
(450, '204-2038', 'Yellow Balzer (FORMAL)', '', 1, '20.0000', '49.0000', '2.0000', '204-2038.jpg', 12, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'yellow-balzer-formal', NULL, NULL, 1, 0, 0),
(451, '204-2040', 'superman blue', '', 1, '20.0000', '49.0000', '2.0000', '204-2040.jpg', 12, NULL, '', '', '', '', '', '', '7.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'superman-blue', NULL, NULL, 1, 0, 0),
(452, '204-2041', 'spiderman red', '', 1, '20.0000', '49.0000', '2.0000', '204-2041.jpg', 12, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'spiderman-red1', NULL, NULL, 1, 0, 0),
(453, '204-2042', 'spiderman black', '', 1, '20.0000', '49.0000', '2.0000', '204-2042.jpg', 12, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'spiderman-black1', NULL, NULL, 1, 0, 0),
(454, '204-2043', 'olaf hoddie blue', '', 1, '20.0000', '49.0000', '2.0000', '204-2043.jpg', 12, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'olaf-hoddie-blue', NULL, NULL, 1, 0, 0),
(455, '204-2044', 'olaf zipper', '', 1, '20.0000', '49.0000', '2.0000', '204-2044.jpg', 12, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'olaf-zipper', NULL, NULL, 1, 0, 0),
(456, '204-2045', 'olaf bike', '', 1, '20.0000', '49.0000', '2.0000', '204-2045.jpg', 12, NULL, '', '', '', '', '', '', '8.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'olaf-bike', NULL, NULL, 1, 0, 0),
(457, '204-2046', 'macquinne zipper', '', 1, '20.0000', '49.0000', '2.0000', '204-2046.jpg', 12, NULL, '', '', '', '', '', '', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'macquinne-zipper', NULL, NULL, 1, 0, 0),
(458, '204-2047', 'raindeer zipper', '', 1, '20.0000', '49.0000', '2.0000', '204-2047.jpg', 12, NULL, '', '', '', '', '', '', '9.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'raindeer-zipper', NULL, NULL, 1, 0, 0),
(459, '204-2048', 'olaf hoddie yellow', '', 1, '20.0000', '49.0000', '2.0000', '204-2048.jpg', 12, NULL, '', '', '', '', '', '', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'olaf-hoddie-yellow', NULL, NULL, 1, 0, 0),
(460, '301-30002', 'Ajrak - 30002', '', 1, '15.0000', '45.0000', '2.0000', '301-30002.jpg', 13, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'ajrak-30002', NULL, NULL, 1, 0, 0),
(461, '301-30003', 'Mustard Base White Square - 30003', '', 1, '15.0000', '45.0000', '2.0000', '301-30003.jpg', 13, NULL, '', '', '', '', '', '', '7.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'mustard-base-white-square-30003', NULL, NULL, 1, 0, 0),
(462, '301-30004', 'Black & White Stripesp - 30004', '', 1, '15.0000', '45.0000', '2.0000', '301-30004.jpg', 13, NULL, '', '', '', '', '', '', '7.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'black-white-stripesp-30004', NULL, NULL, 1, 0, 0),
(463, '301-30006', 'Sea Blue Base Red Flowers - 30006', '', 1, '15.0000', '45.0000', '2.0000', '301-30006.jpg', 13, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'sea-blue-base-red-flowers-30006', NULL, NULL, 1, 0, 0),
(464, '301-30007', 'Grey Base/Yellow White-30007', '', 1, '15.0000', '45.0000', '2.0000', '301-30007.jpg', 13, NULL, '', '', '', '', '', '', '7.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'grey-baseyellow-white-30007', NULL, NULL, 1, 0, 0),
(465, '301-30008', 'Black Dotted -30008', '', 1, '15.0000', '45.0000', '2.0000', '301-30008.jpg', 13, NULL, '', '', '', '', '', '', '11.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'black-dotted-30008', NULL, NULL, 1, 0, 0),
(466, '301-30009', 'Solid Brown/White Lining- 30009', '', 1, '15.0000', '45.0000', '2.0000', '301-30009.jpg', 13, NULL, '', '', '', '', '', '', '10.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'solid-brownwhite-lining-30009', NULL, NULL, 1, 0, 0),
(467, '301-30010', 'White Base Flowers- 30010', '', 1, '15.0000', '45.0000', '2.0000', '301-30010.jpg', 13, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'white-base-flowers-30010', NULL, NULL, 1, 0, 0),
(468, '301-30011', 'peach/Grey Square- 30011', '', 1, '15.0000', '45.0000', '2.0000', '301-30011.jpg', 13, NULL, '', '', '', '', '', '', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'peachgrey-square-30011', NULL, NULL, 1, 0, 0),
(469, '301-30012', 'White Base/Pink Peach- 30012', '', 1, '15.0000', '45.0000', '2.0000', '301-30012.jpg', 13, NULL, '', '', '', '', '', '', '11.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'white-basepink-peach-30012', NULL, NULL, 1, 0, 0),
(470, '301-30013', 'White Base/Blue Grey-30013', '', 1, '15.0000', '45.0000', '2.0000', '301-30013.jpg', 13, NULL, '', '', '', '', '', '', '7.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'white-baseblue-grey-30013', NULL, NULL, 1, 0, 0),
(471, '301-30015', 'Pink Border/Flowers- 30015', '', 1, '15.0000', '45.0000', '2.0000', '301-30015.jpg', 13, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'pink-borderflowers-30015', NULL, NULL, 1, 0, 0),
(472, '301-30016', 'Purple Border Pink Flowers- 30016', '', 1, '15.0000', '45.0000', '2.0000', '301-30016.jpg', 13, NULL, '', '', '', '', '', '', '10.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'purple-border-pink-flowers-30016', NULL, NULL, 1, 0, 0),
(473, '301-30017', 'Black Border/Peach and Yellow-30017', '', 1, '15.0000', '45.0000', '2.0000', '301-30017.jpg', 13, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'black-borderpeach-and-yellow-30017', NULL, NULL, 1, 0, 0),
(474, '301-30018', 'Navy Blue Border- 30018', '', 1, '15.0000', '45.0000', '2.0000', '301-30018.jpg', 13, NULL, '', '', '', '', '', '', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'navy-blue-border-30018', NULL, NULL, 1, 0, 0),
(475, '301-30019', 'Blue Circle Stars- 30019', '', 1, '15.0000', '45.0000', '2.0000', '301-30019.jpg', 13, NULL, '', '', '', '', '', '', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'blue-circle-stars-30019', NULL, NULL, 1, 0, 0),
(476, '301-30021', 'Peach Base Red Small Flowers- 30021', '', 1, '15.0000', '45.0000', '2.0000', '301-30021.jpg', 13, NULL, '', '', '', '', '', '', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'peach-base-red-small-flowers-30021', NULL, NULL, 1, 0, 0),
(477, '301-30023', 'Peach Blue Stripes-30023', '', 1, '15.0000', '45.0000', '2.0000', '301-30023.jpg', 13, NULL, '', '', '', '', '', '', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'peach-blue-stripes-30023', NULL, NULL, 1, 0, 0),
(478, '301-30024', 'Blue Stripes/ White Flowers-30024', '', 1, '15.0000', '45.0000', '2.0000', '301-30024.jpg', 13, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'blue-stripes-white-flowers-30024', NULL, NULL, 1, 0, 0),
(479, '301-30025', 'Peacock- 30025', '', 1, '15.0000', '45.0000', '2.0000', '301-30025.jpg', 13, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'peacock-30025', NULL, NULL, 1, 0, 0),
(480, '301-30027', 'Stripes Border- 30027', '', 1, '15.0000', '45.0000', '2.0000', '301-30027.jpg', 13, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'stripes-border-30027', NULL, NULL, 1, 0, 0),
(481, '301-30028', 'Yellow Flowers/Circle-30028', '', 1, '15.0000', '45.0000', '2.0000', '301-30028.jpg', 13, NULL, '', '', '', '', '', '', NULL, 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'yellow-flowerscircle-30028', NULL, NULL, 1, 0, 0),
(482, '301-30030', 'Blood Red/ Black&White Stripe-30030', '', 1, '15.0000', '45.0000', '2.0000', '301-30030.jpg', 13, NULL, '', '', '', '', '', '', '14.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'blood-red-blackwhite-stripe-30030', NULL, NULL, 1, 0, 0),
(483, '301-30031', 'Golden Flowers Border- 30031', '', 1, '15.0000', '45.0000', '2.0000', '301-30031.jpg', 13, NULL, '', '', '', '', '', '', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'golden-flowers-border-30031', NULL, NULL, 1, 0, 0),
(484, '301-30032', 'Blue Border/White Flowers-30032', '', 1, '15.0000', '45.0000', '2.0000', '301-30032.jpg', 13, NULL, '', '', '', '', '', '', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'blue-borderwhite-flowers-30032', NULL, NULL, 1, 0, 0),
(485, '301-30033', 'Green Base Red Flowers-30033', '', 1, '15.0000', '45.0000', '2.0000', '301-30033.jpg', 13, NULL, '', '', '', '', '', '', '8.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'green-base-red-flowers-30033', NULL, NULL, 1, 0, 0),
(486, '301-30034', 'Solid Peach/White Lining-30034', '', 1, '15.0000', '45.0000', '2.0000', '301-30034.jpg', 13, NULL, '', '', '', '', '', '', '7.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'solid-peachwhite-lining-30034', NULL, NULL, 1, 0, 0),
(487, '301-30035', 'White Base/ Geen,Grey-30035', '', 1, '15.0000', '45.0000', '2.0000', '301-30035.jpg', 13, NULL, '', '', '', '', '', '', '7.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'white-base-geengrey-30035', NULL, NULL, 1, 0, 0),
(488, '301-30036', 'Blue Borders White Flowers-30036', '', 1, '15.0000', '45.0000', '2.0000', '301-30036.jpg', 13, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'blue-borders-white-flowers-30036', NULL, NULL, 1, 0, 0),
(489, '301-30037', 'Multi Lining Bordes/Blue-30037', '', 1, '15.0000', '45.0000', '2.0000', '301-30037.jpg', 13, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'multi-lining-bordesblue-30037', NULL, NULL, 1, 0, 0),
(490, '301-30039', 'Pink & Blue Flowers-30039', '', 1, '15.0000', '45.0000', '2.0000', '301-30039.jpg', 13, NULL, '', '', '', '', '', '', '10.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'pink-blue-flowers-30039', NULL, NULL, 1, 0, 0),
(491, '301-30041', 'Black & White Shapes-30041', '', 1, '15.0000', '45.0000', '2.0000', '301-30041.jpg', 13, NULL, '', '', '', '', '', '', '7.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'black-white-shapes-30041', NULL, NULL, 1, 0, 0),
(492, '301-30043', 'Orange Sliced Flowers-30043', '', 1, '15.0000', '45.0000', '2.0000', '301-30043.jpg', 13, NULL, '', '', '', '', '', '', '7.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'orange-sliced-flowers-30043', NULL, NULL, 1, 0, 0),
(493, '301-30044', 'Blue Circle/White Triangels-30044', '', 1, '15.0000', '45.0000', '2.0000', '301-30044.jpg', 13, NULL, '', '', '', '', '', '', '8.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'blue-circlewhite-triangels-30044', NULL, NULL, 1, 0, 0),
(494, '301-30045', 'Flowers Red White Blue-30045', '', 1, '15.0000', '45.0000', '2.0000', '301-30045.jpg', 13, NULL, '', '', '', '', '', '', '7.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'flowers-red-white-blue-30045', NULL, NULL, 1, 0, 0),
(495, '301-30046', 'Small flowers/ Red Lines- 30046', '', 1, '15.0000', '45.0000', '2.0000', '301-30046.jpg', 13, NULL, '', '', '', '', '', '', NULL, 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'small-flowers-red-lines-30046', NULL, NULL, 1, 0, 0),
(496, '301-30047', 'Orange Base Blue Flowers-30047', '', 1, '15.0000', '45.0000', '2.0000', '301-30047.jpg', 13, NULL, '', '', '', '', '', '', '8.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'orange-base-blue-flowers-30047', NULL, NULL, 1, 0, 0),
(497, '301-30048', 'Grey Waves- 30048', '', 1, '15.0000', '45.0000', '2.0000', '301-30048.jpg', 13, NULL, '', '', '', '', '', '', '10.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'grey-waves-30048', NULL, NULL, 1, 0, 0),
(498, '301-30049', 'Black & White Stripes-30049', '', 1, '15.0000', '45.0000', '2.0000', '301-30049.jpg', 13, NULL, '', '', '', '', '', '', '9.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'black-white-stripes-30049', NULL, NULL, 1, 0, 0),
(499, '301-30051', 'Blue Square Flowers-30051', '', 1, '15.0000', '45.0000', '2.0000', '301-30051.jpg', 13, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'blue-square-flowers-30051', NULL, NULL, 1, 0, 0),
(500, '301-30053', 'white Base/ Purple,Grey-30053', '', 1, '15.0000', '45.0000', '2.0000', '301-30053.jpg', 13, NULL, '', '', '', '', '', '', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'white-base-purplegrey-30053', NULL, NULL, 1, 0, 0),
(501, '301-30055', 'Blue Flowers Pattern-30055', '', 1, '15.0000', '45.0000', '2.0000', '301-30055.jpg', 13, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'blue-flowers-pattern-30055', NULL, NULL, 1, 0, 0),
(502, '301-30056', 'Double Prnt Red/Blue-30056', '', 1, '25.0000', '65.0000', '2.0000', '301-30056.jpg', 13, NULL, '', '', '', '', '', '', '7.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'double-prnt-redblue-30056', NULL, NULL, 1, 0, 0),
(503, '302-30065', 'Peach Base/Dotted Peach-30065', '', 1, '25.0000', '65.0000', '2.0000', '302-30065.jpg', 27, 14, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'peach-basedotted-peach-30065', NULL, NULL, 1, 0, 0),
(504, '302-30067', 'Lime Base/ Peach Dotted-30067', '', 1, '25.0000', '65.0000', '2.0000', '302-30067.jpg', 14, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'lime-base-peach-dotted-30067', NULL, NULL, 1, 0, 0),
(505, '302-30070', 'Brown Square/ Silver Pearl-30070', 'Brown Square/ Silver Pearl-30070', 1, '25.0000', '65.0000', '2.0000', '302-30070.jpg', 14, NULL, '', '', '', '', '', '', NULL, NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 2, 'brown-square-silver-pearl-30070', NULL, NULL, 1, 0, 0),
(506, '302-30071', 'Light Pink Square/Silver Pearl-30071', '', 1, '25.0000', '65.0000', '2.0000', '302-30071.jpg', 14, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'light-pink-squaresilver-pearl-30071', NULL, NULL, 1, 0, 0),
(507, '302-30074', 'Purple Base/Peach Dotted-30074', '', 1, '25.0000', '65.0000', '2.0000', '302-30074.jpg', 14, NULL, '', '', '', '', '', '', NULL, 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'purple-basepeach-dotted-30074', NULL, NULL, 1, 0, 0),
(508, '302-30075', 'Peach Base/3 Hearts-30075', '', 1, '25.0000', '65.0000', '2.0000', '302-30075.jpg', 14, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'peach-base3-hearts-30075', NULL, NULL, 1, 0, 0),
(509, '302-30076', 'Pink Base/Peach Dotted-30076', '', 1, '25.0000', '65.0000', '2.0000', '302-30076.jpg', 14, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'pink-basepeach-dotted-30076', NULL, NULL, 1, 0, 0),
(510, '302-30077', 'Lime Base/3 Hearts-30077', '', 1, '25.0000', '65.0000', '2.0000', '302-30077.jpg', 14, NULL, '', '', '', '', '', '', NULL, 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'lime-base3-hearts-30077', NULL, NULL, 1, 0, 0),
(511, '302-30078', 'Light Grey Square/ Pearls-30078', '', 1, '25.0000', '65.0000', '2.0000', '302-30078.jpg', 14, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'light-grey-square-pearls-30078', NULL, NULL, 1, 0, 0),
(512, '302-30079', 'Green Base/ 3 Hearts-30079', '', 1, '25.0000', '65.0000', '2.0000', '302-30079.jpg', 14, NULL, '', '', '', '', '', '', NULL, 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'green-base-3-hearts-30079', NULL, NULL, 1, 0, 0),
(513, '302-30080', 'Light Pink Dotted.wave-30080', '', 1, '25.0000', '65.0000', '2.0000', '302-30080.jpg', 14, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'light-pink-dottedwave-30080', NULL, NULL, 1, 0, 0),
(514, '302-30081', 'Peach Dotted Wave-30081', '', 1, '25.0000', '65.0000', '2.0000', '302-30081.jpg', 14, NULL, '', '', '', '', '', '', NULL, 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'peach-dotted-wave-30081', NULL, NULL, 1, 0, 0),
(515, '302-30084', 'Greyish Green Circle/ Net Border-300084', '', 1, '25.0000', '65.0000', '2.0000', '302-30084.jpg', 14, NULL, '', '', '', '', '', '', NULL, 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'greyish-green-circle-net-border-300084', NULL, NULL, 1, 0, 0),
(516, '304-30115', 'Orange/Multi Color Circle', 'Orange/Multi Color Circle', 1, '15.0000', '45.0000', '2.0000', '304-30115.jpg', 18, NULL, '', '', '', '', '', '', NULL, NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 2, 'orangemulti-color-circle', NULL, NULL, 1, 0, 0),
(517, '304-30116', 'Pink/Multi Color Cirlce', '', 1, '15.0000', '45.0000', '2.0000', '304-30116.jpg', 18, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'pinkmulti-color-cirlce', NULL, NULL, 1, 0, 0),
(518, '304-30117', 'Blue/Pink Blue Flowers-30117', '', 1, '15.0000', '45.0000', '2.0000', '304-30117.jpg', 18, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'bluepink-blue-flowers-30117', NULL, NULL, 1, 0, 0),
(519, '304-30118', 'Dark Maroon/Net Blue Border-30118', '', 1, '15.0000', '45.0000', '2.0000', '304-30118.jpg', 18, NULL, '', '', '', '', '', '', NULL, 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'dark-maroonnet-blue-border-30118', NULL, NULL, 1, 0, 0),
(520, '304-30119', 'Yellow/Net Blue Borde-30119', '', 1, '15.0000', '45.0000', '2.0000', '304-30119.jpg', 18, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'yellownet-blue-borde-30119', NULL, NULL, 1, 0, 0),
(521, '304-30120', 'Green/ Multi Color Circles-30120', '', 1, '15.0000', '45.0000', '2.0000', '304-30120.jpg', 18, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'green-multi-color-circles-30120', NULL, NULL, 1, 0, 0),
(522, '304-30121', 'Solid Pink/Blue Flowers-30121', '', 1, '15.0000', '45.0000', '2.0000', '304-30121.jpg', 18, NULL, '', '', '', '', '', '', NULL, 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'solid-pinkblue-flowers-30121', NULL, NULL, 1, 0, 0),
(523, '304-30122', 'Light Green/Net Green Flower-30122', 'Light Green/Net Green Flower-30122', 1, '15.0000', '45.0000', '2.0000', '304-30122.jpg', 18, NULL, '', '', '', '', '', '', '1.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 2, 'light-greennet-green-flower-30122', NULL, NULL, 1, 0, 0),
(524, '304-30123', 'Pink/Net Green Flower-30123', '', 1, '15.0000', '45.0000', '2.0000', '304-30123.jpg', 18, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'pinknet-green-flower-30123', NULL, NULL, 1, 0, 0),
(525, '304-30124', 'Peach/Net Green Flower-30124', '', 1, '15.0000', '45.0000', '2.0000', '304-30124.jpg', 18, NULL, '', '', '', '', '', '', NULL, 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'peachnet-green-flower-30124', NULL, NULL, 1, 0, 0),
(526, '304-30125', 'Aqua Dark/Net Aqua Flowers-30125', '', 1, '15.0000', '45.0000', '2.0000', '304-30125.jpg', 18, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'aqua-darknet-aqua-flowers-30125', NULL, NULL, 1, 0, 0),
(527, '304-30126', 'Light Purple/Net Aqua Flowers-30126', '', 1, '15.0000', '45.0000', '2.0000', '304-30126.jpg', 18, NULL, '', '', '', '', '', '', NULL, 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'light-purplenet-aqua-flowers-30126', NULL, NULL, 1, 0, 0),
(528, '304-30127', 'Pink/ Black Flowers-30127', '', 1, '15.0000', '45.0000', '2.0000', '304-30127.jpg', 18, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'pink-black-flowers-30127', NULL, NULL, 1, 0, 0),
(529, '304-30128', 'Purple/White Flower-30128', '', 1, '15.0000', '45.0000', '2.0000', '304-30128.jpg', 18, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'purplewhite-flower-30128', NULL, NULL, 1, 0, 0),
(530, '304-30129', 'Baby Pink/White Flower-30129', '', 1, '15.0000', '45.0000', '2.0000', '304-30129.jpg', 18, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'baby-pinkwhite-flower-30129', NULL, NULL, 1, 0, 0),
(531, '305-30057', 'Brown-30057', '', 1, '15.0000', '45.0000', '2.0000', '305-30057.jpg', 21, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'brown-30057', NULL, NULL, 1, 0, 0),
(532, '305-30058', 'Light Pink-300358', 'Light Pink-300358', 1, '15.0000', '45.0000', '2.0000', '305-30058.jpg', 21, NULL, '', '', '', '', '', '', '2.0000', NULL, 1, '', NULL, 'code128', NULL, '', NULL, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, 2, 'light-pink-300358', NULL, NULL, 1, 0, 0),
(533, '305-30059', 'Dark Blue-30059', '', 1, '15.0000', '45.0000', '2.0000', '305-30059.jpg', 21, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'dark-blue-30059', NULL, NULL, 1, 0, 0),
(534, '305-30060', 'Camel-30060', '', 1, '15.0000', '45.0000', '2.0000', '305-30060.jpg', 21, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'camel-30060', NULL, NULL, 1, 0, 0),
(535, '305-30062', 'Sky Blue-30062', '', 1, '15.0000', '45.0000', '2.0000', '305-30062.jpg', 21, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'sky-blue-30062', NULL, NULL, 1, 0, 0),
(548, '311-30109', 'Maroon/Yellow Blue Branch-30109', '', 1, '15.0000', '45.0000', '2.0000', '', 15, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'maroonyellow-blue-branch-30109', NULL, NULL, 1, 0, 0),
(549, '311-30110', 'Skin/Green Purple Branch-30110', '', 1, '15.0000', '45.0000', '2.0000', '', 15, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'skingreen-purple-branch-30110', NULL, NULL, 1, 0, 0),
(550, '311-30111', 'Blue/Yellow Red Branch-30111', '', 1, '15.0000', '45.0000', '2.0000', '', 15, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'blueyellow-red-branch-30111', NULL, NULL, 1, 0, 0),
(551, '311-30112', 'Pink/White Cheeta Print-30112', '', 1, '15.0000', '45.0000', '2.0000', '', 15, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'pinkwhite-cheeta-print-30112', NULL, NULL, 1, 0, 0),
(552, '311-30113', 'Black/Yellow Pink Branch-30113', '', 1, '15.0000', '45.0000', '2.0000', '', 15, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'blackyellow-pink-branch-30113', NULL, NULL, 1, 0, 0),
(553, '311-30114', 'Red/Yellow Green Branch-30114', '', 1, '15.0000', '45.0000', '2.0000', '', 15, NULL, '', '', '', '', '', '', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'redyellow-green-branch-30114', NULL, NULL, 1, 0, 0),
(554, '312-30089', 'Grey/Pink Butterflies-30089', '', 1, '15.0000', '45.0000', '2.0000', '312-30089.jpg', 16, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'greypink-butterflies-30089', NULL, NULL, 1, 0, 0),
(555, '312-30090', 'White/Pink Butterflies Flowers-30090', '', 1, '15.0000', '45.0000', '2.0000', '312-30090.jpg', 16, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'whitepink-butterflies-flowers-30090', NULL, NULL, 1, 0, 0),
(556, '312-30091', 'Purple Birds Branches-30091', '', 1, '15.0000', '45.0000', '2.0000', '312-30091.jpg', 16, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'purple-birds-branches-30091', NULL, NULL, 1, 0, 0),
(557, '312-30092', 'Aqua Printed-30092', '', 1, '15.0000', '45.0000', '2.0000', '', 16, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'aqua-printed-30092', NULL, NULL, 1, 0, 0),
(558, '312-30093', 'White Music Notes-30093', '', 1, '15.0000', '45.0000', '2.0000', '312-30093.jpg', 16, NULL, '', '', '', '', '', '', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'white-music-notes-30093', NULL, NULL, 1, 0, 0),
(559, '312-30094', 'Light Dark Green/Purple Flowers-30094', '', 1, '15.0000', '45.0000', '2.0000', '312-30094.jpg', 16, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'light-dark-greenpurple-flowers-30094', NULL, NULL, 1, 0, 0),
(560, '312-30095', 'Green City-30095', '', 1, '15.0000', '45.0000', '2.0000', '312-30095.jpg', 16, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'green-city-30095', NULL, NULL, 1, 0, 0),
(561, '312-30096', 'Light Dark Pink/Orange Flowers-30096', '', 1, '15.0000', '45.0000', '2.0000', '312-30096.jpg', 16, NULL, '', '', '', '', '', '', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'light-dark-pinkorange-flowers-30096', NULL, NULL, 1, 0, 0),
(562, '312-30097', 'Pink/Lime Butterflies-30097', '', 1, '15.0000', '45.0000', '2.0000', '312-30097.jpg', 16, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'pinklime-butterflies-30097', NULL, NULL, 1, 0, 0),
(563, '312-30098', 'Peach/ Lime Butteflies-30098', '', 1, '15.0000', '45.0000', '2.0000', '', 16, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'peach-lime-butteflies-30098', NULL, NULL, 1, 0, 0),
(564, '312-30099', 'Peach/Black Print-30099', '', 1, '15.0000', '45.0000', '2.0000', '312-30099.jpg', 16, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'peachblack-print-30099', NULL, NULL, 1, 0, 0),
(565, '312-30100', 'AquaBlue/Golden Leave-30100', '', 1, '15.0000', '45.0000', '2.0000', '312-30100.jpg', 16, NULL, '', '', '', '', '', '', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'aquabluegolden-leave-30100', NULL, NULL, 1, 0, 0),
(566, '312-30101', 'Aqua Green/Blue Flowers-30101', '', 1, '15.0000', '45.0000', '2.0000', '312-30101.jpg', 16, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'aqua-greenblue-flowers-30101', NULL, NULL, 1, 0, 0),
(567, '312-30102', 'Blue City-30102', '', 1, '15.0000', '45.0000', '2.0000', '312-30102.jpg', 16, NULL, '', '', '', '', '', '', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'blue-city-30102', NULL, NULL, 1, 0, 0);
INSERT INTO `sma_tst` (`id`, `code`, `name`, `translated_name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `promotion`, `promo_price`, `start_date`, `end_date`, `supplier1_part_no`, `supplier2_part_no`, `supplier3_part_no`, `supplier4_part_no`, `supplier5_part_no`, `sale_unit`, `purchase_unit`, `brand`, `slug`, `featured`, `weight`, `status`, `free_shipment`, `is_updated`) VALUES
(568, '312-30103', 'Aqua White/Blue Pink Flowers-30103', '', 1, '15.0000', '45.0000', '2.0000', '312-30103.jpg', 16, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'aqua-whiteblue-pink-flowers-30103', NULL, NULL, 1, 0, 0),
(569, '312-30104', 'Blue Printed-30104', '', 1, '15.0000', '45.0000', '2.0000', '312-30104.jpg', 16, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'blue-printed-30104', NULL, NULL, 1, 0, 0),
(570, '312-30105', 'Yellow Red RosesBlue Border-30105', '', 1, '15.0000', '45.0000', '2.0000', '312-30105.jpg', 16, NULL, '', '', '', '', '', '', NULL, 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'yellow-red-rosesblue-border-30105', NULL, NULL, 1, 0, 0),
(571, '312-30106', '3Shade/Yellow Peach Purple-30106', '', 1, '15.0000', '45.0000', '2.0000', '', 16, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, '3shadeyellow-peach-purple-30106', NULL, NULL, 1, 0, 0),
(572, '312-30107', 'Pink City-30107', '', 1, '15.0000', '45.0000', '2.0000', '', 16, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'pink-city-30107', NULL, NULL, 1, 0, 0),
(573, '312-30108', 'Peach Music Notes-30108', '', 1, '15.0000', '45.0000', '2.0000', '312-30108.jpg', 16, NULL, '', '', '', '', '', '', '2.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'peach-music-notes-30108', NULL, NULL, 1, 0, 0),
(574, '302-30131', 'Black/Golden Heart-30131', '', 1, '25.0000', '65.0000', '2.0000', '302-30131.jpg', 14, NULL, '', '', '', '', '', '', NULL, 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'blackgolden-heart-30131', NULL, NULL, 1, 0, 0),
(575, '302-30132', 'Grey/Golden Stone Border-30132', '', 1, '25.0000', '65.0000', '2.0000', '302-30132.jpg', 14, NULL, '', '', '', '', '', '', NULL, 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'greygolden-stone-border-30132', NULL, NULL, 1, 0, 0),
(576, '302-30134', 'Green/Golden Net Border-30134', '', 1, '25.0000', '65.0000', '2.0000', '302-30134.jpg', 14, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'greengolden-net-border-30134', NULL, NULL, 1, 0, 0),
(577, '302-30135', 'Cream/Golden Border-30135', '', 1, '25.0000', '65.0000', '2.0000', '302-30135.jpg', 14, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'creamgolden-border-30135', NULL, NULL, 1, 0, 0),
(578, '302-30066', 'Brownish Pink - 30066', '', 1, '25.0000', '65.0000', '2.0000', '302-30066.jpg', 14, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'brownish-pink-30066', NULL, NULL, 1, 0, 0),
(579, '302-30136', 'Grey Dotted Wave-30136', '', 1, '25.0000', '65.0000', '2.0000', '302-30136.jpg', 14, NULL, '', '', '', '', '', '', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'grey-dotted-wave-30136', NULL, NULL, 1, 0, 0),
(580, '301-30005', 'Army- 30005', '', 1, '15.0000', '45.0000', '2.0000', '301-30005.jpg', 13, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'army-30005', NULL, NULL, 1, 0, 0),
(581, '301-30137', 'Blue Border/Ajrak- 30137', '', 1, '15.0000', '45.0000', '2.0000', '301-30137.jpg', 13, NULL, '', '', '', '', '', '', NULL, 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'blue-borderajrak-30137', NULL, NULL, 1, 0, 0),
(582, '301-30138', 'Black Gold Stripes Border-30138', '', 1, '15.0000', '45.0000', '2.0000', '301-30138.jpg', 13, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'black-gold-stripes-border-30138', NULL, NULL, 1, 0, 0),
(583, '301-30139', 'Black Red White Stripes-30139', '', 1, '15.0000', '45.0000', '2.0000', '301-30139.jpg', 13, NULL, '', '', '', '', '', '', '16.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'black-red-white-stripes-30139', NULL, NULL, 1, 0, 0),
(584, '301-30140', 'White Base Ajrak-30140', '', 1, '15.0000', '45.0000', '2.0000', '301-30140.jpg', 13, NULL, '', '', '', '', '', '', '8.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'white-base-ajrak-30140', NULL, NULL, 1, 0, 0),
(585, '301-30141', 'Multi Color Stripes', '', 1, '15.0000', '45.0000', '2.0000', '301-30141.jpg', 13, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'multi-color-stripes', NULL, NULL, 1, 0, 0),
(586, '306-30142', 'Solid Dark Grey', '', 1, '15.0000', '45.0000', '2.0000', '', 22, NULL, '', '', '', '', '', '', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'solid-dark-grey', NULL, NULL, 1, 0, 0),
(587, '306-30143', 'Solid Black', '', 1, '15.0000', '45.0000', '2.0000', '', 22, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'solid-black', NULL, NULL, 1, 0, 0),
(588, '306-30144', 'Solid Blue', '', 1, '15.0000', '45.0000', '2.0000', '', 22, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'solid-blue', NULL, NULL, 1, 0, 0),
(589, '306-30145', 'Solid Cream', '', 1, '15.0000', '45.0000', '2.0000', '', 22, NULL, '', '', '', '', '', '', '8.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'solid-cream', NULL, NULL, 1, 0, 0),
(590, '306-30146', 'Solid Dark Brown', '', 1, '15.0000', '45.0000', '2.0000', '', 22, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'solid-dark-brown', NULL, NULL, 1, 0, 0),
(591, '306-30147', 'Solid Grey', '', 1, '15.0000', '45.0000', '2.0000', '', 22, NULL, '', '', '', '', '', '', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'solid-grey', NULL, NULL, 1, 0, 0),
(592, '306-30148', 'Solid Camel', '', 1, '15.0000', '45.0000', '2.0000', '', 22, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'solid-camel', NULL, NULL, 1, 0, 0),
(593, '306-30149', 'Solid Maroon', '', 1, '15.0000', '45.0000', '2.0000', '', 22, NULL, '', '', '', '', '', '', '4.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'solid-maroon', NULL, NULL, 1, 0, 0),
(594, '306-30150', 'Solid Navy Blue', '', 1, '15.0000', '45.0000', '2.0000', '', 22, NULL, '', '', '', '', '', '', '6.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'solid-navy-blue', NULL, NULL, 1, 0, 0),
(595, '306-30151', 'Solid Baby Pink', '', 1, '15.0000', '45.0000', '2.0000', '', 22, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'solid-baby-pink', NULL, NULL, 1, 0, 0),
(596, '306-30152', 'Solid Royal Blue', '', 1, '15.0000', '45.0000', '2.0000', '', 22, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'solid-royal-blue', NULL, NULL, 1, 0, 0),
(597, '306-30153', 'Solid White', '', 1, '15.0000', '45.0000', '2.0000', '', 22, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'solid-white', NULL, NULL, 1, 0, 0),
(598, '202-2049', 'Solid Cream', '', 1, '50.0000', '149.0000', '2.0000', '', 10, NULL, '', '', '', '', '', '', '7.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'solid-cream1', NULL, NULL, 1, 0, 0),
(599, '202-2050', 'Grey White Lines', '', 1, '50.0000', '149.0000', '2.0000', '', 10, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'grey-white-lines', NULL, NULL, 1, 0, 0),
(600, '202-2051', 'Light White Box', '', 1, '50.0000', '149.0000', '2.0000', '', 10, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'light-white-box', NULL, NULL, 1, 0, 0),
(604, '302-30064', 'Light Golden/Net Border-30064', '', 1, '25.0000', '65.0000', '2.0000', '302-30064.jpg', 14, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'light-goldennet-border-30064', NULL, NULL, 1, 0, 0),
(605, '305-30154', 'Black/Multi Color Borders', '', 1, '15.0000', '45.0000', '2.0000', '305-30154.jpg', 21, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 'blackmulti-color-borders', NULL, NULL, 1, 0, 0),
(606, '104-4008', 'purple/Purple Perals Chest', '', 1, '73.0000', '269.0000', '2.0000', '104-4008.jpg', 4, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'purplepurple-perals-chest', NULL, NULL, 1, 0, 0),
(607, '104-4009', 'Baby Pink/Pink Pearls Chest Sleeves', '', 1, '73.0000', '269.0000', '2.0000', '104-4009.jpg', 4, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'baby-pinkpink-pearls-chest-sleeves', NULL, NULL, 1, 0, 0),
(608, '104-4010', 'Purple Flowers Printed/Sleveless', '', 1, '84.0000', '269.0000', '2.0000', '104-4010.jpg', 4, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'purple-flowers-printedsleveless', NULL, NULL, 1, 0, 0),
(609, '104-4011', 'PeachPearls/Bow Belt Sleevless', '', 1, '84.0000', '289.0000', '2.0000', '104-4011.jpg', 4, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'peachpearlsbow-belt-sleevless', NULL, NULL, 1, 0, 0),
(610, '104-4012', 'White Printed Flower chest/White Pearls Border', '', 1, '80.0000', '269.0000', '2.0000', '104-4012.jpg', 4, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'white-printed-flower-chestwhite-pearls-border', NULL, NULL, 1, 0, 0),
(611, '104-4013', 'White Neckless/White Pearls Chest', '', 1, '85.0000', '289.0000', '2.0000', '104-4013.jpg', 4, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'white-necklesswhite-pearls-chest', NULL, NULL, 1, 0, 0),
(612, '104-4014', 'Peach Neckless/Peach Pearls Shoulders', '', 1, '85.0000', '289.0000', '2.0000', '104-4014.jpg', 4, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'peach-necklesspeach-pearls-shoulders', NULL, NULL, 1, 0, 0),
(613, '104-4015', 'White/White Bow Pearls Belt', '', 1, '73.0000', '249.0000', '2.0000', '104-4015.jpg', 4, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'whitewhite-bow-pearls-belt', NULL, NULL, 1, 0, 0),
(614, '102-2032', 'Princess Belle', '', 1, '69.0000', '189.0000', '2.0000', '102-2032.jpg', 2, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'princess-belle', NULL, NULL, 1, 0, 0),
(615, '105-5017', 'Purple/Purple Flowers Chest', '', 1, '45.0000', '149.0000', '2.0000', '105-5017.jpg', 5, NULL, '', '', '', '', '', '', '5.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'purplepurple-flowers-chest', NULL, NULL, 1, 0, 0),
(616, '102-2033', 'Sofia with Sequin Skirt', '', 1, '30.0000', '94.0000', '2.0000', '102-2033.jpg', 2, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'sofia-with-sequin-skirt', NULL, NULL, 1, 0, 0),
(618, '101-1069', 'RAINBOW SUMMER DRESS', '', 1, '25.0000', '89.0000', '2.0000', '101-1069.jpg', 1, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'rainbow-summer-dress', NULL, NULL, 1, 0, 0),
(619, '101-1070', 'PEACH & WHITE PRINTED FLOWER DRESS', '', 1, '25.0000', '89.0000', '2.0000', '101-1070.jpg', 1, NULL, '', '', '', '', '', '', '1.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'peach-white-printed-flower-dress', NULL, NULL, 1, 0, 0),
(620, '101-1071', 'RED COLOR DRESS W/ GOLD SEQUENCE COLAR', '', 1, '30.0000', '94.0000', '2.0000', '101-1071.jpg', 1, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'red-color-dress-w-gold-sequence-colar', NULL, NULL, 1, 0, 0),
(621, '101-1072', 'MICKEY MOUSE COTTON DRESS', '', 1, '30.0000', '30.0000', '2.0000', '101-1072.jpg', 1, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'mickey-mouse-cotton-dress', NULL, NULL, 1, 0, 0),
(622, '101-1073', 'Cotton Bud Onesie Dress', '', 1, '30.0000', '94.0000', '2.0000', '101-1073.jpg', 1, NULL, '', '', '', '', '', '', '3.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'cotton-bud-onesie-dress', NULL, NULL, 1, 0, 0),
(623, '101-1074', 'Peacock Printed DRESS', '', 1, '30.0000', '94.0000', '2.0000', '101-1074.jpg', 1, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'peacock-printed-dress', NULL, NULL, 1, 0, 0),
(624, '101-1075', 'SKIN COLOR WITH BIG SEQUENCE LAYERED DRESS', '', 1, '25.0000', '89.0000', '2.0000', '101-1075.jpg', 1, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'skin-color-with-big-sequence-layered-dress', NULL, NULL, 1, 0, 0),
(625, '101-1076', 'SKIN COLOR WITH SEQUENCE SUMMER DRESS', '', 1, '25.0000', '89.0000', '2.0000', '101-1076.jpg', 1, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'skin-color-with-sequence-summer-dress', NULL, NULL, 1, 0, 0),
(626, '101-1077', 'MAONG TOP & PINK SKIRT SUMMER DRESS', '', 1, '25.0000', '89.0000', '2.0000', '101-1077.jpg', 1, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'maong-top-pink-skirt-summer-dress', NULL, NULL, 1, 0, 0),
(627, '101-1078', 'YELLOW LACE DRESS', 'YELLOW LACE DRESS', 2, '25.0000', '89.0000', '2.0000', '101-1078.jpg', 1, NULL, '', '', '', '', '', '', NULL, 1, 1, '', NULL, 'code128', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 2, 2, 1, 'yellow-lace-dress', 1, '0.0000', 1, 0, 0),
(628, '101-1079', 'Summer Dress with Doll Design', 'Summer Dress with Doll Design', 2, '25.0000', '89.0000', '2.0000', '101-1079.jpg', 1, NULL, '', '', '', '', '', '', NULL, 1, 1, '', NULL, 'code128', NULL, '<p>In Informatics, dummy data is benign information that does not contain any useful data, but serves to reserve space where real data is nominally present. Dummy data can be used as a placeholder for both testing and operational purposes.</p>', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '50.0000', '2017-09-19', '2017-09-20', '', NULL, NULL, NULL, NULL, 2, 2, 1, 'summer-dress-with-doll-design', NULL, '0.0000', 1, 1, 1),
(629, '101-1080', 'White Black/Black Border Pink Belt', '', 1, '25.0000', '89.0000', '2.0000', '101-1080.jpg', 1, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'white-blackblack-border-pink-belt', NULL, NULL, 1, 0, 0),
(630, '205-2052', 'Tiger Shirt', '', 1, '15.0000', '45.0000', '2.0000', '205-2052.jpg', 25, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'tiger-shirt', NULL, NULL, 1, 0, 0),
(631, '205-2053', 'Dinosaur', '', 1, '15.0000', '45.0000', '2.0000', '205-2053.jpg', 25, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'dinosaur', NULL, NULL, 1, 0, 0),
(632, '205-2054', 'Tweeter Shirt', '', 1, '15.0000', '45.0000', '2.0000', '205-2054.jpg', 25, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'tweeter-shirt', NULL, NULL, 1, 0, 0),
(633, '205-2055', 'Minon Shirt', '', 1, '15.0000', '45.0000', '2.0000', '205-2055.jpg', 25, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'minon-shirt', NULL, NULL, 1, 0, 0),
(634, '205-2056', 'NAVY BLUE MINION SHIRT', 'NAVY BLUE MINION SHIRT', 2, '15.0000', '45.0000', '2.0000', 'e604152c23daf38d235054032d7bc3db.jpg', 25, NULL, '', '', '', '', '', '', NULL, 1, 1, '', NULL, 'code128', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 2, 2, 1, 'navy-blue-minion-shirt', NULL, '0.0000', 1, 0, 0),
(635, '205-2057', 'Rio 3D Shirts', '', 1, '15.0000', '45.0000', '2.0000', '205-2057.jpg', 25, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'rio-3d-shirts', NULL, NULL, 1, 0, 0),
(636, '205-2058', 'Pirate Shirt', '', 1, '15.0000', '45.0000', '2.0000', '205-2058.jpg', 25, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pirate-shirt', NULL, NULL, 1, 0, 0),
(637, '202-2059', 'Black And White Stripe/Stripes', '', 1, '15.0000', '45.0000', '2.0000', '202-2059.jpg', 25, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'black-and-white-stripestripes', NULL, NULL, 1, 0, 0),
(638, '202-2060', 'Sailor/Blue White Stripes', '', 1, '15.0000', '45.0000', '2.0000', '202-2060.jpg', 25, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'sailorblue-white-stripes', NULL, NULL, 1, 0, 0),
(639, '202-2061', 'Blue White/Long Sleeves Blue White/Long Sleeves Blue White/Long Sleeves Blue White/Long Sleeves Blue White/Long Sleeves', '', 1, '15.0000', '45.0000', '2.0000', '202-2061.jpg', 25, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'blue-whitelong-sleeves', NULL, NULL, 1, 1, 0),
(640, '205-2062', 'Angry bird shirt', '', 1, '15.0000', '45.0000', '2.0000', '205-2062.jpg', 25, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'angry-bird-shirt', NULL, NULL, 1, 0, 0),
(641, '105-5018', 'tiger/ Orange', '', 1, '30.0000', '94.0000', '2.0000', '105-5018.jpg', 5, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'tiger-orange', NULL, NULL, 1, 0, 0),
(642, '105-5019', 'black/ green', '', 1, '30.0000', '94.0000', '2.0000', '105-5019.jpg', 5, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'black-green', NULL, NULL, 1, 0, 0),
(643, '110-1119', 'Pink Long Sleeve Flowers dummy extra name lorem test hello frock', 'Pink Long Sleeve Flowers', 2, '65.0000', '149.0000', '2.0000', '110-1119.jpg', 20, NULL, '', '', '', '', '', '', NULL, 1, 1, '', NULL, 'code128', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 2, 2, 1, 'pink-long-sleeve-flowers-dummy-extra-name-lorem-test-he', NULL, '0.0000', 1, 0, 0),
(644, '107-7303', 'Glitter Dotted Skirt', '', 1, '15.0000', '45.0000', '2.0000', '107-7303.jpg', 7, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'glitter-dotted-skirt', NULL, NULL, 1, 0, 0),
(645, '101-1081', 'White Gown with Multipal Ribbon Belt', '', 1, '45.0000', '119.0000', '2.0000', '101-1081.jpg', 1, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'white-gown-with-multipal-ribbon-belt', NULL, NULL, 1, 0, 0),
(646, '101-1082', 'Pink Gown with Embosed net Flowers', '', 1, '45.0000', '139.0000', '2.0000', '101-1082.jpg', 1, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-gown-with-embosed-net-flowers', NULL, NULL, 1, 0, 0),
(647, '101-1083', 'white Gown with Embosed net Flowers', '', 1, '45.0000', '139.0000', '2.0000', '101-1083.jpg', 1, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'white-gown-with-embosed-net-flowers', NULL, NULL, 1, 0, 0),
(648, '101-1084', 'Satin Gown with Pearls Sequin at Chest - White', '', 1, '45.0000', '169.0000', '2.0000', '101-1084.jpg', 1, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'satin-gown-with-pearls-sequin-at-chest-white', NULL, NULL, 1, 0, 0),
(649, '101-1085', 'Hot Pink Baby Gown with Silver Sequins', '', 1, '30.0000', '94.0000', '2.0000', '101-1085.jpg', 1, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'hot-pink-baby-gown-with-silver-sequins', NULL, NULL, 1, 0, 0),
(650, '101-1086', 'HOT PINK Satin with MULTI COLOR POLKA DOTS', '', 1, '45.0000', '119.0000', '2.0000', '101-1086.jpg', 1, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'hot-pink-satin-with-multi-color-polka-dots', NULL, NULL, 1, 0, 0),
(651, '111-1001', 'Elsa Crown Set', '', 1, '20.0000', '69.0000', '2.0000', '111-1001.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'elsa-crown-set', NULL, NULL, 1, 0, 0),
(652, '111-1002', 'Elsa wall Sticker', '', 1, '10.0000', '10.0000', '2.0000', '111-1002.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'elsa-wall-sticker', NULL, NULL, 1, 0, 0),
(653, '111-1003', 'Blue Gloves', '', 1, '10.0000', '29.0000', '2.0000', '111-1003.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'blue-gloves', NULL, NULL, 1, 0, 0),
(654, '111-1004', 'Anna Crown Set', '', 1, '20.0000', '69.0000', '2.0000', '111-1004.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'anna-crown-set', NULL, NULL, 1, 0, 0),
(655, '111-1005', 'Pink Gloves', '', 1, '10.0000', '29.0000', '2.0000', '111-1005.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-gloves', NULL, NULL, 1, 0, 0),
(656, '111-1006', 'Sofia Crown Set', '', 1, '15.0000', '39.0000', '2.0000', '111-1006.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'sofia-crown-set', NULL, NULL, 1, 0, 0),
(657, '111-1007', 'Sofia Accessories', '', 1, '10.0000', '29.0000', '2.0000', '111-1007.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'sofia-accessories', NULL, NULL, 1, 0, 0),
(658, '111-1008', 'Net Flower Hairband', '', 1, '15.0000', '35.0000', '2.0000', '111-1008.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'net-flower-hairband', NULL, NULL, 1, 0, 0),
(659, '111-1009', 'velvet Crown Hairband', '', 1, '5.0000', '25.0000', '2.0000', '111-1009.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'velvet-crown-hairband', NULL, NULL, 1, 0, 0),
(660, '111-1010', 'Princess Metal Crown', '', 1, '15.0000', '40.0000', '2.0000', '111-1010.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'princess-metal-crown', NULL, NULL, 1, 0, 0),
(661, '111-1011', 'Hat Pin with Long Frills', '', 1, '10.0000', '30.0000', '2.0000', '111-1011.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'hat-pin-with-long-frills', NULL, NULL, 1, 0, 0),
(662, '111-1012', 'Meduim Size Hat Clip', '', 1, '10.0000', '35.0000', '2.0000', '111-1012.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'meduim-size-hat-clip', NULL, NULL, 1, 0, 0),
(663, '111-1013', 'Large Hat Clip', '', 1, '15.0000', '45.0000', '2.0000', '111-1013.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'large-hat-clip', NULL, NULL, 1, 0, 0),
(664, '111-1014', 'PinkBox Hairband', '', 1, '5.0000', '10.0000', '2.0000', '111-1014.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pinkbox-hairband', NULL, NULL, 1, 0, 0),
(665, '111-1015', 'PinkBox Assorted Baby Headband in Box', '', 1, '5.0000', '5.0000', '2.0000', '111-1015.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pinkbox-assorted-baby-headband-in-box', NULL, NULL, 1, 0, 0),
(666, '111-1016', 'PinkBox Assorted Baby Hairband w/o Box', '', 1, '5.0000', '5.0000', '2.0000', '111-1016.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pinkbox-assorted-baby-hairband-wo-box', NULL, NULL, 1, 0, 0),
(667, '111-1017', 'PinkBox Assorted Hair Pins', '', 1, '5.0000', '5.0000', '2.0000', '111-1017.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pinkbox-assorted-hair-pins', NULL, NULL, 1, 0, 0),
(668, '111-1018', 'Pink Wings', '', 1, '10.0000', '35.0000', '2.0000', '111-1018.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'pink-wings', NULL, NULL, 1, 0, 0),
(669, '111-1019', 'Green Wings', '', 1, '10.0000', '35.0000', '2.0000', '111-1019.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'green-wings', NULL, NULL, 1, 0, 0),
(670, '111-1020', 'cotton body inner - short', '', 1, '10.0000', '25.0000', '2.0000', '111-1020.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'cotton-body-inner-short', NULL, NULL, 1, 0, 0),
(671, '111-1021', 'cotton body inner - long', '', 1, '15.0000', '45.0000', '2.0000', '111-1021.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'cotton-body-inner-long', NULL, NULL, 1, 0, 0),
(672, '111-1022', 'Stockings & Tights', '', 1, '15.0000', '40.0000', '2.0000', '111-1022.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'stockings-tights', NULL, NULL, 1, 0, 0),
(673, '111-1023', 'Petti Coat', '', 1, '20.0000', '65.0000', '2.0000', '111-1023.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'petti-coat', NULL, NULL, 1, 0, 0),
(674, '111-1024', 'BOW TIE', 'BOW TIE', 2, '7.0000', '25.0000', '2.0000', '111-1024.jpg', 26, NULL, '', '', '', '', '', '', NULL, 1, 1, '', NULL, 'code128', NULL, '<p>Go sporty this summer with this vintage navy and white striped v-neck t-shirt from the Abercrombie & Fitch. Perfect for pairing with denim and white kicks for a stylish sporty vibe. Will fit a UK 8-10, model shown is a UK 8 and 5’5. !!</p><p>Typography is the work of typesetters, compositors, typographers, graphic designers, art directors, manga artists, comic book artists, graffiti artists, and now—anyone who arranges words, letters, numbers, and symbols for publication, display, or distribution—from clerical workers and newsletter writers to anyone self-publishing materials.</p>', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 2, 2, 1, 'bow-tie', NULL, '0.0000', 1, 0, 0),
(675, '111-1025', 'LONG TIE', '', 1, '7.0000', '25.0000', '2.0000', '111-1025.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'long-tie', NULL, NULL, 1, 0, 0),
(676, '111-1026', 'BALLET SHOES - WHITE', '', 1, '12.0000', '40.0000', '2.0000', '111-1026.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'ballet-shoes-white', NULL, NULL, 1, 0, 0),
(677, '111-1027', 'BALLET SHOES - BLACK', '', 1, '12.0000', '40.0000', '2.0000', '111-1027.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'ballet-shoes-black', NULL, NULL, 1, 0, 0),
(678, '111-1028', 'BALLET SHOES - RED', '', 1, '12.0000', '40.0000', '2.0000', '111-1028.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'ballet-shoes-red', NULL, NULL, 1, 0, 0),
(679, '111-1029', 'BALLET SHOES - PINK', '', 1, '12.0000', '40.0000', '2.0000', '111-1029.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'ballet-shoes-pink', NULL, NULL, 1, 0, 0),
(680, '111-1030', 'MOCCASINS SILVER', '', 1, '12.0000', '40.0000', '2.0000', '111-1030.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'moccasins-silver', NULL, NULL, 1, 0, 0),
(681, '111-1031', 'MOCCASINS GOLD', '', 1, '12.0000', '40.0000', '2.0000', '111-1031.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'moccasins-gold', NULL, NULL, 1, 0, 0),
(682, '111-1032', 'MOCCASINS SKYBLUE', '', 1, '12.0000', '40.0000', '2.0000', '111-1032.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'moccasins-skyblue', NULL, NULL, 1, 0, 0),
(683, '111-1033', 'MOCCASINS RED', '', 1, '12.0000', '40.0000', '2.0000', '111-1033.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'moccasins-red', NULL, NULL, 1, 0, 0),
(684, '111-1034', 'MOCCASINS BROWN', '', 1, '12.0000', '40.0000', '2.0000', '111-1034.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'moccasins-brown', NULL, NULL, 1, 0, 0),
(685, '111-1035', 'MOCCASINS CAMEL', '', 1, '12.0000', '40.0000', '2.0000', '111-1035.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'moccasins-camel', NULL, NULL, 1, 0, 0),
(686, '111-1036', 'Lace Toddler Shoes', '', 1, '12.0000', '40.0000', '2.0000', '111-1036.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'lace-toddler-shoes', NULL, NULL, 1, 0, 0),
(687, '111-1037', 'Crocket Shoes - Brwon Black', '', 1, '12.0000', '40.0000', '2.0000', '111-1037.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'crocket-shoes-brwon-black', NULL, NULL, 1, 0, 0),
(688, '111-1038', 'Crocket Shoes - Purple Pink', '', 1, '12.0000', '40.0000', '2.0000', '111-1038.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'crocket-shoes-purple-pink', NULL, NULL, 1, 0, 0),
(689, '111-1039', 'Crocket Shoes - Red', '', 1, '12.0000', '40.0000', '2.0000', '111-1039.jpg', 26, NULL, '', '', '', '', '', '', '0.0000', 1, 1, NULL, NULL, 'code128', NULL, NULL, 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 'crocket-shoes-red', NULL, NULL, 1, 0, 0),
(690, '205-2063', 'Wolf Blue', 'Wolf', 2, '20.0000', '65.0000', '2.0000', '205-2063.jpg', 25, NULL, '', '', '', '', '', '', NULL, 1, 1, '', NULL, 'code128', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 2, 2, 1, 'wolf-blue', NULL, '0.0000', 1, 0, 0),
(693, '205-20620', 'New Product 2nd', 'Wolf new name', 2, '20.0000', '65.0000', '2.0000', '205-2063.jpg', 25, NULL, '', '', '', '', '', '', NULL, 1, 1, '', NULL, 'code128', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 2, 2, 1, 'wolf-blue', NULL, '0.0000', 1, 0, 0),
(694, '205-20699', 'New Product 1st', 'Wolf new 2nd name', 2, '20.0000', '65.0000', '2.0000', '205-2063.jpg', 25, NULL, '', '', '', '', '', '', NULL, 1, 1, '', NULL, 'code128', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 2, 2, 1, 'wolf-blue', NULL, '0.0000', 1, 0, 0),
(695, '205-20672', 'New Product XOX', 'Wolf new 2nd XOX', 2, '20.0000', '65.0000', '2.0000', '205-2063.jpg', 25, NULL, '', '', '', '', '', '', NULL, 1, 1, '', NULL, 'code128', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 2, 2, 1, 'wolf-blue', NULL, '0.0000', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_units`
--

CREATE TABLE `sma_units` (
  `id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(55) NOT NULL,
  `base_unit` int(11) DEFAULT NULL,
  `operator` varchar(1) DEFAULT NULL,
  `unit_value` varchar(55) DEFAULT NULL,
  `operation_value` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_units`
--

INSERT INTO `sma_units` (`id`, `code`, `name`, `base_unit`, `operator`, `unit_value`, `operation_value`) VALUES
(2, 'Pc1', 'Pc1', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_users`
--

CREATE TABLE `sma_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `last_ip_address` varbinary(45) DEFAULT NULL,
  `ip_address` varbinary(45) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `avatar` varchar(55) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `warehouse_id` int(10) UNSIGNED DEFAULT NULL,
  `biller_id` int(10) UNSIGNED DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `show_cost` tinyint(1) DEFAULT '0',
  `show_price` tinyint(1) DEFAULT '0',
  `award_points` int(11) DEFAULT '0',
  `view_right` tinyint(1) NOT NULL DEFAULT '0',
  `edit_right` tinyint(1) NOT NULL DEFAULT '0',
  `allow_discount` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_users`
--

INSERT INTO `sma_users` (`id`, `last_ip_address`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `avatar`, `gender`, `group_id`, `warehouse_id`, `biller_id`, `company_id`, `show_cost`, `show_price`, `award_points`, `view_right`, `edit_right`, `allow_discount`) VALUES
(1, 0x3a3a31, 0x0000, 'owner', '2c8ab736b2ccab4f50e72d5fd7d21020cbb77ae7', NULL, 'khuram@wistech.biz', NULL, NULL, NULL, NULL, 1351661704, 1523871979, 1, 'Owner', 'Owner', 'Stock Manager', '012345678', NULL, 'male', 1, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(2, 0x3a3a31, 0x3a3a31, 'ken', '2c8ab736b2ccab4f50e72d5fd7d21020cbb77ae7', NULL, 'ummaraasdkram@gmail.com', NULL, NULL, NULL, NULL, 1501488780, 1501587050, 1, 'Ken', 'Edgar', 'fit panter', '00526785105', NULL, 'male', 3, NULL, NULL, 9, 0, 0, 0, 0, 0, 0),
(3, 0x3a3a31, 0x3a3a31, 'umaro', 'd8f0a2db0d34f24e45de3f57dc699fb9eabf15d6', NULL, 'ummarakram@gmail.com', NULL, NULL, NULL, NULL, 1506591558, 1523037364, 1, 'umar', 'Akram', 'Keln', '3424248166', NULL, 'male', 3, NULL, NULL, 10, 0, 0, 0, 0, 0, 0),
(4, NULL, 0x3a3a31, 'jaanmuhammad', '8ebb95d94251d3c47c5246e722699c40934ce078', NULL, 'jaanmuhammad@gmail.com', NULL, NULL, NULL, NULL, 1508675047, 1508675047, 1, 'Jaan', 'Muhammad', 'Wisdom', '05264656788', NULL, 'male', 2, 0, 0, NULL, 0, 0, 0, 1, 0, 0),
(5, 0x3a3a31, 0x3a3a31, 'tayyabababar', '53a0dab424e3d183ead66cf4398a3d9ac3be1c27', NULL, 'tayyabababar@gmail.com', NULL, NULL, NULL, NULL, 1508675152, 1509104198, 1, 'Tayyaba', 'Babar', 'Wisdom', '0523468695', NULL, 'female', 8, NULL, 3, NULL, NULL, 1, 0, 1, 1, 0),
(6, NULL, 0x3a3a31, 'adeel', '85dd9854a8b989bcf82df20490143f9ec7dda749', NULL, 'addel@gmail.com', NULL, NULL, NULL, NULL, 1508769625, 1508769625, 1, 'adeel', 'akram', 'asd', '48554', NULL, 'male', 8, NULL, 3, NULL, 0, 1, 0, 1, 1, 0),
(7, NULL, 0x3a3a31, 'abdullah', '438727b2e0f76eb144f1133d0072acdc610485be', NULL, 'abdullah@gmail.com', NULL, NULL, NULL, NULL, 1508769773, 1508769773, 1, 'abdullah', 'akram', 'asd', '555512', NULL, 'male', 8, 0, 3, NULL, 0, 1, 0, 1, 1, 0),
(8, 0x3a3a31, 0x3a3a31, 'waseem_ch', '775a908bcb48e391744d34a1ce9f3fb7cf39f026', NULL, 'waheed1553@gmail.com', NULL, NULL, NULL, '533adfdb5a59b6eb07442c232a6b71b7c05b9f47', 1510871034, 1513943232, 1, 'Waseem', 'Akram', 'anc', '03424248255', NULL, 'male', 3, NULL, NULL, 11, 0, 0, 0, 0, 0, 0),
(9, 0x3a3a31, 0x3a3a31, 'ali_murtaza', 'b2b2ad73c39a3ddb77b6c43070736aff25632e35', NULL, 'ali@murtaza.net', '5aec2900f5f976dda3dfcdec4bf4c153be8770c9', NULL, NULL, NULL, 1511497854, 1523028457, 1, 'Ali', 'Murazta', 'wisdom', '0342424812666', NULL, 'male', 3, NULL, NULL, 12, 0, 0, 0, 0, 0, 0),
(10, NULL, 0x3a3a31, 'akashahmed', 'b2b2ad73c39a3ddb77b6c43070736aff25632e35', NULL, 'akashahmed@gmail.com', '5aec2900f5f976dda3dfcdec4bf4c153be8770c9', NULL, NULL, NULL, 1522917883, 1522917883, 0, 'akash', 'ahmed', 'wisdom', '2132324234', NULL, 'male', 3, NULL, NULL, 13, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_user_logins`
--

CREATE TABLE `sma_user_logins` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_user_logins`
--

INSERT INTO `sma_user_logins` (`id`, `user_id`, `company_id`, `ip_address`, `login`, `time`) VALUES
(158, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-11-19 22:10:11'),
(159, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-11-20 01:05:31'),
(160, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-11-20 03:50:36'),
(161, 8, NULL, 0x3a3a31, 'waseem_ch', '2017-11-20 07:49:52'),
(162, 8, NULL, 0x3a3a31, 'waseem_ch', '2017-11-20 21:05:57'),
(163, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-11-21 21:53:48'),
(164, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-11-24 00:35:52'),
(165, 8, NULL, 0x3a3a31, 'waseem_ch', '2017-11-24 04:04:24'),
(166, 8, NULL, 0x3a3a31, 'waseem_ch', '2017-11-24 04:24:40'),
(167, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-11-24 20:38:18'),
(168, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-11-24 20:41:02'),
(169, 8, NULL, 0x3a3a31, 'waseem_ch', '2017-11-24 20:43:16'),
(170, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-11-26 19:34:22'),
(171, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-11-27 07:09:36'),
(172, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-11-27 22:00:08'),
(173, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-11-28 11:50:49'),
(174, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-11-29 22:35:15'),
(175, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-11-29 19:45:52'),
(176, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-11-30 10:14:50'),
(177, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-02 13:48:37'),
(178, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-04 14:22:44'),
(179, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-06 08:13:35'),
(180, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-07 05:33:23'),
(181, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-07 10:40:09'),
(182, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-07 13:14:24'),
(183, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-08 08:28:35'),
(184, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-08 09:59:03'),
(185, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-10 10:25:47'),
(186, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-10 14:46:15'),
(187, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-12 08:04:15'),
(188, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-13 07:32:53'),
(189, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-13 13:07:01'),
(190, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-13 13:58:39'),
(191, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-13 14:16:34'),
(192, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-14 07:33:31'),
(193, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-15 11:20:36'),
(194, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-16 14:57:28'),
(195, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-18 08:13:47'),
(196, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-18 12:14:16'),
(197, 8, NULL, 0x3a3a31, 'waseem_ch', '2017-12-18 14:50:27'),
(198, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-18 15:29:42'),
(199, 8, NULL, 0x3a3a31, 'waseem_ch', '2017-12-18 20:12:26'),
(200, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-18 20:16:11'),
(201, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-18 20:17:34'),
(202, 8, NULL, 0x3a3a31, 'waseem_ch', '2017-12-19 07:53:49'),
(203, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-19 07:55:07'),
(204, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-19 12:20:54'),
(205, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-19 12:23:11'),
(206, 8, NULL, 0x3a3a31, 'waseem_ch', '2017-12-19 12:24:24'),
(207, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-21 08:12:45'),
(208, 8, NULL, 0x3a3a31, 'waseem_ch', '2017-12-21 08:14:23'),
(209, 8, NULL, 0x3a3a31, 'waseem_ch', '2017-12-21 12:09:01'),
(210, 8, NULL, 0x3a3a31, 'waseem_ch', '2017-12-22 11:47:12'),
(211, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-22 11:52:05'),
(212, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-27 09:34:07'),
(213, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2017-12-28 07:16:51'),
(214, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2018-01-04 06:24:50'),
(215, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2018-01-14 14:44:22'),
(216, 3, NULL, 0x3a3a31, 'ummarakram@gmail.com', '2018-03-15 09:20:00'),
(217, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2018-03-15 09:24:25'),
(218, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2018-03-18 05:45:26'),
(219, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2018-03-18 06:09:29'),
(220, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2018-03-18 08:21:34'),
(221, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2018-03-18 13:04:17'),
(222, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2018-03-19 06:00:52'),
(223, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2018-03-20 06:27:43'),
(224, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2018-03-21 08:07:49'),
(225, 1, NULL, 0x3137352e3131302e37332e3138, 'owner@tecdiary.com', '2018-03-22 06:14:46'),
(226, 1, NULL, 0x3137352e3131302e37332e3138, 'owner@tecdiary.com', '2018-03-22 07:06:30'),
(227, 1, NULL, 0x3137352e3131302e37332e3138, 'owner@tecdiary.com', '2018-03-22 11:05:40'),
(228, 1, NULL, 0x39342e3230302e3131322e3134, 'owner@tecdiary.com', '2018-03-22 11:09:42'),
(229, 1, NULL, 0x3137352e3131302e37332e3138, 'khuram@wistech.biz', '2018-03-22 11:10:18'),
(230, 1, NULL, 0x3137352e3131302e37332e3138, 'khuram@wistech.biz', '2018-03-22 11:11:12'),
(231, 1, NULL, 0x3137352e3131302e37332e3138, 'khuram@wistech.biz', '2018-03-22 11:11:22'),
(232, 1, NULL, 0x3137352e3131302e37332e3138, 'khuram@wistech.biz', '2018-03-26 07:33:54'),
(233, 1, NULL, 0x3138302e3137382e3134382e3730, 'khuram@wistech.biz', '2018-03-26 15:51:11'),
(234, 1, NULL, 0x3138302e3137382e3134382e313932, 'khuram@wistech.biz', '2018-03-29 10:05:03'),
(235, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-03-31 14:55:03'),
(236, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-01 07:34:47'),
(237, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-02 12:18:36'),
(238, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-03 06:21:47'),
(239, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-03 08:40:06'),
(240, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-03 11:59:38'),
(241, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-03 14:47:26'),
(242, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-05 08:20:12'),
(243, 3, NULL, 0x3a3a31, 'ummarakram@gmail.com', '2018-04-05 08:45:06'),
(244, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-05 11:13:05'),
(245, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-05 13:28:01'),
(246, 3, NULL, 0x3a3a31, 'ummarakram@gmail.com', '2018-04-05 14:39:34'),
(247, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-05 20:01:47'),
(248, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-05 21:23:10'),
(249, 9, NULL, 0x3a3a31, 'ali@murtaza.net', '2018-04-05 21:32:45'),
(250, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-05 22:03:54'),
(251, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-05 22:49:15'),
(252, 9, NULL, 0x3a3a31, 'ali@murtaza.net', '2018-04-06 08:18:56'),
(253, 9, NULL, 0x3a3a31, 'ali@murtaza.net', '2018-04-06 09:47:04'),
(254, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-06 11:10:20'),
(255, 9, NULL, 0x3a3a31, 'ali@murtaza.net', '2018-04-06 11:42:57'),
(256, 9, NULL, 0x3a3a31, 'ali@murtaza.net', '2018-04-06 11:46:11'),
(257, 9, NULL, 0x3a3a31, 'ali@murtaza.net', '2018-04-06 11:50:28'),
(258, 9, NULL, 0x3a3a31, 'ali@murtaza.net', '2018-04-06 11:54:46'),
(259, 9, NULL, 0x3a3a31, 'ali@murtaza.net', '2018-04-06 12:09:51'),
(260, 9, NULL, 0x3a3a31, 'ali@murtaza.net', '2018-04-06 15:27:37'),
(261, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-06 15:33:46'),
(262, 3, NULL, 0x3a3a31, 'ummarakram@gmail.com', '2018-04-06 17:06:30'),
(263, 3, NULL, 0x3a3a31, 'ummarakram@gmail.com', '2018-04-06 17:23:40'),
(264, 3, NULL, 0x3a3a31, 'ummarakram@gmail.com', '2018-04-06 17:56:04'),
(265, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-08 07:37:40'),
(266, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-09 08:17:00'),
(267, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-11 07:22:11'),
(268, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-11 11:12:22'),
(269, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-11 11:33:39'),
(270, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-11 11:40:21'),
(271, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-12 10:03:11'),
(272, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-14 19:35:31'),
(273, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-14 19:36:17'),
(274, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-14 19:41:35'),
(275, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-16 07:52:31'),
(276, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-16 07:53:54'),
(277, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-16 08:00:16'),
(278, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-16 08:09:37'),
(279, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-16 08:13:15'),
(280, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-16 09:17:53'),
(281, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-16 09:26:35'),
(282, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-16 09:32:40'),
(283, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-16 09:36:00'),
(284, 1, NULL, 0x3a3a31, 'khuram@wistech.biz', '2018-04-16 09:46:19');

-- --------------------------------------------------------

--
-- Table structure for table `sma_variants`
--

CREATE TABLE `sma_variants` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `is_updated` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_variants`
--

INSERT INTO `sma_variants` (`id`, `name`, `is_updated`) VALUES
(1, '2-3 years', 0),
(2, '4-5 years', 0),
(3, '6-7 years', 0),
(4, '8-9 years', 0),
(5, '10-11 years', 0),
(6, 'Size 3', 0),
(7, 'size 4', 0),
(8, 'Size 6', 0),
(9, 'Size 8', 0),
(10, 'Size 10', 0),
(11, 'Size 12', 0),
(12, '12-13 years', 0),
(13, '12-13 years child', 0),
(14, '14-15 years', 0),
(16, 'Red M', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses`
--

CREATE TABLE `sma_warehouses` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `map` varchar(255) DEFAULT NULL,
  `phone` varchar(55) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `price_group_id` int(11) DEFAULT NULL,
  `is_updated` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_warehouses`
--

INSERT INTO `sma_warehouses` (`id`, `code`, `name`, `address`, `map`, `phone`, `email`, `price_group_id`, `is_updated`) VALUES
(1, 'HO', 'Head Office', 'Sky Courts, Dubai Land ', NULL, '', 'whi@tecdiary.com', 1, 0),
(2, 'DOM', 'Dubai Outlet Mall', 'Dubai Outlet mall, Al-ain Road, Dubai', NULL, '0105292122', 'whii@tecdiary.com', 1, 0),
(3, 'DFC', 'Dubai Festival City', 'Dubai Festival City, Ikea''s Building, Dubai \r\n', NULL, '501560472', 'sdsdssdds@gmail.com', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses_products`
--

CREATE TABLE `sma_warehouses_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `rack` varchar(55) DEFAULT NULL,
  `avg_cost` decimal(25,4) DEFAULT NULL,
  `is_updated` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_warehouses_products`
--

INSERT INTO `sma_warehouses_products` (`id`, `product_id`, `warehouse_id`, `quantity`, `rack`, `avg_cost`, `is_updated`) VALUES
(709, 430036, 2, '1.0000', NULL, '30.0000', 0),
(797, 691, 1, '0.0000', NULL, '41.0000', 0),
(798, 691, 2, '0.0000', NULL, '41.0000', 0),
(799, 691, 3, '0.0000', NULL, '41.0000', 0),
(1474, 701, 1, '0.0000', NULL, NULL, 0),
(1475, 701, 2, '0.0000', NULL, NULL, 0),
(1476, 701, 3, '0.0000', NULL, NULL, 0),
(1656, 60, 2, '0.0000', NULL, NULL, 0),
(1657, 57, 2, '0.0000', NULL, NULL, 0),
(1658, 59, 2, '0.0000', NULL, NULL, 0),
(1659, 54, 2, '0.0000', NULL, NULL, 0),
(1660, 90, 2, '0.0000', NULL, NULL, 0),
(1661, 203, 2, '2.0000', NULL, NULL, 0),
(1662, 193, 2, '0.0000', NULL, NULL, 0),
(1663, 63, 3, '0.0000', NULL, NULL, 0),
(1664, 62, 2, '0.0000', NULL, NULL, 0),
(1665, 56, 2, '0.0000', NULL, NULL, 0),
(1666, 81, 3, '0.0000', NULL, NULL, 0),
(1667, 55, 2, '0.0000', NULL, NULL, 0),
(1668, 51, 2, '2.0000', NULL, NULL, 0),
(1669, 70, 3, '0.0000', NULL, NULL, 0),
(1670, 70, 2, '0.0000', NULL, NULL, 0),
(1671, 246, 1, '6.0000', NULL, '-231.1110', 0),
(1672, 64, 1, '0.0000', NULL, '68.5714', 0),
(1673, 162, 1, '21.0000', NULL, '-954.7872', 0),
(1674, 51, 1, '5.0000', NULL, '70.2857', 0),
(1675, 89, 2, '0.0000', NULL, NULL, 0),
(1676, 91, 1, '13.0000', NULL, '35.0000', 0),
(1677, 78, 1, '0.0000', NULL, '36.0000', 0),
(1678, 88, 2, '0.0000', NULL, NULL, 0),
(1679, 617, 2, '4.0000', NULL, NULL, 0),
(1680, 247, 1, '0.0000', NULL, '7.3100', 0),
(1681, 248, 1, '0.0000', NULL, '7.3100', 0),
(1682, 249, 1, '0.0000', NULL, '7.3100', 0),
(1683, 250, 1, '0.0000', NULL, '7.3100', 0),
(1684, 251, 1, '0.0000', NULL, '7.3100', 0),
(1685, 252, 1, '0.0000', NULL, '7.3100', 0),
(1686, 253, 1, '0.0000', NULL, '7.3100', 0),
(1687, 254, 1, '0.0000', NULL, '7.3100', 0),
(1688, 255, 1, '0.0000', NULL, '7.3100', 0),
(1689, 256, 1, '0.0000', NULL, '7.3100', 0),
(1690, 257, 1, '0.0000', NULL, '7.3100', 0),
(1691, 258, 1, '0.0000', NULL, '7.3100', 0),
(1692, 259, 1, '0.0000', NULL, '7.3100', 0),
(1693, 260, 1, '0.0000', NULL, '7.3100', 0),
(1694, 261, 1, '0.0000', NULL, '7.3100', 0),
(1695, 262, 1, '0.0000', NULL, '7.3100', 0),
(1696, 263, 1, '0.0000', NULL, '7.3100', 0),
(1697, 264, 1, '0.0000', NULL, '7.3100', 0),
(1698, 265, 1, '0.0000', NULL, '7.3100', 0),
(1699, 266, 1, '0.0000', NULL, '7.3100', 0),
(1700, 267, 1, '0.0000', NULL, '7.3100', 0),
(1701, 268, 1, '0.0000', NULL, '7.3100', 0),
(1702, 269, 1, '0.0000', NULL, '7.3100', 0),
(1703, 270, 1, '0.0000', NULL, '7.3100', 0),
(1704, 271, 1, '0.0000', NULL, '7.3100', 0),
(1705, 272, 1, '0.0000', NULL, '7.3100', 0),
(1706, 273, 1, '0.0000', NULL, '7.3100', 0),
(1707, 274, 1, '0.0000', NULL, '7.3100', 0),
(1708, 275, 1, '0.0000', NULL, '7.3100', 0),
(1709, 276, 1, '0.0000', NULL, '7.3100', 0),
(1710, 277, 1, '0.0000', NULL, '7.3100', 0),
(1711, 278, 1, '0.0000', NULL, '7.3100', 0),
(1712, 279, 1, '0.0000', NULL, '7.3100', 0),
(1713, 280, 1, '0.0000', NULL, '7.3100', 0),
(1714, 281, 1, '0.0000', NULL, '7.3100', 0),
(1715, 282, 1, '0.0000', NULL, '7.3100', 0),
(1716, 283, 1, '0.0000', NULL, '7.3100', 0),
(1717, 284, 1, '0.0000', NULL, '7.3100', 0),
(1718, 285, 1, '0.0000', NULL, '7.3100', 0),
(1719, 286, 1, '0.0000', NULL, '7.3100', 0),
(1720, 287, 1, '0.0000', NULL, '7.3100', 0),
(1721, 288, 1, '0.0000', NULL, '4.2900', 0),
(1722, 289, 1, '0.0000', NULL, '4.2900', 0),
(1723, 290, 1, '0.0000', NULL, '4.2900', 0),
(1724, 291, 1, '0.0000', NULL, '4.2900', 0),
(1725, 292, 1, '0.0000', NULL, '4.2900', 0),
(1726, 293, 1, '0.0000', NULL, '4.2900', 0),
(1727, 294, 1, '0.0000', NULL, '4.2900', 0),
(1728, 295, 1, '0.0000', NULL, '4.2900', 0),
(1729, 296, 1, '0.0000', NULL, '4.2900', 0),
(1730, 297, 1, '0.0000', NULL, '4.2900', 0),
(1731, 298, 1, '0.0000', NULL, '4.2900', 0),
(1732, 299, 1, '0.0000', NULL, '4.2900', 0),
(1733, 300, 1, '0.0000', NULL, '4.2900', 0),
(1734, 301, 1, '0.0000', NULL, '4.2900', 0),
(1735, 302, 1, '0.0000', NULL, '4.2900', 0),
(1736, 303, 1, '0.0000', NULL, '4.2900', 0),
(1737, 304, 1, '0.0000', NULL, '4.2900', 0),
(1738, 305, 1, '0.0000', NULL, '4.2900', 0),
(1739, 306, 1, '0.0000', NULL, '4.2900', 0),
(1740, 307, 1, '0.0000', NULL, '4.2900', 0),
(1741, 308, 1, '0.0000', NULL, '7.4200', 0),
(1742, 309, 1, '0.0000', NULL, '7.4200', 0),
(1743, 310, 1, '0.0000', NULL, '7.4200', 0),
(1744, 311, 1, '0.0000', NULL, '7.4200', 0),
(1745, 312, 1, '0.0000', NULL, '7.4200', 0),
(1746, 313, 1, '0.0000', NULL, '7.4200', 0),
(1747, 314, 1, '0.0000', NULL, '7.4200', 0),
(1748, 315, 1, '0.0000', NULL, '7.4200', 0),
(1749, 316, 1, '0.0000', NULL, '7.4200', 0),
(1750, 317, 1, '0.0000', NULL, '7.4200', 0),
(1751, 318, 1, '0.0000', NULL, '7.4200', 0),
(1752, 319, 1, '0.0000', NULL, '7.4200', 0),
(1753, 320, 1, '0.0000', NULL, '5.6900', 0),
(1754, 321, 1, '0.0000', NULL, '5.6900', 0),
(1755, 322, 1, '0.0000', NULL, '5.6900', 0),
(1756, 323, 1, '0.0000', NULL, '5.6900', 0),
(1757, 324, 1, '0.0000', NULL, '5.6900', 0),
(1758, 325, 1, '0.0000', NULL, '5.6900', 0),
(1759, 326, 1, '0.0000', NULL, '5.6900', 0),
(1760, 327, 1, '0.0000', NULL, '5.6900', 0),
(1761, 328, 1, '0.0000', NULL, '5.6900', 0),
(1762, 329, 1, '0.0000', NULL, '5.6900', 0),
(1763, 330, 1, '0.0000', NULL, '5.6900', 0),
(1764, 331, 1, '0.0000', NULL, '5.6900', 0),
(1765, 332, 1, '0.0000', NULL, '5.6900', 0),
(1766, 333, 1, '0.0000', NULL, '5.6900', 0),
(1767, 334, 1, '0.0000', NULL, '5.6900', 0),
(1768, 335, 1, '0.0000', NULL, '5.6900', 0),
(1769, 336, 1, '0.0000', NULL, '5.6900', 0),
(1770, 337, 1, '0.0000', NULL, '5.6900', 0),
(1771, 338, 1, '0.0000', NULL, '5.6900', 0),
(1772, 339, 1, '0.0000', NULL, '5.6900', 0),
(1773, 340, 1, '0.0000', NULL, '5.6900', 0),
(1774, 341, 1, '0.0000', NULL, '5.6900', 0),
(1775, 342, 1, '0.0000', NULL, '5.6900', 0),
(1776, 343, 1, '0.0000', NULL, '5.6900', 0),
(1777, 344, 1, '0.0000', NULL, '5.6900', 0),
(1778, 345, 1, '0.0000', NULL, '5.6900', 0),
(1779, 346, 1, '0.0000', NULL, '5.6900', 0),
(1780, 347, 1, '0.0000', NULL, '5.6900', 0),
(1781, 348, 1, '0.0000', NULL, '5.6900', 0),
(1782, 349, 1, '0.0000', NULL, '5.6900', 0),
(1783, 350, 1, '0.0000', NULL, '5.6900', 0),
(1784, 351, 1, '0.0000', NULL, '5.6900', 0),
(1785, 352, 1, '0.0000', NULL, '5.6900', 0),
(1786, 353, 1, '0.0000', NULL, '5.6900', 0),
(1787, 354, 1, '0.0000', NULL, '5.6900', 0),
(1788, 355, 1, '0.0000', NULL, '5.6900', 0),
(1789, 356, 1, '0.0000', NULL, '5.6900', 0),
(1790, 357, 1, '0.0000', NULL, '5.6900', 0),
(1791, 358, 1, '0.0000', NULL, '5.6900', 0),
(1792, 359, 1, '0.0000', NULL, '5.6900', 0),
(1793, 360, 1, '0.0000', NULL, '5.6900', 0),
(1794, 361, 1, '0.0000', NULL, '5.6900', 0),
(1795, 362, 1, '0.0000', NULL, '5.6900', 0),
(1796, 363, 1, '0.0000', NULL, '5.6900', 0),
(1797, 364, 1, '0.0000', NULL, '5.6900', 0),
(1798, 365, 1, '0.0000', NULL, '5.6900', 0),
(1799, 366, 1, '0.0000', NULL, '5.6900', 0),
(1800, 367, 1, '0.0000', NULL, '5.6900', 0),
(1801, 368, 1, '0.0000', NULL, '5.6900', 0),
(1802, 369, 1, '0.0000', NULL, '5.6900', 0),
(1803, 370, 1, '0.0000', NULL, '5.6900', 0),
(1804, 371, 1, '0.0000', NULL, '5.6900', 0),
(1805, 372, 1, '0.0000', NULL, '5.6900', 0),
(1806, 373, 1, '0.0000', NULL, '5.6900', 0),
(1807, 102, 1, '0.0000', NULL, '30.0000', 0),
(1808, 105, 1, '0.0000', NULL, '30.0000', 0),
(1809, 104, 1, '0.0000', NULL, '30.0000', 0),
(1810, 106, 1, '0.0000', NULL, '33.0000', 0),
(1811, 103, 1, '0.0000', NULL, '30.0000', 0),
(1812, 107, 1, '0.0000', NULL, '30.0000', 0),
(1813, 108, 1, '0.0000', NULL, '33.0000', 0),
(1814, 109, 1, '0.0000', NULL, '33.0000', 0),
(1815, 1, 1, '0.0000', NULL, '43.0000', 0),
(1816, 50, 1, '0.0000', NULL, '43.0000', 0),
(1817, 52, 1, '0.0000', NULL, '41.0000', 0),
(1818, 53, 1, '0.0000', NULL, '41.0000', 0),
(1819, 54, 1, '0.0000', NULL, '41.0000', 0),
(1820, 55, 1, '0.0000', NULL, '41.0000', 0),
(1821, 56, 1, '0.0000', NULL, '48.0000', 0),
(1822, 57, 1, '0.0000', NULL, '42.0000', 0),
(1823, 58, 1, '0.0000', NULL, '42.0000', 0),
(1824, 59, 1, '0.0000', NULL, '42.0000', 0),
(1825, 60, 1, '0.0000', NULL, '41.0000', 0),
(1826, 61, 1, '6.0000', NULL, '40.0000', 0),
(1827, 62, 1, '0.0000', NULL, '40.0000', 0),
(1828, 63, 1, '0.0000', NULL, '40.0000', 0),
(1829, 65, 1, '0.0000', NULL, '40.0000', 0),
(1830, 66, 1, '0.0000', NULL, '40.0000', 0),
(1831, 68, 1, '0.0000', NULL, '34.0000', 0),
(1832, 69, 1, '0.0000', NULL, '34.0000', 0),
(1833, 70, 1, '0.0000', NULL, '38.0000', 0),
(1834, 71, 1, '0.0000', NULL, '38.0000', 0),
(1835, 72, 1, '0.0000', NULL, '33.0000', 0),
(1836, 73, 1, '0.0000', NULL, '33.0000', 0),
(1837, 74, 1, '0.0000', NULL, '33.0000', 0),
(1838, 75, 1, '0.0000', NULL, '36.0000', 0),
(1839, 76, 1, '0.0000', NULL, '33.0000', 0),
(1840, 77, 1, '0.0000', NULL, '33.0000', 0),
(1841, 79, 1, '0.0000', NULL, '36.0000', 0),
(1842, 80, 1, '0.0000', NULL, '35.0000', 0),
(1843, 81, 1, '0.0000', NULL, '35.0000', 0),
(1844, 82, 1, '0.0000', NULL, '35.0000', 0),
(1845, 83, 1, '0.0000', NULL, '33.0000', 0),
(1846, 84, 1, '0.0000', NULL, '33.0000', 0),
(1847, 85, 1, '0.0000', NULL, '33.0000', 0),
(1848, 86, 1, '0.0000', NULL, '33.0000', 0),
(1849, 88, 1, '0.0000', NULL, '36.0000', 0),
(1850, 89, 1, '0.0000', NULL, '45.0000', 0),
(1851, 90, 1, '0.0000', NULL, '45.0000', 0),
(1852, 92, 1, '0.0000', NULL, '22.0000', 0),
(1853, 93, 1, '0.0000', NULL, '24.0000', 0),
(1854, 94, 1, '0.0000', NULL, '22.0000', 0),
(1855, 95, 1, '0.0000', NULL, '29.0000', 0),
(1856, 96, 1, '0.0000', NULL, '18.0000', 0),
(1857, 97, 1, '0.0000', NULL, '18.0000', 0),
(1858, 98, 1, '0.0000', NULL, '32.0000', 0),
(1859, 99, 1, '0.0000', NULL, '41.0000', 0),
(1860, 100, 1, '0.0000', NULL, '33.0000', 0),
(1861, 101, 1, '0.0000', NULL, '33.0000', 0),
(1862, 91, 2, '0.0000', NULL, NULL, 0),
(1863, 91, 3, '0.0000', NULL, NULL, 0),
(1864, 94, 2, '0.0000', NULL, NULL, 0),
(1865, 94, 3, '0.0000', NULL, NULL, 0),
(1866, 83, 2, '0.0000', NULL, NULL, 0),
(1867, 83, 3, '0.0000', NULL, NULL, 0),
(1868, 78, 2, '0.0000', NULL, NULL, 0),
(1869, 78, 3, '0.0000', NULL, NULL, 0),
(1870, 77, 2, '0.0000', NULL, NULL, 0),
(1871, 77, 3, '0.0000', NULL, NULL, 0),
(1872, 75, 2, '0.0000', NULL, NULL, 0),
(1873, 75, 3, '0.0000', NULL, NULL, 0),
(1874, 74, 2, '0.0000', NULL, NULL, 0),
(1875, 74, 3, '0.0000', NULL, NULL, 0),
(1876, 73, 2, '0.0000', NULL, NULL, 0),
(1877, 73, 3, '0.0000', NULL, NULL, 0),
(1878, 65, 2, '0.0000', NULL, NULL, 0),
(1879, 65, 3, '0.0000', NULL, NULL, 0),
(1880, 66, 2, '0.0000', NULL, NULL, 0),
(1881, 66, 3, '0.0000', NULL, NULL, 0),
(1882, 68, 2, '0.0000', NULL, NULL, 0),
(1883, 68, 3, '0.0000', NULL, NULL, 0),
(1884, 96, 2, '0.0000', NULL, NULL, 0),
(1885, 96, 3, '0.0000', NULL, NULL, 0),
(1886, 100, 2, '0.0000', NULL, NULL, 0),
(1887, 100, 3, '0.0000', NULL, NULL, 0),
(1888, 101, 2, '0.0000', NULL, NULL, 0),
(1889, 101, 3, '0.0000', NULL, NULL, 0),
(1890, 162, 2, '0.0000', NULL, NULL, 0),
(1891, 162, 3, '0.0000', NULL, NULL, 0),
(1892, 246, 2, '0.0000', NULL, NULL, 0),
(1893, 246, 3, '0.0000', NULL, NULL, 0),
(1894, 72, 2, '0.0000', NULL, NULL, 0),
(1895, 72, 3, '0.0000', NULL, NULL, 0),
(1896, 69, 2, '0.0000', NULL, NULL, 0),
(1897, 69, 3, '0.0000', NULL, NULL, 0),
(1898, 79, 2, '0.0000', NULL, NULL, 0),
(1899, 79, 3, '0.0000', NULL, NULL, 0),
(1900, 95, 2, '0.0000', NULL, NULL, 0),
(1901, 95, 3, '0.0000', NULL, NULL, 0),
(1902, 92, 2, '0.0000', NULL, NULL, 0),
(1903, 92, 3, '0.0000', NULL, NULL, 0),
(1904, 71, 2, '0.0000', NULL, NULL, 0),
(1905, 71, 3, '0.0000', NULL, NULL, 0),
(1906, 97, 2, '0.0000', NULL, NULL, 0),
(1907, 97, 3, '0.0000', NULL, NULL, 0),
(1908, 85, 2, '0.0000', NULL, NULL, 0),
(1909, 85, 3, '0.0000', NULL, NULL, 0),
(1910, 86, 2, '0.0000', NULL, NULL, 0),
(1911, 86, 3, '0.0000', NULL, NULL, 0),
(1912, 76, 2, '0.0000', NULL, NULL, 0),
(1913, 76, 3, '0.0000', NULL, NULL, 0),
(1914, 98, 2, '0.0000', NULL, NULL, 0),
(1915, 98, 3, '0.0000', NULL, NULL, 0),
(1916, 84, 2, '0.0000', NULL, NULL, 0),
(1917, 84, 3, '0.0000', NULL, NULL, 0),
(1918, 90, 3, '0.0000', NULL, NULL, 0),
(1919, 93, 2, '0.0000', NULL, NULL, 0),
(1920, 93, 3, '0.0000', NULL, NULL, 0),
(1921, 89, 3, '0.0000', NULL, NULL, 0),
(1922, 245, 1, '0.0000', NULL, NULL, 0),
(1923, 245, 2, '0.0000', NULL, NULL, 0),
(1924, 245, 3, '0.0000', NULL, NULL, 0),
(1925, 244, 1, '0.0000', NULL, NULL, 0),
(1926, 244, 2, '0.0000', NULL, NULL, 0),
(1927, 244, 3, '0.0000', NULL, NULL, 0),
(1928, 102, 2, '0.0000', NULL, NULL, 0),
(1929, 102, 3, '0.0000', NULL, NULL, 0),
(1930, 109, 2, '0.0000', NULL, NULL, 0),
(1931, 109, 3, '0.0000', NULL, NULL, 0),
(1932, 108, 2, '0.0000', NULL, NULL, 0),
(1933, 108, 3, '0.0000', NULL, NULL, 0),
(1934, 105, 2, '0.0000', NULL, NULL, 0),
(1935, 105, 3, '0.0000', NULL, NULL, 0),
(1936, 106, 2, '0.0000', NULL, NULL, 0),
(1937, 106, 3, '0.0000', NULL, NULL, 0),
(1938, 104, 2, '0.0000', NULL, NULL, 0),
(1939, 104, 3, '0.0000', NULL, NULL, 0),
(1940, 103, 2, '0.0000', NULL, NULL, 0),
(1941, 103, 3, '0.0000', NULL, NULL, 0),
(1942, 107, 2, '0.0000', NULL, NULL, 0),
(1943, 107, 3, '0.0000', NULL, NULL, 0),
(1944, 88, 3, '0.0000', NULL, NULL, 0),
(1945, 82, 2, '0.0000', NULL, NULL, 0),
(1946, 82, 3, '0.0000', NULL, NULL, 0),
(1947, 81, 2, '0.0000', NULL, NULL, 0),
(1948, 1, 2, '0.0000', NULL, NULL, 0),
(1949, 1, 3, '0.0000', NULL, NULL, 0),
(1950, 50, 2, '0.0000', NULL, NULL, 0),
(1951, 50, 3, '0.0000', NULL, NULL, 0),
(1952, 51, 3, '0.0000', NULL, NULL, 0),
(1953, 52, 2, '0.0000', NULL, NULL, 0),
(1954, 52, 3, '0.0000', NULL, NULL, 0),
(1955, 55, 3, '0.0000', NULL, NULL, 0),
(1956, 56, 3, '0.0000', NULL, NULL, 0),
(1957, 57, 3, '0.0000', NULL, NULL, 0),
(1958, 58, 2, '0.0000', NULL, NULL, 0),
(1959, 58, 3, '0.0000', NULL, NULL, 0),
(1960, 59, 3, '0.0000', NULL, NULL, 0),
(1961, 60, 3, '0.0000', NULL, NULL, 0),
(1962, 63, 2, '0.0000', NULL, NULL, 0),
(1963, 64, 2, '0.0000', NULL, NULL, 0),
(1964, 64, 3, '0.0000', NULL, NULL, 0),
(1965, 53, 2, '0.0000', NULL, NULL, 0),
(1966, 53, 3, '0.0000', NULL, NULL, 0),
(1967, 54, 3, '0.0000', NULL, NULL, 0),
(1968, 62, 3, '0.0000', NULL, NULL, 0),
(1978, 719, 1, '1.0000', NULL, '350.0000', 0),
(1979, 719, 2, '4.0000', NULL, '350.0000', 0),
(1980, 719, 3, '0.0000', NULL, '350.0000', 0),
(2008, 748, 1, '6.0000', NULL, '350.0000', 0),
(2009, 748, 2, '6.0000', NULL, '350.0000', 0),
(2010, 748, 3, '6.0000', NULL, '350.0000', 0),
(2011, 749, 1, '6.0000', NULL, '350.0000', 0),
(2012, 749, 2, '6.0000', NULL, '350.0000', 0),
(2013, 749, 3, '5.0000', NULL, '350.0000', 0),
(2014, 750, 1, '6.0000', NULL, '350.0000', 0),
(2015, 750, 2, '6.0000', NULL, '350.0000', 0),
(2016, 750, 3, '6.0000', NULL, '350.0000', 0),
(2017, 751, 1, '-1.0000', NULL, '350.0000', 0),
(2018, 751, 2, '6.0000', NULL, '350.0000', 0),
(2019, 751, 3, '6.0000', NULL, '350.0000', 0),
(2020, 752, 1, '6.0000', NULL, '350.0000', 0),
(2021, 752, 2, '6.0000', NULL, '350.0000', 0),
(2022, 752, 3, '6.0000', NULL, '350.0000', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses_products_variants`
--

CREATE TABLE `sma_warehouses_products_variants` (
  `id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `rack` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_warehouses_products_variants`
--

INSERT INTO `sma_warehouses_products_variants` (`id`, `option_id`, `product_id`, `warehouse_id`, `quantity`, `rack`) VALUES
(24, 12, 51, 2, '2.0000', NULL),
(25, 6, 246, 1, '3.0000', NULL),
(26, 1, 162, 1, '8.0000', NULL),
(27, 11, 51, 1, '5.0000', NULL),
(28, 5, 162, 1, '3.0000', NULL),
(29, 4, 162, 1, '3.0000', NULL),
(30, 3, 162, 1, '7.0000', NULL),
(31, 2, 162, 1, '5.0000', NULL),
(32, 7, 246, 1, '3.0000', NULL),
(33, 1, 749, 1, '8.0000', NULL),
(34, 2, 749, 3, '5.0000', NULL),
(35, 3, 719, 1, '7.0000', NULL),
(36, 4, 719, 2, '4.0000', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_wishlist`
--

CREATE TABLE `sma_wishlist` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_wishlist`
--

INSERT INTO `sma_wishlist` (`id`, `user_id`, `product_id`) VALUES
(1, 1, 749),
(3, 3, 752),
(4, 3, 719);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sma_addresses`
--
ALTER TABLE `sma_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `sma_adjustments`
--
ALTER TABLE `sma_adjustments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

--
-- Indexes for table `sma_adjustment_items`
--
ALTER TABLE `sma_adjustment_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `adjustment_id` (`adjustment_id`);

--
-- Indexes for table `sma_api_keys`
--
ALTER TABLE `sma_api_keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_api_limits`
--
ALTER TABLE `sma_api_limits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_api_logs`
--
ALTER TABLE `sma_api_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_authors`
--
ALTER TABLE `sma_authors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_brands`
--
ALTER TABLE `sma_brands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `sma_calendar`
--
ALTER TABLE `sma_calendar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_captcha`
--
ALTER TABLE `sma_captcha`
  ADD PRIMARY KEY (`captcha_id`),
  ADD KEY `word` (`word`);

--
-- Indexes for table `sma_cart`
--
ALTER TABLE `sma_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_categories`
--
ALTER TABLE `sma_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_cod`
--
ALTER TABLE `sma_cod`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_combo_items`
--
ALTER TABLE `sma_combo_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_companies`
--
ALTER TABLE `sma_companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `group_id_2` (`group_id`);

--
-- Indexes for table `sma_costing`
--
ALTER TABLE `sma_costing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_currencies`
--
ALTER TABLE `sma_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_customer_groups`
--
ALTER TABLE `sma_customer_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_date_format`
--
ALTER TABLE `sma_date_format`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_deliveries`
--
ALTER TABLE `sma_deliveries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_deposits`
--
ALTER TABLE `sma_deposits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_expenses`
--
ALTER TABLE `sma_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_expense_categories`
--
ALTER TABLE `sma_expense_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_gift_cards`
--
ALTER TABLE `sma_gift_cards`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `card_no` (`card_no`);

--
-- Indexes for table `sma_gift_card_topups`
--
ALTER TABLE `sma_gift_card_topups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `card_id` (`card_id`);

--
-- Indexes for table `sma_groups`
--
ALTER TABLE `sma_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_landing_page_block`
--
ALTER TABLE `sma_landing_page_block`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_landing_page_products`
--
ALTER TABLE `sma_landing_page_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_login_attempts`
--
ALTER TABLE `sma_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_notifications`
--
ALTER TABLE `sma_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_order_ref`
--
ALTER TABLE `sma_order_ref`
  ADD PRIMARY KEY (`ref_id`);

--
-- Indexes for table `sma_pages`
--
ALTER TABLE `sma_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_payments`
--
ALTER TABLE `sma_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_paypal`
--
ALTER TABLE `sma_paypal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_permissions`
--
ALTER TABLE `sma_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_pos_register`
--
ALTER TABLE `sma_pos_register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_pos_settings`
--
ALTER TABLE `sma_pos_settings`
  ADD PRIMARY KEY (`pos_id`);

--
-- Indexes for table `sma_price_groups`
--
ALTER TABLE `sma_price_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `sma_printers`
--
ALTER TABLE `sma_printers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_products`
--
ALTER TABLE `sma_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`),
  ADD KEY `category_id_2` (`category_id`),
  ADD KEY `unit` (`unit`),
  ADD KEY `brand` (`brand`),
  ADD KEY `author` (`author`) USING BTREE,
  ADD KEY `publisher` (`publisher`);

--
-- Indexes for table `sma_product_authors`
--
ALTER TABLE `sma_product_authors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_product_photos`
--
ALTER TABLE `sma_product_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_product_prices`
--
ALTER TABLE `sma_product_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `price_group_id` (`price_group_id`);

--
-- Indexes for table `sma_product_variants`
--
ALTER TABLE `sma_product_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_promotions`
--
ALTER TABLE `sma_promotions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_promotion_for_items`
--
ALTER TABLE `sma_promotion_for_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_promotion_items`
--
ALTER TABLE `sma_promotion_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_publishers`
--
ALTER TABLE `sma_publishers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_purchases`
--
ALTER TABLE `sma_purchases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_purchase_items`
--
ALTER TABLE `sma_purchase_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_id` (`purchase_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_quotes`
--
ALTER TABLE `sma_quotes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_quote_items`
--
ALTER TABLE `sma_quote_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quote_id` (`quote_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_returns`
--
ALTER TABLE `sma_returns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_return_items`
--
ALTER TABLE `sma_return_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `return_id` (`return_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_id_2` (`product_id`,`return_id`),
  ADD KEY `return_id_2` (`return_id`,`product_id`);

--
-- Indexes for table `sma_sales`
--
ALTER TABLE `sma_sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_sale_items`
--
ALTER TABLE `sma_sale_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_id_2` (`product_id`,`sale_id`),
  ADD KEY `sale_id_2` (`sale_id`,`product_id`);

--
-- Indexes for table `sma_sessions`
--
ALTER TABLE `sma_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `sma_settings`
--
ALTER TABLE `sma_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `sma_shop_settings`
--
ALTER TABLE `sma_shop_settings`
  ADD PRIMARY KEY (`shop_id`);

--
-- Indexes for table `sma_sizing_info`
--
ALTER TABLE `sma_sizing_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_skrill`
--
ALTER TABLE `sma_skrill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_stock_counts`
--
ALTER TABLE `sma_stock_counts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

--
-- Indexes for table `sma_stock_count_items`
--
ALTER TABLE `sma_stock_count_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stock_count_id` (`stock_count_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_suspended_bills`
--
ALTER TABLE `sma_suspended_bills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_suspended_items`
--
ALTER TABLE `sma_suspended_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_tax_rates`
--
ALTER TABLE `sma_tax_rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_transfers`
--
ALTER TABLE `sma_transfers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_transfer_items`
--
ALTER TABLE `sma_transfer_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transfer_id` (`transfer_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_translated_category_name`
--
ALTER TABLE `sma_translated_category_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_translated_product_name`
--
ALTER TABLE `sma_translated_product_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_units`
--
ALTER TABLE `sma_units`
  ADD PRIMARY KEY (`id`),
  ADD KEY `base_unit` (`base_unit`);

--
-- Indexes for table `sma_users`
--
ALTER TABLE `sma_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`,`warehouse_id`,`biller_id`),
  ADD KEY `group_id_2` (`group_id`,`company_id`);

--
-- Indexes for table `sma_user_logins`
--
ALTER TABLE `sma_user_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_variants`
--
ALTER TABLE `sma_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_warehouses`
--
ALTER TABLE `sma_warehouses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_warehouses_products`
--
ALTER TABLE `sma_warehouses_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

--
-- Indexes for table `sma_warehouses_products_variants`
--
ALTER TABLE `sma_warehouses_products_variants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

--
-- Indexes for table `sma_wishlist`
--
ALTER TABLE `sma_wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sma_addresses`
--
ALTER TABLE `sma_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sma_adjustments`
--
ALTER TABLE `sma_adjustments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sma_adjustment_items`
--
ALTER TABLE `sma_adjustment_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8783;
--
-- AUTO_INCREMENT for table `sma_api_keys`
--
ALTER TABLE `sma_api_keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_api_limits`
--
ALTER TABLE `sma_api_limits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_api_logs`
--
ALTER TABLE `sma_api_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_authors`
--
ALTER TABLE `sma_authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `sma_brands`
--
ALTER TABLE `sma_brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sma_calendar`
--
ALTER TABLE `sma_calendar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sma_captcha`
--
ALTER TABLE `sma_captcha`
  MODIFY `captcha_id` bigint(13) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_categories`
--
ALTER TABLE `sma_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `sma_combo_items`
--
ALTER TABLE `sma_combo_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_companies`
--
ALTER TABLE `sma_companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `sma_costing`
--
ALTER TABLE `sma_costing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sma_currencies`
--
ALTER TABLE `sma_currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sma_customer_groups`
--
ALTER TABLE `sma_customer_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sma_date_format`
--
ALTER TABLE `sma_date_format`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sma_deliveries`
--
ALTER TABLE `sma_deliveries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sma_deposits`
--
ALTER TABLE `sma_deposits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_expenses`
--
ALTER TABLE `sma_expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_expense_categories`
--
ALTER TABLE `sma_expense_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_gift_cards`
--
ALTER TABLE `sma_gift_cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sma_gift_card_topups`
--
ALTER TABLE `sma_gift_card_topups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_groups`
--
ALTER TABLE `sma_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sma_landing_page_block`
--
ALTER TABLE `sma_landing_page_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `sma_landing_page_products`
--
ALTER TABLE `sma_landing_page_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `sma_login_attempts`
--
ALTER TABLE `sma_login_attempts`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sma_notifications`
--
ALTER TABLE `sma_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sma_order_ref`
--
ALTER TABLE `sma_order_ref`
  MODIFY `ref_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sma_pages`
--
ALTER TABLE `sma_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sma_payments`
--
ALTER TABLE `sma_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sma_permissions`
--
ALTER TABLE `sma_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sma_pos_register`
--
ALTER TABLE `sma_pos_register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sma_price_groups`
--
ALTER TABLE `sma_price_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sma_printers`
--
ALTER TABLE `sma_printers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_products`
--
ALTER TABLE `sma_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=753;
--
-- AUTO_INCREMENT for table `sma_product_authors`
--
ALTER TABLE `sma_product_authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `sma_product_photos`
--
ALTER TABLE `sma_product_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `sma_product_prices`
--
ALTER TABLE `sma_product_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_product_variants`
--
ALTER TABLE `sma_product_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sma_promotions`
--
ALTER TABLE `sma_promotions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT for table `sma_promotion_for_items`
--
ALTER TABLE `sma_promotion_for_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `sma_promotion_items`
--
ALTER TABLE `sma_promotion_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=340;
--
-- AUTO_INCREMENT for table `sma_publishers`
--
ALTER TABLE `sma_publishers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `sma_purchases`
--
ALTER TABLE `sma_purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_purchase_items`
--
ALTER TABLE `sma_purchase_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=966;
--
-- AUTO_INCREMENT for table `sma_quotes`
--
ALTER TABLE `sma_quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_quote_items`
--
ALTER TABLE `sma_quote_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_returns`
--
ALTER TABLE `sma_returns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_return_items`
--
ALTER TABLE `sma_return_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_sales`
--
ALTER TABLE `sma_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sma_sale_items`
--
ALTER TABLE `sma_sale_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `sma_sizing_info`
--
ALTER TABLE `sma_sizing_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sma_stock_counts`
--
ALTER TABLE `sma_stock_counts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sma_stock_count_items`
--
ALTER TABLE `sma_stock_count_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=413;
--
-- AUTO_INCREMENT for table `sma_suspended_bills`
--
ALTER TABLE `sma_suspended_bills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_suspended_items`
--
ALTER TABLE `sma_suspended_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_tax_rates`
--
ALTER TABLE `sma_tax_rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sma_transfers`
--
ALTER TABLE `sma_transfers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_transfer_items`
--
ALTER TABLE `sma_transfer_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_translated_category_name`
--
ALTER TABLE `sma_translated_category_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_translated_product_name`
--
ALTER TABLE `sma_translated_product_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_units`
--
ALTER TABLE `sma_units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sma_users`
--
ALTER TABLE `sma_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sma_user_logins`
--
ALTER TABLE `sma_user_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=285;
--
-- AUTO_INCREMENT for table `sma_variants`
--
ALTER TABLE `sma_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `sma_warehouses`
--
ALTER TABLE `sma_warehouses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sma_warehouses_products`
--
ALTER TABLE `sma_warehouses_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2023;
--
-- AUTO_INCREMENT for table `sma_warehouses_products_variants`
--
ALTER TABLE `sma_warehouses_products_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `sma_wishlist`
--
ALTER TABLE `sma_wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
