<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sync_system extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->large_num = 1844674;
        $this->onlinedb = $this->load->database('online_db', TRUE);
    }


    public function sync_sales($table_name){
        $this->db->select('*')->from($table_name)->where('is_sync' , 0)->order_by('id', 'asc');
         //echo $this->db->get_compiled_select(); die;
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data = array(
                    'date' => $row->date,
                    'reference_no' => $row->reference_no,
                    'customer_id' => $row->customer_id,
                    'customer' => $row->customer,
                    'biller_id' => $row->biller_id,
                    'biller' => $row->biller,
                    'warehouse_id' => $row->warehouse_id,
                    'note' => $row->note,
                    'staff_note' => $row->staff_note,
                    'total' => $row->total,
                    'product_discount' => $row->product_discount,
                    'order_discount_id' => $row->order_discount_id,
                    'total_discount' => $row->total_discount,
                    'order_discount' => $row->order_discount,
                    'product_tax' => $row->product_tax,
                    'order_tax_id' => $row->order_tax_id,
                    'order_tax' => $row->order_tax,
                    'total_tax' => $row->total_tax,
                    'shipping' => $row->shipping,
                    'grand_total' => $row->grand_total,
                    'sale_status' => $row->sale_status,
                    'payment_status' => $row->payment_status,
                    'payment_term' => $row->payment_term,
                    'due_date' => $row->due_date,
                    'created_by' => $row->created_by,
                    'updated_by' => $row->updated_by,
                    'updated_at' => $row->updated_at,
                    'total_items' => $row->total_items,
                    'pos' => $row->pos,
                    'paid' => $row->paid,
                    'return_id' => $row->return_id,
                    'surcharge' => $row->surcharge,
                    'attachment' => $row->attachment,
                    'return_sale_ref' => $row->return_sale_ref,
                    'sale_id' => $row->sale_id,
                    'return_sale_total' => $row->return_sale_total,
                    'rounding' => $row->rounding,
                    'suspend_note' => $row->suspend_note,
                    'api' => $row->api,
                    'shop' => $row->shop,
                    'address_id' => $row->address_id,
                    'reserve_id' => $row->reserve_id,
                    'hash' => $row->hash,
                    'is_sync' => $row->is_sync,
                    'local_id' => $row->id,
                );

                $this->onlinedb->insert('sma_'.$table_name, $data);
                $insert_id = $this->onlinedb->insert_id();
                $this->get_sale_items($row->id, 'sale_items', $insert_id);
            }
            $this->sync_orders_live_to_local();
            return true;
        }
    }


    public function get_sale_items($sale_id,$table_name,$inserted_id){
        $this->db->select('*')->from($table_name)->where('sale_id' , $sale_id);
        //echo $this->db->get_compiled_select(); die;
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = array(
                    'sale_id'=>  $inserted_id,
                    'product_id'      => $row->product_id,
                    'product_code'    => $row->product_code,
                    'product_name'    => $row->product_name,
                    'product_type'    => $row->product_type,
                    'option_id'       => $row->option_id,
                    'net_unit_price'  => $row->net_unit_price,
                    'unit_price'      => $row->unit_price,
                    'quantity'        => $row->quantity,
                    'product_unit_id' => $row->product_unit_id,
                    'product_unit_code' => $row->product_unit_code,
                    'unit_quantity' => $row->unit_quantity,
                    'warehouse_id'    => $row->warehouse_id,
                    'tax_rate_id'     => $row->tax_rate_id,
                    'tax'             => $row->tax,
                    'discount'        => $row->discount,
                    'item_discount'   => $row->item_discount,
                    'subtotal'        => $row->subtotal,
                    'serial_no'       => $row->serial_no,
                    'real_unit_price' => $row->real_unit_price,
                    'sale_item_id' => $row->sale_item_id,
                    'comment'         => $row->comment,
                    'local_id'         => $row->id,
                );
            }
            if ($data) {
                $this->insert_record_into_livedb($data, $table_name);
                return true;
            }
            return false;
        }

    }


    public function insert_record_into_livedb($data, $table_name){
        if(!empty($data)){
            $this->onlinedb->insert_batch('sma_'.$table_name, $data);
            $insert_id = $this->onlinedb->insert_id();
            return $insert_id;
        }
    }


    public function insert_record_into_localdb($data, $table_name){
        if(!empty($data)){
            $this->db->insert_batch($table_name, $data);
            return true;
        }
    }

    //Sync items from live to local - it has relation with the sync of sales
    public function sync_orders_live_to_local(){
        //Delete the synced items from local
        $tables = array('sales', 'sale_items');
        $this->db->where('is_sync', 0);
        $this->db->delete($tables);

        //get record from Live to Local (Sales table)
        $this->onlinedb->select('*')->from('sma_sales')->where('is_sync' , 0);
        //echo $this->db->get_compiled_select(); die;
        $query = $this->onlinedb->get();
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = array(
                    'id' => $row->id,
                    'date' => $row->date,
                    'reference_no' => $row->reference_no,
                    'customer_id' => $row->customer_id,
                    'customer' => $row->customer,
                    'biller_id' => $row->biller_id,
                    'biller' => $row->biller,
                    'warehouse_id' => $row->warehouse_id,
                    'note' => $row->note,
                    'staff_note' => $row->staff_note,
                    'total' => $row->total,
                    'product_discount' => $row->product_discount,
                    'order_discount_id' => $row->order_discount_id,
                    'total_discount' => $row->total_discount,
                    'order_discount' => $row->order_discount,
                    'product_tax' => $row->product_tax,
                    'order_tax_id' => $row->order_tax_id,
                    'order_tax' => $row->order_tax,
                    'total_tax' => $row->total_tax,
                    'shipping' => $row->shipping,
                    'grand_total' => $row->grand_total,
                    'sale_status' => $row->sale_status,
                    'payment_status' => $row->payment_status,
                    'payment_term' => $row->payment_term,
                    'due_date' => $row->due_date,
                    'created_by' => $row->created_by,
                    'updated_by' => $row->updated_by,
                    'updated_at' => $row->updated_at,
                    'total_items' => $row->total_items,
                    'pos' => $row->pos,
                    'paid' => $row->paid,
                    'return_id' => $row->return_id,
                    'surcharge' => $row->surcharge,
                    'attachment' => $row->attachment,
                    'return_sale_ref' => $row->return_sale_ref,
                    'sale_id' => $row->sale_id,
                    'return_sale_total' => $row->return_sale_total,
                    'rounding' => $row->rounding,
                    'suspend_note' => $row->suspend_note,
                    'api' => $row->api,
                    'shop' => $row->shop,
                    'address_id' => $row->address_id,
                    'reserve_id' => $row->reserve_id,
                    'hash' => $row->hash,
                    'is_sync' => 1,
                    'local_id' => $row->local_id,
                );
            }
            $this->insert_record_into_localdb($data , 'sales' );

            //Update on live Table 0 To 1 (synced)
            $this->onlinedb->where('is_sync', 0);
            $this->onlinedb->set('is_sync', 1);
            $this->onlinedb->update('sma_sales');
        }



        //get record from Live to Local (Sale Item Table)
        $this->onlinedb->select('*')->from('sma_sale_items')->where('is_sync' , 0);
        $query2 = $this->onlinedb->get();
        if ($query2->num_rows() > 0) {
            foreach (($query2->result()) as $row) {
                $record[] = array(
                    'id' =>  $row->id,
                    'sale_id' =>  $row->sale_id,
                    'product_id'      => $row->product_id,
                    'product_code'    => $row->product_code,
                    'product_name'    => $row->product_name,
                    'product_type'    => $row->product_type,
                    'option_id'       => $row->option_id,
                    'net_unit_price'  => $row->net_unit_price,
                    'unit_price'      => $row->unit_price,
                    'quantity'        => $row->quantity,
                    'product_unit_id' => $row->product_unit_id,
                    'product_unit_code' => $row->product_unit_code,
                    'unit_quantity' => $row->unit_quantity,
                    'warehouse_id'    => $row->warehouse_id,
                    'tax_rate_id'     => $row->tax_rate_id,
                    'tax'             => $row->tax,
                    'discount'        => $row->discount,
                    'item_discount'   => $row->item_discount,
                    'subtotal'        => $row->subtotal,
                    'serial_no'       => $row->serial_no,
                    'real_unit_price' => $row->real_unit_price,
                    'sale_item_id' => $row->sale_item_id,
                    'comment'         => $row->comment,
                    'local_id'         => $row->local_id,
                    'is_sync'         => 1,
                );
            }
            $this->insert_record_into_localdb($record , 'sale_items');

            //Update on live Table 0 To 1 (synced)
            $this->onlinedb->where('is_sync', 0);
            $this->onlinedb->set('is_sync', 1);
            $this->onlinedb->update('sale_items');
        }
    }

    //GEt the last id of any table from the Lcoal database
    public function get_local_table_last_id($table_name){
        $this->db->select('id')->from($table_name)->order_by('id', 'DESC')->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                return $row->id;
            }
        }
   }

    //GEt the last id of any table from the Live database
    public function get_live_table_last_id($table_name){
        $this->onlinedb->select('id')->from('sma_'.$table_name)->order_by('id', 'DESC')->limit(1);
        $query = $this->onlinedb->get();
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                return $row->id;
            }
        }
   }


    //Sync the brands from Live to Local database.
    public function sync_brands($table_name){
        //get the last id of brands table local database
        $local_tablelast_id =  $this->get_local_table_last_id($table_name);
        //get the last id of brands table live database
        $live_tablelast_id =  $this->get_live_table_last_id($table_name);

        //when Both live and local databases have the same number of rows
        if($local_tablelast_id == $live_tablelast_id){
            //Now check if previous record updated on live then update it also on local.
            //check any update on the live database or not...using is_updated = 0 Or is_updated = 1
            $this->onlinedb->select('*')->from('sma_'.$table_name)->where('is_updated' , 1);
            $query = $this->onlinedb->get();
            if ($query->num_rows() > 0) {
                foreach (($query->result()) as $row) {
                    $data = array(
                        'id' => $row->id,
                        'code'=> $row->code,
                        'name'=> $row->name,
                        'image'=> $row->image,
                        'slug' => $row->slug,
                        'is_updated' => 0,
                    );
                    $this->db->where('id', $row->id);
                    $this->db->update($table_name, $data);
                }
                //Update on live Table 0 To 1 (synced)
                $this->onlinedb->where('is_updated', 1);
                $this->onlinedb->set('is_updated', 0);
                $this->onlinedb->update('sma_'.$table_name);
                return true;
            }
            return true;
        }else{
            //To check if added new record in the Live database
            //And insert this record in the local database.
            //Delete all the brands from local
            $this->db->empty_table($table_name);

            //Fetch the all brands from Live server
            $this->onlinedb->select('*')->from('sma_'.$table_name);
            $query = $this->onlinedb->get();
            if ($query->num_rows() > 0) {
                foreach (($query->result()) as $row) {
                    $data[] = array(
                        'id' => $row->id,
                        'code' => $row->code,
                        'name' => $row->name,
                        'image' => $row->image,
                        'slug' => $row->slug,
                        'is_updated' => 0,
                    );
                }
                $this->db->insert_batch($table_name, $data);
                return true;
            }
            return true;
        }

    }


    //Download the image sync
    public function download_product_image($image_name){
        //To download the primary image
        $live_directory_address = 'http://cutsandfits.com/assets/uploads/';
        copy($live_directory_address.$image_name, 'H:/xampp/htdocs/cutsandfits2/assets/uploads/'.$image_name);

        //To download the thumbnail
        copy($live_directory_address.'thumbs/'.$image_name, 'H:/xampp/htdocs/cutsandfits2/assets/uploads/thumbs/'.$image_name);

    return true;
    }

    //Sync the Table from Live to Local database.
    public function synchronization($table_name){
        //get the last id of brands table local database
        $local_tablelast_id =  $this->get_local_table_last_id($table_name);
        //get all records from the live database where live id greter then the local id
        $this->onlinedb->select('*')->from('sma_'.$table_name)->where('id >' , $local_tablelast_id);
        $query = $this->onlinedb->get();
        //echo $this->onlinedb->get_compiled_select(); die;
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                //only to download product images
                if($table_name == 'products'){
                    $this->download_product_image($row->image);
                }
                $this->db->insert($table_name, $row);
            }
        }
        //get all records from the live database where is_updated = 1
        $this->onlinedb->select('*')->from('sma_'.$table_name)->where('is_updated' ,1);
        $sql = $this->onlinedb->get();
        if ($sql->num_rows() > 0) {
            foreach (($sql->result()) as $row) {
                $this->db->where('id', $row->id);
                $this->db->update($table_name, $row);
            }
            //Update on local Table 0 To 1 (synced)
            $this->db->where('is_updated', 1);
            $this->db->set('is_updated', 0);
            $this->db->update($table_name);

            //Update on live Table 0 To 1 (synced)
            $this->onlinedb->where('is_updated', 1);
            $this->onlinedb->set('is_updated', 0);
            $this->onlinedb->update('sma_'.$table_name);
        }

        return true;
    }


    //check connection
    public function check_secure_connection(){
        echo 'asd'; die;
    }

}