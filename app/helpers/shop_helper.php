<?php defined('BASEPATH') OR exit('No direct script access allowed');

if(! function_exists('convert_currency')) {
    function convert_currency($amount, $from, $to) {
        $data = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=$from&to=$to");
        preg_match("/<span class=bld>(.*)<\/span>/", $data, $converted);
        $converted = preg_replace("/[^0-9.]/", "", $converted[1]);
        return number_format(round($converted, 3), 2);
    }
}
if(! function_exists('isMobile')) {
    function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
}

if(! function_exists('multi_array_search')) {
    function multi_array_search($search_for, $search_in) {
        foreach ($search_in as $element) {
            if ( ($element === $search_for) || (is_array($element) && multi_array_search($search_for, $element)) ){
                return true;
            }
        }
        return false;
    }
}