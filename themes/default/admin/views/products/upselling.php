<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-bullhorn"></i><?= lang('promotion'); ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?= admin_url('products/create_upselling') ?>">
                                <i class="fa fa-plus-circle"></i> <?= lang('add_upselling') ?>
                            </a>
                        </li>
                        <!--<li class="divider"></li>-->
                        <li class="hide"><a href="#" id="deletePromotions" data-action="delete"><i class="fa fa-trash-o"></i> <?= lang('delete_promotions') ?></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo $this->lang->line("All the promotions."); ?></p>

                <div class="table-responsive">
                    <table id="GPData" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkth" type="checkbox" name="check"/>
                            </th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Min/Max Amount</th>
                            <th>No. of Buy - No. of Free</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($promotions as $promotion) {
                            ?>
                            <tr>
                                <td>
                                    <div class="text-center"><input class="checkbox multi-select" type="checkbox" name="val[]"
                                                   value="<?= $promotion->id ?>"/></div>
                                </td>
                                <td class="text-center"><?= $promotion->name; ?></td>
                                <td class="text-center"><?php if($promotion->type == 'buyOneGetCategoryFree'){ echo 'Category Promotion'; }elseif ($promotion->type == 'buyOneGetOne'){echo 'Buy X get X Product (Same-product)';}elseif ($promotion->type == 'buyOneGetOtherProductFree'){echo 'Buy X get X Product (Other-product)';}else{ echo $promotion->type;} ?></td>
                                <td class="text-center"><?= $promotion->minAmount . '/' . $promotion->maxAmount ; ?></td>
                                <td class="text-center"><?= $promotion->how_many_buy; ?> - <?= $promotion->how_many_free; ?></td>
                                <td class="text-center"><?= $promotion->startDate; ?> - <?= $promotion->endDate; ?></td>
                                <td class="text-center"><?= ucfirst($promotion->status); ?></td>
                                <td class="text-center">
                                    <?php echo '<a class="tip" title="' . $this->lang->line("Edit_Promotion") . '" href="' . admin_url('products/edit_upselling/' . $promotion->id) . '"><i class="fa fa-edit"></i></a> <a href="#" class="tip po" title="' . $this->lang->line("Delete_Promotion") . '" data-content="<p>' . lang('r_u_sure') . '</p><a class=\'btn btn-danger\' href=\'' . admin_url('products/delete_upselling/' . $promotion->id) . '\'>' . lang('i_m_sure') . '</a> <button class=\'btn po-close\'>' . lang('no') . '</button>"><i class="fa fa-trash-o"></i></a>'; ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>