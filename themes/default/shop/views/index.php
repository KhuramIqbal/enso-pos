<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .carousel .carousel-control span {
        background-color: #fff;
        color: #000;
        width: 48px;
        height: 48px;
        padding-top: 14px !important;
        font-size: 20px;
        left: 30%;
    }

    .carousel .carousel-control span:hover {
        opacity: 1;
        padding-top: 11px !important;
        font-size: 25px;
    }

    .carousel .carousel-control {
        opacity: 1;

    }

    .carousel .carousel-control:hover {
        opacity: 1;
    }

    .carousel .carousel-control span .fa {

    }

    .caption-link {
        color: #222 !important;
        position: relative;
        z-index: 10;
        font-weight: 400;
        padding: 8px 20px;
        font-size: 14px;
        text-transform: uppercase;
        display: inline-block;
        background: none;
        text-align: center;
        border: 1px solid #222;
        /* margin-top: 35px;*/
    }

    .caption-link:hover {
        background: #C6B9A5;
        text-decoration: none;
        border: 1px solid #C6B9A5;
    }

    .carousel-caption {
        left: 55% !important;
        bottom: 15% !important;
        text-align: justify;
        line-height: 30px;
        color: #272727;
        font-size: 14px;
        right: 0%;
    }

    .progressbar {
        width: 0%;
        height: 5px;
        background-color: #8B8781;
    }

    .carousel-inner {
        background-color: #f8f8f8;
    }

    .carousel-control.right, .carousel-control.left {
        background: none;
    }

    @media screen and (min-width: 768px)
        .carousel-caption {
            left: 50% !important;

        }

        .carousel .carousel-indicators {
            display: none !important;
        }
        #carousel-example-generic a.left.carousel-control{
            top: 50%;
        }
        #carousel-example-generic a.right.carousel-control{
            top: 50%;
        }
        .carousel-inner h1{
            text-align: left;
            font-size: 225%;
            color: #222222;
            font-weight: 800;
            margin-bottom: 20px;
        }
        .carousel-inner h1::after {
            width: 7%;
            left: 31%;
            border-bottom: 2px solid #83807D;


        }
        .carousel-inner h1 p {
            font-size: 14px;
            text-transform: lowercase;
            font-family: "AkkuratPro-Regular";
            margin: 0;
            padding: 0;
        }
        .carousel-caption{
            text-shadow: none !important;
        }




</style>


<?php if (!empty($slider)) { ?>
    <section class="slider-container">
        <div class="container-fluid">
            <div class="row">
                <div id="carousel-example-generic" class="carousel slide banner7" data-ride="carousel">
                    <ol class="carousel-indicators margin-bottom-sm">
                        <?php
                        $sr = 0;
                        foreach ($slider as $slide) {
                            if (!empty($slide->image)) {
                                echo '<li data-target="#carousel-example-generic" data-slide-to="' . $sr . '" class="' . ($sr == 0 ? 'active' : '') . '"></li> ';
                            }
                            $sr++;
                        }
                        ?>
                    </ol>

                    <div class="carousel-inner" role="listbox">
                        <?php
                        $sr = 0;
                        foreach ($slider as $slide) {
                            if (!empty($slide->image)) {
                                echo '<div class="item' . ($sr == 0 ? ' active' : '') . '">';
                                echo '<img width="100%" src="' . base_url('assets/uploads/' . $slide->image) . '" alt="">';
                                if (!empty($slide->caption)) {
                                    echo '<div style="display: none;" class="carousel-caption hidden-xs">' . $slide->caption;
                                }
                                if (empty($slide->link)) {
                                    echo '<a class="caption-link" href="#">Read more </a>';
                                } else {
                                    echo '<a class="caption-link" href="' . $slide->link . '">Read more </a>';
                                }
                                echo '</div>';
                                echo '</div>';
                            }
                            $sr++;
                        }
                        ?>
                        <div class="progressbar"></div>
                    </div>

                    <a class="left carousel-control hidden-xs" href="#carousel-example-generic" role="button"
                       data-slide="prev">
                        <span style=" " class="fa fa-angle-left" aria-hidden="true"></span>
                        <span class="sr-only"><?= lang('prev'); ?></span>
                    </a>
                    <a class="right carousel-control hidden-xs" href="#carousel-example-generic" role="button"
                       data-slide="next">
                        <span style="" class="fa fa-angle-right" aria-hidden="true"></span>
                        <span class="sr-only"><?= lang('next'); ?></span>
                    </a>
                </div>
            </div>
        </div>
    </section>
<?php } ?>




<div class="main-row ">
    <div class="container">
        <div class="row">
            <div class="main-col col-sm-12 col-md-12">
                <div class="row sub-row">
                    <div class="sub-col col-sm-12 col-md-12">
                        <div class="cmsblock">
                            <div class="description">
                                <div class="home">
                                    <?= $our_history; ?>
                                </div>
                            </div>
                        </div>
                        <div id="cmsblock-34" class="cmsblock">
                            <div class="description">
                                <div class="container">
                                    <div class="row">
                                        <?= $house_collection; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



