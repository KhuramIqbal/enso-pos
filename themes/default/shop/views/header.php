<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<script type="text/javascript">if (parent.frames.length !== 0) { top.location = '<? /*= site_url(); */ ?>'; }</script>-->
    <title><?= $page_title; ?></title>
    <meta name="description" content="<?= $page_desc; ?>">
    <meta property="og:image" content="<?= base_url('assets/uploads/' . $image_path); ?>"/>
    <link rel="shortcut icon" href="<?= $assets; ?>images/icon.png">

    <link href="<?= $assets; ?>css/libs.min.css" rel="stylesheet">
    <link href="<?= $assets; ?>css/styles.min.css" rel="stylesheet">
    <link href="<?= $assets; ?>css/custom_style.css" rel="stylesheet">
    <link href="<?= $assets; ?>css/owl.carousel.css" rel="stylesheet">
    <link href="<?= $assets; ?>css/owl.theme.default.min.css" rel="stylesheet">

    <script src="<?= $assets; ?>js_enso/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="<?= $assets; ?>js_enso/jquery-ui.js" type="text/javascript"></script>
    <link href="<?= $assets; ?>css_enso/jquery-ui.css" rel="stylesheet" media="screen"/>
    <link href="<?= $assets; ?>css_enso/oclayerednavigation.css" rel="stylesheet">
    <script src="<?= $assets; ?>js_enso/oclayerednavigation.js" type="text/javascript"></script>
    <link href="<?= $assets; ?>css_enso/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <script src="<?= $assets; ?>js_enso/bootstrap.min.js" type="text/javascript"></script>
    <link href="<?= $assets; ?>css_enso/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="//fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Lora" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Karla:400,700" rel="stylesheet">
    <script src="<?= $assets; ?>js_enso/custommenu.js" type="text/javascript"></script>
    <script src="<?= $assets; ?>js_enso/mobile_menu.js" type="text/javascript"></script>
    <script src="<?= $assets; ?>js_enso/jquery.plugin.js" type="text/javascript"></script>
    <script src="<?= $assets; ?>js_enso/jquery.nivo.slider.js" type="text/javascript"></script>
    <link href="<?= $assets; ?>css_enso/ocslideshow.css" rel="stylesheet"/>
    <link href="<?= $assets; ?>css_enso/custommenu.css" rel="stylesheet"/>
    <link href="<?= $assets; ?>css_enso/animate.css" rel="stylesheet"/>
    <link href="<?= $assets; ?>css_enso/owl.carousel.css" rel="stylesheet"/>
    <script src="<?= $assets; ?>js_enso/jquery.elevatezoom.js" type="text/javascript"></script>
    <script src="<?= $assets; ?>js_enso/owl.carousel.js" type="text/javascript"></script>
    <!--<script src="<?= $assets; ?>js_enso/ocquickview.js" type="text/javascript"></script>-->
    <link href="<?= $assets; ?>css_enso/ocquickview.css" rel="stylesheet">
    <link href="<?= $assets; ?>css_enso/stylesheet.css" rel="stylesheet">
    <link href="<?= $assets; ?>css_enso/Pe-icon-7-stroke.css" rel="stylesheet">
<style>
    .margiletf20{
        margin-left: 20px;
    }
</style>

</head>
<body class="common-home home1">


<header>
    <div class="container">
        <div class="row">
            <div id="logo">
                <a href="<?= site_url(); ?>">
                    <img src="<?= base_url(); ?>assets/uploads/logos/logo.jpg" title="Enso" alt="Enso"
                         class="img-responsive">
                </a>
            </div>
            <div class="top-area">
                <?php if ($this->loggedIn) { ?>
                    <span class="dropdown" id="profile_link">
                        <a class="dropdown-toggle" href="javascript:void(0)"  data-toggle="dropdown" data-hover="dropdown">
                            Hi, <?= ucfirst($this->session->userdata('username')); ?>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= base_url().'profile'; ?>">Profile</a></li>
                            <li><a href="<?= base_url().'shop/orders'; ?>">Orders</a></li>
                            <li><a href="<?= base_url().'shop/quotes'; ?>">Quotations</a></li>
                            <li><a href="<?= base_url().'shop/downloads'; ?>">Downloads</a></li>
                            <li><a href="<?= base_url().'shop/addresses'; ?>">Addresses</a></li>
                        </ul>
                    </span>

                    <a href="<?= shop_url('wishlist'); ?>" id="wishlist-total" class="margiletf20 hidden-xs" title="(0)"><i
                                class="fa fa-heart-o"></i>(<span id="total-wishlist"><?= $wishlist; ?></span>)
                    </a>
                    <a href="<?= base_url(); ?>logout" class="margiletf20" title="Logout"><i
                                class="fa fa-power-off"></i>
                    </a>
                <?php } else { ?>
                    <a href="<?= base_url(); ?>login" class="login ">login</a> or <a
                            href="<?= base_url(); ?>register" class="register">register</a>
                    <a href="<?= base_url(); ?>login" id="wishlist-total" class="hidden-xs" title="(0)"><i
                                class="fa fa-heart-o"></i> (0)
                    </a>
                <?php } ?>

            </div>

            <script type="text/javascript">
                $(document).ready(function () {
                    $('.search-container > i').click(function () {
                        $("#search").slideToggle();
                        $(this).toggleClass("pe-7s-search pe-7s-close")
                    });
                    $("#search > input").on("change paste keyup", function () {
                        $('#search > button').toggleClass('expand-search2');
                    });
                    $("#cart").click(function () {
                        $("#cart").addClass("open");
                    });
                    $("#profile_link").click(function () {
                        $("#profile_link").addClass("open");
                    });
                });
            </script>
            <div id="cart" class="btn-group cart-btn">
                <button type="button" data-toggle="dropdown" data-loading-text="Loading..."
                        class="btn btn-inverse btn-block btn-lg dropdown-toggle">
                    <span id="cart-total"><strong>cart (<span class="cart-total-items"></span>) </strong></span>
                </button>
                <ul class="dropdown-menu pull-right">
                    <div id="cart-contents">
                        <table class="table table-condensed table-striped table-cart" id="cart-items"></table>
                        <div id="cart-links" class="text-center margin-bottom-md">
                            <div class="btn-group btn-group-justified" role="group"
                                 aria-label="View Cart and Checkout Button">
                                <div class="btn-group">
                                    <a class="btn btn-default btn-sm" href="<?= site_url('cart'); ?>"><i
                                                class="fa fa-shopping-cart"></i> <?= lang('view_cart'); ?></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn btn-default btn-sm" href="<?= site_url('cart/checkout'); ?>"><i
                                                class="fa fa-check"></i> <?= lang('checkout'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <li id="cart-empty">
                        <p class="text-center cart-empty">Your shopping cart is empty!</p>
                    </li>
                </ul>
            </div>


            <div class="search-container">
                <i class="pe-7s-search"></i>
                <?= shop_form_open('products', 'id="product-search-form"'); ?>
                <div id="search" class="input-group">
                    <input name="query" type="text" class="form-control input-lg" id="product-search" aria-label="Search..." placeholder="Search...">
                    <!-- <i class="pe-7s-search"></i> -->
                    <button type="button" class="btn btn-default btn-lg"><i class="pe-7s-search"></i></button>
                </div>
                <?= form_close(); ?>
            </div>
            <div class="main-menu">
                <div class="container">
                    <div class="ma-nav-mobile-container">
                        <div class="hozmenu">
                            <div class="navbar">
                                <div id="navbar-inner" class="navbar-inner navbar-inactive">
                                    <div class="menu-mobile">
                                        <a class="btn btn-navbar navbar-toggle">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </a>
                                        <!-- <span class="brand navbar-brand">categories</span> -->
                                    </div>
                                    <ul id="ma-mobilemenu" class="mobilemenu nav-collapse collapse">
                                        <li>
                                            <span class=" button-view1 no-close"><a
                                                        href="<?= base_url() ?>">home</a><span
                                                        class="ttclose"><a href="javascript:void(0)"></a>
                                                </span>
                                            </span>
                                        </li>
                                        <li>
                                            <span class=" button-view1 no-close"><a
                                                        href="<?= base_url() . 'shop/products' ?>">e-boutique</a><span
                                                        class="ttclose"><a href="javascript:void(0)"></a>
                                                </span>
                                            </span>
                                        </li>
                                        <li>
                                            <span class=" button-view1 no-close"><a
                                                        href="javascript:void(0)">collections</a><span
                                                        class="ttclose"><a href="javascript:void(0)"></a>
                                                </span>
                                            </span>
                                        </li>
                                        <li>
                                            <span class=" button-view1 no-close"><a
                                                        href="javascript:void(0)">average</a><span
                                                        class="ttclose"><a href="javascript:void(0)"></a>
                                                </span>
                                            </span>
                                        </li>
                                        <li>
                                            <span class=" button-view1 no-close"><a
                                                        href="<?= base_url() .'page/contact-us' ?>">Contact</a><span
                                                        class="ttclose"><a href="javascript:void(0)"></a>
                                                </span>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-container visible-lg visible-md">

                        <div class="nav1">
                            <div class="nav2">
                                <div id="pt_custommenu" class="pt_custommenu">
                                    <div id="pt_menu_home" class="pt_menu act">
                                        <div class="parentMenu">
                                            <a href="<?= base_url(); ?>"><span>Home</span></a>
                                        </div>
                                    </div>
                                    <div id="pt_menu20" class="pt_menu nav-1 pt_menu_no_child">
                                        <div class="parentMenu">
                                            <a href="<?= base_url() . 'shop/products'; ?>">
                                                <span>e-boutique</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div id="pt_menu_link32" class="pt_menu">
                                        <div class="parentMenu"><a href="#"><span>collections</span></a></div>
                                    </div>
                                    <div id="pt_menu_link33" class="pt_menu">
                                        <div class="parentMenu"><a href="#"><span>average</span></a></div>
                                    </div>
                                    <div id="pt_menu_link24" class="pt_menu">
                                        <div class="parentMenu"><a href="<?= base_url(); ?>page/contact-us"><span>Contact</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <script type="text/javascript">
                        //<![CDATA[
                        var body_class = $('body').attr('class');
                        if (body_class.search('common-home') != -1) {
                            $('#pt_menu_home').addClass('act');
                        }

                        var CUSTOMMENU_POPUP_EFFECT = 0;
                        var CUSTOMMENU_POPUP_TOP_OFFSET = 70
                        //]]>
                    </script>
                </div>
            </div>
            <div class="headerSpace unvisible" style="height: 120px;"></div>
        </div>
    </div>
</header>


<div class="wrapper">