<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $is_cod_active = $this->site->getActivePaymentMethod('COD','cod');
    if($is_cod_active){
        $cod_data = $this->site->getActivePaymentMethodData('COD','cod');
    }
?>

<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div id="content" class="col-sm-12">
                        <h1>Checkout</h1>
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="<?= $this->loggedIn ? '' : '#collapse-checkout-option' ?>" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" aria-expanded="true">Step 1: Checkout Options <i class="fa fa-caret-down"></i></a></h4>
                                </div>
                                <div class="panel-collapse collapse <?= $this->loggedIn ? '' : 'in' ?>" id="collapse-checkout-option" aria-expanded="true" style="">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h2>New Customer</h2>
                                                <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
                                                <a href="<?= base_url().'register'; ?>"><input type="button" value="Continue" id="button-account" data-loading-text="Loading..." class="btn btn-primary"></a>
                                            </div>
                                            <div class="col-sm-6">
                                                    <h2>Returning Customer</h2>
                                                    <p><strong>I am a returning customer</strong></p>
                                                    <form action="<?= base_url()?>login" class="validate fv-form fv-form-bootstrap" accept-charset="utf-8" novalidate="novalidate" method="post" enctype="multipart/form-data">
                                                        <button type="submit" class="fv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                                                        <div class="form-group">
                                                            <label class="control-label" for="input-email">E-Mail Address/User name</label>
                                                            <input type="text" name="identity" value="" placeholder="E-Mail Address/User name" id="username" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label" for="input-password">Password</label>
                                                            <input type="password" name="password" value="" placeholder="Password" id="password" class="form-control">
                                                            <a class="forgot-password" href="#">Forgotten Password</a></div>
                                                        <input type="submit" value="Login" class="btn btn-primary">
                                                    </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Step 2: Address & Delivery Details</h4>
                                </div>
                                <div class="panel-collapse collapse <?= $this->loggedIn ? 'in' : '' ?>" id="collapse-payment-address">
                                    <div class="panel-body">

                                        <?php
                                        if ($this->loggedIn) {
                                            if (!empty($addresses)) {
                                                echo shop_form_open('order', 'class="validate"');
                                                echo '<div class="row">';
                                                echo '<div class="col-sm-12 text-bold">' . lang('select_address') . '</div>';
                                                $r = 1;
                                                foreach ($addresses as $address) {
                                                    ?>
                                                    <div class="col-sm-6">
                                                        <div class="checkbox bg">
                                                            <label>
                                                                <input type="radio" name="address"
                                                                       value="<?= $address->id; ?>" <?= $r == 1 ? 'checked' : ''; ?>>
                                                                <span>
                                                                        <?= $address->line1; ?><br>
                                                                    <?= $address->line2; ?><br>
                                                                    <?= $address->city; ?> <?= $address->state; ?><br>
                                                                    <?= $address->postal_code; ?> <?= $address->country; ?>
                                                                    <br>
                                                                    <?= lang('phone') . ': ' . $address->phone; ?>
                                                                    </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $r++;
                                                }
                                                echo '</div>';
                                            }
                                            if (count($addresses) < 6 && !$this->Staff) {
                                                echo '<div class="row margin-bottom-lg">';
                                                echo '<div class="col-sm-12"><a href="#" id="add-address" class="btn btn-primary btn-sm">' . lang('add_new_address') . '</a></div>';
                                                echo '</div>';
                                            }
                                            ?>
                                            <div class="form-group">
                                                <?= lang('comment_any', 'comment'); ?>
                                                <?= form_textarea('comment', set_value('comment'), 'class="form-control tip" id="comment" style="height:100px;"'); ?>
                                            </div>

                                            <?php if ($customer->award_points != 0 && $Settings->each_spent > 0) { ?>
                                                <div class="checkbox form-group">
                                                    <label>
                                                        <input name="redeemd_point" class="show" type="checkbox"
                                                               value="1">Redeem Point(s)
                                                        [<?= ' You have ' . $customer->award_points . ' Award points '; ?>
                                                        ]
                                                    </label><br/>
                                                    <label><i class="fa fa-check"></i>If you want to use these reward
                                                        points. Check the above checkbox.</label>
                                                </div>
                                            <?php } ?>

                                            <?php if ($is_cod_active) { ?>
                                                <div class="checkbox form-group">
                                                    <label><input name="payment_by" class="show" type="checkbox"
                                                                  value="cod">Cash On Delivery</label><br/>
                                                    <?php if ($this->sma->formatMoney($this->cart->total()) > $this->sma->formatMoney($cod_data[0]->fixed_charges)) { ?>
                                                        <label><i class="fa fa-check"></i> Your order qualifies for FREE
                                                            Shipping! </label>
                                                    <?php } else { ?>
                                                        <label><i class="fa fa-info-circle"></i>
                                                            Add <?= $this->sma->formatMoney($cod_data[0]->fixed_charges) - $this->sma->formatMoney($this->cart->total()) ?>
                                                            AED of eligible item(s) to your order to qualify for FREE
                                                            Shipping. <a target="_blank"
                                                                         href="<?= base_url('page/shipping-policy') ?>">Details</a>
                                                        </label>
                                                    <?php } ?>

                                                </div>
                                            <?php } ?>

                                            <?php
                                            if (!empty($addresses) && !$this->Staff) {
                                                echo form_submit('add_order', lang('submit_order'), 'class="btn btn-theme pull-right"');
                                            } elseif ($this->Staff) {
                                                echo '<div class="alert alert-warning margin-bottom-no">' . lang('staff_not_allowed') . '</div>';
                                            } else {
                                                echo '<div class="alert alert-warning margin-bottom-no">' . lang('please_add_address_first') . '</div>';
                                            }
                                            echo form_close();
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>























































                    </div>


                </div>
                <code class="text-muted">* <?= lang('shipping_rate_info'); ?></code>
            </div>
        </div>
    </div>
</section>
