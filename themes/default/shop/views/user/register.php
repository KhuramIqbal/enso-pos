<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .well{
        background-color: #ffffff;
    }
</style>


<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php
                if($this->session->flashdata('error_msg')){
                    echo '<div class="alert alert-danger">'. $this->session->flashdata('error_msg').'</div>';
                }
                 ?>
                <div class="row">
                    <div id="content" class="col-sm-12">

                        <h1>Register Account</h1>
                        <p>If you already have an account with us, please login at the <strong><a href="<?= base_url().'login' ?>">login page</a></strong>.</p>
                        <form action="<?= base_url() ?>register" class="validate fv-form fv-form-bootstrap form-horizontal" enctype="multipart/form-data" role="form" method="post" accept-charset="utf-8" novalidate="novalidate">

                            <fieldset id="account">
                                <legend>Your Personal Details</legend>
                                <div class="form-group required" style="display: none;">
                                    <label class="col-sm-2 control-label">Customer Group</label>
                                    <div class="col-sm-10">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="customer_group_id" value="1" checked="checked">
                                                Default</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-firstname">First Name</label>
                                    <div class="col-sm-10">
                                        <?= form_input('first_name', '', 'class="form-control" placeholder="First Name"  id="first_name" required="required" pattern=".{3,10}"'); ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-lastname">Last Name</label>
                                    <div class="col-sm-10">
                                        <?= form_input('last_name', '', 'class="form-control" placeholder="Last Name" id="last_name" required="required"'); ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-email">User Name</label>
                                    <div class="col-sm-10">
                                        <?= form_input('username', set_value('username'), 'class="form-control tip" placeholder="User Name" id="username" required="required"'); ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-email">E-Mail</label>
                                    <div class="col-sm-10">
                                        <input type="email" id="email" name="email" placeholder="E-Mail" class="form-control" required="required"/>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-telephone">Telephone</label>
                                    <div class="col-sm-10">
                                        <?= form_input('phone', '', 'class="form-control" placeholder="Telephone" id="phone"'); ?>
                                    </div>
                                </div>

                                <div class="form-group hide">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-telephone">Company</label>
                                        <?= form_input('company', 'wisdom', 'class="form-control tip" id="company"'); ?>
                                    </div>
                                </div>

                            </fieldset>
                            <fieldset>
                                <legend>Your Password</legend>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-password">Password</label>
                                    <div class="col-sm-10">
                                        <?= form_password('password', '', 'class="form-control tip" id="password" placeholder="Password" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"'); ?>
                                        <span class="help-block"><?= lang('pasword_hint'); ?></span>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-confirm">Password Confirm</label>
                                    <div class="col-sm-10">
                                        <?= form_password('password_confirm', '', 'class="form-control" placeholder="Password Confirm" id="password_confirm" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-bv-identical="true" data-bv-identical-field="password" data-bv-identical-message="' . lang('pw_not_same') . '"'); ?>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="buttons">
                                <div class="pull-right">
                                    <input type="checkbox" name="agree" value="1">

                                    <input type="submit" value="Continue" class="btn btn-primary">
                                </div>
                            </div>
                        </form>
                    </div>











                </div>
            </div>
        </div>
    </div>
</section>
